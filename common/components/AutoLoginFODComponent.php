<?php

namespace common\components;

use Yii;
use yii\base\Component;

use yii\helpers\Json;
use common\models\User;
use common\modules\reportes\models\Historial;

class AutoLoginFODComponent extends Component {
  
    /**
     * Funcion de autologin si la galleta idFODSessionKey existe. 
     */
    public static function  validateFodSession(){
        if(Yii::$app->user->isGuest){
            if(!empty($_COOKIE['idFODSessionKey'])){ //ingresa solo si existe galleta
                //solo si es un usuario visitante es decir no hay login
                //Creamos el url de verificar sesión 
                $UrlCheckSession = Yii::$app->params["api"] . 'usuario/verificarSesion/';
                if(isset($_COOKIE['idFODSessionKey'])){
                    $UrlCheckSession = Yii::$app->params["api"] . 'usuario/verificarSesion/' . $_COOKIE['idFODSessionKey'];
                }
                $jsoncall = @file_get_contents($UrlCheckSession);
                $jsonResponse = $jsoncall ? Json::decode($jsoncall) : false;    
                
                //valoramos la respuesta
                if ($jsonResponse) {
                    if(!empty($jsonResponse["estado"])){
                        $user = User::findOne(["username" => $jsonResponse["sesion"]["username"]]);
                        if(!empty($user)){
                            $user->foto = $jsonResponse["infoPersona"]["Foto"];
                            $user->nombre = $jsonResponse["infoPersona"]["Nombre"];                       
                            $user->save();
                            
                            //se ejecuta el login automático
                            if(Yii::$app->user->login(
                                $user, 3600 * 24 * 30)){
                                $nuevo_login = new Historial();
                                $nuevo_login->save();                
                            }
                            //redireccion al mismo lugar donde se encuetre el usuario para que reaparesca el wg de acceso
                            return Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl());
                        }                    
                    }               
                }           
            }
        }       
    }

}

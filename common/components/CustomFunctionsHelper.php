<?php

namespace common\components;

use Yii;
use yii\base\Component;

class CustomFunctionsHelper extends Component {

    /**
     * The objetive of this function is return the excerpt of a content
     *
     * @param string $content to return the excerpt
     * @param integer $limit the  max words number to show
     * @return string content
     */
    public function getTheExcerpt($content, $limit) {
        $content = explode(' ', $content, $limit);
        if (count($content) >= $limit) {
            array_pop($content);
            $content = implode(" ", $content) . '...';
        } else {
            $content = implode(" ", $content);
        }
        $content = preg_replace('/\[.+\]/', '', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

        return $content;
    }

    public function getMonthName($monthNumber) {
        switch ($monthNumber) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Setiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
        }
    }

    /**
     * Finds all of the keywords (words that appear most) on param $str 
     * and return them in order of most occurrences to less occurrences.
     * @param string $str The string to search for the keywords.
     * @param int $minWordLen[optional] The minimun length (number of chars) of a word to be considered a keyword.
     * @param boolean $asArray[optional] Specifies if the function returns a string with the 
     * keywords separated by a comma ($asArray = false) or a keywords array ($asArray = true).
     * @return mixed A string with keywords separated with commas if param $asArray is true, 
     * an array with the keywords otherwise.
     */
    function extractKeywords($str, $minWordLen = 3, $asArray = false) {

        $str = Yii::$app->Formatting->remove_accents($str);
        $words = preg_split('/\s+/', trim($str));
//
//        return $asArray ? $final_keywords : implode(', ', $final_keywords);
//        $str = preg_replace('/[^\p{L}0-9 ]/', ' ', $str);
//        $str = trim(preg_replace('/\s+/', ' ', $str));
//        $str = preg_replace('/\s+/', ' ', $str);
//        $words = explode(' ', $str);
        $keywords = array();
        while (($c_word = array_shift($words)) !== null) {
            if (strlen($c_word) < $minWordLen)
                continue;

            $c_word = strtolower($c_word);
            if (array_key_exists($c_word, $keywords))
                $keywords[$c_word][1] ++;
            else
                $keywords[$c_word] = array($c_word, 1);
        }

        $final_keywords = array();
        foreach ($keywords as $keyword_det) {
            $sinNNN = str_replace("ñ", "nnn", $keyword_det[0]);
            $keywordFinal = preg_replace('/[^a-zA-Z0-9]+/', '', $sinNNN);
            $keywordFinal = str_replace("nnn", "ñ", $keywordFinal);
            array_push($final_keywords, $keywordFinal);
        }
        return $asArray ? $final_keywords : implode(', ', $final_keywords);
    }

    public function arrayToInt(array $arr) {
        foreach ($arr as &$a) {
            $a = (int) $a;
        }
        return $arr;
    }

}

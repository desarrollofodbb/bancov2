<?php

namespace common\modules\datos_de_interes\controllers;

use Yii;
use common\modules\datos_de_interes\models\DatosDeInteres;
use common\modules\datos_de_interes\models\DatosDeInteresSearch;
use common\models\Configuraciones;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DatosDeInteresController implements the CRUD actions for DatosDeInteres model.
 */
class DatosDeInteresController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DatosDeInteres models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DatosDeInteresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings());

        $this->view->params['configuraciones_generales'] = $configuraciones;

        $requested_path = \Yii::getAlias('@webroot');

        if (strpos($requested_path, "frontend/web")) {
            return $this->render('@frontend/views/datos-de-interes/index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DatosDeInteres model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//        $this->layout = "@frontend/views/layouts/main.php";

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DatosDeInteres model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new DatosDeInteres();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash("success", "El dato de interés ha sido agregado correctamente.");
            return $this->redirect(['update', 'id' => $model->idDatosDeInteres]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DatosDeInteres model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash("success", "El dato de interés ha sido actualizado correctamente.");
            return $this->redirect(['update', 'id' => $model->idDatosDeInteres]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DatosDeInteres model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DatosDeInteres model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DatosDeInteres the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = DatosDeInteres::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

    /**
     * Displays a single Status model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSlug($slug) {
        $model = DatosDeInteres::find()->where(['Slug' => $slug])->one();
        if (!is_null($model)) {
            return $this->render('@frontend/views/datos_de_interes/view', ['model' => $model,]);
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

}

<?php

namespace common\modules\datos_de_interes\controllers;

use yii\web\Controller;

/**
 * Default controller for the `datos_de_interes` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

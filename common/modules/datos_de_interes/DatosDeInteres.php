<?php

namespace common\modules\datos_de_interes;

/**
 * datos_de_interes module definition class
 */
class DatosDeInteres extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\datos_de_interes\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\datos_de_interes\models\DatosDeInteres */

$module = \Yii::$app->controller->module;

$this->title = $module->params["t_add_new"];
?>
<div class="datos_de_interes-create">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php
    echo
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

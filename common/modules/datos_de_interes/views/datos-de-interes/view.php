<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\datos_de_interes\models\DatosDeInteres */

$this->title = $model->idDatosDeInteres;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'DatosDeInteres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datos_de_interes-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <p>
        <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idDatosDeInteres], ['class' => 'btn btn-primary']) ?>
        <?php
        echo
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idDatosDeInteres], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    echo
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idDatosDeInteres',
            'Titulo',
            'Slug',
            'Descripcion',
            'FechaDeCreacion',
            'FechaDeModificacion',
            'Autor',
        ],
    ])
    ?>

</div>

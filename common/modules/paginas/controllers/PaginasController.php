<?php

namespace common\modules\paginas\controllers;

use Yii;
use common\modules\paginas\models\Paginas;
use common\modules\paginas\models\PaginasSearch;
use common\models\Configuraciones;
use common\modules\datos_de_interes\models\DatosDeInteres;
use common\modules\preguntas_frecuentes\models\PreguntasFrecuentes;
use frontend\models\ContactForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaginasController implements the CRUD actions for Paginas model.
 */
class PaginasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all Paginas models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PaginasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Paginas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//        $this->layout = "@frontend/views/layouts/main.php";

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Paginas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Paginas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->idPaginas]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Paginas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', yii::t('app', 'La página se actualizó correctamente'));
            return $this->redirect(['update', 'id' => $model->idPaginas]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Paginas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Paginas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paginas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Paginas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

    /**
     * Displays a single Status model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSlug($slug) {
        $model = Paginas::find()->where(['Slug' => $slug])->one();
        if (!is_null($model)) {
            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
            $configuraciones["generalHeaderImageFeatured"] = $model->imagenRelated;
            $this->view->params['configuraciones_generales'] = $configuraciones;

            $dataProvider = null;
            $ContactForm = null;

            if ($slug == "sobre-el-banco-de-recursos") {
                $dataProvider = DatosDeInteres::getDataOfInterest();
            }
            if ($slug == "preguntas-frecuentes") {
                $dataProvider = PreguntasFrecuentes::getFrequentQuestions();
                $ContactForm = new ContactForm();
                if ($ContactForm->load(Yii::$app->request->post()) && $ContactForm->validate()) {
                    if ($ContactForm->sendEmail($configuraciones["contacto_correo_info"] ? $configuraciones["contacto_correo_info"] : Yii::$app->params['contactFormEmail'])) {
                        Yii::$app->session->setFlash('success', $configuraciones["contacto_mensaje_gracias"]);
                    } else {
                        Yii::$app->session->setFlash('error', 'Se ha producido un error al enviar correo electrónico.');
                    }

                    return $this->refresh();
                }
            }


            return $this->render('@frontend/views/paginas/view', [
                        'dataProvider' => $dataProvider,
                        'model' => $model,
                        'ContactForm' => $ContactForm,
                        'page' => $slug,
                        'pagina' => $model,
                        'configuraciones' => $configuraciones,
            ]);
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContactenos() {

        $pagina = Paginas::find()->where(['Slug' => "contactenos"])->one();

        if (!is_null($pagina)) {

            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
            $configuraciones["generalHeaderImageFeatured"] = $pagina->imagenRelated;
            $this->view->params['configuraciones_generales'] = $configuraciones;

            return $this->sendMail($configuraciones, $pagina);
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

    protected function sendMail($configuraciones, $pagina) {
        $ContactForm = new ContactForm();
        if ($ContactForm->load(Yii::$app->request->post()) && $ContactForm->validate()) {
            if ($ContactForm->sendEmail($configuraciones["contacto_correo_info"] ? $configuraciones["contacto_correo_info"] : Yii::$app->params['contactFormEmail'])) {
                Yii::$app->session->setFlash('success', $configuraciones["contacto_mensaje_gracias"]);
            } else {
                Yii::$app->session->setFlash('error', 'Se ha producido un error al enviar correo electrónico.');
            }

            return $this->refresh();
        } else {
            return $this->render('@frontend/views/paginas/content/contactenos', [
                        'ContactForm' => $ContactForm,
                        'pagina' => $pagina,
                        'configuraciones' => $configuraciones,
            ]);
        }
    }

}

<?php

return [
    'components' => [
    // list of component configurations
    ],
    'params' => [
        // list of actions parameters
        "a_create" => Yii::t('app', 'Crear'),
        "a_update" => Yii::t('app', 'Actualizar'),
        "a_delete" => Yii::t('app', 'Eliminar'),
        // list of titles parameters
        "t_add_new" => Yii::t('app', 'Agregar nueva'),
        "t_update_page" => Yii::t('app', 'Actualizar página'),
        "t_image_featured" => Yii::t('app', 'Imagen destacada'),
        // list of message parameters
        "m_set_image" => Yii::t('app', 'Seleccionar la imagen destacada'),
    ],
];


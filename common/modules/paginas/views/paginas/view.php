<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\paginas\models\Paginas */

$this->title = $model->idPaginas;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Paginas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paginas-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <p>
        <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idPaginas], ['class' => 'btn btn-primary']) ?>
        <?php
        echo
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idPaginas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    echo
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPaginas',
            'Titulo',
            'Slug',
            'Descripcion',
            'Imagen',
            'FechaDeCreacion',
            'FechaDeModificacion',
            'Autor',
        ],
    ])
    ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\paginas\models\Paginas */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;

$this->title = $module->params["t_update_page"];
?>
<div class="paginas-update">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php
    echo
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\filemanager\widgets\FileInput;
use pendalf89\filemanager\widgets\TinyMCE;

/* @var $this yii\web\View */
/* @var $model common\modules\paginas\models\Paginas */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
?>

<div class="paginas-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col col-md-9">
            <?php echo $form->field($model, 'Titulo')->textInput() ?>


            <?=
            $form->field($model, 'Descripcion')->widget(TinyMCE::className(), [
                'clientOptions' => [
                    'language' => 'es',
                    'menubar' => false,
                    'height' => 200,
                    'image_dimensions' => true,
                    'plugins' => [
                        'advlist autolink lists link image charmap print preview anchor searchreplace visualblocks code contextmenu table',
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
                ],
            ]);
            ?>
        </div>

        <div class="col col-md-3">

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $module->params['t_image_featured'] ?></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="container img-imagen-destacada">
                        <?php
                        if ($model->Imagen) {
                            if ($model->imagenRelated) {
                                echo Html::img(Yii::$app->params['urlBack'] . $model->imagenRelated->getThumbUrl('small'));
                            } else {
                                echo Html::img("");
                            }
                        }
                        ?>
                    </div>
                    <?php
                    echo $form->field($model, 'Imagen')->widget(FileInput::className(), [
                        'buttonTag' => 'button',
                        'buttonName' => $module->params["m_set_image"],
                        'buttonOptions' => ['class' => 'btn btn-default btn-block'],
                        'options' => ['class' => 'form-control'],
                        // Widget template
                        'template' => '<div class=""><div class="hidden">{input}</div><div class="input-group"><span class="input-group-btn">{button}</span></div></div>',
                        // Optional, if set, only this image can be selected by user
                        'thumb' => 'small',
                        // Optional, if set, in container will be inserted selected image
                        'imageContainer' => '.img-imagen-destacada',
                        // Default to FileInput::DATA_URL. This data will be inserted in input field
                        'pasteData' => FileInput::DATA_ID,
                        // JavaScript function, which will be called before insert file data to input.
                        // Argument data contains file data.
                        // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                        'callbackBeforeInsert' => 'function(e, data) {
        console.log( data );
    }',])->label(false);
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->


        </div>
    </div>
    <div class="row">
        <div class="col col-md-12">
            <div class="form-inline">
                <div class="form-group">
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <div class="form-group">
                    <?php echo Html::a(Yii::t('app', 'Cancelar'), ['/paginas/paginas'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

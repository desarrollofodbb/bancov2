<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\paginas\models\PaginasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Páginas');
$this->params['breadcrumbs'][] = $this->title;

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
?>
<div class="paginas-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
    <?php echo Html::a($module->params["t_add_new"], ['create'], ['class' => 'btn btn-success']); ?>
    </p>-->

    <?php
    echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'Imagen',
                'label' => $module->params["t_image_featured"],
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->imagenRelated) {
                        return Html::img(Yii::$app->params["urlBack"] . $model->imagenRelated->getThumbUrl('small'));
                    } else {
                        return "";
                    }
                },
            ],
            'Titulo',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Yii::$app->params['urlFront'] . $model->Slug, ['target' => '_blank', 'title' => Yii::t('yii', 'View'),]);
                    },
//                            'delete' => function ($url, $model, $key) {
//                        $data_confirm = Yii::t('app', 'Está seguro que desea eliminar la pagina?');
//                        $text = '<span class="glyphicon glyphicon-trash"></span>';
//                        $toroute = Url::toRoute(['/paginas/paginas/delete', 'id' => $key]);
//                        $aria_label = Yii::t('app', 'Eliminar');
//                        return Html::a($text, $toroute, ['data-method' => "post", "data-confirm" => $data_confirm, "data-pjax" => "0", "aria-label" => $aria_label, "title" => $aria_label]);
//                    },
                        ]
                    ]
                ],
            ]);
            ?>
</div>

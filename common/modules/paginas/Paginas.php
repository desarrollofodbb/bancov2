<?php

namespace common\modules\paginas;

/**
 * paginas module definition class
 */
class Paginas extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\paginas\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

}

<?php

namespace common\modules\paginas\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\paginas\models\Paginas;

/**
 * PaginasSearch represents the model behind the search form of `common\modules\paginas\models\Paginas`.
 */
class PaginasSearch extends Paginas {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Titulo', 'Descripcion', 'FechaDeCreacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Paginas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idPaginas' => $this->idPaginas,
            'FechaDeCreacion' => $this->FechaDeCreacion,
            'Autor' => $this->Autor
        ]);


        $query->andFilterWhere(['COLLATE Latin1_General_CI_AI like', 'Titulo', "%" . $this->Titulo . "%"]);

        return $dataProvider;
    }

}

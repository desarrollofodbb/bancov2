<?php

namespace common\modules\recursos\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\User;

/**
 * This is the model class for table "TDenuncias".
 *
 * @property integer $rIdRecursos
 * @property integer $rIdUsuarios
 * @property string $Motivo
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 * @property integer $Estado
 *
 * @property User $rIdUsuarios0
 * @property TRecursos $rIdRecursos0
 */
class Denuncias extends \yii\db\ActiveRecord {

    //Denuncias states 
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TDenuncias';
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rIdRecursos', 'rIdUsuarios', 'Motivo'], 'required'],
            ['Estado', 'default', 'value' => self::STATUS_PENDING],
            [['rIdRecursos', 'rIdUsuarios', 'Estado'], 'integer'],
            [['Motivo'], 'string'],
            [['FechaDeCreacion', 'FechaDeModificacion'], 'safe'],
            [['rIdUsuarios'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rIdUsuarios' => 'id']],
            [['rIdRecursos'], 'exist', 'skipOnError' => true, 'targetClass' => Recursos::className(), 'targetAttribute' => ['rIdRecursos' => 'idRecursos']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rIdRecursos' => Yii::t('app', 'Recurso'),
            'rIdUsuarios' => Yii::t('app', 'Usuario'),
            'Motivo' => Yii::t('app', 'Motivo'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Modificación'),
            'Estado' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario() {
        return $this->hasOne(User::className(), ['id' => 'rIdUsuarios']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecurso() {
        return $this->hasOne(Recursos::className(), ['idRecursos' => 'rIdRecursos']);
    }

    /**
     * Notify to user that the resource has been rejected
     * @return \yii\db\ActiveQuery
     */
    public function notifyComplaint() {

        $mailStatus = Yii::$app->mailer
                ->compose(
                        ['html' => 'nuevaDenuncia'], [
                    'denuncia' => $this,
                        ]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo(Yii::$app->params['adminEmail'])
                ->setSubject(Yii::$app->params["complaintNotificationSubject"])
                ->send();

        return $mailStatus;
    }

    public static function IsUserComplaintThisResource($resourceId, $userId) {
        return self::find()->where(["rIdRecursos" => $resourceId, "rIdUsuarios" => $userId])->one();
    }

}

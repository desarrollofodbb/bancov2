<?php

namespace common\modules\recursos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\recursos\models\Recursos;

/**
 * RecursosAdvancedSearch represents the model behind the search form of `common\modules\recursos\models\Recursos`.
 */
class RecursosAdvancedSearch extends Recursos {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [
//                ['Nombre'],
//                'required', "skipOnEmpty" => true, "message" => Yii::t("app", "Debe ingresar el nombre del recurso")
//            ],
            [
                ['Nombre', 'ObjetivoDelRecurso', 'FechaDeCreacion'],
                'safe'
            ],
            [
                [
                    'tipo_de_licencia',
                    'idioma',
                    'es_recurso',
                    'tipo_de_recurso',
                    'publico_meta',
                    'sistemas_operativos',
                    'tipo_de_recurso',
                    'tipo_de_recurso',
                    'materia',
                    'contexto_educativo',
                    'tags',
                    'autores_ids',
                ],
                'safe'
            ],
            ['Nombre', "string", "message" => Yii::t("app", "El título debe ser texto")]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Recursos::find()->approved();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->params["recursosPerPage"],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->andWhere('0=1');
            return $dataProvider;
        }


        $valoresRequested = isset($params["RecursosAdvancedSearch"]) ? $this->getValoresPost($params["RecursosAdvancedSearch"]) : array();

        if ($valoresRequested) {
            $query->joinWith("valoresDeCaracteristicas")
                    ->andFilterWhere(['idValoresDeCaracteristicas' => $valoresRequested])
                    ->select(["TRecursos.Nombre", "idRecursos", "TRecursos.Slug", "Recurso", "ObjetivoDelRecurso", "Autor", "TRecursos.ActualizadoPor"])
                    ->groupBy(["TRecursos.Nombre", "idRecursos", "TRecursos.Slug", "Recurso", "ObjetivoDelRecurso", "Autor", "TRecursos.ActualizadoPor"])
                    ->having("COUNT(DISTINCT RRecursosValoresDeCaracteristicas.rIdValores) = " . count($valoresRequested))
                    //->distinct(true)
					;
        }
// grid filtering conditions
        $query->andFilterWhere([
            'idRecursos' => $this->idRecursos,
            'FechaDeCreacion' => $this->FechaDeCreacion,
            'Autor' => $this->Autor,
        ]);

        $query->andFilterWhere(['COLLATE Latin1_General_CI_AI like', 'TRecursos.Nombre', "%" . $this->Nombre . "%"]);


// echo "SEARCH--> " . $query;


        return $dataProvider;
    }

    public function getValoresPost($params) {

        $valoresRequested = [];
        foreach ($params as $key => $valor) {
            if ($valor && ($key != "Nombre")) {
                $valoresRequested[] = $valor;
            }
        }
        return $valoresRequested;
    }

}

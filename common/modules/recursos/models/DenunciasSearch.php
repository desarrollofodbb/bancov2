<?php

namespace common\modules\recursos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DenunciasSearch represents the model behind the search form of `common\modules\recursos\models\Recursos`.
 */
class DenunciasSearch extends Denuncias {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                ['Estado', 'Motivo'],
                'safe'
            ],
            ['Motivo', "string", "message" => "El Motivo debe ser texto"],
            ['Estado', "integer", "message" => "El Estado debe ser un número 1  o un 2"]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Denuncias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Estado' => $this->Estado
        ]);

        $query->andFilterWhere(['like', 'Motivo', $this->Motivo]);
        $query->orderBy("FechaDeCreacion DESC");
        return $dataProvider;
    }

}

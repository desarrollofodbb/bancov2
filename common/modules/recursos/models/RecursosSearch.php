<?php

namespace common\modules\recursos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\recursos\models\Recursos;

/**
 * RecursosSearch represents the model behind the search form of `common\modules\recursos\models\Recursos`.
 */
class RecursosSearch extends Recursos {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                ['Nombre', 'ObjetivoDelRecurso', 'FechaDeCreacion'],
                'safe'
            ],
            ['Nombre', "string", "message" => "El título debe ser texto"]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Recursos::find()->approved();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andFilterWhere('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idRecursos' => $this->idRecursos,
            'FechaDeCreacion' => $this->FechaDeCreacion,
            'Autor' => $this->Autor
        ]);

        $query->andFilterWhere(['COLLATE Latin1_General_CI_AI like', 'Nombre', "%" . $this->Nombre . "%"]);


        return $dataProvider;
    }

}

<?php

namespace common\modules\recursos\models;

use Yii;

/**
 * This is the model class for table "TCriteriosDeEvaluacion".
 *
 * @property integer $idCriteriosDeEvaluacion
 * @property string $Nombre
 * @property integer $Tipo
 * @property string $CreadoEn
 * @property string $ActualizadoEn
 * @property integer $CreadoPor
 * @property integer $ActualizadoPor
 *
 * @property RRecursosCriteriosDeEvaluacion[] $rRecursosCriteriosDeEvaluacions
 * @property TRecursos[] $rIdRecursos
 */
class CriteriosDeEvaluacion extends \yii\db\ActiveRecord {

    const TYPE_QUALITY = 1;
    const TYPE_EDUCATIONAL = 2;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TCriteriosDeEvaluacion';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Nombre', 'Tipo'], 'required'],
            [['Nombre'], 'string'],
            [['Tipo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idCriteriosDeEvaluacion' => 'Id Criterios De Evaluacion',
            'Nombre' => 'Nombre',
            'Tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursosCriteriosDeEvaluacions() {
        return $this->hasMany(RRecursosCriteriosDeEvaluacion::className(), ['rIdCriteriosDeEvaluacion' => 'idCriteriosDeEvaluacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos() {
        return $this->hasMany(TRecursos::className(), ['idRecursos' => 'rIdRecursos'])->viaTable('RRecursosCriteriosDeEvaluacion', ['rIdCriteriosDeEvaluacion' => 'idCriteriosDeEvaluacion']);
    }

}

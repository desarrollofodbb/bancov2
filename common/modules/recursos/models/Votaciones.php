<?php

namespace common\modules\recursos\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\User;

/**
 * This is the model class for table "RVotaciones".
 *
 * @property integer $rIdRecursos
 * @property integer $rIdUsuarios
 * @property integer $Votacion
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 *
 * @property User $Usuario
 * @property Recursos $Recursos
 */
class Votaciones extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'RVotaciones';
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rIdRecursos', 'rIdUsuarios', 'Votacion'], 'required'],
            [['rIdRecursos', 'rIdUsuarios', 'Votacion'], 'integer'],
            [['FechaDeCreacion', 'FechaDeModificacion'], 'safe'],
            [['rIdUsuarios'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rIdUsuarios' => 'id']],
            [['rIdRecursos'], 'exist', 'skipOnError' => true, 'targetClass' => Recursos::className(), 'targetAttribute' => ['rIdRecursos' => 'idRecursos']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rIdRecursos' => Yii::t('app', 'Recurso'),
            'rIdUsuarios' => Yii::t('app', 'Usuario'),
            'Votacion' => Yii::t('app', 'Votación'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha De Creación'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Modificación'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario() {
        return $this->hasOne(User::className(), ['id' => 'rIdUsuarios']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecurso() {
        return $this->hasOne(Recursos::className(), ['idRecursos' => 'rIdRecursos']);
    }

    public static function getCurrentUserVotacion($rIdRecursos) {
        $votacion = new Votaciones();
        if (!Yii::$app->user->isGuest) {
            $votacionPorRecurso = self::find()->where(["rIdRecursos" => $rIdRecursos, "rIdUsuarios" => Yii::$app->user->identity->id])->one();
            if (!is_null($votacionPorRecurso)) {
                $votacion = $votacionPorRecurso;
            }
        }
        return $votacion;
    }

}

<?php

namespace common\modules\recursos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\recursos\models\Recomendaciones;

/**
 * RecomendacionesSearch represents the model behind the search form of `common\modules\recursos\models\Recomendaciones`.
 */
class RecomendacionesSearch extends Recomendaciones {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['idRecomendaciones', 'Descripcion', 'rIdRecursos', 'rIdUsuarios', 'FechaDeCreacion', 'FechaDeModificacion', 'FechaDeModeracion', 'Estado', 'ActualizadoPor'], 'integer'],
            [['Titulo', 'Url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Recomendaciones::find()->approved();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idRecomendaciones' => $this->idRecomendaciones,
            'Descripcion' => $this->Descripcion,
            'rIdRecursos' => $this->rIdRecursos,
            'rIdUsuarios' => $this->rIdUsuarios,
            'FechaDeCreacion' => $this->FechaDeCreacion,
            'FechaDeModificacion' => $this->FechaDeModificacion,
            'FechaDeModeracion' => $this->FechaDeModeracion,
            'Estado' => $this->Estado,
            'ActualizadoPor' => $this->ActualizadoPor,
        ]);

        $query->andFilterWhere(['like', 'Titulo', $this->Titulo])
                ->andFilterWhere(['like', 'Url', $this->Url]);

        return $dataProvider;
    }

}

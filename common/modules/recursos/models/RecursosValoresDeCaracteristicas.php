<?php

namespace common\modules\recursos\models;

use Yii;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;

/**
 * This is the model class for table "RecursosValoresDeCaracteristicas".
 *
 * @property integer $rIdValores
 * @property integer $rIdRecursos 
 *
 * @property Recursos $recursos
 * @property ValoresDeCaracteristicas $valoresDeCaracteristicas
 */
class RecursosValoresDeCaracteristicas extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%RRecursosValoresDeCaracteristicas}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rIdValores'], 'exist', 'skipOnError' => true, 'targetClass' => ValoresDeCaracteristicas::className(), 'targetAttribute' => ['rIdValores' => 'idValoresDeCaracteristicas']],
            [['rIdRecursos'], 'exist', 'skipOnError' => true, 'targetClass' => Recursos::className(), 'targetAttribute' => ['rIdRecursos' => 'idRecursos']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecurso() {
        return $this->hasOne(Recursos::className(), ['idRecursos' => 'rIdRecursos']);
    }

}

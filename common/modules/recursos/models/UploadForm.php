<?php

namespace common\modules\recursos\models;

use Yii;
use common\models\User;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class UploadForm extends Recursos {

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /**
     * @var UploadedFile
     */
    public $recursoFile;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['recursoFile'];
        $scenarios[self::SCENARIO_UPDATE] = ['recursoFile'];
        return $scenarios;
    }

    public function rules() {
        return [
            [
                ['recursoFile'],
                'file',
                'skipOnEmpty' => true,
                'maxSize' => Yii::$app->params["maxFileSize"],
            ],
            [
                ['recursoFile'],
                'required',
                'skipOnEmpty' => false,
                'when' => function($model) {
            return (($model->scenario == self::SCENARIO_CREATE) && ($model->Tipo == Recursos::TYPE_FILE));
        },
                'whenClient' => "function (attribute, value) {
        return (($('#recursos-tipo').val() == '" . Recursos::TYPE_FILE . "') && ($('#recursos-recurso').length == 0));
    }",
                'message' => Yii::t("app", "El recurso es requerido"),
            ],
        ];
    }

    public function upload($userId, $archivoId) {
        if ($this->validate()) {
            $path = Yii::$app->params['pathRecursos'] . '/' . $userId;
            FileHelper::createDirectory($path);
            $this->recursoFile->saveAs($path . "/" . $archivoId . '_' . preg_replace('/[^a-zA-Z0-9-_\.]/', '', $this->recursoFile->baseName) . '.' . $this->recursoFile->extension);
            return true;
        } else {
            return false;
        }
    }

}

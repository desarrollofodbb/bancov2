<?php

namespace common\modules\recursos\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\User;

/**
 * This is the model class for table "RFavoritos".
 *
 * @property integer $rIdRecursos
 * @property integer $rIdUsuarios
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 *
 * @property User $rIdUsuarios0
 * @property TRecursos $rIdRecursos0
 */
class Favoritos extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'RFavoritos';
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rIdRecursos', 'rIdUsuarios'], 'required'],
            [['rIdRecursos', 'rIdUsuarios'], 'integer'],
            [['FechaDeCreacion', 'FechaDeModificacion'], 'safe'],
            [['rIdUsuarios'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rIdUsuarios' => 'id']],
            [['rIdRecursos'], 'exist', 'skipOnError' => true, 'targetClass' => Recursos::className(), 'targetAttribute' => ['rIdRecursos' => 'idRecursos']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rIdRecursos' => Yii::t('app', 'Recurso'),
            'rIdUsuarios' => Yii::t('app', 'Autor'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha De Creación'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Modificación'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios() {
        return $this->hasOne(User::className(), ['id' => 'rIdUsuarios']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos() {
        return $this->hasOne(TRecursos::className(), ['idRecursos' => 'rIdRecursos']);
    }

    public static function resourceIsInUserBag($resourceId, $userId) {
        return self::find()->where(["rIdRecursos" => $resourceId, "rIdUsuarios" => $userId])->one();
    }

}

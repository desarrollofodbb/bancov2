<?php

namespace common\modules\recursos\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\helpers\Html;

/**
 * This is the model class for table "TColecciones".
 *
 * @property integer $rIdColeccion
 * @property integer $rIdUsuarios
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 *
 * @property User $rIdUsuarios0
 * @property TRecursos $rIdRecursos0
 */
class Colecciones extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TColecciones';
    }

    public function behaviors() {
        return [
            [//SluggableBehavior automatically fills the specified attribute with a value that can be used a slug in a URL.
                'class' => SluggableBehavior::className(),
                'attribute' => 'Nombre',
                'slugAttribute' => 'Slug',
//                'immutable' => true,
                'ensureUnique' => true,
            ],            
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
                'value' => function ($event) {
                    return new Expression("GETDATE()");
                },
            ],
            [//BlameableBehavior automatically fills the specified attributes with the current user ID.
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'Autor',
                'updatedByAttribute' => 'Autor',                
            ],                      
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Nombre'], 'required', "message" => Yii::t("app", "El nombre de la colección es requerido")],
            [['idColeccion', 'Autor'], 'integer'],
            [['FechaDeCreacion', 'FechaDeModificacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idColeccion' => Yii::t('app', 'Coleccion'),
            'Autor' => Yii::t('app', 'Autor'),            
            'Nombre' => Yii::t('app', 'Nombre'),
            'Slug' => Yii::t('app', 'Slug'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha De Creación'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Modificación'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios() {
        return $this->hasOne(User::className(), ['id' => 'Autor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos() {
        return $this->hasMany(TColeccionesRecursos::className(), ['idColeccion' => 'idColeccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor() {
        return $this->hasOne(User::className(), ['id' => 'Autor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreview($Autor,$idColeccion) {
        $recursosModel = new Recursos();  
        $recursos = $recursosModel->getRecursosRelacionados($idColeccion);
        //$resultado = '../../recursosthumbnail/Wiki_no_image.png';
        $num = rand(1,8);
        $filerand = $num . ".png";
        $resultado = '../../recursosthumbnail/'.$filerand;         
        $Nombre = "";
        $filename = "";
        foreach ($recursos as $recurso) {
            $files = $recurso->getArchivos()->All();
            $Nombre = "";
            $rutaorg = Yii::getAlias('@recursos') . '/' . $recurso->Autor . '/';
            $rutathumb = Yii::getAlias('@recursos') . 'thumbnail/' . $recurso->Autor . '/';
     
            foreach ($files as $file) {            
              $idArchivo = $file->idArchivo;  
              $Nombre = $file->Nombre;
              $Tipo = $file->Tipo;
              $filename = $rutaorg . $idArchivo . '_' . $Nombre;
              if(!file_exists($filename)){
                continue;
              }      
              if (filesize($filename) > 11){
                $exifImageType = exif_imagetype($filename);
                if ($exifImageType > 0 && $exifImageType < 18) {
                  $fileType = 'image';
                }
                else{
                  continue;
                }
              }                     
              // if(!exif_imagetype($filename)) {
              //   continue;
              // }
              $fileresult = $rutathumb .'tb_' . $idArchivo . '_' . $Nombre;    

              if(!file_exists($fileresult)){
                $resultado = '../../recursosthumbnail/' . $recurso->Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
                $createdir = \yii\helpers\FileHelper::createDirectory($rutathumb, $mode = 0775, $recursive = true);      
                $width = 605;
                $height = 605;
                Image::thumbnail($filename, $width, $height)->save(Yii::getAlias($fileresult), ['quality' => 80]);
                break;            
              }
              else{
                $resultado = '../../recursosthumbnail/' . $recurso->Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
                break;
              }
            }
            if(!isset($resultado)){
              $rutaorg = Yii::getAlias('@recursos') . '/';
              $rutathumb = Yii::getAlias('@recursos') . 'thumbnail/';
              $filename = $rutaorg . 'not-found-image.jpg';
              $fileresult = $rutathumb .'tb_not-found-image.jpg';   
              if(!file_exists($fileresult)){
                $resultado = '../../recursosthumbnail/tb_not-found-image.jpg';
                $createdir = \yii\helpers\FileHelper::createDirectory($rutathumb, $mode = 0775, $recursive = true);      
                $width = 605;
                $height = 605;
                Image::thumbnail($filename, $width, $height)->save(Yii::getAlias($fileresult), ['quality' => 80]);            
              }
              else{
                $num = rand(1,8);
                $filerand = $num . ".png";
                $resultado = '../../recursosthumbnail/'.$filerand;
              }          
            }
        }

        if(isset($resultado)){
          return Html::img($resultado, ["alt" => $filename . "*" . $Nombre, "style" => "width:100%;height:100%;border-radius:10%", "class"=>"w3-border w3-round-xlarge"]);    
        }
        else{
          return false;
        }
        
    }
}

<?php

namespace common\modules\recursos\models;
/**
 * 
 * @param string $tipo El tipo de archivo a importar. Ej: $CFG->GLBBASEDATOS, $CFG->GLBUTILIDADES
 * @param string $archivo El nombre del archivo
 * @return string
 */

use Yii;
use common\modules\filemanager\models\Mediafile;
use backend\modules\caracteristicas\models\Caracteristicas;
use common\models\User;
use common\modules\reportes\models\Eventos;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use cornernote\linkall\LinkAllBehavior;
use yii\helpers\ArrayHelper;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii2mod\moderation\enums\Status;
use yii2mod\moderation\ModerationBehavior;
use yii2mod\moderation\ModerationQuery;
use yii\helpers\Url;
use yii\bootstrap\Button;
use yii\imagine\Image;
use SoapClient;
use SOAPHeader;
/**
 * This is the model class for table "Recursos".
 *
 * @property integer $idRecursos
 * @property string $Nombre
 * @property string $Slug
 * @property string $Tipo
 * @property string $Recurso
 * @property integer $rIdArchivos
 * @property string $Objetivo del recurso
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 * @property integer $Autor
 * @property string $ActualizadoPor
 * @property string $Estado
 * @property string $FechaDeModeracion
 * @property integer $insignia
 *
 * @property user $autor
 * @property user $actualizadoPor
 * @property Archivo $archivo
 */
class Recursos extends \yii\db\ActiveRecord {

    public $id;
    public $tipo_de_licencia;
    public $idioma;
    public $es_recurso;
    public $tipo_de_recurso;
    public $publico_meta;
    public $sistemas_operativos;
    public $materia;    
    public $materia_f;
    public $contexto_educativo;
    public $tags;
    public $attachment_50;
    public $genero;
    public $ocupacion;
    public $pais;
    public $provincia;
    public $canton;
    public $distrito;
    public $centroeducativo;
    
//    -- campos relacionales many to many
    public $autores_ids;
    public $criterios_ids;

    //Recursos tipos
    const TYPE_FILE = 1;
    const TYPE_URL = 2;
    const TYPE_EMBED = 3;

    //campos temporales para el recurso
    public $url_recurso;
    public $embed_recurso;
    public $image_show;
    //filetemp
    public $recursoFileTemp;
    public $tempCurrentFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%TRecursos}}';
    }

    public function behaviors() {
        return [
            [//SluggableBehavior automatically fills the specified attribute with a value that can be used a slug in a URL.
                'class' => SluggableBehavior::className(),
                'attribute' => 'Nombre',
                'slugAttribute' => 'Slug',
//                'immutable' => true,
                'ensureUnique' => true,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
                'value' => function ($event) {
                    return new Expression("GETDATE()");
                },
            ],
            [//BlameableBehavior automatically fills the specified attributes with the current user ID.
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'Autor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
            LinkAllBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'isDeleted' => true
                ],
//                'replaceRegularDelete' => true // mutate native `delete()` method
            ],
            'moderation' => [
                'class' => ModerationBehavior::class,
                'statusAttribute' => 'Estado',
                'moderatedByAttribute' => false,
            ],
        ];
    }

    public static function find() {
        $query = new ModerationQuery(get_called_class());
        $query->where(['isDeleted' => false]);
        return $query;
    }

    public function afterSave($insert, $changedAttributes) {

        //------ save aurotres reltionship ------------------------
        // if ($this->autores_ids) {
        //      $users = [];
        //      foreach ($this->autores_ids as $autor_id) {
        //          $user = User::getUserById($autor_id);
        //          if ($user) {
        //              $users[] = $user;
        //          }
        //      }
        //      $this->linkAll('autores', $users);
        //  }
        if($this->Autor){
          $users = [];

          $user = User::getUserById($this->Autor);
          if ($user) {
            $users[] = $user;
          }

          $this->linkAll('autores', $users);          
        }
        //------ save valores de caracteristicas reltionship -------
        $valoresDeCaracteristicas = array();
        $caracteristicas = Caracteristicas::find()->all();

        foreach ($caracteristicas as $caracteristica) {
            $valor_field = str_replace("-", "_", $caracteristica->Slug);
            $current_values = $this->$valor_field;

            if (!is_array($current_values)) {
                $current_values = array($this->$valor_field);
            }
// $log_filename = $_SERVER['DOCUMENT_ROOT'] . '/log';
//     if (!file_exists($log_filename))
//     {
//         // create directory/folder uploads.
//         mkdir($log_filename, 0777, true);
//     }
//     $log_file_data = $log_filename.'/recursos.log';     

            foreach ($current_values as $valor_id) {
           
                if(!is_numeric($valor_id)){
                  $modelVC = new ValoresDeCaracteristicas();

                  $modelVC->Caracteristicas_id = 10;

                  $modelVC->Nombre = $valor_id;
                  $modelVC->Slug = $valor_id;
                  $modelVC->CreadoEn = date('Y-m-d H:i:s');
                  $modelVC->ActualizadoEn = date('Y-m-d H:i:s');
                  $modelVC->CreadoPor = $this->Autor;
                  $modelVC->ActualizadoPor = $this->Autor;
                  $modelVC->Posicion = 99;
                  $modelVC->save();       
                  $valor_id = $modelVC->idValoresDeCaracteristicas;       
  
                }
                $valorCaracteristica = ValoresDeCaracteristicas::find()->where(['idValoresDeCaracteristicas' => $valor_id])->one();

                if ($valorCaracteristica) {
                    $valoresDeCaracteristicas[] = $valorCaracteristica;
                }
            }
        }
        if (count($valoresDeCaracteristicas) > 0) {//si se estan modificando 
            $this->linkAll('valoresDeCaracteristicas', $valoresDeCaracteristicas);
        }

        //------ save criterios de evaluacion reltionship ------------------------
        if ($this->criterios_ids) {
            $criterios = [];
            foreach ($this->criterios_ids as $criterio_id) {
                $criterio = CriteriosDeEvaluacion::findOne($criterio_id);
                if ($criterio) {
                    $criterios[] = $criterio;
                }
            }

            $this->linkAll('criteriosDeEvaluacion', $criterios);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeModeration() {
        $this->FechaDeModeracion = new Expression("GETDATE()"); // log the moderation date

        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $currenYear = date("Y");
        $minYear = $currenYear - 200;
        if (Yii::$app->user->isGuest){
        return [
            [['Nombre'], 'required', "message" => Yii::t("app", "El nombre del recurso es requerido")],
            [['Tipo'], 'required', "message" => "El recurso es requerido"],
            [['haspreview'], 'in', 'range' => [1, 1], "message" => Yii::t("app", "Una imagen de vista previa es requerida")],
            [['url_recurso'],
                'required',
                "message" => "URL es requerida si no incluye ningún archivo",
                'when' => function($model) {
                  return $model->hasfiles == 1;
                },
                'whenClient' => "function (attribute, value) {
                  return ($('#hasfiles').val() == '0');
                }",
            ],
            [['url_recurso'], 'url'],
            [['ObjetivoDelRecurso'], 'required', "message" => Yii::t("app", "El resumen del recurso es requerido")],
            [['ObjetivoDelRecurso'], 'string', 'length' => [140], "message" => Yii::t("app", "El resumen del recurso debe tener al menos 140 caracteres")],
            [['ObjetivoDelRecurso'], 'string', 'length' => [0, 600], "message" => Yii::t("app", "El resumen del recurso no debe exceder los 600 caracteres")],            
            [['autorRecurso'], 'required', "message" => Yii::t("app", "El nombre del autor es requerido")], 
            [['anioRecurso'], 'required', "message" => Yii::t("app", "El año del recurso es requerido")],
            [['anioRecurso'], 'integer', "integerOnly" => true, 'min' => $minYear, 'max' => $currenYear],
            [['Nombre'], 'string', 'length' => [0, 135], "message" => Yii::t("app", "El nombre del recurso no debe exceder los 135 caracteres")],
            [['ObjetivoDelRecurso'], 'string'],
            [['idtime'], 'integer'],
            [['anioRecurso'], 'integer'],
            [['ObjetivoDelRecurso', 'autores_ids'], 'safe'],
//            [['autores_ids'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            // [['criterios_ids'], 'required', "message" => Yii::t("app", "Debe validar al menos una de las características")],
            [['tipo_de_licencia'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],

            [['idioma'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],

            [['tipo_de_recurso'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['publico_meta'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['sistemas_operativos'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['materia'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['contexto_educativo'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['tags'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            ['Estado', 'default', 'value' => Status::PENDING],
            ['Autor', 'default', 'value' => 0],
            ['isDeleted', 'default', 'value' => false],
            ['Estado', 'in', 'range' => Status::getConstantsByName()],
            ['Tipo', 'default', 'value' => self::TYPE_FILE],
            // ['Tipo', 'in', 'range' => [self::TYPE_FILE, self::TYPE_URL, self::TYPE_EMBED]],
            ['Recurso', "validateUserReachedTheLimit", 'when' => function($model) {
//      
                    if ($model->recursoFileTemp) {
                        $autor = $model->autor;
                        if ($model->tempCurrentFile) {//si ya tiene un archivo  
                            return $autor->haveUserReachedTheLimit($model->archivo->Tamano, $model->recursoFileTemp->size);
                        } else {//si no tenia ningun archivo 
                            return $autor->haveUserReachedTheLimit(0, $model->recursoFileTemp->size);
                        }
                    } else {
                        return false; //no hace validaci'on, ni muestra ningun error
                    }
                }],
        ];
        }
        return [
            [['Nombre'], 'required', "message" => Yii::t("app", "El nombre del recurso es requerido")],
            [['Tipo'], 'required', "message" => "El recurso es requerido"],
            [['haspreview'], 'in', 'range' => [1, 1], "message" => Yii::t("app", "Una imagen de vista previa es requerida")],
            [['url_recurso'],
                'required',
                "message" => "URL es requerida si no incluye ningún archivo",
                'when' => function($model) {
                  return $model->hasfiles == 1;
                },
                'whenClient' => "function (attribute, value) {
                  return ($('#hasfiles').val() == '0');
                }",
            ],
            [['url_recurso'], 'url'],
            [['ObjetivoDelRecurso'], 'required', "message" => Yii::t("app", "El resumen del recurso es requerido")],
            [['ObjetivoDelRecurso'], 'string', 'length' => [140], "message" => Yii::t("app", "El resumen del recurso debe tener al menos 140 caracteres")],
            [['ObjetivoDelRecurso'], 'string', 'length' => [0, 500], "message" => Yii::t("app", "El resumen del recurso no debe exceder los 500 caracteres")],            
            [['autorRecurso'], 'required', "message" => Yii::t("app", "El nombre del autor es requerido")], 
            [['anioRecurso'], 'required', "message" => Yii::t("app", "El año del recurso es requerido")],
            [['anioRecurso'], 'integer', "integerOnly" => true, 'min' => $minYear, 'max' => $currenYear],
            [['Nombre'], 'string', 'length' => [0, 135], "message" => Yii::t("app", "El nombre del recurso no debe exceder los 135 caracteres")],
            [['ObjetivoDelRecurso'], 'string'],
            [['idtime'], 'integer'],
            [['anioRecurso'], 'integer'],
            [['ObjetivoDelRecurso', 'autores_ids'], 'safe'],
//            [['autores_ids'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            // [['criterios_ids'], 'required', "message" => Yii::t("app", "Debe validar al menos una de las características")],
            [['tipo_de_licencia'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],

            [['idioma'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],

            [['tipo_de_recurso'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['publico_meta'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['sistemas_operativos'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['materia'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['contexto_educativo'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            [['tags'], 'required', "message" => Yii::t("app", "Debe seleccionar al menos un valor")],
            ['Estado', 'default', 'value' => Status::PENDING],
            ['Autor', 'default', 'value' => 0],
            ['isDeleted', 'default', 'value' => false],
            ['Estado', 'in', 'range' => Status::getConstantsByName()],
            ['Tipo', 'default', 'value' => self::TYPE_FILE],

        ];
    }

    public function validateUserReachedTheLimit($attribute, $params) {
        $this->addError("Tipo", Yii::$app->params["spaceLimitPerUserMessage"]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idRecursos' => Yii::t('app', 'Id'),
            'Nombre' => Yii::t('app', 'Nombre'),
            'anioRecurso' => Yii::t('app', 'Año'),
            'autorRecurso' => Yii::t('app', 'Autor'),
            'idtime' => Yii::t('app', 'idTime'),
            'Slug' => Yii::t('app', 'Slug'),
            'ObjetivoDelRecurso' => Yii::t('app', 'Resumen del recurso'),
            'Tipo' => Yii::t('app', ''),
            'Recurso' => Yii::t('app', 'Recurso'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha de creación'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha de modificación'),
            'Autor' => Yii::t('app', 'Author'),
            'ActualizadoPor' => Yii::t('app', 'Actualizado por'),
            'autores_ids' => Yii::t('app', 'Autor(es)'),
            'criterios_ids' => Yii::t('app', 'Por favor, revise su recurso en función de estas características'),
            'criterios_validacion_calidad_ids' => Yii::t('app', 'Criterios de calidad requeridos'),
            'criterios_validacion_educacion_ids' => Yii::t('app', 'Criterios educativos básicos'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagenRelated() {
        return $this->hasOne(Mediafile::className(), ['id' => 'Imagen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getFilesDataAll($Autor,$idRecursos,$format="") {
        $rutasfiles = "";
        $files = $this->getArchivos()->All();
        $Nombre = "";
        $rutaorg = '/../../imagesrecursos/' . $this->Autor . '/';
        $rutathumb = '/../../' . $this->Autor . '/';
 
        $preview = 0;
        $key = 0;
        foreach ($files as $file) {
          $idArchivo = $file->idArchivo;  
          $Nombre = $file->Nombre;
          $key++;
          if(substr($Nombre, 0,7)=="preview"){
            $preview = 1;
          }
          $Tipo = $file->Extension;
          $filename = $rutaorg . $idArchivo . '_' . $Nombre;
          if(strpos($file->Tipo,"text")>0){
            $Tipo = "text";  
          }               
          if(strpos($file->Tipo,"mage")>0){
            $Tipo = "image";  
          }
          if(strpos($file->Tipo,"pdf")>0){
            $Tipo = "pdf";  
          }    
          if(strpos($file->Tipo,"xls")>0||strpos($file->Tipo,"doc")>0){
            $Tipo = "office";  
          } 
          if(strpos($file->Tipo,"html")>0){
            $Tipo = "html";  
          }              
          if(strpos($file->Tipo,"xml")>0){
            $Tipo = "xml";  
          }            
          if($rutasfiles==""){                                       
            $rutasfiles = $filename . ":" . $Tipo . ":" . $file->Tamano . ":" . $file->Nombre.":". $key.":". $file->idArchivo;
          }
          else{
            $rutasfiles = $rutasfiles . ";" . $filename . ":" . $Tipo . ":" . $file->Tamano . ":" . $file->Nombre.":". $key.":". $file->idArchivo;
          }            
        }
        return $rutasfiles;
    }

    /**
     * Is a Profuturo resource
     * @param 
     * @return boolean
     */
    public function isProfuturo($idRecursos) {

      $idValoresDeCaracteristicas = 0;
      $count = 0;
      $query = (new Query)->select('idValoresDeCaracteristicas')
      ->from('TvaloresDeCaracteristicas')
      ->where(['Caracteristicas_id' => 4])
      ->andWhere(['Nombre' => 'Profuturo']);
      $rows = $query->all();
      foreach ($rows as $registro) {   
       $idValoresDeCaracteristicas = $registro['idValoresDeCaracteristicas'];
      }

      $count = (new Query())->select('rIdValores')
      ->from('RRecursosValoresDeCaracteristicas')
      ->where(['rIdValores' =>$idValoresDeCaracteristicas])
      ->andWhere(['rIdRecursos' => $idRecursos])
      ->count();       
      return $count;
    }

    public function getPreviewBusqueda($Autor,$idArchivo,$Nombre){
      $rutaorg = Yii::getAlias('@recursos') . '/' . $Autor . '/';
      $rutathumb = Yii::getAlias('@recursos') . 'thumbnail/' . $Autor . '/';
      if($Nombre=="random"){
        $num = rand(1,8);
        $filerand = $num . ".png";
        $resultado = '../../recursosthumbnail/'.$filerand;
      }
      else{
        $fileresult = $rutathumb .'tb_' . $idArchivo . '_' . $Nombre; 
        if(file_exists($fileresult)){  
          $resultado = '../../recursosthumbnail/' . $Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
        } 
        else{
          $num = rand(1,8);
          $filerand = $num . ".png";
          $resultado = '../../recursosthumbnail/'.$filerand;          
        }     
        
      }
      
      return $resultado;
    }    

    public function getPreviewEvento($Autor,$idArchivo,$Nombre){
      $rutaorg = Yii::getAlias('@recursos') . '/' . $Autor . '/';
      $rutathumb = Yii::getAlias('@recursos') . 'thumbnail/' . $Autor . '/';
      if($Nombre=="random"){
        $num = rand(1,8);
        $filerand = $num . ".png";
        $resultado = '../../recursosthumbnail/'.$filerand;
      }
      else{
        $fileresult = $rutathumb .'tb_' . $idArchivo . '_' . $Nombre; 
        if(file_exists($fileresult)){  
          $resultado = '../../recursosthumbnail/' . $Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
        } 
        else{
          $num = rand(1,8);
          $filerand = $num . ".png";
          $resultado = '../../recursosthumbnail/'.$filerand;          
        }     
        
      }
      
      return $resultado;
    }

    public function getPreview($Autor,$idRecursos,$format="",$guest=0) {

        $files = $this->getArchivos()->All();
        $Nombre = "";
        $rutaorg = Yii::getAlias('@recursos') . '/' . $this->Autor . '/';
        $rutathumb = Yii::getAlias('@recursos') . 'thumbnail/' . $this->Autor . '/';

        $preview = 0;
        $entro = 0;
        foreach ($files as $file) {
            $entro = 1;
          $idArchivo = $file->idArchivo;  
          $Nombre = $file->Nombre;
          
          if(substr($Nombre, 0,7)=="preview"){
            $preview = 1;
          }
          $Tipo = $file->Tipo;
          $filename = $rutaorg . $idArchivo . '_' . $Nombre;
          if(!file_exists($filename)){
            continue;
          }  
          if (filesize($filename) > 11){
            $exifImageType = exif_imagetype($filename);
            if ($exifImageType > 0 && $exifImageType < 18) {
              $fileType = 'image';
            }
            else{
              continue;
            }
          }    
          // if(!exif_imagetype($filename)) {
          //   continue;
          // }
          $fileresult = $rutathumb .'tb_' . $idArchivo . '_' . $Nombre; 

          if(!file_exists($fileresult)){
            $resultado = '../../recursosthumbnail/' . $Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
            $createdir = \yii\helpers\FileHelper::createDirectory($rutathumb, $mode = 0775, $recursive = true);      
            $width = 800;
            $height = 800;
            Image::resize($filename, $width, $height, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND)->save(Yii::getAlias($fileresult), ['quality' => 100]);            
          }
          else{
            $resultado = '../../recursosthumbnail/' . $Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
            if($format=="menu"){
              $resultado = '../../recursosthumbnail/' . $Autor . '/' . 'tb_' . $idArchivo . '_' . $Nombre;
            }          
            
          }
          if($preview==1){
            break;
          }
        }
        $filerand = "";
        if(!isset($resultado)){
          $rutaorg = Yii::getAlias('@recursos') . '/';
          $rutathumb = Yii::getAlias('@recursos') . 'thumbnail/';
          $filename = $rutaorg . 'not-found-image.jpg';
          $fileresult = $rutathumb .'tb_not-found-image.jpg';   
          if(!file_exists($fileresult)){
            $resultado = '/recursosthumbnail/Wiki_no_image.png';
            $createdir = \yii\helpers\FileHelper::createDirectory($rutathumb, $mode = 0775, $recursive = true);      
            $width = 605;
            $height = 605;
            Image::thumbnail($filename, $width, $height,\Imagine\Image\ManipulatorInterface::THUMBNAIL_INSET)->save(Yii::getAlias($fileresult), ['quality' => 80]);            
          }
          else{
            $num = rand(1,8);
            $filerand = $num . ".png";
            $resultado = '../../recursosthumbnail/'.$filerand;
            // if($format=="menu"){
            //   $resultado =  '';//'recursosthumbnail/Wiki_no_image.png';
            // }
          }          
        }
        if(isset($resultado)){     

          if($format=="menu"){
            return $resultado;
          }
          if($format=="menupreview"){
            if($filerand!=""){
              return "";  
            }
            return $resultado;
          }        
          if($format=="simple"){
            return Html::img($resultado, ["alt" => $Nombre, "class"=>"w3-border w3-round-xlarge"]);    
          } 
          else{
            if($format=="simplezoom"){
              if($guest==0){
                return Html::img($resultado, ["alt" => $Nombre, "class"=>"w3-border w3-round-xlarge w3-hover-opacity", "style" => "cursor:pointer"]);    
              }
              else{
                return Html::img($resultado, ["alt" => $Nombre, "class"=>"w3-border w3-round-xlarge w3-hover-opacity", "style" => "cursor:pointer", "onClick" => "document.getElementById('id01').style.display='block'"]);
              }
              
            } 
            else{            
              return Html::img($resultado, ["alt" => $Nombre, "style" => "width:100%;height:100%;border-radius:10%", "class"=>"w3-border w3-round-xlarge"]);    
            }
          } 
        }
        else{
          return false;
        }
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor() {
        return $this->hasOne(User::className(), ['id' => 'Autor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArchivo() {
        return $this->hasOne(Archivos::className(), ['idArchivo' => 'rIdArchivos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArchivos() {        
        return $this->hasMany(Archivos::className(), ['idRecursos' => 'idRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualizadoPor() {
        return $this->hasOne(User::className(), ['id' => 'ActualizadoPor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutores() {
        return $this->hasMany(User::className(), ['id' => 'rIdUsuarios'])
                        ->viaTable('RAutores', ['rIdRecursos' => 'idRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriteriosDeEvaluacion() {
        return $this->hasMany(CriteriosDeEvaluacion::className(), ['idCriteriosDeEvaluacion' => 'rIdCriteriosDeEvaluacion'])
                        ->viaTable('RRecursosCriteriosDeEvaluacion', ['rIdRecursos' => 'idRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriteriosDeValidacion() {
        return $this->hasMany(CriteriosDeValidacion::className(), ['idCriteriosDeValidacion' => 'rIdCriteriosDeValidacion'])
                        ->viaTable('RRecursosCriteriosDeValidacion', ['rIdRecursos' => 'idRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriteriosDeValidacionCalidad() {
        return CriteriosDeValidacion::find()
                        ->innerJoin("RRecursosCriteriosDeValidacion", "RRecursosCriteriosDeValidacion.rIdCriteriosDeValidacion = TCriteriosDeValidacion.idCriteriosDeValidacion")
                        ->where([
                            "RRecursosCriteriosDeValidacion.rIdRecursos" => $this->idRecursos,
                            "TCriteriosDeValidacion.Tipo" => CriteriosDeValidacion::TYPE_QUALITY
                        ])
                        ->orderBy("Nombre ASC")
                        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriteriosDeValidacionEducacion() {
        return CriteriosDeValidacion::find()
                        ->innerJoin("RRecursosCriteriosDeValidacion", "RRecursosCriteriosDeValidacion.rIdCriteriosDeValidacion = TCriteriosDeValidacion.idCriteriosDeValidacion")
                        ->where([
                            "RRecursosCriteriosDeValidacion.rIdRecursos" => $this->idRecursos,
                            "TCriteriosDeValidacion.Tipo" => CriteriosDeValidacion::TYPE_EDUCATIONAL
                        ])
                        ->orderBy("Nombre ASC")
                        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursosValoresDeCaracteristicas() {
        return $this->hasMany(RecursosValoresDeCaracteristicas::className(), ['rIdRecursos' => 'idRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValoresDeCaracteristicas() {
        return $this->hasMany(ValoresDeCaracteristicas::className(), ['idValoresDeCaracteristicas' => 'rIdValores'])
                        ->viaTable('RRecursosValoresDeCaracteristicas', ['rIdRecursos' => 'idRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoValores() {
        return $this->hasMany(ValoresDeCaracteristicas::className(), ['idValoresDeCaracteristicas' => 'rIdValores'])
                        ->viaTable('RRecursosValoresDeCaracteristicas', ['rIdRecursos' => 'idRecursos'])->where(["Caracteristicas_id" => 4]);
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsValores() {
        return $this->hasMany(ValoresDeCaracteristicas::className(), ['idValoresDeCaracteristicas' => 'rIdValores'])
                        ->viaTable('RRecursosValoresDeCaracteristicas', ['rIdRecursos' => 'idRecursos'])->where(["Caracteristicas_id" => 10]);
    }

    /**
     * Print tags 
     * @return \yii\db\ActiveQuery
     */
    public function printTags($prefix, $inputOptions, $format="") {
        $MaxTagsToShow = Yii::$app->params["maxTagsToShow"];
        $RelatedTags = $this->getTagsValores()->limit($MaxTagsToShow)->all();
        if ($RelatedTags) {
            foreach ($RelatedTags as $tag) {
               if($tag->Nombre=="ProFuturo"){
                    $resultado = Yii::$app->params["urlBack"] . "/uploads/etiqueta-profuturo666666.png";
                    $url = Yii::$app->homeUrl . '?tag=' . $tag->idValoresDeCaracteristicas;
                    echo '<a href="'.$url.'">';
                    echo Html::img($resultado, ["alt" => $tag->Nombre, "class"=>"w3-round-xlarge w3-hover-opacity", "style" => "margin-right:10px; font-size:15px;margin-bottom:10px;width:140px"]);                      
                    echo '</a>';  
                    break;               
               } 
            }
            foreach ($RelatedTags as $tag) {
                if($format==""){
                  $text = $prefix . $tag->Nombre;
                  $url = ["/recursos/recursos/tags", "tag" => $tag->Slug];
                  echo Html::a($text, $url, $inputOptions);
                }
                else{
                  if($tag->Nombre=="ProFuturo"){

                  }
                  else{
                      $options = [
                        'style' => [
                          'margin-right' => '10px',
                          'font-size' => '15px',
                          'margin-bottom' => '10px',
                        ],
                        'class' => [
                        'btn',
                        'w3-button w3-dark-gray w3-round-xlarge',
                        ]
                      ];
                      echo Html::a($tag->Nombre, Yii::$app->homeUrl . '?tag=' . $tag->idValoresDeCaracteristicas , $options);                       
                  }
             
                  // echo Button::widget([
                  //   'label' => $tag->Nombre,
                  //   'id' => $tag->idValoresDeCaracteristicas,
                  //   'options' => ['class' => 'w3-button w3-light-gray w3-round-large','onClick' => "resultadobusqueda($tag->idValoresDeCaracteristicas,'','','','','','','','','')"],
                  // ]); 
                }
            }
        }
    }

    public function printPublicacion() {
        $autor = $this->autor;
        
        $printResult = Yii::t("app", "Publicado ") . $this->getFechaDeCreacion() . Yii::t("app", " por ")  ;        
        $url = '<span>'.$printResult.'</span><a style="color:#e83e8c" target="_self" href="'. Url::to(Yii::$app->params["urlFront"] . "/recursos/recursos/mis-recursos?Autor=" . Html::encode($autor->id)) . '">'. $autor->username . '</a>';
        
     //   $printResult .= '<span class="fucsia-link">';
       // $printResult .=  $url;
    
        $administrador_general = Yii::$app->params["administrador_general_default_role"];
        $administrador_de_plataforma = Yii::$app->params["administrador_de_plataforma_default_role"];

        // if ($autor->UserHasRole($administrador_general) ||
        //         $autor->UserHasRole($administrador_de_plataforma)) {
        //     $printResult .= Yii::$app->params["fundacionOmarDengo"];
        // } else {
        //     $printResult .= $autor->username;
        // }
     //   $printResult .= '</span> ' ;
//$printResult .= $autor->username;
        echo $url;
        //$printResult;
    }

    /**
     * @return string
     */
    public function getFechaDeCreacion() {
        $fecha_moderacion = $this->FechaDeModeracion;
        $fecha_creacion = $this->FechaDeCreacion;
        $fecha = $fecha_moderacion ? $fecha_moderacion : $fecha_creacion;
		date_default_timezone_set('America/Costa_Rica'); 
        $d = new \DateTime($fecha);
        $datos = Yii::$app->formatter->asRelativeTime($d);
		return $datos;
    }

    /**
     * This functions return the values by each charasteristic
     * @return ValoresDeCaracteristicas 
     */
    public function getValoresPorCaracteristicas() {
        return ValoresDeCaracteristicas::getValoresPorCaracteristicas();
    }

    /**
     * Return the recurso's path
     * @param integer $idUsuario user id
     */
    public function getRecursoPath() {
        return Yii::$app->params["pathRecursos"] . '/' . $this->Autor . '/' . $this->rIdArchivos . '_' . $this->Recurso;
    }

    /**
     * Return the recurso's url
     */
    public function getRecursoUrl() {
        return Yii::$app->params["urlRecursos"] . '/' . $this->Autor . '/' . $this->rIdArchivos . '_' . $this->Recurso;
    }

    /**
     * Remove file from deirectory
     * @return type
     */
    public function deleteRecursoFile($current = null) {
        if ($current) {
            $archivo = Yii::$app->params["pathRecursos"] . '/' . $this->Autor . "/" . $this->rIdArchivos . '_' . $this->Recurso;
            if (file_exists($archivo)) {
                unlink($archivo);
            }
        }
    }

    public function getColecciones($idRecursos,$Autor){

      $query = (new Query)->select('TColecciones.idColeccion, TColecciones.Nombre')
       ->from('TColecciones')
       ->where(['Autor' => $Autor])
       ->innerJoin('TColeccionesRecursos', 'TColeccionesRecursos.idColeccion = TColecciones.idColeccion')
       ->andWhere(['TColeccionesRecursos.idRecursos' => $idRecursos])
       ->all();
       return $query;

    }

    public function getRecursosRelacionados($idColeccion = null) {

      if($idColeccion!=null){
     
        return Recursos::find()
                        ->join("INNER JOIN","TColeccionesRecursos","TRecursos.idRecursos = TColeccionesRecursos.idRecursos")
                        ->where(['IdColeccion' => $idColeccion])
                        ->limit(Yii::$app->params["relatedResourcesLimit"])
                        ->orderBy(new Expression('RAND()'))
                        ->all();
      }
      else{
        $tags = ArrayHelper::map($this->tagsValores, "idValoresDeCaracteristicas", "idValoresDeCaracteristicas");        
        return Recursos::find()->approved()
                        ->leftJoin("RRecursosValoresDeCaracteristicas", "TRecursos.idRecursos = RRecursosValoresDeCaracteristicas.rIdRecursos")
                        ->andWhere(['rIdValores' => $tags])
                        ->andWhere(['!=', 'rIdRecursos', $this->idRecursos])
                        ->limit(Yii::$app->params["relatedResourcesLimit"])
                        ->orderBy(new Expression('RAND()'))
                        ->all();        
      }
    }

    /**
     * Recursos más vistos
     * @return \yii\db\ActiveQuery
     */
    public static function getRecursosMasVistos() {

         $query = Recursos::find()->approved()
                ->select(["TRecursos.idRecursos", "TRecursos.Nombre", "TRecursos.Slug", "TRecursos.ObjetivoDelRecurso", "TRecursos.FechaDeCreacion", "TRecursos.Autor"])
                ->innerJoin("[TEventos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]")
                ->andWhere(["[TEventos].[Categoria]" => Eventos::CATEGORY_RECURSOS]) //solo los eventos de recursos
                ->andWhere(["[TEventos].[Accion]" => Eventos::VER_RECURSO]) //solo los eventos de ver recurso
                ->andWhere(["YEAR([TEventos].[FechaDeCreacion])" => date("Y")]) //filtrar por año
                // ->andFilterWhere(["MONTH([TEventos].[FechaDeCreacion])" => date("Y")]) //filtrar por mes
                ->limit(Yii::$app->params["mostViewedResourcesLimit"]) //filtrar por el top
                ->groupBy(["TRecursos.idRecursos", "TRecursos.Nombre", "TRecursos.Slug", "TRecursos.ObjetivoDelRecurso", "TRecursos.FechaDeCreacion", "TRecursos.Autor"])
                ->orderBy("COUNT(*) DESC")
                ->all();
//  $log_filename = $_SERVER['DOCUMENT_ROOT'] . '/log';

//      if (!file_exists($log_filename))
//      {
//          // create directory/folder uploads.
//          mkdir($log_filename, 0777, true);
//      }
//      $log_file_data = $log_filename.'/log.log';     
// // $t=time();
// // $hora = date("H:i:s",$t);       
//      file_put_contents($log_file_data, "Start-VE-" . $query->createCommand()->getRawSql() . "\n", FILE_APPEND);                 
      return $query;
    }

    /**
     * Recursos más recientes
     * @return \yii\db\ActiveQuery
     */
    public static function getRecursosMasRecientes() {
        return Recursos::find()
                        ->approved()
                        ->limit(Yii::$app->params["mostRecentResourcesLimit"])
                        ->orderBy(new Expression('TRecursos.FechaDeModeracion DESC'));
    }

    /**
     * Sends an email to validator general email.
     * @return \yii\db\ActiveQuery
     */
    public function sendEmailToResourcesValidator() {



        $mailStatus = Yii::$app->mailer
                ->compose(
                        ['html' => 'nuevoRecursoPublicado'], [
                    'recurso' => $this,
                        ]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo(Yii::$app->params["validatorEmail"])
                ->setSubject(Yii::$app->params["newResourceNotificationSubject"])
                ->send();

        return $mailStatus;
    }

    /**
     * Notify to user that the resource has been approved
     * @return \yii\db\ActiveQuery
     */
    public function notifyApproved() {

      $email = $this->autor->email;      
      $email2 = Yii::$app->params["administradorEmail"] ;
              
      $urlws = Yii::$app->params["urlWS"] . "/utils/callmail.php";               ;
  
      $path_to_email_template = 'recursoAprobado';
      $mensaje = Yii::$app->mailer
                 ->render($path_to_email_template, ['recurso'=>$this]);

      $params = array(
        'from'=>Yii::$app->params['supportEmail'] ,
        'nombre'=> Yii::$app->name,
        'para'=>$email,
        'asunto'=>Yii::$app->params["approvedNotificationSubject"],
        'paracopy'=>$email2,
        'nomreceptor'=>$this->autor->nombre,        
        'mensaje'=>$mensaje
      );  

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $urlws);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");   
      curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: multipart/form-data'));
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);   
      curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);  
      curl_setopt($ch, CURLOPT_TIMEOUT, 100);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $correo = curl_exec($ch);

      if ($correo === FALSE) {
        $resultado = 0;
        curl_close ($ch);
      }else{
        curl_close ($ch);
        $resultado = 1;
      }    

      return $resultado;

        // $mailStatus = Yii::$app->mailer
        //         ->compose(
        //                 ['html' => 'recursoAprobado'], [
        //             'recurso' => $this,
        //                 ]
        //         )
        //         ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
        //         ->setTo($this->autor->email)
        //         ->setSubject(Yii::$app->params["approvedNotificationSubject"])
        //         ->send();

        // return $mailStatus;
    }

    /**
     * Notify to user that the resource has been rejected
     * @return \yii\db\ActiveQuery
     */
    public function notifyRejected() {

      $email = $this->autor->email;        
      $email2 = Yii::$app->params["administradorEmail"] ;
// $email = "murillex1@hotmail.com";
// $email2 = "gmurillo@reinsa.co.cr";
              
      $urlws = Yii::$app->params["urlWS"] . "/utils/callmail.php";               ;
      $linkrecurso = Url::to(["/recursos/recursos/actualizar-recurso", "recurso" => $this->Slug]);
      $path_to_email_template = 'recursoRechazado';
      $mensaje = Yii::$app->mailer
                 ->render($path_to_email_template, ['recurso'=>$this,'linkrecurso'=>$linkrecurso]);

      $params = array(
        'from'=>Yii::$app->params['supportEmail'] ,
        'nombre'=> Yii::$app->name,
        'para'=>$email,
        'asunto'=>Yii::$app->params["rejectedNotificationSubject"],
        'paracopy'=>$email2,
        'nomreceptor'=>$this->autor->nombre,
        'mensaje'=>$mensaje
      );  

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $urlws);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");   
      curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: multipart/form-data'));
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);   
      curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);  
      curl_setopt($ch, CURLOPT_TIMEOUT, 100);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $correo = curl_exec($ch);

      if ($correo === FALSE) {
        $resultado = 0;
        curl_close ($ch);
      }else{
        curl_close ($ch);
        $resultado = 1;
      }                  
//**
        // $mailStatus = Yii::$app->mailer
        //         ->compose(
        //                 ['html' => 'recursoRechazado'], [
        //             'recurso' => $this,
        //                 ]
        //         )
        //         ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
        //         ->setTo($this->autor->email)                
        //         ->setSubject(Yii::$app->params["rejectedNotificationSubject"])
        //         ->send();


        return $resultado;
    }

    /**
     * @return int
     */
    public function getRecomendacionesCount() {
        return (int) Recomendaciones::find()->approved()
                        ->andWhere(['rIdRecursos' => $this->idRecursos])
                        ->count();
    }

    /**
     * @return int
     */
    public function getPendingRecomendacionesCount() {
        return (int) Recomendaciones::find()->pending()
                        ->andWhere(['rIdRecursos' => $this->idRecursos])
                        ->count();
    }

    /**
     * @return int
     */
    public function getStars($stars) {
        return (int) Votaciones::find()
                        ->andWhere(['rIdRecursos' => $this->idRecursos])
                        ->andWhere(['Votacion' => $stars])
                        ->count();
    }   

    /**
     * @return int
     */
    public function getPercent($stars) {
        $totthis = (int) Votaciones::find()
                        ->andWhere(['rIdRecursos' => $this->idRecursos])
                        ->andWhere(['Votacion' => $stars])
                        ->count() ;
        $tottot = (int) Votaciones::find()
                        ->andWhere(['rIdRecursos' => $this->idRecursos])               
                        ->count() ;      
        if($tottot<=0) {
          return 0;  
        }
        return number_format(($totthis / $tottot) * 100);                
    }   
     

}

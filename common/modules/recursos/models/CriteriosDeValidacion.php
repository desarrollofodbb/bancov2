<?php

namespace common\modules\recursos\models;

use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "TCriteriosDeValidacion".
 *
 * @property integer $idCriteriosDeValidacion
 * @property string $Nombre
 * @property integer $Tipo
 * @property string $CreadoEn
 * @property string $ActualizadoEn
 * @property integer $CreadoPor
 * @property integer $ActualizadoPor
 *
 * @property RRecursosCriteriosDeValidacion[] $rRecursosCriteriosDeValidacions
 * @property TRecursos[] $rIdRecuros
 * @property User $creadoPor
 * @property User $actualizadoPor
 */
class CriteriosDeValidacion extends \yii\db\ActiveRecord {

    const TYPE_QUALITY = 1;
    const TYPE_EDUCATIONAL = 2;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TCriteriosDeValidacion';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Nombre', 'Tipo', 'CreadoPor', 'ActualizadoPor'], 'required'],
            [['Nombre'], 'string'],
            [['Tipo', 'CreadoPor', 'ActualizadoPor'], 'integer'],
            [['CreadoEn', 'ActualizadoEn'], 'safe'],
            [['CreadoPor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['CreadoPor' => 'id']],
            [['ActualizadoPor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ActualizadoPor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idCriteriosDeValidacion' => 'Id Criterios De Validacion',
            'Nombre' => 'Nombre',
            'Tipo' => 'Tipo',
            'CreadoEn' => 'Creado En',
            'ActualizadoEn' => 'Actualizado En',
            'CreadoPor' => 'Creado Por',
            'ActualizadoPor' => 'Actualizado Por',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRRecursosCriteriosDeValidacions() {
        return $this->hasMany(RecursosCriteriosDeValidacion::className(), ['rIdCriteriosDeValidacion' => 'idCriteriosDeValidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRIdRecuros() {
        return $this->hasMany(Recursos::className(), ['idRecursos' => 'rIdRecuros'])->viaTable('RRecursosCriteriosDeValidacion', ['rIdCriteriosDeValidacion' => 'idCriteriosDeValidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreadoPor() {
        return $this->hasOne(User::className(), ['id' => 'CreadoPor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualizadoPor() {
        return $this->hasOne(User::className(), ['id' => 'ActualizadoPor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getCriteriosCalidad() {
        return ArrayHelper::map(
                        self::find()
                                ->select(["Nombre", "idCriteriosDeValidacion"])
                                ->where(["Tipo" => self::TYPE_QUALITY])
                                ->orderBy("Nombre ASC")
                                ->all(), "idCriteriosDeValidacion", "Nombre"
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getCriteriosEducacion() {
        return ArrayHelper::map(
                        self::find()
                                ->select(["Nombre", "idCriteriosDeValidacion"])
                                ->where(["Tipo" => self::TYPE_EDUCATIONAL])
                                ->orderBy("Nombre ASC")
                                ->all(), "idCriteriosDeValidacion", "Nombre"
        );
    }

}

<?php

namespace common\modules\recursos\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii2mod\behaviors\PurifyBehavior;
use yii2mod\moderation\enums\Status;
use yii2mod\moderation\ModerationBehavior;
use yii2mod\moderation\ModerationQuery;
use common\models\User;

/**
 * This is the model class for table "TRecomendaciones".
 *
 * @property integer $idRecomendaciones
 * @property string $Titulo
 * @property integer $Descripcion
 * @property string $Url
 * @property integer $rIdRecursos
 * @property integer $rIdUsuarios
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 * @property integer $FechaDeModeracion
 * @property integer $Estado
 *
 * @property TRecursos $Recurso
 * @property User $Usuario
 */
class Recomendaciones extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TRecomendaciones';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'rIdUsuarios',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
            ],
            'purify' => [
                'class' => PurifyBehavior::class,
                'attributes' => ['Descripcion'],
                'config' => [
                    'HTML.SafeIframe' => true,
                    'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
                    'AutoFormat.Linkify' => true,
                    'HTML.TargetBlank' => true,
                    'HTML.Allowed' => 'a[href], iframe[src|width|height|frameborder], img[src]',
                ],
            ],
            'moderation' => [
                'class' => ModerationBehavior::class,
                'statusAttribute' => 'Estado',
                'moderatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @return ModerationQuery
     */
    public static function find() {
        return new ModerationQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Titulo', 'Descripcion', 'Url', 'rIdRecursos'], 'required'],
            ['Estado', 'default', 'value' => Status::PENDING],
            ['Estado', 'in', 'range' => Status::getConstantsByName()],
            [['Titulo', 'Descripcion'], 'string'],
            [['Titulo'], 'string', 'length' => [0, 135], "message" => Yii::t("app", "El título de la recomendación no debe exceder los 135 caracteres")],
            [['Descripcion'], 'string', 'length' => [0, 1000], "message" => Yii::t("app", "El título de la recomendación no debe exceder los 300 caracteres")],
            [['Url'], 'url'],
            [['rIdRecursos', 'rIdUsuarios', 'FechaDeCreacion', 'FechaDeModificacion', 'FechaDeModeracion', 'Estado'], 'integer'],
            [['rIdRecursos'], 'exist', 'skipOnError' => true, 'targetClass' => Recursos::className(), 'targetAttribute' => ['rIdRecursos' => 'idRecursos']],
            [['rIdUsuarios'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rIdUsuarios' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idRecomendaciones' => Yii::t('app', 'Id Recomendaciones'),
            'Titulo' => Yii::t('app', 'Título'),
            'Descripcion' => Yii::t('app', 'Descripción'),
            'Url' => Yii::t('app', 'Url'),
            'rIdRecursos' => Yii::t('app', 'R Id Recursos'),
            'rIdUsuarios' => Yii::t('app', 'R Id Usuarios'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha De Creacion'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Actualizacion'),
            'FechaDeModeracion' => Yii::t('app', 'Fecha De Moderacion'),
            'Estado' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecurso() {
        return $this->hasOne(Recursos::className(), ['idRecursos' => 'rIdRecursos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario() {
        return $this->hasOne(User::className(), ['id' => 'rIdUsuarios']);
    }

    /**
     * @return mixed
     */
    public function getAuthorName() {
        if ($this->usuario->hasMethod('getUsername')) {
            return $this->usuario->getUsername();
        }

        return $this->usuario->username;
    }

    /**
     * @return string
     */
    public function getContent() {
        return nl2br($this->Descripcion);
    }

    /**
     * Get avatar of the user
     *
     * @return string
     */
    public function getAvatar() {
        if ($this->usuario->hasMethod('getAvatar')) {
            return $this->usuario->getAvatar();
        }

        return 'http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&f=y&s=50';
    }

    /**
     * @return string
     */
    public function getPostedDate() {
        return Yii::$app->formatter->asRelativeTime($this->FechaDeCreacion);
    }

    /**
     * @return string
     */
    public function getAnchorUrl() {
        return "#recomendacion-{$this->idRecomendaciones}";
    }

    public function printPublicacion() {
        $autor = $this->usuario;
        $printResult = Yii::t("app", "Publicado ") . $this->getFechaDeCreacion() . Yii::t("app", " por ");
        $printResult .= '<span class="fucsia-link">';

        $administrador_general = Yii::$app->params["administrador_general_default_role"];
        $administrador_de_plataforma = Yii::$app->params["administrador_de_plataforma_default_role"];

        if ($autor->UserHasRole($administrador_general) ||
                $autor->UserHasRole($administrador_de_plataforma)) {
            $printResult .= Yii::$app->params["fundacionOmarDengo"];
        } else {
            $printResult .= $autor->username;
        }
        $printResult .= '</span>';

        echo $printResult;
    }

    /**
     * @return string
     */
    public function getFechaDeCreacion() {
        return Yii::$app->formatter->asRelativeTime($this->FechaDeCreacion);
    }

    /**
     * Notify to user that the resource has been rejected
     * @return \yii\db\ActiveQuery
     */
    public function notifyToAuthor() {

        $recurso = Recursos::findOne($this->rIdRecursos);
        $mailStatus = Yii::$app->mailer
                ->compose(
                        ['html' => 'nuevaRecomendacion'], [
                    'recomendacion' => $this,
                    'recurso' => $recurso,
                        ]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($recurso->autor->email)
                ->setSubject(Yii::$app->params["recommendationNotificationSubject"])
                ->send();

        return $mailStatus;
    }

    public function beforeModeration() {
        $this->FechaDeModeracion = time(); // log the moderation date

        return true;
    }

}

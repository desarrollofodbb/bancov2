<?php

namespace common\modules\recursos\models;

use Yii;
use yii\helpers\FileHelper;

class ApprovalForm extends Recursos {

    public $criterios_validacion_calidad_ids;
    public $criterios_validacion_educacion_ids;

    public function afterSave($insert, $changedAttributes) {
        //------ save criterios de validacion reltionship ------------------------
        if ($this->criterios_validacion_calidad_ids || $this->criterios_validacion_educacion_ids) {

            $criterios_validacion = [];
            if ($this->criterios_validacion_calidad_ids) {
                foreach ($this->criterios_validacion_calidad_ids as $criterio_id) {
                    $criterio = CriteriosDeValidacion::findOne($criterio_id);
                    if (!is_null($criterio)) {
                        $criterios_validacion[] = $criterio;
                    }
                }
            }
            if ($this->criterios_validacion_educacion_ids) {
                foreach ($this->criterios_validacion_educacion_ids as $criterio_id) {
                    $criterio = CriteriosDeValidacion::findOne($criterio_id);
                    if (!is_null($criterio)) {
                        $criterios_validacion[] = $criterio;
                    }
                }
            }
            $this->linkAll('criteriosDeValidacion', $criterios_validacion);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function rules() {
        return [
            [["criterios_validacion_calidad_ids", "criterios_validacion_educacion_ids"], "safe"]
        ];
    }

}

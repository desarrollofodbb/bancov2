<?php

namespace common\modules\recursos\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "TArchivos".
 *
 * @property integer $idArchivo
 * @property string $Nombre
 * @property string $Tipo
 * @property integer $Tamano
 * @property string $Extension
 *
 * @property TRecursos[] $tRecursos
 */
class Archivos extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TArchivos';
    }

    public function behaviors() {
        return [
            [//BlameableBehavior automatically fills the specified attributes with the current user ID.
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CreadoPor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Nombre', 'Tipo', 'Tamano', 'Extension'], 'required'],
            [['Nombre', 'Tipo', 'Extension'], 'string'],
            [['Tamano'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idArchivo' => 'Id Archivo',
            'Nombre' => 'Nombre',
            'Tipo' => 'Tipo',
            'Tamano' => 'Tamano',
            'Extension' => 'Extension',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos() {
        return $this->hasMany(Recursos::className(), ['rIdArchivos' => 'idArchivo']);
    }

    /**
     * Save file by passing arguments model
     * @param UploadForm $uploadForm objeto uploadForm
     */
    public function saveRecursoFile($uploadForm) {

        $this->Nombre = $uploadForm->baseName;
        $this->Tipo = $uploadForm->type;
        $this->Tamano = $uploadForm->size;
        $this->Extension = $uploadForm->extension;
        return $this->save();

    }
    /**
     * Save file by passing arguments model
     * @param UploadForm $uploadForm objeto uploadForm
     */
    public function saveRecursoFileNew($rutaorg,$Autor,$idtime,$idRecursos) {

      $files = scandir($rutaorg);
      $oldfolder = $rutaorg."/";
$log_filename = $_SERVER['DOCUMENT_ROOT'] . '/log';
    if (!file_exists($log_filename))
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    $log_file_data = $log_filename.'/log.log';
      // $modelArch = $this;
      $modelArch = new Archivos();      
      foreach($files as $fname) {
file_put_contents($log_file_data, "A-" . $rutaorg . " ** " . $fname . "\n", FILE_APPEND);          
        if($fname != '.' && $fname != '..') {            
          $fim = finfo_open(FILEINFO_MIME_TYPE);
          $mime = finfo_file($fim, $oldfolder.$fname);    
          $ext =  pathinfo($oldfolder.$fname,PATHINFO_EXTENSION);            
file_put_contents($log_file_data, "B-" . $mime . " " . $ext . "\n", FILE_APPEND);                
          $modelArch->Nombre = $fname;
          $modelArch->Tipo = $mime;
          $modelArch->Tamano = filesize($oldfolder.$fname);
          $modelArch->Extension = $ext;
          $modelArch->idRecursos = $idRecursos;
          $modelArch->save();          
file_put_contents($log_file_data, "C- Save :) " . $mime . " " . $fname . "\n", FILE_APPEND);                    
          $idFile = $modelArch->idArchivo; 
          $newfolder = Yii::$app->params["pathRecursos"] . '/' . $Autor . '/' . $idFile . '_'; 
          if (!file_exists($newfolder))
          {
              // create directory/folder uploads.
              mkdir($newfolder, 0777, true);
          }
          if(rename($oldfolder.$fname, $newfolder.$fname)){
            $modelArch = new Archivos();
            // file_put_contents($log_file_data, ">". $idFile . "> D- Move? " . $fname . "\n", FILE_APPEND);          
          }
        }
      }
      rmdir($rutaorg);
        
        return true;
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

$module = \Yii::$app->controller->module;

$this->title = $module->params["t_c_add_new"];
?>
<div class="crear-coleccion">
    <div class="main-box large-box">
        <h1><?php echo Html::encode($this->title) ?><span class="icon icon-fod-icon-pencil"></span></h1>
            <?php
            echo
            $this->render('formColeccion', [
                'model' => $model,

            ])
            ?>
    </div>
</div>

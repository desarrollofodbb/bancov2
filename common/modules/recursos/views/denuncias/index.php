<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\recursos\models\DenunciasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Denuncias');

// get the module to which the currently requested controller belongs
$module = \Yii::$app->getModule("recursos");
?>
<div class="paginas-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php
    echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'Estado',
                'label' => Yii::t("app", "Estado"),
                'format' => 'raw',
                'value' => function ($model) {
                    switch ($model->Estado) {
                        case $model::STATUS_PENDING:
                            return Yii::t("app", "Pendiente");
                            break;
                        case $model::STATUS_APPROVED:
                            return Yii::t("app", "Aprobada (el recurso ha sido eliminado)");
                            break;
                        case $model::STATUS_REJECTED:
                            return Yii::t("app", "Rechazada");
                            break;
                    }
                },
                'filter' => [
                    $searchModel::STATUS_PENDING => Yii::t("app", "Pendientes"),
                    $searchModel::STATUS_APPROVED => Yii::t("app", "Aprobadas"),
                    $searchModel::STATUS_REJECTED => Yii::t("app", "Rechazadas"),
                ]
            ],
            [
                'attribute' => 'rIdRecursos',
                'label' => Yii::t("app", "Recurso"),
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->recurso) {
                        return Html::a($model->recurso->Nombre, Url::to(Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . $model->recurso->Slug), ['target' => '_blank']);
                    } else {
                        return Yii::t("app", "Al parecer el recurso ya ha sido eliminado");
                    }
                },
                    ],
                    [
                        'attribute' => 'rIdUsuarios',
                        'label' => Yii::t("app", "Usuario"),
                        'format' => 'html',
                        'value' => function ($model) {
                            if ($model->usuario) {
                                $texto = "<strong>Usuario: </strong>" . $model->usuario->username . '<br>';
                                $texto .= "<strong>Correo: </strong>" . $model->usuario->email;
                                return $texto;
                            } else {
                                return Yii::t("app", "Al parecer el usuario ya ha sido eliminado o desactivado");
                            }
                        },
                    ],
                    [
                        'attribute' => 'FechaDeCreacion',
                        'label' => Yii::t("app", "Fecha"),
                        'format' => 'date',
                        'value' => function ($model) {
                            return $model->FechaDeCreacion;
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ["/recursos/denuncias/ver-denuncia", "rIdRecursos" => $model->rIdRecursos, "rIdUsuarios" => $model->rIdUsuarios], ['title' => Yii::t('yii', 'Ver detalle de denuncia'),]);
                            },
                                    'delete' => function ($url, $model, $key) {
                                $data_confirm = Yii::t('app', 'Está seguro que desea eliminar el recurso?');
                                $text = '  <span class="glyphicon glyphicon-trash"></span> Eliminar recurso ';
                                $toroute = Url::toRoute([
                                            '/recursos/denuncias/eliminar-recurso',
                                            'rIdRecursos' => $model->rIdRecursos,
                                            'rIdUsuarios' => $model->rIdUsuarios,
                                ]);
                                $aria_label = Yii::t('app', 'Eliminar recurso');
                                if ($model->Estado == $model::STATUS_PENDING) {
                                    return Html::a($text, $toroute, [
                                                'data-method' => "post",
                                                "data-confirm" => $data_confirm,
                                                "data-pjax" => "0",
                                                "aria-label" => $aria_label,
                                                "title" => $aria_label
                                    ]);
                                }
                            },
                                ]
                            ]
                        ],
                    ]);
                    ?>
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Denuncias */

$this->title = Yii::t("app", "Detalle de denucia al recurso '{resource}'", [
            "resource" => $model->getRecurso()->where(["idRecursos" => $model->rIdRecursos])->one()->Nombre,
        ]);
?>
<div class="denuncias-view">
    <h3><?php echo Html::encode($this->title) ?></h3>

    <?php
    echo
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'rIdRecursos',
                'label' => Yii::t("app", "Recurso"),
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->recurso) {
                        return Html::a($model->recurso->Nombre, Url::to(Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . $model->recurso->Slug), ['target' => '_blank']);
                    } else {
                        return $model->getRecurso()->where(["idRecursos" => $model->rIdRecursos])->one()->Nombre;
                    }
                },
                    ],
                    [
                        'attribute' => 'rIdUsuarios',
                        'label' => Yii::t("app", "Usuario"),
                        'format' => 'html',
                        'value' => function ($model) {
                            if ($model->usuario) {
                                $texto = "<strong>Usuario: </strong>" . $model->usuario->username . '<br>';
                                $texto .= "<strong>Correo: </strong>" . $model->usuario->email;
                                return $texto;
                            } else {
                                return Yii::t("app", "Al parecer el usuario ya ha sido eliminado o desactivado");
                            }
                        },
                    ],
                    [
                        'attribute' => 'Motivo',
                        'label' => Yii::t("app", "Motivo"),
                        'format' => 'html',
                        'value' => function ($model) {
                            return $model->Motivo;
                        },
                    ],
                    [
                        'attribute' => 'FechaDeCreacion',
                        'label' => Yii::t("app", "Fecha"),
                        'format' => 'date',
                        'value' => function ($model) {
                            return $model->FechaDeCreacion;
                        },
                    ],
                    [
                        'attribute' => 'Estado',
                        'label' => Yii::t("app", "Estado"),
                        'format' => 'text',
                        'value' => function ($model) {
                            switch ($model->Estado) {
                                case $model::STATUS_PENDING:
                                    return Yii::t("app", "Pendiente");
                                    break;
                                case $model::STATUS_APPROVED:
                                    return Yii::t("app", "Aprobada (el recurso ha sido eliminado)");
                                    break;
                                case $model::STATUS_REJECTED:
                                    return Yii::t("app", "Rechazada");
                                    break;
                            }
                        },
                    ],
                ],
            ])
            ?>

            <div class="form-inline">
                <?php
                echo Html::a(Yii::t("app", "Volver a denuncias"), ["/recursos/denuncias/index"], [ 'class' => 'btn btn-default']);
                ?>

                <?php
                if ($model->Estado == $model::STATUS_PENDING) {
                    $data_confirm = Yii::t('app', '¿Está seguro que desea rechazar la denuncia?');
                    $text = 'Rechazar denuncia ';
                    $toroute = Url::toRoute([
                                '/recursos/denuncias/rechazar-denuncia',
                                'rIdRecursos' => $model->rIdRecursos,
                                'rIdUsuarios' => $model->rIdUsuarios,
                    ]);
                    $aria_label = Yii::t('app', 'Rechazar denuncia');
                    echo Html::a($text, $toroute, [
                        'data-method' => "post",
                        "data-confirm" => $data_confirm,
                        "data-pjax" => "0",
                        "aria-label" => $aria_label,
                        "title" => $aria_label,
                        'class' => 'btn btn-primary'
                    ]);
                }
                ?>

                <?php
                if ($model->Estado == $model::STATUS_PENDING) {
                    $data_confirm = Yii::t('app', '¿Está seguro que desea eliminar el recurso?');
                    $text = 'Eliminar el recurso ';
                    $toroute = Url::toRoute([
                                '/recursos/denuncias/eliminar-recurso',
                                'rIdRecursos' => $model->rIdRecursos,
                                'rIdUsuarios' => $model->rIdUsuarios,
                    ]);
                    $aria_label = Yii::t('app', 'Aprobar denuncia y eliminar el recurso');
                    echo Html::a($text, $toroute, [
                        'data-method' => "post",
                        "data-confirm" => $data_confirm,
                        "data-pjax" => "0",
                        "aria-label" => $aria_label,
                        "title" => $aria_label,
                        'class' => 'btn btn-danger'
                    ]);
                }
                ?>

    </div>

</div>

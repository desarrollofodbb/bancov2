<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use pendalf89\tinymce\TinyMce;

/* @var $this \yii\web\View */
/* @var $recomendacionesModel \yii2comments\comments\models\CommentModel */
/* @var $encryptedEntity string */
/* @var $formId string comment form id */
?>
<div class="recomendaciones-form-container">
    <?php
    $urlForm = $recomendacionesModel->isNewRecord ? '/recursos/recomendaciones/create' : "/recursos/recomendaciones/update?id={$recomendacionesModel->idRecomendaciones}";
    $form = ActiveForm::begin([
                'options' => [
                    'class' => 'recomendaciones-box',
                ],
                'action' => Url::to([$urlForm]),
    ]);
    ?>

    <?php echo $form->field($recomendacionesModel, 'Titulo', ['template' => '{input}{error}'])->textInput(['placeholder' => Yii::t('app', 'Escriba un título para la recomendación...'), "class" => "form-control round-form-control"]) ?>
    <?php
    echo
            $form->field($recomendacionesModel, 'Descripcion', ['template' => '{input}{error}'])
            ->textarea(
                    ['placeholder' => Yii::t('app', 'Escriba una descripción para la recomendación...')
                        , 'rows' => 6, "class" => "form-control round-form-control"]
            )->hint(Yii::t("app", "Descripción debería contener como máximo 1,000 letras."));
    ?> 

    <?php echo $form->field($recomendacionesModel, 'Url', ['template' => '{input}{error}'])->textInput(['placeholder' => Yii::t('app', 'Agregue el enlace recomendado...'), "class" => "form-control round-form-control"]) ?>
    <?php echo $form->field($recomendacionesModel, 'rIdRecursos')->hiddenInput(["value" => $recurso->idRecursos])->label(false); ?>
    <?php echo $form->field($recomendacionesModel, 'rIdUsuarios')->hiddenInput(["value" => Yii::$app->user->identity->id])->label(false); ?>
    <input type="hidden" name="recurso" value="<?php echo $recurso->Slug; ?>">
    <div class="recomendaciones-box-partial">
        <div class="button-container show">
            <?php
            echo Html::submitButton($recomendacionesModel->isNewRecord ? Yii::t('app', 'Enviar recomendación') : Yii::t('app', 'Actualizar recomendación'), [
                'class' => 'btn btn-default btn-md',
            ])
            ?>
        </div>
    </div>
    <?php $form->end(); ?>
    <div class="clearfix"></div>
</div>

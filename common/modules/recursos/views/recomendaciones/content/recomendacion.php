<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii2mod\moderation\enums\Status;

/* @var $this \yii\web\View */
/* @var $model \common\modules\recursos\models\Recomendaciones */
/* @var $maxLevel null|integer comments max level */
?> 
<div class="lined-title lined-sm">
    <div class="round-ico-wrap">
        <div class="round-ico big-ico">
            <?php
            $user = $model->usuario;
            echo $user->getAvatarImg();
            ?>
        </div>
    </div>
    <h3>
        <?php
        $text = Html::encode($model->Titulo);
//        $url = Url::to(["/recursos/re/ver-recurso", "recurso" => $model->Slug]);
//        $options = ["class" => ""];
//        echo Html::a($text, $url, $options);
        echo $text;
        ?> 
    </h3>
</div>
<div class="lined-box">
    <div class="body-lined"> 
        <p>
            <?php $model->printPublicacion(); ?>
        </p>
        <?php echo $model->getContent(); ?> 
    </div>
    <div class="recomendacion-tool-bar"> 

        <?php
        if ($model->isPending()) {
            $Text = '<span class="glyphicon glyphicon-thumbs-up"></span> ' . Yii::t("app", "Aprobar");
            echo
            Html::a($Text, [
                '/recursos/recomendaciones/approve',
                'id' => $model->idRecomendaciones,
                'urlredirect' => $urlredirect,
                    ], [
                'data' => [
                    'method' => 'post',
                    'params' =>
                    [
                        'id' => $model->idRecomendaciones,
                        'urlredirect' => $urlredirect,
                    ],
                ],
                "class" => 'fucsia-link sm-link',
            ]);
        }
//                    "data-confirm" => $data_confirm
        ?>
        <?php
        if ($model->isPending()) {
            $Text = ' <span class="glyphicon glyphicon-thumbs-down "></span> ' . Yii::t("app", "Rechazar");
            $data_confirm = Yii::t("app", "¿Seguro que desea rechazar la recomendación?");

            echo
            Html::a($Text, [
                '/recursos/recomendaciones/reject',
                'id' => $model->idRecomendaciones,
                'urlredirect' => "/recursos/recomendaciones/aprobar-recomendaciones?recurso={$model->recurso->Slug}",
                    ], [
                'data' => [
                    'method' => 'post',
                    'params' =>
                    [
                        'id' => $model->idRecomendaciones,
                        'urlredirect' => "/recursos/recomendaciones/aprobar-recomendaciones?recurso={$model->recurso->Slug}",
                    ],
                ],
                "class" => 'fucsia-link sm-link',
                "data-confirm" => $data_confirm
            ]);
        }
        ?>
        <?php
        if (Yii::$app->user->can("/recursos/recomendaciones/delete", ["id" => $model->idRecomendaciones])) {
            $Text = '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t("app", "Eliminar");
            $data_confirm = Yii::t("app", "¿Seguro que desea eliminar la recomendación?");

            echo
            Html::a($Text, [
                '/recursos/recomendaciones/delete',
                'id' => $model->idRecomendaciones,
                'urlredirect' => $urlredirect,
                    ], [
                'data' => [
                    'method' => 'post',
                    'params' =>
                    [
                        'id' => $model->idRecomendaciones,
                        'urlredirect' => $urlredirect,
                    ],
                ],
                "class" => 'fucsia-link sm-link',
                "data-confirm" => $data_confirm
            ]);
        }
        ?> 
        <?php
        if (Yii::$app->user->can("/recursos/recomendaciones/update", ["id" => $model->idRecomendaciones])) {
            $Text = '<span class="glyphicon glyphicon-pencil "></span> ' . Yii::t("app", "Editar");


            echo
            Html::a($Text, [
                '/recursos/recomendaciones/update',
                'id' => $model->idRecomendaciones
                    ], [
                "class" => 'fucsia-link sm-link'
            ]);
        }
        ?>
        <?php if ($model->Url): ?>
            <?php
            $text = '<span class="glyphicon glyphicon-new-window "></span> ' . Yii::t("app", "Abrir sitio web");
            echo Html::a($text, $model->Url, ["class" => "fucsia-link sm-link", "target" => "_blank"]);
            ?>
        <?php endif; ?>
    </div>
</div>

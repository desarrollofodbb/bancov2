<?php

use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\helpers\Html;
?>
<div class="recomendaciones-wrapper" id="recomendaciones-wrapper">
    <div class="recomendaciones row">
        <div class="col-md-12 col-sm-12">
            <div class="title-block clearfix"> 
                <h4 class="top-space underlined-title">
                    <?php echo Yii::t('app', 'Recomendaciones ({0})', $recurso->getRecomendacionesCount()); ?>
                    <?php
                    $urlaprobar = ["/recursos/recomendaciones/aprobar-recomendaciones", "recurso" => $recurso->Slug];
                    $pendingcount = $recurso->getPendingRecomendacionesCount();
                    if (Yii::$app->user->can("/recursos/recomendaciones/aprobar-recomendaciones", ["recurso" => $recurso->Slug]) && ($pendingcount > 0)) {
                        echo
                        Html::a(Yii::t('app', 'Revisar recomendaciones ({0})', $pendingcount), $urlaprobar, ["class" => "pull-right"]);
                    }
                    ?>
                </h4>
                <div class="title-separator"></div>
            </div>

            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->can("/recursos/recomendaciones/create")) : ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <?php
                        echo $this->render('_recomendaciones-form', [
                            'recurso' => $recurso,
                            'recomendacionesModel' => $recomendacionesModel,
                        ]);
                        ?>
                    </div> 
                </div>
                <br>
            <?php endif; ?>
            <?php
            $emptyText = Yii::t("app", "El recurso aún no tiene recomendaciones.");
            echo ListView::widget(
                    [
                        'dataProvider' => $recomendacionesDataProvider,
                        'layout' => "{items}\n{pager}",
                        'emptyText' => $emptyText,
                        'emptyTextOptions' => [
                            'tag' => 'div',
                            'class' => 'col-md-12',
                        ],
                        'options' => [
                            'tag' => 'div',
                            'class' => 'row recomendaciones-list',
                        ],
                        'itemOptions' => [
                            'tag' => 'div',
                            'id' => '',
                            'class' => "col-xs-12 col-sm-6 col-ms-6 col-lg-6",
                        ],
                        'itemView' => function ($model, $key, $index) {
                    return $this->render('content/recomendacion', [
                                'model' => $model,
                                'index' => $index,
                                'urlredirect' => "/recursos/recursos/ver-recurso?recurso={$model->recurso->Slug}",
                    ]);
                },
                    ]
            );
            ?> 
        </div>
    </div>
</div>

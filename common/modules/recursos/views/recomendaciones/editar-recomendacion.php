<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\preguntas_frecuentes\models\PreguntasFrecuentes */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;

$this->title = Yii::t("app", "Actualizar recomendación");
?>
<div class="recomendacion-update">
    <div class="main-box large-box">
        <h1><?php echo Html::encode($this->title) ?><span class="icon icon-fod-icon-pencil"></span></h1>
        <br>
        <br>
        <?php
        echo
        $this->render('_recomendaciones-form', [
            'recomendacionesModel' => $recomendacionesModel,
            'recurso' => $recomendacionesModel->recurso,
        ])
        ?>

    </div>
</div>

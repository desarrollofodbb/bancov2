<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

    public function actionDeleteFile() {
      $idArchivo = $_POST["idArchivo"];
      $query = (new Query)->select('idArchivo,idRecursos,CreadoPor,Nombre')
       ->from('TArchivos')
       ->where(['idArchivo' => $idArchivo]);      
      $rows = $query->all();
      foreach ($rows as $registro) {  
        $idArchivo = $registro['idArchivo'];
        $idRecursos = $registro['idRecursos'];
        $CreadoPor = $registro['CreadoPor'];
        $Nombre = $registro['Nombre'];
        $folder = Yii::$app->params["pathRecursos"] . '/' . $CreadoPor . '/' . $idArchivo . '_' .$Nombre;
        unlink($folder);
        Yii::$app->db->createCommand()->delete('TArchivos', [
                 'idArchivo' => $idArchivo,
               ])->execute(); 
      }

      return true;
    }
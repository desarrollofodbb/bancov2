<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use common\widgets\Alert;
use pendalf89\tinymce\TinyMce;
use common\modules\recursos\models\CriteriosDeEvaluacion;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
$autor = $model->isNewRecord ? User::getCurrentUser() : $model->autor;
?>
<style type="text/css">
* {
  box-sizing: border-box;
}

/* Create two unequal columns that floats next to each other */
.fs {
  font-size: 10px;    
}
.column {
  float: left;
  padding: 5px;
/*  height: 300px;  Should be removed. Only for demonstration */
}

.left {
  width: 25%;
}
.center {
  width: 15%;
}


/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
#drop_file_zone {
/*    background-color: #EEE;*/
    border: #999 1px solid;
    /*width: 290px;*/
    height: 130px;
    padding: 1px;
    font-family: "Source Sans Pro Bold";
    font-size: 100%;
}
/*#drag_upload_file {
    width:50%;
    margin:0 auto;
}*/
#drag_upload_file p {
    text-align: center;
}
#drag_upload_file #selectfile {
    display: none;
}    
/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    width: 25%;
  }
}
</style>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="custom.js"></script>

<script type="text/javascript">
var idautor = <?php echo $autor->id; ?>;    
var count = 0;
function preview(filepreview) {
    
    var newDiv = $('<div id="zone_preview" class="column center"></div>');  //create Div Element w/ jquery
    $( "#drop_file_zone" ).append(newDiv);    
    frameid = "frame";
    if(count>0){
        frameid = "frame" + count;
    }
    var newImg = $('<img id="' + frameid + '" src="" width="100px" height="100px"></div>');  //create Div Element w/ jquery
    newDiv.append(newImg);
    var newDiv2 = $('<div class = "fs" id = "status"></div>');  //create Div Element w/ jquery

    newDiv.append(newDiv2);    
            // <div class = "fs" id="status"></div>      
    fid =  document.getElementById(frameid)         
    fid.src=URL.createObjectURL(filepreview);
    response = filepreview.name;
    message = "<div>" + response + "</div>"
    newDiv2.append(message);     
    count = count + 1;
}    
var fileobj;
function upload_file(e) {
    e.preventDefault();
    ajax_file_upload(e.dataTransfer.files);
}
 
function file_explorer() {
    document.getElementById('selectfile').click();
    document.getElementById('selectfile').onchange = function() {
        files = document.getElementById('selectfile').files;
        ajax_file_upload(files);
      //  preview(event.target.files[0]);
    };
}
 
function ajax_file_upload(file_obj) {
    if(file_obj != undefined) {
        var form_data = new FormData();
        for(i=0; i<file_obj.length; i++) {  
            form_data.append('file[]', file_obj[i]);  
            form_data.append('userid', idautor);
            preview(file_obj[i])
            //frame.src=URL.createObjectURL(file_obj[i]);
        }
        $.ajax({
            type: 'POST',
            url: '/server/ajax.php',
            contentType: false,
            processData: false,
            data: form_data,
            success:function(response) {


                $('#selectfile').val('');
            }
        });
    }
}    

</script>  
<div class="recursos-form">
    <br>
    <?php echo Alert::widget() ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'Nombre')->textInput(
                    [
//                            "placeholder" => "Nombre*",
                        "class" => "form-control round-form-control"
            ])->label("Nombre*");
            ?>
            <?php
            echo $form->field($model, 'ObjetivoDelRecurso')->textarea(
                    [
                        "rows" => 8,
                        "class" => "form-control round-form-control"
            ])->label("Objetivo del recurso*");
            ?>
            <fieldset>
                <?php
                $modelArchivoRelated = $model->archivo;
                $autor = $model->isNewRecord ? User::getCurrentUser() : $model->autor;

                echo $form->field($model, "Tipo")->dropDownList([
                    $model::TYPE_FILE => Yii::t("app", "Subir archivo"),
                    $model::TYPE_URL => Yii::t("app", "Agregar una URL"),
                    $model::TYPE_EMBED => Yii::t("app", "Embed"),
                        ], [
                    "prompt" => Yii::t("app", "Seleccione la fuente del recurso"),
                    "onchange" => ""
                    . "jQuery('div[id^=\"tipo-recurso-\"]').addClass('hidden');"
                    . "jQuery('#tipo-recurso-'+jQuery(this).val()).removeClass('hidden');",
//                    'class' => ""
                ])->hint(Yii::t("app", "Su espacio disponible para subir archivos es de: {tamano}", [
                            "tamano" => $autor->getUserRemainingSpace($modelArchivoRelated ? $modelArchivoRelated->Tamano : null)
                ]));
                ?>
                <div id="tipo-recurso-<?php echo $model::TYPE_FILE; ?>" class="<?php echo $model->Tipo == $model::TYPE_FILE ? "" : "hidden"; ?>">

                    <?php
                    $pluginOptions = [
                        'showRemove' => false,
                        'showUpload' => false,
                        'mainClass' => 'input-group-sm'
                    ];
                    if (!$model->isNewRecord && ($model->Tipo == $model::TYPE_FILE)) {
                        $pluginOptions['initialCaption'] = $model->Recurso;
//                                [
////                    'initialPreview' => [
////                        $model->getRecursoUrl(),
////                    ],
////                    'initialPreviewAsData' => true,
////                    'overwriteInitial' => true,
//                            
//                        ];
                    }
                    ?> 
                    <?php if (!$model->isNewRecord && ($model->Tipo == $model::TYPE_FILE)): ?>
                        <?php
                        echo $form->field($model, "Recurso")->hiddenInput()->label(false);
                        echo Html::a(Yii::t("app", "Descargar archivo actual"), $model->getRecursoUrl(), ["class" => "btn btn-sm btn-default", "target" => "_blank", "download" => ""]);
                        ?> 
                    <?php endif; ?> 
                    <?php
                    echo $form->field($modelRecurso, 'recursoFile')->widget(FileInput::classname(), [
                        'options' => [
                            'multiple' => false,
                        ],
                        'pluginOptions' => $pluginOptions
                    ])->label(false);
                    ?>  
                </div>
                <div id="tipo-recurso-<?php echo $model::TYPE_URL; ?>" class="<?php echo $model->Tipo == $model::TYPE_URL ? "" : ""; ?>">
                    <?php
                    echo $form->field($model, 'url_recurso')->textInput(["class" => "form-control round-form-control"])->hint(Yii::t("app", "Su espacio disponible para subir archivos es de: {tamano}", [
                            "tamano" => $autor->getUserRemainingSpace($modelArchivoRelated ? $modelArchivoRelated->Tamano : null)
                ]));
                    ?>
                </div>                
    <div id="drop_file_zone" ondrop="upload_file(event)" ondragover="return false" class="row">
        <div id="drag_upload_file" class="column left">
            <p>Arrastre sus archivos aquí o</p>
            <p><input type="button" value="Busque archivo(s)" onclick="file_explorer();"></p>
            <input type="file" id="selectfile" multiple>
        </div>
<!--         <div id="zone_preview" class="column center"> 
            <img id="frame" src="" width="100px" height="100px"/>   
            <div class = "fs" id="status"></div>        
        </div> -->

    </div>


                <div id="tipo-recurso-<?php echo $model::TYPE_EMBED; ?>" class="<?php echo $model->Tipo == $model::TYPE_EMBED ? "" : "hidden"; ?>">
                    <?php
                    echo
                    $form->field($model, 'embed_recurso')->widget(TinyMce::className(), [
                        'clientOptions' => [
                            'language' => 'es',
                            'selector' => 'textarea.tinymce',
                            'plugins' => [
                                'media noneditable',
                            ],
                            'noneditable_editable_class' => 'mce-content-body',
                            'menubar' => false,
//                        'height' => 300,                       
                            'toolbar' => 'media',
                            'media_live_embeds' => true,
                            'media_alt_source' => false,
                            'media_poster' => false,
                            'media_dimensions' => false,
                            'valid_elements' => 'iframe[src|width|height|name|align]',
                            'extended_valid_elements' => 'iframe[src|width|height|name|align]',
                            'invalid_elements' => 'strong,b,em,i,p',
                        ],
                    ])->hint(Yii::t("app", "Asegurese de agregar un código embed válido"));
                    ?>
                </div>
            </fieldset>

            <?php
            echo
            $form->field($model, 'autores_ids')->widget(Select2::className(), [
                'model' => $model,
                'attribute' => 'autores_ids',
                'language' => 'es',
                'showToggleAll' => false,
                'data' => ArrayHelper::map(User::findByRole("autor"), 'id', 'username'),
                'options' => [
                    'multiple' => true,
                    "class" => "form-control round-form-control",
//                        "placeholder" => "Seleccione el autor o los autores",
                ],
                'pluginOptions' => [
//                'allowClear' => true,
                ],
                "changeOnReset" => false
            ])->label("Seleccione el autor o los autores");
            ?>
        </div> 
    </div>
    <div class="row">
        <?php
        foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
            ?> 
            <div class="col-xs-12 col-sm-4 col-ms-4 col-lg-4">
                <?php
                $valor_key = explode("-", $caracteristica_key);
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
  mb_convert_encoding($caracteristica_nombre, 'UTF-8', 'UTF-8');
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo
                $form->field($model, $caracteristica_field)->widget(Select2::className(), [
                    'model' => $model,
                    'attribute' => $caracteristica_field,
                    'language' => 'es',
                    'showToggleAll' => false,
                    'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                    'options' => [
                        'multiple' => $caracteristica_tipoSeleccion,
                        "class" => "form-control round-form-control",
                        "placeholder" => $placeholder,
                    ],
                    'pluginOptions' => [
//                'allowClear' => true,
                    ],
                ])->label($caracteristica_nombre);
                ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row criterios-de-evaluacion banco-ckeckboxlist"> 
        <div class="col col-md-12">
            <?php
            echo $form->field($model, "criterios_ids")
                    ->checkboxList(ArrayHelper::map(CriteriosDeEvaluacion::find()->select(["Nombre", "idCriteriosDeEvaluacion"])->all(), "idCriteriosDeEvaluacion", "Nombre"));
            ?>
        </div>
    </div>

    <?php
    echo Html::submitButton($model->isNewRecord ? $module->params["a_publish"] : $module->params["a_update"], [
        'class' => $model->isNewRecord ? 'btn btn-default' : 'btn btn-primary',
    ])
    ?>

    <?php ActiveForm::end(); ?>
</div>

<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    public function actionViewBusquedas () {
     $Nombre = ""; 
     $Autor = "";
     $AutorRecurso = "";
     $anioRecurso1 = "";
     $anioRecurso2 = "";
     $limit = 0; //Cantidad de Registros 
     $offset = 0; //Registros a saltar

     if(isset($_POST['limit'])){
       $limit = $_POST['limit']; 
     }

     if(isset($_POST['offset'])){
       $offset = $_POST['offset']; 
     }
     
     if(isset($_POST['Nombre'])){
       $Nombre = $_POST['Nombre']; 
     }
     if(isset($_POST['Autor'])){
       $Autor = $_POST['Autor']; 
     }
     if(isset($_POST['AutorRecurso'])){
       $AutorRecurso = $_POST['AutorRecurso']; 
     }     
     if(isset($_POST['anioRecurso1'])){
       $anioRecurso1 = $_POST['anioRecurso1']; 
     }          
     if(isset($_POST['anioRecurso2'])){
       $anioRecurso2 = $_POST['anioRecurso2']; 
     }   
     if($limit>0){
       $query = (new Query)->select('TRecursos.idRecursos, TRecursos.Slug, TRecursos.Nombre')
       ->from('TRecursos')
       ->where(['like', 'Nombre' => $Nombre])
       ->andWhere(['Revisado' => 1])
       ->limit($limit)->offset($offset);      
     }
     else{
       $query = (new Query)->select('TRecursos.idRecursos, TRecursos.Slug, TRecursos.Nombre')
       ->from('TRecursos')
       ->where(['like', 'Nombre' => $Nombre])
       ->andWhere(['Revisado' => 1]);         
     }


    $count=0;
    $model = new Recursos();

    foreach ($query as $registros) {
      $count++;

    }
    if($count==0){
      $output .= '<li><a href="#" class="text-bold text-italic">No hay nuevas notificaciones</a></li>';      
    }
    $data = array(
       'mensajes' => $output,
       'cantidad'  => $count
    );
    echo json_encode($data);

    return json_encode($data);
    }
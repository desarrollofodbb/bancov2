<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use common\modules\recursos\widgets\RecursosRelacionados;
use common\modules\recursos\models\Favoritos;
use common\modules\recursos\models\Denuncias;
use common\modules\recursos\models\Recursos;
use common\widgets\Alert;
use common\modules\yii2comments\widgets\Comment;
use kartik\rating\StarRating;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; // or kartik\select2\Select2
use yii\web\JsExpression;
use common\modules\recursos\models\Colecciones;


$this->title = $model->Nombre;

  
      $user = $model->autor;            
      $ValoresTipo = $model->getTipoValores()->All();
      foreach ($ValoresTipo as $valor) {
        $TipoRec =  $valor->Nombre;
      }              

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */
$actual_link = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<style>

.card {
  // box-shadow: 0 0 20px rgba(0,0,0,.2);
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  border:none;
/*  max-width: 600px;*/
/*  background: #005E9B;*/
  flex-basis: 250px;
/*  color: white;*/
  padding: 2em;
/*  text-align: center;*/
  
  display: grid;
  grid-template-columns: 4fr;
  grid-column-gap: 2em;
  grid-template-areas: 
    "header"
    "image"
    "imagefooter"
    "body"
    "footer ";
}
/*  .card {
    text-align: left;
    grid-template-columns: 2fr 2fr;
    grid-template-areas:
      "image header"
      "image body"
      "imagefooter footer";
  } */

.resource-header     { grid-area: header; }
.resource-body { grid-area: body; }
.resource-info     { grid-area: footer; }
.resource-img      { grid-area: image; }
.imagefooter-list      { grid-area: imagefooter; }



.resource-info {
/*  font-weight: 300;
  opacity: .7;*/
  margin-left:15px;
}

.resource-header {
  grid-area: header;
/*  letter-spacing: 1px;*/
/*  font-size: 2rem;
  margin: .75em 0 0;*/
  line-height: 1;
}

.resource-header::after {
  content: '';
  display: block;
  width: 2em;
  height: 1px;
  background: #5bcbf0;
  margin: .5em auto .65em;
  opacity: .25;
}

.resource-body {
  grid-area: body;
  line-height: 1;
/*  text-transform: uppercase;
  font-size: .875rem;
  letter-spacing: 3px;*/
/*  margin: 0 0 .5vem;
  line-height: 1;*/
/*  color: #5bcbf0;*/
}

.resource-img {
  justify-self: center;
  width: 100%;
  height: 100%;
/*  border-radius: 50%;*/
  border: 2px solid white;
  object-fit: cover;
}

.imagefooter-list {
/*  justify-self: center;*/
/*  list-style: none;
  justify-content: space-between;
  display: flex;*/
  max-width: 100%;
/*  margin: .5em 0 0;
  padding: 0;*/
}

.imagefooter-link {
  color: #5bcbf0;
  opacity: .5;
}

.imagefooter-link:hover,
.imagefooter-link:focus {
  opacity: 1;
}

.bio-title {
  color: #0090D1;
  font-size: 1.25rem;
  // font-weight: 300;
/*  letter-spacing: 1px;*/
  text-transform: uppercase;
  line-height: 1;
  margin: 0;
}

.margentop{
    margin-top: 0px;
}

.w3-height{
  height: 625px;  
}

.w3-heightasi{
  height: 605px;  
}

.w3-heightimage{
  height: 395px;  
}

@media (min-width: 600px) {
  .card {
    text-align: left;
    grid-template-columns: 2fr 2fr;
    grid-template-areas:
      "image header"
      "image body"
      "imagefooter footer";
  } 
  
  .resource-header::after {
    margin-left: 0;
  }

  .margentop{
    margin-top: 10px;
  }

  .w3-height{
    height: 245px;  
  }

  .w3-heightasi{
    height: 225px;  
  }  
  .w3-heightimage{
    height: 145px;  
  }  
}

.boton2 {
  background-color: #EBEDEC;
  color: #EBEDEC;
  padding: 5px 5px;
  margin-bottom: 5px;
  font-size: 13px;
  cursor: pointer;
}

/* Green */
.exito2 {
  border-color: #EBEDEC;
  color: #EBEDEC;
}

.exito2:hover {
  background-color: #666666;
  color: white;
}

h2.xhover:hover{
    color:#00CC99;
}

        .btn-circle.btn-sm { 
            width: 35px; 
            height: 35px; 
            padding: 6px 0px; 
            border-radius: 20px; 
            font-size: 8px; 
            text-align: center; 
        } 
        .btn-circle.btn-md { 
            width: 50px; 
            height: 50px; 
            padding: 7px 10px; 
            border-radius: 25px; 
            font-size: 10px; 
            text-align: center; 
        } 
        .btn-circle.btn-xl { 
            width: 70px; 
            height: 70px; 
            padding: 10px 16px; 
            border-radius: 35px; 
            font-size: 12px; 
            text-align: center; 
        } 
.select2-container--krajee .select2-selection {
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    background-color: #EBEDEC;

    border: none;
    border-radius: 30px;
    color: #555555;
    font-size: 14px;
    outline: 0;
}       
.center {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 200px;
} 
</style>



<?php 
  echo Alert::widget(); 
  if (Yii::$app->user->isGuest){

  }
  else{
?>
<div class="w3-container">
  <div id="id02" class="w3-modal"  >
    <div class="w3-modal-content w3-animate-zoom w3-round-xlarge w3-height" style="width:375px;max-height:645px;">
      <div class="w3-container w3-center w3-padding">
      <span style="color: #00CC99" onclick="cierra('id02')" class="w3-button w3-xlarge w3-hover-opacity w3-display-topright" title="Cerrar / Cancelar"><i style="color: black;font-size: 20px">&times;</i></span>
        <h2 style="color:black;font-weight: bold;margin-top: 20px">Crear colección</h2>
      </div>        
      <div class="w3-container w3-padding"  >
        <div class="w3-half" style="margin-bottom: 20px;padding-right: 10px">
          <div class="w3-display-container">        
            <img src="<?php echo $model->getPreview($model->Autor,$model->idRecursos,"menu"); ?>" alt="Car" class="w3-border w3-round-xlarge w3-heightimage" style="max-height: 600px;width: 100%"> 
          </div>
        </div>
        <div class="w3-hide-small" style="margin-top: 20px;margin-bottom: 20px"></div>
        <div class="w3-half">
          <h4 style="color:#666666;margin-left: 0px">Nombre</h4>
            <input class="w3-round-xxlarge" style="outline:none;height:35px; border:none;width: 100%;background-color: #EBEDEC;margin-left: 0px;color:#333333;padding-left: 5px;padding-right: 5px;font-size: 15px; " maxlength="50"; type="text" id="idNewColeccion">    
          <br><br>
            <button id="btn_creacoleccion" name="btn_creacoleccion" class="w3-button_green w3-round-xxlarge w3-border-0" style="font-size: 15px;padding-bottom:5px;padding-top:5px;padding-right: 25px;padding-left: 25px" onclick="crearcoleccion()"> Guardar</button>

        </div>
      </div>
    </div>
  </div>     
</div>

<div class="w3-container">
  <div id="id03" class="w3-modal"  >
    <div class="w3-modal-content w3-animate-zoom w3-round-xlarge w3-heightasi" style="width:375px;max-height:775px">      
      <div class="w3-container w3-padding" >
        <div class="w3-half w3-padding" style="margin-top: 30px">
          <div class="w3-display-container" style="margin-bottom: 20px">        
            <img src="<?php echo $model->getPreview($model->Autor,$model->idRecursos,"menu"); ?>" alt="Car" style="width:100%;    max-height: 600px;" class="w3-border w3-round-xlarge w3-heightimage"> 
          </div>
        </div>
        <span style="color: #00CC99" onclick="cierra('id03')" class="w3-button w3-xlarge w3-hover-opacity w3-display-topright" title="Cerrar / Cancelar"><i style="color: black;font-size: 20px">&times;</i></span>
        <div class="w3-hide-small" style="margin-top: 50px;margin-bottom: 20px"></div>                 
        <div class="w3-half margentop">

          <h3 style="color:black;margin-left: 0px">Asignado a</h3>

            <input class="w3-round-xxlarge" style="font-size:15px;height:25px; border:none;width: 100%;background-color: none;margin-left: 0px" disabled type="text" id="idOldColeccion">    

          <br><br>

<!--             <button id="btn_cancelar2" style="font-weight: normal;font-size: 15px" name="btn_cancelar2" class="w3-button_grayligth w3-round-xxlarge w3-border-0" onclick="cierra('id03')">Cancelar</button> -->
            <button id="btn_guardacoleccion" style="font-weight: normal;font-size: 15px;padding-right: 25px;padding-left: 25px" name="btn_guardarencoleccion" class="w3-button_green w3-round-xxlarge w3-border-0" onclick="coleccion(document.getElementById('idOldColeccion').value,'id03','')">Confirmar</button>


       </div>
      </div>
    </div>
  </div>     
</div>

<div class="w3-hide-large w3-hide-medium">
  <div style="margin-top:30px">.</div>
</div>

<div class="w3-content" style="max-width:1400px;margin-top:80px">  
<div class="card">
    <div class="w3-display-container">
    <img src="<?php echo $model->getPreview($model->Autor,$model->idRecursos,"menu"); ?>" alt="Car" class="w3-border w3-round-xlarge resource-img" style="max-height: 600px;"> 
      <div class="w3-display-bottomright w3-padding-large">

  <div class="w3-dropdown-hover w3-center" style="background-color: transparent;">
  <button id="zoom-but4" onclick="myClick(this)" class="w3-button_none w3-round-xxlarge" style="vertical-align: 0px" title="Compartir"><i style="font-size:25px;text-shadow:2px 2px 4px #000000;color:white;background-color: transparent;" class="fa fa-share-alt"></i></button>
        <div id="zoom2" class="w3-dropdown-content w3-bar-block w3-card-4 w3-animate-zoom w3-round-xxlarge " style="height: 50px; margin-bottom: 10px;left:55px;z-index: 1;top: 0; position: absolute; ">
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px; margin-top: 10px;background-color:#00CC99" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link ?>" target="_blank"><i class="fa fa-facebook-f" style="color:white;font-size: 25px"></i></a>
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://api.whatsapp.com/send?text=<?php echo "Un recurso interesante -> " . $actual_link ?>" target="_blank"><i class="fa fa-whatsapp" style="color:white;font-size: 25px"></i></a>
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://twitter.com/intent/tweet?text= Un recurso interesante -> &url=<?php echo $actual_link ?>" target="_blank"><i class="fa fa-twitter" style="color:white;font-size: 25px"></i></a>          
        </div>  
  </div>

    
          <?php
            // if (Yii::$app->user->can('/recursos/favoritos/agregar-recurso-mochila')) {
              $tooltip = "Este recurso te gusta";
              $agregarText = '<span class="fa fa-heart" style="font-size:25px;color:#D4145A;text-shadow:2px 2px 4px #000000;" tooltip="Este recurso te gusta"></span> ' . Yii::t('app', "");
              if (Favoritos::resourceIsInUserBag($model->idRecursos, Yii::$app->user->identity->id)) {
                echo Html::a($agregarText, [
                                '/recursos/favoritos/eliminar-de-mi-mochila',
                                'rIdRecursos' => $model->idRecursos,
                                'rIdUsuarios' => Yii::$app->user->identity->id,
                                'recurso' => $model->Slug,
                                    ], [
                                'data' => [
                                    'method' => 'post',
                                    'params' =>
                                    [
                                        'rIdRecursos' => $model->idRecursos,
                                        'rIdUsuarios' => Yii::$app->user->identity->id,
                                        'recurso' => $model->Slug,
                                    ],
                                ],
                                "class" => 'fucsia-link sm-link',
                                "title" => $tooltip,
                            ]);
              } else {
                $agregarText = '<span class="fa fa-heart-o w3-text-white"  style="font-size:25px;text-shadow:2px 2px 4px #000000;" tooltip="Aún no te gusta este recurso"></span> ' . Yii::t('app', "");
                $tooltip = "Aún no te gusta este recurso";
                echo Html::a($agregarText, ['/recursos/favoritos/agregar-recurso-mochila'], [
                                'data' => [
                                    'method' => 'post',
                                    'params' =>
                                    [
                                        'recursoId' => $model->idRecursos,
                                        'recurso' => $model->Slug,
                                    ],
                                ],
                                "class" => 'fucsia-link sm-link',
                                "title" => $tooltip,
                            ]);
              }
            // }
          ?> 
       
      </div>    
  </div>
    <div class="imagefooter-list">
<div class="w3-white w3-hide-small" style="margin-top:10px">

          <?php ?>

          <a id="link-denunciar-recurso" class="fucsia-link sm-link" onclick="mostrarDenunciar();" title="Denunciar" style="margin-right: 10px;margin-top:10px">Denunciar</a>


          <?php if (Yii::$app->user->can("/recursos/recursos/actualizar-recurso", ["recurso" => $model->Slug])): ?>
            <a href="<?php echo Url::to(["/recursos/recursos/actualizar-recurso", "recurso" => $model->Slug]) ?>" class="fucsia-link sm-link" style="margin-right: 10px;margin-top:10px">
              <?php echo Yii::t("app", "Editar"); ?>
            </a> 
          <?php endif; ?>
          <?php if (Yii::$app->user->can("/recursos/recursos/delete", ["recurso" => $model->Slug])): ?>
          <?php
            $eliminarText = '' . Yii::t('app', "Eliminar");
            echo Html::a($eliminarText, ['/recursos/recursos/delete', "recurso" => $model->Slug], [
                            'data' => [
                                'method' => 'post',
                                "confirm" => Yii::t("app", "¿Está seguro que desea eliminar el recurso?"),
                                'params' =>
                                [
                                    'recurso' => $model->Slug,
                                ],
                            ],
                            "class" => 'fucsia-link sm-link',
                            "style" => "margin-right: 10px;margin-top:10px",
                        ]);
          ?>
          <?php endif; ?> 
<!-- /// -->


  <?php if (Yii::$app->user->can(Yii::$app->params["verificador_default_role"])): ?>
    <a href="<?php echo Url::toRoute(["/recursos/recursos/aprobar-recurso", "recurso" => $model->Slug]) ?>" class="fucsia-link sm-link" style="margin-top: 10px">
      <?php echo Yii::t("app", "Revisar "); ?>
      
    </a>
  <?php endif; ?> 


<!-- ///     -->      


        <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->id != $model->Autor) && (Yii::$app->user->can('/recursos/denuncias/agregar-denuncia'))): ?>
          <?php if (!Denuncias::IsUserComplaintThisResource($model->idRecursos, Yii::$app->user->identity->id)): ?>
            <div id="modalDenuncia" class="row denuncias-content" style="display:none;">
              <div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
              <br>
                <?php
                  $form = ActiveForm::begin(
                                ['action' => ["/recursos/denuncias/agregar-denuncia"] ]);
                ?>
                <input type="hidden" name="recurso" value="<?php echo $model->Slug; ?>">
                <?php echo $form->field($modelDenuncia, 'rIdRecursos')->hiddenInput(["value" => $model->idRecursos])->label(false); ?>
                <?php echo $form->field($modelDenuncia, 'rIdUsuarios')->hiddenInput(["value" => Yii::$app->user->identity->id])->label(false); ?>
                <?php
                  echo $form->field($modelDenuncia, 'Motivo')->textarea(
                                        [
                                            "rows" => 6,
                                            "class" => "form-control round-form-control text-area-row"
                                ])->label("Indique el motivo de su denuncia*");
                ?>
                <?php
                  echo Html::submitButton(Yii::t("app", "Enviar denuncia"), ['class' => 'btn btn-default btn-md', "data-confirm" => Yii::t("app", "Seguro que quiere proceder con la denuncia del recurso")]);
                  ActiveForm::end();
                ?>
              </div>
            </div>
          <?php endif; ?>
        <?php endif; ?>
          


</div>    
<br>
<div class="row w3-hide-small" style="padding-left: 10px;letter-spacing: 1px !important;">
      <?php if ($model->tagsValores): ?>  
      <p>
      <?php
        $RelatedTags = $model->printTags("#", ["class" => "gris-link", "style" => "margin-right: 10px;font-family: 'Sans Pro Regular';font-size:15px"],"button");
      ?>
      </p>
      <?php endif; ?>       
</div>
    </div>
    <div class="resource-header">

                  <?php 
                    $coleccionesrecurso = $model->getColecciones($model->idRecursos,Yii::$app->user->identity->id);

                    $colarr = "";
                    $colnam = "Seleccionar colección";
                    foreach ($coleccionesrecurso as $coleccionrecurso) {

                      if ($colarr==""){
                        $colval = $coleccionrecurso['idColeccion']; 
                        $colnam = $coleccionrecurso['Nombre'] ;
                      }
                      else{
                        $colarr = $colarr + "," + $coleccionrecurso['idColeccion'];  
                      }
                      
                    }
                    $modelColeccion = new Colecciones();                            
                    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); 
                    $url = \yii\helpers\Url::to(['collection-list']);            
                    $ruta = Yii::$app->getUrlManager()->createUrl("/recursos/recursos/recurso-coleccion");
                    $idRecursos = $model->idRecursos;

                    echo "<input type='' hidden='' id='idRecursos' value='".$idRecursos."'>";
                    echo "<input type='' hidden='' id='rutarecursocoleccion' value='".$ruta."'>";
                    echo $form->field($modelColeccion, 'idColeccion')->widget(Select2::classname(), [
                      'changeOnReset' => false,                          
                      'options' => [
                        'multiple'=>false, 
                        'id' => "selectcol",
                        'value' => $colarr,
                        'selection__arrow' => 'background-color:#45B390',
                        'initValueText' => $colnam,
                        'placeholder' => $colnam, //"Seleccionar Colección",                        
                        'class' => 'w3-input;  w3-round-xlarge; select2-selection2',
                        'style' => 'border:none;background-color:#EBEDEC ',  
                      ],                   
                      'pluginOptions' => [
                        'allowClear' => true,
                        'style' => 'border:none;background-color:#EBEDEC ',
                        'ajax' => [
                          'url' => $url,
                          'dataType' => 'json',
                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],                      
                      ],
                      'pluginEvents' => [
                         "change" => "function() { // function to make ajax call here 
                          if(this.value==''){
                            return;
                          }
                          if(this.value==0){                            
                            document.getElementById('id02').style.display='block';  
                          }
                          else{
                            if(dispara==1){
                              document.getElementById('id03').style.display='block';
                              texto = this.options[this.selectedIndex].text;
                              if(texto.length>22){
                                texto = texto.substring(0, 22) + '...';
                              }
                              document.getElementById('idOldColeccion').value = texto;                                
                            }
                          }
                          
                         }",
                      ],                              
                    ])->label(false);       
                    $text = '<span class="glyphicon glyphicon-heart-empty"></span> ' . Yii::t('app', "Guardar");
                    $textCC = '<span class="glyphicon glyphicon-heart-empty"></span> ' . Yii::t('app', "Guardar");
                    ActiveForm::end(); 
                  ?>      
      <div class="w3-hide-medium w3-hide-large">
        <h2  style="color:#000009;font-weight:bold;margin-top: 20px"><?php echo $model->Nombre; ?></h2>
      </div>                   
<div class="w3-hide-small">  
<?php if($model->isProfuturo($model->idRecursos)){  ?>
<div class="w3-row">
<div class="w3-half">
    <h2  style="color:#000009;font-weight:bold;margin-top: 20px"><?php echo $model->Nombre; ?></h2>  
</div>                     
<div class="w3-half w3-right-align">
  <?php
    $imagen = Yii::$app->params["urlBack"] . "/uploads/etiqueta-profuturo666666.png";
  ?>
  <img style="width:50%;font-size:12px;margin-bottom:5px;margin-top: 20px" src="<?php echo $imagen ?>">
</div>  
</div>
<?php } else{?>
  <h2  style="color:#000009;font-weight:bold;margin-top: 20px"><?php echo $model->Nombre; ?></h2> 
<?php } ?>

  


            <div class="stars-vote-form" style="margin-bottom: 15px">
              <?php
                $formVotacion = ActiveForm::begin(['action' => ["/recursos/votaciones/votar-recurso"],
                                                   'options' => [
                                                    "role" => "form",
                                                    'class' => "form-vertical",
                                                    'id' => "form-votaciones2",
                                                   ]
                                ]);
              ?>
              <input type="hidden" name="recurso" value="<?php echo $model->Slug; ?>">
              <?php echo $formVotacion->field($modelVotacion, 'rIdRecursos')->hiddenInput(["value" => $model->idRecursos])->label(false); ?>
              <?php echo $formVotacion->field($modelVotacion, 'rIdUsuarios')->hiddenInput(["value" => Yii::$app->user->identity->id])->label(false); ?>
              <?php
                echo $formVotacion->field($modelVotacion, 'Votacion')->widget(StarRating::classname(), [
                                'pluginOptions' => [
                                    'size' => 'sm',
                                    'step' => 1,
                                    'filledStar' => '<i class="fa fa-star"></i>',
                                    'emptyStar' => '<i class="fa fa-star-o"></i>',                                    
                                ],
                                'options' =>[
                                  'id' => "field-votaciones2",  
                                ],
                                'pluginEvents' => [
                                    "change" => "function() { 
                                        jQuery('#form-votaciones2').submit(); }",
                                ]
                            ])->label(false);
              ?> 
              <?php
                ActiveForm::end();
              ?> 
            </div>
         


<!--       <p class="w3-xxlarge"><i class="fa fa-user-circle"></i></p> -->
      <h3 style="color:#333333;font-weight:normal">Autor: <?php if($model->autorRecurso<>""){echo $model->autorRecurso;}else{ echo "No Indicado"; };  ?></h3>
     
 
      <h3 style="color:#333333;font-weight:normal"><?php 
  
      $model->printPublicacion(); 

      ?></h3>
      <h3 style="color:#333333;font-weight:normal">Año: <?php if($model->anioRecurso<>""){echo $model->anioRecurso;}else{ echo "No Indicado"; };  ?></h3>

      <h3 style="color:#333333;font-weight:normal;margin-bottom: 15px">Tipo: <?php echo $TipoRec; ?></h3>  


        <h3 style="font-weight:normal;margin-bottom: 15px;color:#000009"><?php echo HtmlPurifier::process($model->ObjetivoDelRecurso); ?></h3>

      <h2 style="color:#000009;font-weight:bold;margin-bottom: 20px">Recursos disponibles</h2>
      <div class="row" style="padding-left: 10px">

            <?php
            $files = $model->getArchivos()->All();

            foreach ($files as $file) {

              $idArchivo = $file->idArchivo;  
              $Nombre = $file->Nombre;      
              if(strpos($Nombre, "preview") !== false){
                continue;
              }      
              $toroute = Url::toRoute(['/recursos/recursos/descargar-recurso', 'recurso' => $model->Slug,'idArchivo' => $file->idArchivo,'idRecursos' => $model->idRecursos,'Nombre' => $file->Nombre, 'file' => $model->Recurso]);
              ?>
            <?php if (Yii::$app->user->can('/recursos/recursos/descargar-recurso')): ?>
              <?php
                // $toroute = Url::toRoute(['/recursos/recursos/descargar-recurso', 'recurso' => $model->Slug]);
                $aria_label = Yii::t('app', 'Eliminar');
                if(strpos($file->Tipo,"mage")>0){
                  $tipofile = " Imagen";
                  $icon = "fa fa-file-image-o";
                }
                else{
                  if(strpos($file->Tipo,"pdf")>0){
                    $tipofile = " PDF";
                    $icon = "fa fa-file-pdf-o";
                  }   
                  else{
                    if(strpos($file->Tipo,"xls")>0){
                      $tipofile = " Excel";
                      $icon = "fa fa-file-excel-o";
                    }  
                    else{
                      if(strpos($file->Tipo,"doc")>0){
                        $tipofile = " Documento";
                        $icon = "fa fa-file-word-o";
                      }    
                      else{
                        if(strpos($file->Tipo,"xml")>0){
                          $tipofile = " XML";
                          $icon = "fa fa-file-code-o";
                        }                 
                        else{
                          if(strpos($file->Tipo,"zip")>0 || strpos($file->Tipo,"rar")>0){
                            $tipofile = " Archivo Zip";
                            $icon = "fa fa-file-zip-o";
                          }   
                          else{
                            if(strpos($file->Tipo,"video")>0){
                              $tipofile = " Video";
                              $icon = "fa fa-file-video-o";
                            }                 
                            else{
                              $tipofile = $file->Tipo;
                              $icon = "fa fa-file-text-o";
                            }
                          }                         
                        }           
                      }                      
                    }                    
                  }                  
                }
      
                echo Html::a(Yii::t("app", "<i class='" . $icon ."' ></i> " .$tipofile   ), $toroute, [
                                  'data-method' => "post",
                                  "data-pjax" => "0",
                                  "aria-label" => Yii::t("app", "Descargar " . $tipofile),
                                  "title" => Yii::t("app", "Descargar " . $tipofile . ' ' . $Nombre),
                                  "style" => "margin-right: 10px;font-size:15px;margin-bottom:5px; text-decoration: none",
                                  "class" => 'boton2 exito2 w3-round-xxlarge w3-padding'
                ]);

              ?> 
            <?php endif; ?>  
            <?php }?> 
            <!-- foreach -->

            <?php
              if ($model->Recurso != "https://www.archivosbancorecursos.com" && filter_var($model->Recurso, FILTER_VALIDATE_URL)) {
                echo Html::a(Yii::t("app", "<i class='fa fa-external-link'></i> Sitio web"), $model->Recurso, ["class" => " boton2 exito2 w3-round-xxlarge w3-padding", "style" => "margin-right: 10px; font-size:15px; text-decoration: none ", "target" => "_blank","title" => $model->Recurso]);
              }
            ?>  

<!--             <?php if ($model->Tipo == $model::TYPE_EMBED): ?> -->
<!--               <div class="responsive-video"> -->
<!--                 <?php echo $model->Recurso; ?> -->
<!--               </div> -->
<!--             <?php endif; ?>  -->


           
          
      </div>

    </div>
    </div>
    <div class="resource-body w3-hide-medium w3-hide-large">
      <div class="w3-hide-small">
        <h2  style="color:#000009;font-weight:bold"><?php echo $model->Nombre; ?></h2>
      </div>

          <?php if (Yii::$app->user->can('/recursos/votaciones/votar-recurso')): ?>
            <div class="stars-vote-form" style="margin-bottom: 15px">
              <?php
                $formVotacion = ActiveForm::begin(['action' => ["/recursos/votaciones/votar-recurso"],
                                                   'options' => [
                                                    "role" => "form",
                                                    'class' => "form-vertical",
                                                    'id' => "form-votaciones",
                                                   ]
                                ]);
              ?>
              <input type="hidden" name="recurso" value="<?php echo $model->Slug; ?>">
              <?php echo $formVotacion->field($modelVotacion, 'rIdRecursos')->hiddenInput(["value" => $model->idRecursos])->label(false); ?>
              <?php echo $formVotacion->field($modelVotacion, 'rIdUsuarios')->hiddenInput(["value" => Yii::$app->user->identity->id])->label(false); ?>
              <?php
                echo $formVotacion->field($modelVotacion, 'Votacion')->widget(StarRating::classname(), [
                                'pluginOptions' => [
                                    'size' => 'sm',
                                    'step' => 1,
                                    'filledStar' => '<i class="fa fa-star"></i>',
                                    'emptyStar' => '<i class="fa fa-star-o"></i>',                                    
                                ],
                                'pluginEvents' => [
                                    "change" => "function() { 
                                        jQuery('#form-votaciones').submit(); }",
                                ]
                            ])->label(false);
              ?> 
              <?php
                ActiveForm::end();
              ?> 
            </div>
          <?php endif; ?>          


<!--       <p class="w3-xxlarge"><i class="fa fa-user-circle"></i></p> -->
      <h3 style="color:#333333;font-weight:normal">Autor: <?php if($model->autorRecurso<>""){echo $model->autorRecurso;}else{ echo "No Indicado"; };  ?></h3>
     

      <h3 style="color:#333333;font-weight:normal"><?php $model->printPublicacion(); ?></h3>
      <h3 style="color:#333333;font-weight:normal">Año: <?php if($model->anioRecurso<>""){echo $model->anioRecurso;}else{ echo "No Indicado"; };  ?></h3>

      <h3 style="color:#333333;font-weight:normal;margin-bottom: 15px">Tipo: <?php echo $TipoRec; ?></h3>
  </div>

    </div>



     
    <div class="resource-info w3-hide-large w3-hide-medium">

        <h3 style="font-weight:normal;margin-bottom: 15px;color:#000009"><?php echo HtmlPurifier::process($model->ObjetivoDelRecurso); ?></h3>

      <h2 style="color:#000009;font-weight:bold;margin-bottom: 20px">Recursos disponibles</h2>
      <div class="row" style="padding-left: 10px">

            <?php
           
            $files = $model->getArchivos()->All();

            foreach ($files as $file) {

              $idArchivo = $file->idArchivo;  
              $Nombre = $file->Nombre;            
              $toroute = Url::toRoute(['/recursos/recursos/descargar-recurso', 'recurso' => $model->Slug,'idArchivo' => $file->idArchivo,'idRecursos' => $model->idRecursos,'Nombre' => $file->Nombre]);
              ?>
            <?php if (Yii::$app->user->can('/recursos/recursos/descargar-recurso')): ?>
              <?php
                // $toroute = Url::toRoute(['/recursos/recursos/descargar-recurso', 'recurso' => $model->Slug]);
                $aria_label = Yii::t('app', 'Eliminar');
                if(strpos($file->Tipo,"mage")>0){
                  $tipofile = " Imagen";
                  $icon = "fa fa-file-image-o";
                }
                else{
                  if(strpos($file->Tipo,"pdf")>0){
                    $tipofile = " PDF";
                    $icon = "fa fa-file-pdf-o";
                  }   
                  else{
                    if(strpos($file->Tipo,"xls")>0){
                      $tipofile = " Excel";
                      $icon = "fa fa-file-excel-o";
                    }  
                    else{
                      if(strpos($file->Tipo,"doc")>0){
                        $tipofile = " Documento";
                        $icon = "fa fa-file-word-o";
                      }    
                      else{
                        if(strpos($file->Tipo,"xml")>0){
                          $tipofile = " XML";
                          $icon = "fa fa-file-code-o";
                        }                 
                        else{
                          if(strpos($file->Tipo,"zip")>0 || strpos($file->Tipo,"rar")>0){
                            $tipofile = " Archivo Zip";
                            $icon = "fa fa-file-zip-o";
                          }   
                          else{
                            if(strpos($file->Tipo,"video")>0){
                              $tipofile = " Video";
                              $icon = "fa fa-file-video-o";
                            }                 
                            else{
                              $tipofile = $file->Tipo;
                              $icon = "fa fa-file-text-o";
                            }
                          }                         
                        }           
                      }                      
                    }                    
                  }                  
                }
      
                echo Html::a(Yii::t("app", "<i class='" . $icon ."' ></i> " .$tipofile   ), $toroute, [
                                  'data-method' => "post",
                                  "data-pjax" => "0",
                                  "aria-label" => Yii::t("app", "Descargar " . $tipofile),
                                  "title" => Yii::t("app", "Descargar " . $tipofile),
                                  "style" => "margin-right: 10px;font-size:15px;margin-bottom:5px; text-decoration: none",
                                  "class" => 'boton2 exito2 w3-round-xxlarge w3-padding'
                ]);

              ?> 
            <?php endif; ?>  
            <?php }?> 
            <!-- foreach -->

            <?php
              if ($model->Tipo == $model::TYPE_URL && $model->Recurso != "https://www.archivosbancorecursos.com") {
                echo Html::a(Yii::t("app", "<i class='fa fa-external-link'></i> Sitio web"), $model->Recurso, ["class" => " boton2 exito2 w3-round-xxlarge w3-padding", "style" => "margin-right: 10px; font-size:15px; text-decoration: none ", "target" => "_blank"]);
              }
            ?>  

<!--             <?php if ($model->Tipo == $model::TYPE_EMBED): ?> -->
<!--               <div class="responsive-video"> -->
<!--                 <?php echo $model->Recurso; ?> -->
<!--               </div> -->
<!--             <?php endif; ?>  -->


           
          
      </div>
    </div>
</div>


  <div class="w3-hide-large w3-hide-medium w3-container" style="margin-top:5px"><p style="color:white">X</p></div>
<div class="">


  <div style="margin-left: 15px" class="w3-round-xxlarge w3-hide-small" onclick="myAccFunc()">

  <h2 class="xhover" style="cursor: pointer; font-weight: bold !important;">Comentarios y valoraciones <i class="fa fa-caret-down"></i></h2>     

   
  </div>
  <div style="margin-left: 15px" class="w3-round-xxlarge w3-hide-large w3-hide-medium" onclick="myAccFunc()">

  <h2 class="xhover" style="cursor: pointer; font-weight: bold !important;">Comentarios y valoraciones <i class="fa fa-caret-down"></i></h2>     

   
  </div>  
  <div style="margin-left: 15px" id="demoAcc" class="w3-hide">

  <div class="w3-half" style="padding-right: 15px; font-size: 15px;">

    <?php
      echo Comment::widget([
            'model' => $model,
            'relatedTo' => 'User ' . \Yii::$app->user->identity->username . ' commented on the page ' . \yii\helpers\Url::current(),
            'maxLevel' => 1,
            // set `pageSize` with custom sorting
            'dataProviderConfig' => [
                'sort' => [
                    'attributes' => ['id'],
                    'defaultOrder' => ['id' => SORT_DESC],
                ],
                'pagination' => [
                    'pageSize' => 30
                ],
            ],
            'entityIdAttribute' => 'idRecursos',
            // your own config for comments ListView, for example:
            'listViewConfig' => [
                'emptyText' => Yii::t('app', 'El recurso aún no tiene comentarios.'),
            ]
        ]);
      ?>     
   </div>
   <div class="w3-half">
    <div class="w3-container">
    
    <?php 
      $npor5green = $model->getPercent(5); 
      $npor5gray = 100 - $npor5green; 
      $numper5 = $model->getStars(5);
      $totstar5 = $numper5 * 5;
      $npor4green = $model->getPercent(4); 
      $npor4gray = 100 - $npor4green;
      $numper4 = $model->getStars(4);
      $totstar4 = $numper4 * 4;
      $npor3green = $model->getPercent(3); 
      $npor3gray = 100 - $npor3green;
      $numper3 = $model->getStars(3);
      $totstar3 = $numper3 * 3;
      $npor2green = $model->getPercent(2); 
      $npor2gray = 100 - $npor2green;
      $numper2 = $model->getStars(2);
      $totstar2 = $numper2 * 2;
      $npor1green = $model->getPercent(1); 
      $npor1gray = 100 - $npor1green;
      $numper1 = $model->getStars(1);      
      $totstar1 = $numper1 * 1;
      $totper = $numper1 + $numper2 + $numper3 + $numper4 + $numper5;
      $totstar = $totstar1 + $totstar2 + $totstar3 + $totstar4 + $totstar5;
      if($totper<=0){
        $prostar = 0;
      }
      else{
        $prostar = number_format(($totstar / $totper));  
      }
      
    ?>
    <div class="w3-row">
      <div class="w3-quarter">
      <h2 style="color:#00CC99"><?php if($prostar>0){ ?><i class="fa fa-star"></i> <?php } else{ ?><i class="fa fa-star-o"></i> <?php }?> <?php if($prostar>1){ ?><i class="fa fa-star"></i> <?php } else{ ?><i class="fa fa-star-o"></i> <?php }?> <?php if($prostar>2){ ?><i class="fa fa-star"></i> <?php } else{ ?><i class="fa fa-star-o"></i> <?php }?> <?php if($prostar>3){ ?><i class="fa fa-star"></i> <?php } else{ ?><i class="fa fa-star-o"></i> <?php }?> <?php if($prostar>4){ ?><i class="fa fa-star"></i> <?php } else{ ?><i class="fa fa-star-o"></i> <?php }?> </h2>
      </div>
      <div class="w3-three-quarter">
      <h3>promedio en <?php echo $totper;?> valoraciones</h3>  
      </div>
    </div>
    <div class="w3-row" style="color:white">.</div>
    <div class="w3-row">
      <div class="w3-quarter">
        <h2 style="color:#00CC99"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></h2>      
      </div>
      <div class="w3-half">

        <div class="w3-col w3-green-fod" style="height:24px;width:<?php echo $npor5green;?>%">
        </div>
        <div class="w3-light-gray w3-col" style="height:24px;width:<?php echo $npor5gray;?>%"></div>
      </div>
      <div class="w3-quarter">
        <div class="w3-quarter">
        <text style="color:black;font-size:14px;margin-left: 5px">  <?php echo $npor5green;?>% 
        </text>
        </div>
        <div class="w3-three-quarter">
          <text style="color:black;font-size:14px;margin-left: 10px">  <?php echo $numper5; if($numper5==1){echo " Persona";}else{ echo " Personas"; }?> 
          </text>            
        </div>
      </div>    
    </div>


    <div class="w3-row">
      <div class="w3-quarter">
        <h2 style="color:#00CC99"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></h2>      
      </div>
      <div class="w3-half">      
        <div class="w3-col w3-green-fod" style="height:24px;width:<?php echo $npor4green;?>%">
        </div>
        <div class="w3-light-gray w3-col" style="height:24px;width:<?php echo $npor4gray;?>%"></div>
      </div>
      <div class="w3-quarter">
        <div class="w3-quarter">
        <text style="color:black;font-size:14px;margin-left: 5px">  <?php echo $npor4green;?>% 
        </text>
        </div>
        <div class="w3-three-quarter">
          <text style="color:black;font-size:14px;margin-left: 10px">  <?php echo $numper4; if($numper4==1){echo " Persona";}else{ echo " Personas"; }?> 
          </text>            
        </div>
      </div>    
    </div>

    <div class="w3-row">
      <div class="w3-quarter">
        <h2 style="color:#00CC99"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
        </h2>        
      </div>
      <div class="w3-half">      
        <div class="w3-col w3-green-fod" style="height:24px;width:<?php echo $npor3green;?>%">
        </div>
        <div class="w3-light-gray w3-col" style="height:24px;width:<?php echo $npor3gray;?>%"></div>
      </div>
      <div class="w3-quarter">
        <div class="w3-quarter">
        <text style="color:black;font-size:14px;margin-left: 5px">  <?php echo $npor3green;?>% 
        </text>
        </div>
        <div class="w3-three-quarter">
          <text style="color:black;font-size:14px;margin-left: 10px">  <?php echo $numper3; if($numper3==1){echo " Persona";}else{ echo " Personas"; }?> 
          </text>            
        </div>
      </div>    
    </div>

    <div class="w3-row">
      <div class="w3-quarter">
        <h2 style="color:#00CC99"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></h2>       
      </div>
      <div class="w3-half">
        <div class="w3-col w3-green-fod" style="height:24px;width:<?php echo $npor2green;?>%">
        </div>
        <div class="w3-light-gray w3-col" style="height:24px;width:<?php echo $npor2gray;?>%"></div>
      </div>
      <div class="w3-quarter">
        <div class="w3-quarter">
        <text style="color:black;font-size:14px;margin-left: 5px">  <?php echo $npor2green;?>% 
        </text>
        </div>
        <div class="w3-three-quarter">
          <text style="color:black;font-size:14px;margin-left: 10px">  <?php echo $numper2; if($numper2==1){echo " Persona";}else{ echo " Personas"; }?> 
          </text>            
        </div>
      </div>    
    </div> 

    <div class="w3-row">
      <div class="w3-quarter">
        <h2 style="color:#00CC99"><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></h2>  
      </div>
      <div class="w3-half">      
        <div class="w3-col w3-green-fod" style="height:24px;width:<?php echo $npor1green;?>%">
        </div>
        <div class="w3-light-gray w3-col" style="height:24px;width:<?php echo $npor1gray;?>%"></div>
      </div>
      <div class="w3-quarter">
        <div class="w3-quarter">
        <text style="color:black;font-size:14px;margin-left: 5px">  <?php echo $npor1green;?>% 
        </text>
        </div>
        <div class="w3-three-quarter">
          <text style="color:black;font-size:14px;margin-left: 10px">  <?php echo $numper1; if($numper1==1){echo " Persona";}else{ echo " Personas"; }?> 
          </text>            
        </div>
      </div>    
    </div>

    </div>
   </div>

</div>
<div class="w3-row" style="margin-top: 20px; margin-bottom: 20px">
  <div class="w3-half">
  <h2 style="margin-left: 15px;font-weight: bold">Otros recursos</h2>        
  </div>  

                
</div>
</div>

<div class="w3-contentnomargins w3-marginleft" style="padding-block-end: 150px;">
      <?php
        
        echo RecursosRelacionados::widget(["recurso" => $model]);
         
      ?> 
</div>
<?php }
?>
<script type="text/javascript">
var dispara = 1;
function cierra(param){
  $('#selectcol').val('').trigger('change');
  document.getElementById(param).style.display='none'    
}
function myAccFunc() {
  var x = document.getElementById("demoAcc");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}    
  $( document ).ready(function() {

  });
  function crearcoleccion(){
    var Nombre  = document.getElementById("idNewColeccion").value;  
    var Autor = <?php echo Yii::$app->user->identity->id ?>;    

    rutacol = "/recursos/recursos/crear-coleccion";
    $.ajax({        
      url: rutacol,
      type: "POST",
      data:{
        Nombre : Nombre,
        Autor : Autor
      },
      success: function (data) {
        coleccion(data,"id02",Nombre);
      }
    });      
  }

  function coleccion(param,dialog,nombre){
    var idCol = document.getElementById("selectcol").value;
    var idRec = document.getElementById("idRecursos").value;
    var ruta  = document.getElementById("rutarecursocoleccion").value;    

    // var ruta = ruta + "?idColeccion="+idCol+"&idRecursos="+idRec;
    if(idCol==0||idCol==""){
        idCol = param;
    }
    if(idCol==0||idCol==""){
      alert("Debe seleccionar o crear una coleccion para agregar el recurso")  ;
      return;
    }
    $.ajax({
        url: ruta,
        type: "POST",
        data:{
          idColeccion : idCol,
          idRecursos : idRec
        },
        success: function (data) {
          if(dialog=="id02"){
            var newOption = new Option(nombre, idCol, false, false);
            dispara = 0;
            $('#selectcol').append(newOption).trigger('change');    
            $('#selectcol').val(idCol).trigger('change');        
          }
          else{
            dispara = 1;
            $('#selectcol').val(idCol).trigger('change');                
          }
          
          alert(data);
          document.getElementById(dialog).style.display='none' ;
        }
    });     
            
  }
</script>


<?php

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;

global $configuraciones_request;
$configuraciones_request = $configuraciones;

// get the module to which the currently requested controller belongs
global $module;
$module = Yii::$app->getModule('recursos');
$this->title = Yii::t("app", "Recursos más recientes");
?>
<div class="main-box large-box">
    <h1><?php echo $this->title; ?><span class="icon icon-fod-icon-cloud"></span></h1>
    <div class="link-group pull-right">
        <?php
        $text = Yii::t("app", "Búsqueda Avanzada") . ' <span class="icon icon-fod-icon-binocular"></span>';
        $options = [
            "class" => "fucsia-link"
        ];
        echo
        Html::a($text, Url::to(["/recursos/recursos/resultados-de-busqueda"]), $options);
        ?> 
    </div>

    <?php
    $emptyText = Yii::t("app", "No se encontraron resultados");
    $summaryText = Yii::t("app", "Mostrando") . " <b>{begin}-{end}</b> " . Yii::t("app", "de") . " <b>{totalCount}</b> " . Yii::t("app", "recursos");
    $summary_layout = "<div class='col-xs-12 top-space'>{summary}</div>";
    $items_layout = "{items}";

    $pager_layout = "<div class='row'>"
            . "<div class='col-md-12 text-center'>{pager}</div>"
            . "</div>";

    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => $emptyText,
        'emptyTextOptions' => [
            'tag' => 'div',
            'class' => 'alert alert-warning',
            'role' => "alert"
        ],
        'options' => [
            'tag' => 'div',
            'id' => 'recursos-tags',
            'class' => 'row',
        ],
        'itemOptions' => [
            'tag' => 'div',
            'id' => '',
            'class' => "col-xs-12 top-space",
        ],
        'summary' => $summaryText,
//        'summary' => '',
        'summaryOptions' => [
            'tag' => 'div',
            'id' => '',
            'class' => '',
        ],
        'layout' => "$summary_layout\n$items_layout\n$pager_layout",
        'itemView' => function ($model, $key, $index, $widget) {
    global $configuraciones_request;
    global $module;
    return $this->render('content/recurso', [
                'model' => $model,
                'index' => $index,
                'configuraciones' => $configuraciones_request,
                'module' => $module->params,
    ]);
},
        'pager' => [
            'firstPageLabel' => Yii::t("app", "Primero"),
            'lastPageLabel' => Yii::t("app", "Último"),
        ]
    ]);
    ?>
</div>
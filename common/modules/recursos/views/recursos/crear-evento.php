<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;
use common\modules\reportes\models\Eventos;


/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarEvento($Slug, $Autor, $evento, $redsocial) {

        $model = Recursos::find()->andFilterWhere(['Slug' => $Slug])->one();
        // if($model->Autor != Yii::$app->user->identity->id){
          Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::SHARE_RECURSO, "Recurso fue compartido en " . $redsocial, $model->idRecursos, $Slug, $Autor);
          return true;
        // }

        return "";

    }

    public function actionCrearEvento() {
      $Slug = $_POST["Slug"];
      $Autor = $_POST["Autor"];            
      $evento = $_POST["evento"];  
      $redsocial = $_POST["redsocial"];  
      $this->agregarEvento($Slug,$Autor,$evento,$redsocial);     
    }
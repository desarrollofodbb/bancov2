<?php

use yii\widgets\ActiveForm;
use common\widgets\Alert;
use common\modules\recursos\models\CriteriosDeValidacion;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
$this->title = Yii::t("app", "Aprobar recurso");
?>

<div class="container aprobar-recursos">
    <div class="main-box large-box">
        <h1><?php echo Html::encode($this->title) ?><span class="icon icon-fod-icon-pencil"></span></h1>
        <?php if (Yii::$app->user->can(Yii::$app->params["verificador_default_role"])): ?> 
            <br>
            <?php
            echo \yii\bootstrap\Alert::widget([
                'body' => '<strong>Info: </strong> ' . Yii::$app->params["approvalInfoMessage"],
                'options' => ["class" => "alert-info"],
            ]);
            ?>
            <?php echo Alert::widget() ?>
            <?php $form = ActiveForm::begin(); ?>
            <div class="row banco-ckeckboxlist">
                <div class="col-md-12 col-xs-12">
                    <?php
                    echo $form->field($model, "criterios_validacion_calidad_ids")
                            ->checkboxList(CriteriosDeValidacion::getCriteriosCalidad());
                    ?> 
                </div>
            </div>
            <div class="row banco-ckeckboxlist">
                <div class="col-md-12  col-xs-12">
                    <?php
                    echo $form->field($model, "criterios_validacion_educacion_ids")
                            ->checkboxList(CriteriosDeValidacion::getCriteriosEducacion());
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12  col-xs-12">          	
                    <?php echo Html::submitButton($module->params["a_send"], ['class' => 'btn btn-default btn-lg', "name" => "btn-aprobar"]); ?>
                </div>
                <!--                <div class="form-group">
                <?php
//                    $data_confirm = Yii::t('app', '¿Seguro que desea rechazar el recurso?');
//                    echo Html::submitButton($module->params["a_reject"], ["data-confirm" => $data_confirm, 'class' => 'btn btn-primary', 'name' => "btn-rechazar"]);
                ?>
                                </div>-->

            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>
<?php

use yii\widgets\ListView;
use yii\helpers\Html;
use common\widgets\Alert;

global $configuraciones_request;
$configuraciones_request = $configuraciones;

// get the module to which the currently requested controller belongs
global $module;
$module = Yii::$app->getModule('colecciones');
?>
<div class="main-box large-box">
    <h1><?php
        if ($queryParams) {
            echo Html::encode(Yii::t("app", "Resultados de búsqueda"));
        } else {
            echo Html::encode(Yii::t("app", "Mostrando todos las colecciones"));
        }
        ?><span class="icon icon-fod-icon-cloud"></span></h1>
    <br>
    <?php echo Alert::widget() ?>

    <?php
    echo $this->render("busqueda-avanzada", ["model" => $searchModel]);
    ?>

    <?php
    $emptyText = Yii::t("app", "No se encontraron resultados");
    $summaryText = Yii::t("app", "Mostrando") . " <b>{begin}-{end}</b> " . Yii::t("app", "de") . " <b>{totalCount}</b> " . Yii::t("app", "recursos");
    $summary_layout = "<div class='col-xs-12 top-space'>{summary}</div>";
    $items_layout = "{items}";

    $pager_layout = "<div class='row'>"
            . "<div class='col-md-12 text-center'>{pager}</div>"
            . "</div>";

    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => $emptyText,
        'emptyTextOptions' => [
            'tag' => 'div',
            'class' => 'alert alert-warning',
            'role' => "alert"
        ],
        'options' => [
            'tag' => 'div',
            'id' => 'recursos-tags',
            'class' => 'row',
        ],
        'itemOptions' => [
            'tag' => 'div',
            'id' => '',
            'class' => "col-xs-12 top-space",
        ],
        'summary' => $summaryText,
//        'summary' => '',
        'summaryOptions' => [
            'tag' => 'div',
            'id' => '',
            'class' => '',
        ],
        'layout' => "$summary_layout\n$items_layout\n$pager_layout",
        'itemView' => function ($model, $key, $index, $widget) {
    global $configuraciones_request;
    global $module;
    return $this->render('content/coleccion', [
                'model' => $model,
                'index' => $index,
                'configuraciones' => $configuraciones_request,
                'module' => $module->params,
//                    'ExcerptContent' => $this->context->get_the_excerpt($model->DescripcionProducto, 25),
                    // Pass as many params as you want here
    ]);
},
        'pager' => [
            'firstPageLabel' => Yii::t("app", "Primero"),
            'lastPageLabel' => Yii::t("app", "Último"),
        ]
    ]);
    ?>
</div>
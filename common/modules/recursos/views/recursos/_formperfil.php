<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\User;
use common\modules\recursos\models\Recursos;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use common\widgets\Alert;
use pendalf89\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
// class Recursos {
//     public $genero;
//     public $ocupacion;
//     public $pais;
//     public $provincia;
//     public $canton;
//     public $distrito;
//     public $centroeducativo;
// }
?>

<style type="text/css">
* {
  box-sizing: border-box;
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
.w3-col-size{
  width: 90%;  
}

@media screen and (max-width: 600px) {
  .column {
    width: 25%;
  }
  .w3-col-size{
    width: 100%;
  }
}
* {
  box-sizing: border-box;
}

    input[type='radio']:after {
        width: 25px;
        height: 25px;
        border-radius: 15px;
        top: -1px;
        left: -1px;
        position: relative;
        background-color: white;
        content: '';
        display: inline-block;
        visibility: visible;
/*        border: 2px solid white;*/
    }

    input[type='radio']:checked:after {
        width: 25px;
        height: 25px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #00CC99;      
        content: '';
        display: inline-block;
        visibility: visible;
/*        border: 2px solid white;*/
    }
input[type="date"]::-webkit-calendar-picker-indicator {
  cursor: pointer;
  border-radius: 4px;
  margin-right: 2px;
  width: 32px;
  height: 32px;  
/*  opacity: 0.6;*/
  background-color: #00CC99;
  background-image: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="0 0 24 24"><path fill="%23ffffff" d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z"/></svg>');    
  color: white;
  border-radius: 50%;
/*  filter: invert(0.8);*/
}
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 15px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 22px;
  width: 22px;
  background-color: white;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: white;
  border: solid #00CC99;
  border-width: 3px 3px 3px 3px;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;

}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 3px;
  top: -2px;
  width: 10px;
  height: 15px;
  border: solid #00CC99;
  border-width: 0 5px 5px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>


<div  style="margin-top:80px">  
  <h1 class="w3-hide-medium w3-hide-large margin-top:30px" style="color:white">.</h1>
  <div class="w3-container" style="width: 100%" >
    <div class="w3-col" style="width:5%;padding-top: 20px">
      <p style="color:white"> . </p>
    </div>
    <div class="w3-col" style="padding-left:15px;  width:90%;padding-top: 20px">
      <h1 style="font-size: 30px;font-weight: bold;margin-bottom: 20px"><?php echo Html::encode($this->title) ?></h1>
    </div>
    <div class="w3-col" style="width:5%;padding-top: 20px">
      <p style="color:white"> . </p>
    </div>  
  </div>

  <div class="w3-container w3-light-gray" style="width: 100%"> 
    <div class="w3-col" style="width:5%;padding-top: 20px">
      <p style="color:white"> . </p>
    </div>
    <div class="w3-col" style="padding-left:15px;  width:90%;padding-top: 20px">
      <h3 style="font-weight: bold">Información Personal</h3>
    </div>    
    <div class="w3-container">
      <div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <input id = "personanac" style="color: #666666;font-weight: normal;" class="w3-radio" style="background-color: rgb(0, 204, 153);" type="radio" checked name="check" onclick="onlyOne(this,this.name)"> Nacional </input>
            <input id = "personaext" style="color: #666666;font-weight: normal;margin-left: 10px" class="w3-radio" type="radio" name="check" onclick="onlyOne(this,this.name)"> Extranjero</input>       
          </h3> 
        </div>
      </div>      
      <div class="col-xs-12 col-sm-5 col-ms-5 col-lg-5">
        <div class="input-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px"> 
          <input id="cedula" class="w3-inputnew" style="border:none;font-size: 16px;border-radius: 15px 0px 0px 15px" type="text" placeholder=" Identificación 000000000"></input>    
          <div class="input-group-append"> 
            <button type="submit" class="w3-bar-item w3-button" id="butbusca" style="border-radius: 0px 15px 15px 0px;background-color: white" onclick="verificarCedula();"><i style="color:#717070" class="fa fa-search fa-2x"></i></button>  
          </div>
        </div>
        <div id="errorIDEx" style="display: none;">
          <div class="alertEspecial alert alert-danger">
            <i class="fa fa-warning"></i><p style="font-size: 12px"> Debe especificar una identificación válida</p>
          </div>
        </div>
      <div id="mensajeErrorCedula"></div>        
      </div>
      <div id="div-menor" class="col-xs-12 col-sm-7 col-ms-7 col-lg-7">
        <div class="form-group" style="margin-left:25px; margin-top: 20px;margin-bottom: 15px"> 
          <h2>     
            <label class="container">
              <p style="color: #666666;font-weight: normal; font-size: 16px">Soy menor de edad</p>
              <input id="menor" type="checkbox" onchange="menor()">
              <span class="checkmark"></span>
            </label>            
          </h2>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
      </div>            
      <?php 
        echo Alert::widget() ;
        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
      ?>
      <div id = "div-nombre" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label class="control-label" style="font-weight: bold;font-size:16px;" for="nombre">Nombre *</label>
            <input id = "nombre" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" style="background-color: rgb(0, 204, 153);" type="text" placeholder="  Indique nombre"></input>   
          </h3> 
        </div>
      </div>  
      <div id = "div-apellido1" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label id="labelapellido1" class="control-label" style="font-weight: bold;font-size:16px" for="apellido1">Primer apellido *</label>            
            <input id = "apellido1" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="text" placeholder="  Indique apellido"></input>       
          </h3> 
        </div>
      </div>           
      <div id = "div-apellido2" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label id="labelapellido2"  class="control-label" style="font-weight: bold;font-size:16px" for="apellido2">Segundo apellido</label>            
            <input id = "apellido2" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="text" placeholder="  Indique apellido"></input>       
          </h3> 
        </div>
      </div>         
      <div id = "div-fecha" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label id="labelfecha" class="control-label" style="font-weight: bold;font-size:16px" for="fecha">Fecha de nacimiento</label>            
            <input id = "fecha" style="padding:0px; height: 36px; color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="date"></input>       
          </h3> 
        </div>
      </div>
      <div id = "div-genero" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Masculino",
              "2" => "Femenino"
            ];
              $modelrec = new Recursos;
              echo $form->field($modelrec, 'genero')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione genero', 'multiple' => false],
                ])->label('Sexo',['style' => 'font-size:16px']);
          ?>       
          </h3> 
        </div>
      </div>   
      <div id = "div-email" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label class="control-label" style="font-weight: bold;font-size:16px" for="email">Correo</label>            
            <input id = "email" style="padding:0px; height: 36px; color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge"  type="email" placeholder="  correo@electronico.com"></input>       
          </h3> 
          <div id="errorEmailEx" style="display: none;">
            <div class="alertEspecial alert alert-danger">
              <i class="fa fa-warning"></i> <p style="font-size: 12px" id="msgmail">Revise el formato del correo</p>
            </div>
          </div>          
        </div>
      </div>      
      <div id = "div-telefono" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label class="control-label" style="font-weight: bold;font-size:16px" for="telefono">Teléfono</label>            
            <input id = "telefono" style="padding:0px; height: 36px; color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="tel" placeholder="  0000-0000" pattern="[0-9]{4}-[0-9]{4}"></input>       
          </h3> 
        </div>
      </div>  
      <div id = "div-ocupacion" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Docente",
              "2" => "Estudiante"
            ];
              $modelrec = new Recursos;
              echo $form->field($modelrec, 'ocupacion')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione ocupación', 'multiple' => false],
                'pluginEvents' => [
                'change' => 'function() { // function to make ajax call here 
                  if(bandera==0){
                    showCE(this.text);
                    showMT(this.text);
                  }
                 }'],                
                ])->label('Ocupación',['style' => 'font-size:16px']);
          ?>       
          </h3> 
        </div>
      </div>   
      <div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
      </div>      
      <div id = "div-pais" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Costa Rica",
              "2" => "Guatemala"
            ];
              $modelrec = new Recursos;
              echo $form->field($modelrec, 'pais')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione país', 'multiple' => false],
                ])->label('País',['style' => 'font-size:16px']);
          ?>       
          </h3> 
        </div>
      </div>      
      <div id = "div-provincia" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Alajuela",
              "2" => "San José"
            ];
              $modelrec = new Recursos;
              echo $form->field($modelrec, 'provincia')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione provincia', 'multiple' => false],
                'pluginEvents' => [
                'change' => 'function() { // function to make ajax call here 
                   cargarCantones(this.value);
                 }'],
                ])->label('Provincia',['style' => 'font-size:16px']);
          ?>       
          </h3> 
        </div>
      </div>   
      <div id = "div-canton" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Alajuela",
              "2" => "San Ramón"
            ];
              $modelrec = new Recursos;
              echo $form->field($modelrec, 'canton')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione cantón', 'multiple' => false],
                'pluginEvents' => [
                'change' => 'function() { // function to make ajax call here 
                   cargarDistritos(this.value);
                 }'],
                ])->label('Cantón',['style' => 'font-size:16px']);
          ?>       
          </h3> 
        </div>
      </div>  
      <div id = "div-distrito" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Alajuela",
              "2" => "Grecia"
            ];
              $modelrec = new Recursos;
              echo $form->field($modelrec, 'distrito')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione distrito', 'multiple' => false],
                ])->label('Distrito',['style' => 'font-size:16px']);
          ?>       
          </h3> 
        </div>
      </div>  
      <div id = "div-direccion" class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label class="control-label" style="font-weight: bold;font-size:16px" for="direccion">Dirección</label>            
            <input id = "direccion" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="text" placeholder=" Indique dirección"></input>       
          </h3> 
        </div>
      </div>  
      <div id = "div-ce_encabezado">
        <div class="w3-col" style="width:5%;padding-top: 20px">
          <p style="color:white"> . </p>
        </div>
        <div class="w3-col" style="width:90%;padding-top: 20px">
         <h3 style="font-weight: bold">Información del centro educativo</h3>
        </div>        
        <div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
          <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
            <h3>
              <input id = "ce_publico" style="color: #666666;font-weight: normal;" class="w3-radio" style="background-color: rgb(0, 204, 153);" type="radio" checked name="check" onclick="onlyOne(this,this.name)"> Público </input>
              <input id = "ce_privado" style="color: #666666;font-weight: normal;margin-left: 10px" class="w3-radio" type="radio" name="check" onclick="onlyOne(this,this.name)"> Privado</input>       
              <input id = "ce_otro" style="color: #666666;font-weight: normal;margin-left: 10px" class="w3-radio" type="radio" name="check" onclick="onlyOne(this,this.name)"> Otro</input>
            </h3> 
          </div>
        </div>
      </div>
      <div id = "div-centroeducativo" class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
              <?php
                $data = [
                "1" => "Colegio Santa Teresa",
                "2" => "Escuela Republica Guatemala"
                ];
                $modelrec = new Recursos;
                echo $form->field($modelrec, 'centroeducativo')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Seleccione centro educativo', 'multiple' => false],
                'pluginEvents' => [
                'change' => 'function() { // function to make ajax call here 
                  if(bandera==0){
                    cargarCEUbicacion(this.value);
                  }
                 }'],
                ])->label('Centro educativo',['style' => 'font-size:16px']);
              ?>       
          </h3> 
        </div>
      </div>   
      <div id="div-ce_direccion">  
        <div class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
          <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
            <h3>
              <label class="control-label" style="font-weight: bold;font-size:16px;" for="ce_provincia">Provincia</label>
              <input id = "ce_provincia" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" style="background-color: rgb(0, 204, 153);" disabled type="text" placeholder="  Provincia"></input>   
            </h3> 
          </div>
        </div>       
        <div class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
          <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
            <h3>
              <label class="control-label" style="font-weight: bold;font-size:16px;" for="ce_canton">Cantón</label>
              <input id = "ce_canton" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" style="background-color: rgb(0, 204, 153);" disabled type="text" placeholder="  Cantón"></input>   
            </h3> 
          </div>
        </div>   
        <div class="col-xs-12 col-sm-3 col-ms-3 col-lg-3">
          <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
            <h3>
              <label class="control-label" style="font-weight: bold;font-size:16px;" for="ce_distrito">Distrito</label>
              <input id = "ce_distrito" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" style="background-color: rgb(0, 204, 153);" disabled type="text" placeholder="  Distrito"></input>   
            </h3> 
          </div>
        </div>   
      </div>
 
      <div class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
      </div>
      <div id="div-materias" class="col-xs-12 col-sm-12 col-ms-12 col-lg-12">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>      
          <?php
            $data = [
              "1" => "Materia 1",
              "2" => "Materia 2"
            ];
            $modelrec = new Recursos;
            echo $form->field($modelrec, 'materia_f')->widget(Select2::classname(), [
              'data' => $data,
              'options' => ['placeholder' => 'Seleccione materia', 'multiple' => false],
            ])->label('Materia',['style' => 'font-size:16px;color:black']);
          ?>       
          </h3> 
        </div>
      </div>  

      <div id = "div-clave" class="col-xs-12 col-sm-6 col-ms-6 col-lg-6">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label class="control-label" style="font-weight: bold;font-size:16px" for="clave">Contraseña</label>            
            <input id = "clave" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="password" placeholder="  Indique contraseña"></input>
            <small id="passwordHelpBlock" style = "font-size:11px;font-weight: bold" class="form-text text-muted">Mínimo 8 caracteres, mayúsculas, mínusculas y al menos uno de estos caracteres *%+!$&()?</small>
          </h3> 
          <div id="errorClave" style="display: none;">
            <div class="alertEspecial alert alert-danger"><i class="fa fa-warning"></i> <p style="font-size: 12px" id="msgclave"> Error en formato de contraseña</p></div>            
          </div>          
        </div>
      </div>    
      <div id = "div-confirmaClave" class="col-xs-12 col-sm-6 col-ms-6 col-lg-6">
        <div class="form-group" style="margin-left:25px; margin-top: 15px;margin-bottom: 15px">
          <h3>
            <label class="control-label" style="font-weight: bold;font-size:16px" for="confirmaClave">Confirmación de contraseña</label>            
            <input id = "confirmaClave" style="color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="password" placeholder="  Confirme contraseña"></input>       
          </h3> 
          <div id="errorClaveConfirm" style="display: none;">
            <div class="alertEspecial alert alert-danger">
              <i class="fa fa-warning"></i> <p style="font-size: 12px">La contraseña no conincide</p>
            </div>
          </div>          
        </div>
      </div>                  
    </div>
   <?php ActiveForm::end(); ?>      
    <div class="w3-twothird w3-container">
      <div class="w3-hide-large w3-hide-medium" style="margin-top:0px;color: white">.</div>
      <div class="w3-hide-large w3-hide-medium" style="margin-top:0px;color: white">.</div>
    </div>
  </div>
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 20px">
    <p style="color:white"> . </p>
  </div>
  <div class="w3-container w3-center w3-padding-32" style="padding-block-end: 150px;">
    <button class="w3-button_orange w3-round-xxlarge w3-padding-large" style="background-color:white;color:#666666; font-size:17px;font-weight: bold;border-color: #666666; border-style:solid">Cancelar</button>    
    <button id="but-registrarse" class="w3-button_orange w3-round-xxlarge w3-padding-large" style="background-color:#F29222;font-size:17px;font-weight: bold" onclick="validarFormulario()">Registrarse</button>
  </div>  
  <div style="padding-block-end: 180px;"></div>
</div>
<div class="w3-container">
<div class="w3-modal" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="ConfirmarRegistro">
  <div class="w3-modal-content w3-animate-zoom w3-round-xlarge" style="width: 50%" role="document">
    <div class="modal-content w3-animate-zoom w3-round-xlarge">
      <div class="w3-container w3-center w3-padding">
<!--         <span style="color: #00CC99" onclick="cierra('confirmar')" class="w3-button w3-xlarge w3-hover-opacity w3-display-topright" title="Cerrar / Cancelar"><i style="color: black;font-size: 40px">&times;</i></span>    -->     
        <h2 style="color:black;font-weight: bold;margin-top: 20px">Proceso exitoso</h2>
      </div>    
      <div class="modal-body">
          <h1>¡Felicidades!</h1>
          <p>Ha completado el registro.</p>
          <p>Revise su correo electrónico para activar su cuenta de usuario por medio del enlace enviado.</p>
          <p>O copie el ID y el código de activación que está en el correo.</p>
      </div>
          <br><br>
      <div class="w3-container w3-center w3-padding">          
      <div class="w3-third">                    
        <p style="color:white">A</p>
      </div>
      <div class="w3-third">          
            <button id="btn_finalizar" name="btn_creacoleccion" class="w3-button_green w3-round-xxlarge w3-border-0" style="font-size: 15px;padding-bottom:5px;padding-top:5px;padding-right: 25px;padding-left: 25px" onclick="cierra('confirmar')"> Aceptar</button>
      </div>      
      <div class="w3-third">                    
        <p style="color:white">B</p>
      </div>
      </div>
    </div>
  </div>
</div>
</div>
 <div class="w3-modal" id="spinner" tabindex="-1" role="dialog" aria-labelledby="Spinner">
    <div class="w3-modal-content w3-animate-zoom w3-round-xlarge" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h1>Estamos procesando su solicitud</h1>
          <p>Esto puede tardar unos segundos.</p>
          <p>
            <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
            <span class="sr-only">Cargando...</span>
          </p>
        </div>
      </div>
    </div>
  </div>
<div class="w3-content" hidden="">
      <a id="home" class="w3-bar-item w3-button fa fa-user fa-2x  w3-round-xxlarge"  style="text-decoration: none;color:#717070" data-toggle="tooltip" title = "Home" href="/" hidden="">
      </a> 
</div>

<script type="text/javascript">
  $(document).ready(function () {
  //  restaurar("1");

});
  function onlyOne(object,value){
    switch(object.id) {
      case "personanac":
        limpiar();
        document.getElementById("cedula").placeholder = " Identificación 000000000";
        $("#div-nombre").hide();
        $("#div-apellido1").hide();        
        $("#div-apellido2").hide();
        $("#div-fecha").hide();
        $("#div-genero").hide();        
        $("#div-email").hide();   
        $("#div-telefono").hide();   
        $("#div-ocupacion").hide();   
        $("#div-pais").hide();
        $("#div-provincia").hide();
        $("#div-canton").hide();
        $("#div-distrito").hide();
        $("#div-direccion").hide();
        $("#div-ce_encabezado").hide();
        $("#div-centroeducativo").hide();
        $("#div-ce_direccion").hide();
        $("#div-materias").hide();
        $("#div-menor").hide();
        $("#div-clave").hide();
        $("#div-confirmaClave").hide();
        $("#but-registrarse").hide();

        $('#recursos-pais').val("CRC").trigger('change');

        document.getElementById("recursos-pais").disabled = true;
      
        // code block
        break;
      case "personaext":
        limpiar();
        document.getElementById("cedula").placeholder = " Pasaporte, DIMEX o residencia";  
        document.getElementById("menor").disabled = false;
        document.getElementById("nombre").disabled = false;
        document.getElementById("apellido1").disabled = false;
        document.getElementById("apellido2").disabled = false;
        document.getElementById("fecha").disabled = false;
        document.getElementById("recursos-genero").disabled = false;   
        document.getElementById("menor").checked = false;  
        document.getElementById("menor").disabled = false;     
        document.getElementById("recursos-pais").disabled = false;
        $("#div-nombre").show();
        $("#div-apellido1").show();        
        $("#div-apellido2").show();
        $("#div-fecha").show();
        $("#div-genero").show();        
        $("#div-email").show();   
        $("#div-telefono").show();   
        $("#div-ocupacion").show();   
        $("#div-pais").show();
        $("#div-provincia").show();
        $("#div-canton").show();
        $("#div-distrito").show();
        $("#div-direccion").show();
        $("#div-ce_encabezado").hide();
        $("#div-centroeducativo").hide();
        $("#div-ce_direccion").hide();
        $("#div-materias").hide();    
        $("#div-menor").show();
        $("#div-clave").show();
        $("#div-confirmaClave").show();               
        $("#but-registrarse").show();
        // code block
        break;
      case "ce_publico":
        $("#div-centroeducativo").show();
        $("#div-ce_direccion").show();
        // code block
        break;
      case "ce_privado":
        $("#div-centroeducativo").hide(); 
        $("#div-ce_direccion").hide();
        // code block
        break;
      case "ce_otro":
        $("#div-centroeducativo").hide(); 
        $("#div-ce_direccion").hide();
        // code block
        break;                        
      default:
        // code block
    }
  }

function cierra(param){
  document.getElementById(param).style.display='none'   
  document.getElementById("home").click(); 
}

  function menor(){
    esmenor = document.getElementById("menor").checked;

    if(esmenor==true){

      $("#div-apellido2").hide();      
      $("#div-apellido1").hide();
      $("#div-fecha").hide();
      $("#div-genero").hide();
      $("#div-telefono").hide();
      $("#div-ocupacion").hide();
      $("#div-pais").hide();
      $("#div-provincia").hide();
      $("#div-canton").hide();
      $("#div-direccion").hide();
      $("#div-distrito").hide();
      $("#div-centroeducativo").show();
      $("#div-ce_encabezado").hide();
      $("#div-ce_direccion").hide();  
    }
    else{
      $("#div-apellido2").show();      
      $("#div-apellido1").show();
      $("#div-fecha").show();
      $("#div-genero").show();
      $("#div-telefono").show();
      $("#div-ocupacion").show();
      $("#div-pais").show();
      $("#div-provincia").show();
      $("#div-canton").show();
      $("#div-direccion").show();
      $("#div-distrito").show();  
 
    }
  }
</script>



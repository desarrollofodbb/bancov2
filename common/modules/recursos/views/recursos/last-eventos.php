<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    /**
     * The user has many followers
     * @param 
     * @return boolean
     */
    public function actionLastEventos($Autor) {

     $row = (new Query())->select('FechaDeModificacion')
    ->from('TEventos')
    ->where(['Autor' => $Autor])
    ->orderBy(['FechaDeModificacion ' => SORT_DESC  ]);    
    ->one();

     $lastdate_noreve = $row['FechaDeModificacion'];


     $FollowQuery = (new Query)->select('rIdUsuarios')
     ->from('RAutoresSeguidores')
     ->where(['rIdSeguidores' => $Autor]);
     
     $EventsQuery = (new Query)->select('idEventos')
     ->from('TEventosSeguidores')
     ->where(['Autor' => $Autor]);

     $row2 = (new Query())->select('FechaDeModificacion')
    ->from('TEventos')
    ->where(['Autor' => $FollowQuery])    
    ->andWhere(['not in', 'idEventos', $EventsQuery])
    ->orderBy(['FechaDeModificacion ' => SORT_DESC  ]);        
    ->one();     

     $lastdate_segeve = $row2['FechaDeModificacion'];


    if($lastdate_segeve > $lastdate_noreve){
      return $lastdate_segeve;  
    }
    return $lastdate_noreve;    

    }    
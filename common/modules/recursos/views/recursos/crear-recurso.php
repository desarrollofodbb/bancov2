<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

$module = \Yii::$app->controller->module;

$this->title = "Agregar recurso";//$module->params["t_add_new"];
?>


            <?php
          
            echo
            $this->render('_form', [
                'model' => $model,
                'modelRecurso' => $modelRecurso,
                'caracteristicasDeRecursos' => $caracteristicasDeRecursos,
            ])
            ?>


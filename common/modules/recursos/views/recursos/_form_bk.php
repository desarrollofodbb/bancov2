<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use common\widgets\Alert;
use pendalf89\tinymce\TinyMce;
use common\modules\recursos\models\CriteriosDeEvaluacion;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
$autor = $model->isNewRecord ? User::getCurrentUser() : $model->autor;
$time = time();
$filepreview = $model->getPreview($model->Autor,$model->idRecursos,'menupreview');
if($filepreview!=""){
  $filepreview = "". $filepreview;
}
$filesall = $model->getFilesDataAll($model->Autor,$model->idRecursos,'menupreview');


?>
<style type="text/css">
* {
  box-sizing: border-box;
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
.w3-col-size{
  width: 90%;  
}
@media screen and (max-width: 600px) {
  .column {
    width: 25%;
  }
  .w3-col-size{
    width: 100%;
  }
}
* {
  box-sizing: border-box;
}

</style>




<input type="text" name="text" hidden id="userid" value="<?php echo $autor->id ?>">
<div  style="max-width:1400px;margin-top:80px;padding-block-end: 100px;">  
<h1 class="w3-hide-medium w3-hide-large margin-top:30px" style="color:white">.</h1>
<div class="w3-container" style="width: 100%" >
  <div class="w3-col" style="width:5%;padding-top: 20px">
     <p style="color:white"> . </p>
  </div>
  <div class="w3-col" style="padding-left:15px;  width:90%;padding-top: 20px">
    <h1 style="font-size: 30px;font-weight: bold"><?php echo Html::encode($this->title) ?></h1>
  </div>
  <div class="w3-col" style="width:5%;padding-top: 20px">
      <p style="color:white"> . </p>
  </div>  
</div>

  <?php echo Alert::widget() ?>
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?php
            echo $form->field($model, 'idtime')->textInput(
                    [
                        "value" => $time,
                        "id" => "idtime",
                        "style" => "display:none",
                        "class" => "w3-input w3-round-xlarge"
            ])->label("");
            echo $form->field($model, 'haspreview')->textInput(
                    [
                        "value" => 0,
                        "id" => "haspreview",
                        "style" => "display:none",
                        "class" => "w3-input w3-round-xlarge"
            ])->label("");

            ?>
<div class="w3-container w3-light-gray" style="width: 100%"> 
<?php    

            echo $form->field($model, 'hasfiles')->textInput(
                    [
                        "value" => 0,
                        "id" => "hasfiles",
                        "style" => "display:none",
                        "class" => "w3-input w3-round-xlarge"
            ])->label("");
            ?>
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 20px">
     <p style="color:white"> . </p>
  </div>    
  <div class="w3-col w3-col-size" style="padding-top: 20px" >
            <?php
 $btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' .
    'onclick="alert(\'Call your custom code here.\')">' .
    '<i class="glyphicon glyphicon-tag"></i>' .
    '</button>'; 


            ?>  

    <div class="w3-third w3-container">
      <div class="kv-avatar-hint">
      <h4 style="font-weight: bold;font-size: 16px">Previsualización</h4>
      </div>        
      <div class="kv-avatar w3-round-xxlarge">
        <div class="file-loading">
            <input id="attachment_50" name="attachment_50[]" type="file">
    <?php

      $fi = FileInput::widget([
      'name' => 'attachment_50[]', 
      'options'=>[
        'multiple'=>false,      
      ],
      'pluginOptions' => [
        'uploadUrl' => Url::to(['/server/ajax.php']),
        'uploadExtraData' => [
          'id' => $autor->id,
          'idtime' => $time
      ],
      'maxFileCount' => 1,
      'overwriteInitial' => true,
      'maxFileSize' => 1500,
      'showClose' =>false,
      'showCaption' => false,
      'showBrowse'=> false,
      'showCancel'=>false, 
      'showUploadStats' => false,     
      'browseLabel' => '',
      'removeLabel' => '',
      'minImageWidth' => 50,
      'minImageHeight' => 50,
      'browseOnZoneClick'=> true,      
      'browseIcon' => '<i class="glyphicon glyphicon-folder-open"></i>',
      'removeIcon' => '<i class="glyphicon glyphicon-remove"></i>',
      'removeTitle' => 'Cancelar o deshacer cambios',
      'elErrorContainer' => '#kv-avatar-errors-1',
      'msgErrorClass' => 'alert alert-block alert-danger',
    // 'defaultPreviewContent' => '<img src="/samples/default-avatar-male.png" alt="Your Avatar">',
      'layoutTemplates'=> '{main2: "{preview} " +  $btnCust + " {remove} {browse}"} ',
      'allowedFileExtensions' => ["jpg", "png", "gif"],
      'minImageWidth' => 175,
      'minImageHeight' => 195
      ]
  ]);
  ?>                    

        </div>
    </div>

  </div>        
  <div class="w3-twothird w3-container">
          <div class="w3-hide-large w3-hide-medium" style="margin-top:0px;color: white">.</div>
            <?php
            echo $form->field($model, 'Nombre')->textInput(
                    [
//                            "placeholder" => "Nombre*",
                        "class" => "w3-input w3-round-xlarge",
                        'style' => ['border'=> 'none']
            ])->label("Título*", ['style' => 'font-weight: bold;font-size:16px']);
            ?>
            <div class="w3-hide-large w3-hide-medium" style="margin-top:0px;color: white">.</div>
            <?php
            echo $form->field($model, 'autorRecurso')->textInput(
                    [
//                            "placeholder" => "Nombre*",
                        "class" => "w3-input w3-round-xlarge",
                        'style' => ['border'=> 'none']
            ])->label("Autor*",['style' => 'font-size:16px']);
            ?>            
            <div class="w3-hide-large w3-hide-medium" style="margin-top:0px;color: white">.</div>
            <?php
            echo $form->field($model, 'ObjetivoDelRecurso')->textarea(
                    [
                        "rows" => 10,
                        "class" => "w3-input w3-round-xlarge",
                        'style' => ['border'=> 'none']
            ])->label("Resumen*",['style' => 'font-size:16px']);
            ?>
    </div>
  </div>
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 20px">
     <p style="color:white"> . </p>
  </div>
</div>
<div class="w3-container w3-light-gray" style="width: 100%"> 
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 20px">
     <p style="color:white"> . </p>
  </div>    
    <div class="w3-col w3-col-size" style="margin-top: 15px">

        <div class="col-xs-12 col-sm-4 col-ms-4 col-lg-4" style="margin-bottom: 15px">
            <?php
            echo $form->field($model, 'anioRecurso')->textInput(
                    [
//                            "placeholder" => "Nombre*",
                        "class" => "w3-input w3-round-xlarge",
                        'style' => ['border'=> 'none']
            ])->label("Año*",['style' => 'font-size:16px']);
            ?>        
        </div>
        <?php
        $counter = 0;
        foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];            
                if($valor_key[3]==3){
                    $counter++;
                    continue;
                }            
            ?> 
            <div class="col-xs-12 col-sm-4 col-ms-4 col-lg-4" style="margin-bottom: 15px">
                <?php


                $tags = false;
                if($valor_key[3]==10){
                    $tags = true;
                }

                if($counter>3 && $counter<=6){
                  if($valor_key[3]==5){
                    $caracteristica_nombre = "Publico meta";
                  }
                  if($valor_key[3]==6){
                    $caracteristica_nombre = "Usos poderosos de la tecnologia";
                  }                  
                  $counter++;
                }   

                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo
                $form->field($model, $caracteristica_field)->widget(Select2::className(), [
                    'model' => $model,
                    'attribute' => $caracteristica_field,
                    'language' => 'es',
                    'showToggleAll' => false,
                    'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                    'options' => [
                        'multiple' => $caracteristica_tipoSeleccion,
                        "class" => "w3-input;  w3-round-xlarge;",
                        'style' => 'border:none ',
                        "placeholder" => $placeholder,
                    ],
                    'pluginOptions' => [
                       'tags' => $tags,
                       'tokenSeparators' => [',', ' '],
                       'maximumInputLength' => 20,
                       'maximumSelectionLength' => 7,
                    ],
                ])->label($caracteristica_nombre,['style' => 'font-size:16px']);
                $counter++;

                
                ?>

            </div>
        <?php endforeach; ?>

    </div>
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 20px">
     <p style="color:white"> . </p>
  </div>
  </div>
<div class="w3-container w3-light-gray" style="width: 100%">       
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 10px">
     <p style="color:white"> . </p>
  </div>    
  <div class="w3-col w3-col-size" style="padding-bottom: 10px;padding-left: 10px;padding-right: 10px">
    <div class="col-xs-14"> 
        
                <?php
                $modelArchivoRelated = $model->archivo;
                $autor = $model->isNewRecord ? User::getCurrentUser() : $model->autor;

//                 echo $form->field($model, "Tipo")->dropDownList([
//                     $model::TYPE_FILE => Yii::t("app", "Subir archivo"),
//                     $model::TYPE_URL => Yii::t("app", "Agregar una URL"),
//                     $model::TYPE_EMBED => Yii::t("app", "Embed"),
//                         ], ['options'=>[$model::TYPE_URL => ['Selected'=>true]], 'style' => 'display:none'], [
//                     "prompt" => Yii::t("app", "Seleccione la fuente del recurso"),
//                     "onchange" => ""
//                     . "jQuery('div[id^=\"tipo-recurso-\"]').addClass('hidden');"
//                     . "jQuery('#tipo-recurso-'+jQuery(this).val()).removeClass('hidden');",
// //                    'class' => ""
//                 ])
                ?>

                <div id="tipo-recurso-<?php echo $model::TYPE_URL; ?>" class="<?php echo $model->Tipo == $model::TYPE_URL ? "" : ""; ?>">
                    <?php
                    echo $form->field($model, 'url_recurso')->label("URL Recurso",['style' => 'font-size:16px'])->textInput(["class" => "w3-input w3-round-xlarge","style" => "border:none;font-size:16px","onchange" => "previewyoutube(this.value)"])->hint(Yii::t("app", "Su espacio disponible para subir archivos es de: {tamano}", [
                            "tamano" => $autor->getUserRemainingSpace($modelArchivoRelated ? $modelArchivoRelated->Tamano : null)
                ]));
                    ?>
                </div>     

                <div class="file-loading">
                  <input id="attachment_48" name="attachment_48[]" type="file"  multiple>
                </div>                           


         

            <?php
//             echo
//             $form->field($model, 'autores_ids')->widget(Select2::className(), [
//                 'model' => $model,
//                 'attribute' => 'autores_ids',
//                 'language' => 'es',
//                 'showToggleAll' => false,
//                 'data' => ArrayHelper::map(User::findByRole("autor"), 'id', 'username'),
//                 'value' => [$autor->id],
//                 'options' => [
//                     'multiple' => false,
//                     "class" => "w3-input w3-round-xlarge", 
//                     'style' => 'display:none'
// //                        "placeholder" => "Seleccione el autor o los autores",
//                 ],
//                 'pluginOptions' => [
//                   'style' => 'display:none'  
// //                'allowClear' => true,
//                 ],
//                 "changeOnReset" => false
//             ])->label("Seleccione el autor o los autores");
            ?>
</div>
        <div class="w3-content">    
          <div class="w3-row">
              .
          </div>  
        </div> 
    </div>    
  <div class="w3-col w3-hide-small" style="width:5%;padding-top: 10px">
     <p style="color:white"> . </p>
  </div>    
</div>

<div class="w3-container w3-center w3-padding-32">
    <?php
    echo Html::submitButton($model->isNewRecord ? $module->params["a_publish"] . " recurso" : $module->params["a_update"] . " Recurso", [
        'class' => $model->isNewRecord ? 'w3-button_orange w3-round-xxlarge w3-text-white w3-padding-large' : 'w3-btn w3-round-xxlarge w3-text-white w3-padding',
        'style' => 'background-color:#F29222;font-size:17px;font-weight: bold',
    ])
    ?>
</div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
  var files = [];
  var filestype = [];
  var filessize = [];
  var filesname = [];
  var fileskey = [];
  var filesid = [];
  files[0] = files[1] = files[2] = files[3] = files[4] = files[5] = files[6] = files[7] = files[8] = files[9] = ""; 
  filestype[0] = filestype[1] = filestype[2] = filestype[3] = filestype[4] = filestype[5] = filestype[6] = filestype[7] = filestype[8] = filestype[9] = "";   
  filessize[0] = filessize[1] = filessize[2] = filessize[3] = filessize[4] = filessize[5] = filessize[6] = filessize[7] = filessize[8] = filessize[9] = "";     
  filesname[0] = filesname[1] = filesname[2] = filesname[3] = filesname[4] = filesname[5] = filesname[6] = filesname[7] = filesname[8] = filesname[9] = "";      
  fileskey[0] = fileskey[1] = fileskey[2] = fileskey[3] = fileskey[4] = fileskey[5] = fileskey[6] = fileskey[7] = fileskey[8] = fileskey[9] = "";   
  filesid[0] = filesid[1] = fileskey[2] = filesid[3] = filesid[4] = filesid[5] = filesid[6] = filesid[7] = filesid[8] = filesid[9] = "";    
  var filepreview = '<?php echo $filepreview ?>';
  var filesall = '<?php echo $filesall ?>';
  var myarray = filesall.split(';');

  for(var i = 0; i < myarray.length; i++) {
    arraydet = myarray[i].split(':');
    for(var j = 0; j < arraydet.length; j++) {
      switch(j) {
      case 0:
        files[i] = arraydet[j];
      break;
      case 1:
        filestype[i] = arraydet[j];
      break;
      case 2:
        filessize[i] = arraydet[j];
      break;
      case 3:
        filesname[i] = arraydet[j];
      break;      
      case 4:
        fileskey[i] = arraydet[j];
      break;        
      case 5:
        filesid[i] = arraydet[j];
      break;         
      }

        
    }

  }


  var $el1 = $("#attachment_48");
  var $el2 = $("#attachment_50");

  var idtime =  document.getElementById('idtime').value;  
  var userid = $("#userid").val();

  function previewyoutube(link){
//xvideo_id = explode("?v=", link);
    var fpreview = document.getElementById("haspreview").value;


    var video_id = link.split("?v=");
//xvideo_id = xvideo_id[1];
    var video_id = video_id[1];
//thumbnail="http://img.youtube.com/vi/".xvideo_id."/maxresdefault.jpg";
    thumbnail="http://img.youtube.com/vi/"+video_id+"/maxresdefault.jpg";

    var idtime =  document.getElementById('idtime').value;  
    var userid = $("#userid").val();    
    rutacol = "/server/previewyoutube.php";
    $.ajax({        
      url: rutacol,
      type: "POST",
      data:{
        'id' : userid,
        'youtube' : link,
        'preview' : 0,
        'idtime' : idtime
      },
      error: function (data) {
        console.log(data);
      },
      success: function (data) {

        var obj = JSON.parse(data);     
        console.log(obj);        
        thumbnail = obj.rutaurl;
        if(thumbnail=="Not valid link"){

        }
        else{
          if(fpreview==1){
            var $el1 = $("#attachment_48");
            if ($el1.data('fileinput')) {
              $el1.fileinput('destroy');
            }             
            create48(thumbnail,obj.type,obj.size,obj.caption,obj.key);          
          }
          else{
            var $el2 = $("#attachment_50");
            if ($el2.data('fileinput')) {
              $el2.fileinput('destroy');
            }       
                        
            create50(thumbnail,obj.type,obj.size,obj.caption,obj.key);
          }          
        }

      }
    });   
     
  }

function create50(thumbnail,type,size,caption,key){
  rutaDeleteFile = "/recursos/recursos/delete-file";
  previewinicio = filepreview;
  var $el2 = $("#attachment_50");
  if(thumbnail!=""){
    previewinicio = thumbnail;
  }
  if(previewinicio!=""){
    var fpreview = document.getElementById("haspreview");
    fpreview.value = 1;   
      $el2.fileinput({
        uploadUrl: "<?php echo Url::to(['/server/ajax.php']); ?>",
        deleteUrl: '/server/file-delete.php',
        language: "es",
        uploadExtraData: {
                'id' : userid,
                'field' : 'attachment_50',
                'preview' : 1,
                'idtime' : idtime
        },  
        uploadAsync: true,
        showUpload: false, // hide upload button
        showUploadStats : false,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        showCancel: false,    
        browseLabel: '',
        removeLabel: '',    
        overwriteInitial: false, // append files to initial preview
        minFileCount: 1,
        maxFileCount: 1,
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancelar o deshacer cambios',    
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',  
        allowedFileExtensions: ["jpg", "png", "gif"],
        overwriteInitial: true,
        maxFileSize: 1500,    
        browseOnZoneClick: true,
        minImageWidth: 100,
        minImageHeight: 100,        
        initialPreviewAsData: true,
        removeFromPreviewOnError: true,
     initialPreview: [
         previewinicio,
     ],    
      }).on("filebatchselected", function(event, files) {
        $el2.fileinput("upload");
      });
  }
  else{
      $el2.fileinput({
        uploadUrl: "<?php echo Url::to(['/server/ajax.php']); ?>",
        deleteUrl: '/server/file-delete.php',
        language: "es",
        uploadExtraData: {
                'id' : userid,
                'field' : 'attachment_50',
                'preview' : 1,
                'idtime' : idtime
        },  
        uploadAsync: true,
        showUpload: false, // hide upload button
        showUploadStats : false,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        showCancel: false,    
        browseLabel: '',
        removeLabel: '',    
        overwriteInitial: false, // append files to initial preview
        minFileCount: 1,
        maxFileCount: 1,
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancelar o deshacer cambios',    
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',  
        allowedFileExtensions: ["jpg", "png", "gif"],
        overwriteInitial: true,
        maxFileSize: 1500,    
        browseOnZoneClick: true,
        initialPreviewAsData: true,
        removeFromPreviewOnError: true   
      }).on("filebatchselected", function(event, files) {
        $el2.fileinput("upload");
      });    
  }
} 

function create48(thumbnail,type,size,caption,key){
  rutaDeleteFile = "/recursos/recursos/delete-file";
  var $el1 = $("#attachment_48");

  if(filesname[0]!=""){
      $el1.fileinput({
        uploadUrl: "<?php echo Url::to(['/server/ajax.php']); ?>",
        deleteUrl: '/server/file-delete.php',
        language: "es",
        uploadExtraData: {
                'id' : userid,
                'field' : 'attachment_48',
                'preview' : 0,
                'idtime' : idtime
        },  
        pluginOptions:{
          deleteUrl: '/server/file-delete.php',
        },
        uploadAsync: true,
        showUpload: false, // hide upload button
        showUploadStats : false,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        showCancel: false,    
        browseLabel: '',
        removeLabel: '',    
        overwriteInitial: false, // append files to initial preview
        minFileCount: 1,
        maxFileCount: 10,
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancelar o deshacer cambios',    
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',      
        browseOnZoneClick: true,
            initialPreviewAsData: true,
            removeFromPreviewOnError: true,
        initialPreview:[
        files[0],files[1],files[2],files[3],files[4],files[5],files[6],files[7],files[8],thumbnail,
        ],
        initialPreviewConfig: [
        {'type': filestype[0], 'size': filessize[0], 'caption': filesname[0], 'url': rutaDeleteFile , 'key': filesid[0], 'idFile_a': filesid[0], 'deleteExtraData' : {'idFile' : filesid[0]}},
        {'type': filestype[1], 'size': filessize[1], 'caption': filesname[1], 'url': rutaDeleteFile , 'key': filesid[1], 'idFile_a': filesid[1], 'deleteExtraData' : {'idFile' : filesid[1]}},
        {'type': filestype[2], 'size': filessize[2], 'caption': filesname[2], 'url': rutaDeleteFile , 'key': filesid[2], 'idFile_a': filesid[2], 'deleteExtraData' : {'idFile' : filesid[2]}},
        {'type': filestype[3], 'size': filessize[3], 'caption': filesname[3], 'url': rutaDeleteFile , 'key': filesid[3], 'idFile_a': filesid[3], 'deleteExtraData' : {'idFile' : filesid[3]}},
        {'type': filestype[4], 'size': filessize[4], 'caption': filesname[4], 'url': rutaDeleteFile , 'key': filesid[4], 'idFile_a': filesid[4], 'deleteExtraData' : {'idFile' : filesid[4]}},
        {'type': filestype[5], 'size': filessize[5], 'caption': filesname[5], 'url': rutaDeleteFile , 'key': filesid[5], 'idFile_a': filesid[5], 'deleteExtraData' : {'idFile' : filesid[5]}},
        {'type': filestype[6], 'size': filessize[6], 'caption': filesname[6], 'url': rutaDeleteFile , 'key': filesid[6], 'idFile_a': filesid[6], 'deleteExtraData' : {'idFile' : filesid[6]}},
        {'type': filestype[7], 'size': filessize[7], 'caption': filesname[7], 'url': rutaDeleteFile , 'key': filesid[7], 'idFile_a': filesid[7], 'deleteExtraData' : {'idFile' : filesid[7]}},
        {'type': filestype[8], 'size': filessize[8], 'caption': filesname[8], 'url': rutaDeleteFile , 'key': filesid[8], 'idFile_a': filesid[8], 'deleteExtraData' : {'idFile' : filesid[8]}},
        {'type': type, 'size': size, 'caption': caption, 'url': rutaDeleteFile , 'key': key, 'idFile_a': key, 'deleteExtraData' : {'idFile' : key}},                            
        ],
        

      }).on("filebatchselected", function(event, files) {
        $el1.fileinput("upload");

      });
  }
  else{
    if(thumbnail!=""){
      $el1.fileinput({
        uploadUrl: "<?php echo Url::to(['/server/ajax.php']); ?>",
        deleteUrl: '/server/file-delete.php',
        language: "es",
        uploadExtraData: {
                'id' : userid,
                'field' : 'attachment_48',
                'preview' : 0,
                'idtime' : idtime
        },  
        pluginOptions:{
          deleteUrl: '/server/file-delete.php',
        },
        uploadAsync: true,
        showUpload: false, // hide upload button
        showUploadStats : false,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        showCancel: false,    
        browseLabel: '',
        removeLabel: '',    
        overwriteInitial: false, // append files to initial preview
        minFileCount: 1,
        maxFileCount: 10,
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancelar o deshacer cambios',    
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',      
        browseOnZoneClick: true,
            initialPreviewAsData: true,
            removeFromPreviewOnError: true,
        initialPreview:[
          thumbnail,
        ],
        initialPreviewConfig: [
        {caption: thumbnail, width: "100%", url: rutaDeleteFile, key: key},
                            
        ],            
      }).on("filebatchselected", function(event, files) {
        $el1.fileinput("upload");

      });   
    }
    else{
      $el1.fileinput({
        uploadUrl: "<?php echo Url::to(['/server/ajax.php']); ?>",
        deleteUrl: '/server/file-delete.php',
        language: "es",
        uploadExtraData: {
                'id' : userid,
                'field' : 'attachment_48',
                'preview' : 0,
                'idtime' : idtime
        },  
        pluginOptions:{
          deleteUrl: '/server/file-delete.php',
        },
        uploadAsync: true,
        showUpload: false, // hide upload button
        showUploadStats : false,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        showCancel: false,    
        browseLabel: '',
        removeLabel: '',    
        overwriteInitial: false, // append files to initial preview
        minFileCount: 1,
        maxFileCount: 10,
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancelar o deshacer cambios',    
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',      
        browseOnZoneClick: true,
            initialPreviewAsData: true,
            removeFromPreviewOnError: true,
      }).on("filebatchselected", function(event, files) {
        $el1.fileinput("upload");

      });         
    }
 
  }
}

$(document).ready(function() {
  var $el1 = $("#attachment_48");
  $el1.on('fileloaded', function(event, file, previewId, fileId, index, reader) {
    var fpreview = document.getElementById("hasfiles");
    fpreview.value = 1;
    console.log("hasfiles");
  });
  $el1.on('fileremoved', function(event, id, index) {
    var fpreview = document.getElementById("hasfiles");
    fpreview.value = 0;
    console.log("no hasfiles");
  }); 
  if(files[0]!=""){
    var fpreview = document.getElementById("hasfiles");
    fpreview.value = 1;    
  }
  
  create48("","","","","");
  var $el2 = $("#attachment_50");  
  $el2.on('fileloaded', function(event, file, previewId, fileId, index, reader) {
    var fpreview = document.getElementById("haspreview");
    fpreview.value = 1;
    console.log("haspreview " + file);
  });
  $el2.on('fileremoved', function(event, id, index) {
    var fpreview = document.getElementById("haspreview");
    fpreview.value = 0;
    console.log("no haspreview");
  });  
  create50("","","","","");



});
</script>
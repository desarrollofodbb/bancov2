<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;

$this->title = "Crear cuenta";//$module->params["t_add_new"];
?>

<?php
    echo $this->render('_formperfil', [
                'model' => $model,
         ])
?>
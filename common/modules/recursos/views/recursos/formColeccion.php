<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;


/* @var $this yii\web\View */
/* @var $model app\models\Colecciones */
/* @var $form ActiveForm */
?>
<div class="basic-views-index">
   

    <?php $form = ActiveForm::begin(); $module = \Yii::$app->controller->module; $model = new \app\models\Colecciones(); ?>
        
        <?= $form->field($model, 'Nombre') ?>
        <?= $form->field($model, 'Slug') ?>
        <?= $form->field($model, 'Autor') ?>
        <?= $form->field($model, 'Estado') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- basic-views-index -->

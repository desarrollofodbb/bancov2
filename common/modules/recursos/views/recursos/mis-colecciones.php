<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
$userid = Yii::$app->user->identity->id;
if(isset($_GET["Autor"])){
  $userid = $_GET["Autor"];    
}
$user = new User();
$user = $user->findIdentity($userid);   
$resp = "No";
$textS = "Dejar de Seguir";
$class = "fa fa-thumbs-o-down";
$follower = "Eres seguidor";
if(!$user->isFollower($userid,Yii::$app->user->identity->id)){
  $resp = "Si";
  $textS = "Seguir";
  $class = "fa fa-thumbs-o-up";
  $follower = "No lo sigues";
}
if($userid==Yii::$app->user->identity->id){
  $follower = "";
}
$countCollections = $user->countCollections($userid);
$countResources = $user->countResources($userid);
$countFollowers = $user->countFollowers($userid);
?>
<!-- <link rel="stylesheet" href="css/demo.css"> -->
<style type="text/css">
.container {
  height: 150px;
  position: relative;
}
.center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
* {
  box-sizing: border-box;
}

body {
  font-family: "Source Sans Pro", sans-serif;
  line-height: 1.6;
  min-height: 100vh;
  display: grid;
  place-items: center;
}

.card {
  // box-shadow: 0 0 20px rgba(0,0,0,.2);
  width: 80%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  max-width: 600px;
  background: white;
  flex-basis: 250px;
  color: black;
  padding: 2em;
    border:none;
  text-align: center;
    border-radius: 5%;
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 2em;
  grid-template-areas: 
    "image"
    "name"    
    "description";
}


.profile-name     { grid-area: name; }
.profile-info     { grid-area: description; }
.profile-img      { grid-area: image; }




.profile-info {
  font-weight: 300;

}

.profile-name {
  grid-area: name;
  letter-spacing: 1px;
/*  font-size: 2rem;*/
  margin: .75em 0 0;
  line-height: 1;
}

/*.profile-name::after {
  content: '';
  display: block;
  width: 2em;
  height: 1px;
  background: #5bcbf0;
  margin: .5em auto .65em;
  opacity: .25;
}*/

.profile-position {
  text-transform: uppercase;
  font-size: .875rem;
  letter-spacing: 3px;
  margin: 0 0 .5vem;
  line-height: 1;
  color: #5bcbf0;
}

.profile-img {
  justify-self: center;
  max-width: 100%;
  border-radius: 50%;
  border: 2px solid white;
}

.social-list {
  justify-self: center;
  list-style: none;
  justify-content: space-between;
  display: flex;
  width: 75px;
  margin: .5em 0 0;
  padding: 0;
}

.social-link {
  color: #5bcbf0;
  opacity: .5;
}

.social-link:hover,
.social-link:focus {
  opacity: 1;
}

.bio-title {
  color: #0090D1;
  font-size: 1.25rem;
  // font-weight: 300;
  letter-spacing: 1px;
  text-transform: uppercase;
  line-height: 1;
  margin: 0;
}

.horizontal-center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 20%;
}

.vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

@media (min-width: 600px) {
  .card {
    text-align: left;
    grid-template-columns: 2fr 2fr;
    grid-template-areas:
      "image name"
      "image description"
  } 
  .profile-img{
    justify-self: right;
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);      
  margin-left: 35%;
  margin-right: auto;

  }
  .profile-name::after {
    margin-left: 0;
  }
</style>
<?php 
  // if ($misColecciones): 
?>
<div class="w3-container w3-content" style="max-width:1400px;margin-top:100px"> 
    <div class="w3-hide-large" style="margin-top:25px"></div>
    <input type="text" id="userid" hidden="" value="<?php echo Yii::$app->user->identity->id ?>">
    <input type="text" id="useridFollow" hidden="" value="<?php echo $userid ?>">
<div class="card">
    <?php 
      echo $user->getAvatarImg("perfil2");
    ?>  
    <h1 style="font-weight:bold;color:#00CC99" class="profile-name"><?php echo utf8_decode($user->nombre); ?></h1>
    <div class="profile-info">
     <h3 style="font-weight:normal;" class="no-margin" id="countfollowers" ><?php echo $countFollowers;?><?php if($countFollowers>1||$countFollowers==0){ echo " seguidores";} else{ echo " seguidor";} ?> </h3> 
     <h3 style="margin-top:5px;font-weight:normal;"><?php echo $countCollections ?> colecciones</h3> 
<!--      <h3 style="font-weight:normal;"  id="follower" ><?php echo $follower ?></h3> -->

                        <?php if($userid!=Yii::$app->user->identity->id){?>




                        <?php
                          $text2 = '<h3 style="margin-top:5px;font-weight:normal;">' . $countResources . ' recursos' . '</h3>';
                          echo Html::a($text2, Url::to(["/recursos/recursos/mis-recursos?Autor=" . Html::encode($userid) ])); 
                   
                        ?>   

<!--                              <div class="w3-container w3-white">  -->                        
        <button id="btn_seguir" name="btn_seguir" style="font-size: 16px;margin-top: 10px;padding-left: 30px; padding-right: 30px" class="w3-button_green  w3-round-xxlarge" param="<?php echo $resp; ?>" onclick="favoritos(this)"><?php echo $textS; ?></button>            
        </div>                                                          
<!--                             </div> -->
                                   
                        <?php } else{
                            
                          $text = '<h3 style="margin-top:5px;font-weight:normal;">' . $countResources . ' recursos' . '</h3>';
                          echo Html::a($text, Url::to(["/recursos/recursos/mis-recursos"])); 
                   
                        } ?>        
    </div>
</div>
           
   </div>

<div class="w3-contentnomargins w3-marginleft" style="padding-block-end: 150px;"> 

  <div class="wf-container">       

  <h2 style="font-weight: bold;margin-left: 5px; margin-top:25px; margin-bottom: 25px">Colecciones</h2>    

    <?php if ($countCollections>0): ?>

            <?php foreach ($misColecciones as $coleccion): ?>

                  <?php
                    echo $this->render("content/coleccion-relacionada", [
                        "model" => $coleccion
                    ]);
                  ?>
            <?php endforeach; ?>   

    <?php else: 
            
            echo '<h2 style="color:#646464;font-weight: bold;margin-top: 20px; text-align: center;">Todavía no tiene colecciones.</h2>';
       ?>     <br>
        
            <div style="text-align: center;">    
              <a href="<?php echo Yii::$app->homeUrl; ?>" type="button" class="w3-button_green w3-round-xxlarge w3-border-0" style="font-size: 15px;padding-bottom:5px;padding-top:5px;padding-right: 25px;padding-left: 25px;color: #FFFFFF;">Explore recursos para crear su primera colección</a>
            </div>  

            <div class="w3-third">                    
              <p style="color:white">B</p>
            </div>

    <?php endif; ?>                      



    </div>

</div>

<?php 
// endif; 
?>
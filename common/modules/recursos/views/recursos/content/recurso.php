<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */
?> 
<div class="lined-title">
    <div class="round-ico-wrap">
        <div class="round-ico big-ico">
            <?php
            $user = $model->autor;
            echo $user->getAvatarImg();
            ?>
        </div>
    </div>
    <h3>
        <?php
        $text = Html::encode($model->Nombre);
        $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $model->Slug]);
        $options = ["class" => ""];
        echo $model->isDeleted ? $text : Html::a($text, $url, $options);
        ?> 
    </h3>
</div>
<div class="lined-box <?php echo $model->isDeleted ? "recurso-eliminado" : ""; ?>">
    <p>
        <?php $model->printPublicacion(); ?>
    </p>
    <div class="body-lined">
        <?php if ($model->tagsValores): ?>
            <h4> 
                <?php
                $RelatedTags = $model->printTags("", []);
                ?> 
            </h4>
        <?php endif; ?>
        <?php
        $the_content = $model->ObjetivoDelRecurso;
        echo Yii::$app->CustomFunctionsHelper->getTheExcerpt($the_content, Yii::$app->params["excerptLengthRecursosListados"]);
        ?> 
    </div>
    <?php
    if (!$model->isDeleted) {
        $text = '<span class="icon icon-fod-icon-plus"></span>';
        $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $model->Slug]);
        $options = ["class" => "md-link fucsia-link"];
        echo Html::a($text, $url, $options);
    }
    ?>
</div>

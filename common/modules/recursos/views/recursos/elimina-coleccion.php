<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function eliminaColeccion($Slug, $Autor) {

        if(Yii::$app->db->createCommand()->delete('TColecciones', [
                 'Slug' => $Slug,
                 'Autor' => $Autor,
               ])->execute()){
            return "Coleccion eliminada correctamente";
        }
        return "No se pudo eliminar colección";

    }

    public function actionEliminaColeccion() {
      $Slug = $_POST["Slug"];
      $Autor = $_POST["Autor"];            
      $this->eliminaColeccion($Slug,$Autor);     
    }
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;

$this->title = $module->params["t_update_page"];
?>


            <?php
            echo
            $this->render('_form', [
                'model' => $model,
                'modelRecurso' => $modelRecurso,
                'caracteristicasDeRecursos' => $caracteristicasDeRecursos,
            ])
            ?> 


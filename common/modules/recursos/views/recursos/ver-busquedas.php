<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    function actionVerBusquedas() {

     $Nombre = ""; 
     $Autor = "";
     $AutorRecurso = "";
     $anioRecurso1 = "";
     $anioRecurso2 = "";
     $limit = 0; //Cantidad de Registros 
     $offset = 0; //Registros a saltar

     if(isset($_POST['limit'])){
       $limit = $_POST['limit']; 
     }

     if(isset($_POST['offset'])){
       $offset = $_POST['offset']; 
     }
     
     if(isset($_POST['Nombre'])){
       $Nombre = $_POST['Nombre']; 
     }
     if(isset($_POST['Autor'])){
       $Autor = $_POST['Autor']; 
     }
     if(isset($_POST['AutorRecurso'])){
       $AutorRecurso = $_POST['AutorRecurso']; 
     }     
     if(isset($_POST['anioRecurso1'])){
       $anioRecurso1 = $_POST['anioRecurso1']; 
     }          
     if(isset($_POST['anioRecurso2'])){
       $anioRecurso2 = $_POST['anioRecurso2']; 
     }   

      return $this->ViewBusquedas($Nombre,$Autor,$AutorRecurso,$anioRecurso1,$anioRecurso2,$limit,$offset);

    }   
<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

    /**
     * The user is follower yet
     * @param 
     * @return boolean
     */
    public function isRecursoColeccion($idColeccion, $idRecursos) {

     $count = (new Query())->select('idColeccion')
    ->from('TColeccionesRecursos')
    ->where(['idColeccion' =>$idColeccion])
    ->andWhere(['idRecursos' => $idRecursos])
    ->count();

        return $count == 1;
    }

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarRecursoColeccion($idColeccion, $idRecursos) {
      if(!$this->isRecursoColeccion($idColeccion,$idRecursos)){
        return Yii::$app->db->createCommand()->insert('TColeccionesRecursos', [
                 'idColeccion' => $idColeccion,
                 'idRecursos' => $idRecursos,
               ])->execute();        
      }
      else{
        return true;
      }
    }

    public function actionRecursoColeccion() {
      $idColeccion = $_POST["idColeccion"];
      $idRecursos = $_POST["idRecursos"];            
      $this->agregarRecursoColeccion($idColeccion,$idRecursos);     
    }
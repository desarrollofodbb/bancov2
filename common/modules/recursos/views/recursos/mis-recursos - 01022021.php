<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
$userid = Yii::$app->user->identity->id;
if(isset($_GET["Autor"])){
  $userid = $_GET["Autor"];    
}
$user = new User();
$user = $user->findIdentity($userid);   
$resp = "No";
$textS = "Dejar de seguir";
$class = "";
$follower = "Lo sigues";
if(!$user->isFollower($userid,Yii::$app->user->identity->id)){
  $resp = "Si";
  $textS = "Seguir";
  $class = "";
  $follower = "No lo sigues";
}
if($userid==Yii::$app->user->identity->id){
  $follower = "";
}
$countCollections = $user->countCollections($userid);
$countResources = $user->countResources($userid);
$countFollowers = $user->countFollowers($userid);

?>
<!-- <link rel="stylesheet" href="css/demo.css"> -->
<style type="text/css">

.container {
  height: 150px;
  position: relative;
}
.center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

* {
  box-sizing: border-box;
}

body {
  font-family: 'Fira Sans', sans-serif;
  line-height: 1.6;
  min-height: 100vh;
  display: grid;
  place-items: center;
}

.card {
  // box-shadow: 0 0 20px rgba(0,0,0,.2);
  width: 80%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  max-width: 600px;
  background: white;
  flex-basis: 250px;
  color: black;
  padding: 2em;
    border:none;
  text-align: center;
    border-radius: 5%;
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 2em;
  grid-template-areas: 
    "image"  
    "name"
    "description";
}


.profile-name     { grid-area: name; }
.profile-info     { grid-area: description; }
.profile-img      { grid-area: image; }




.profile-info {
  font-weight: 300;

}

.profile-name {
  grid-area: name;
  letter-spacing: 1px;
/*  font-size: 2rem;*/
  margin: .75em 0 0;
  line-height: 1;
}

/*.profile-name::after {
  content: '';
  display: block;
  width: 2em;
  height: 1px;
  background: #5bcbf0;
  margin: .5em auto .65em;
  opacity: .25;
}*/

.profile-position {
  text-transform: uppercase;
  font-size: .875rem;
  letter-spacing: 3px;
  margin: 0 0 .5vem;
  line-height: 1;
  color: #5bcbf0;
}

.profile-img {
  justify-self: center;
  max-width: 100%;
  border-radius: 50%;
  border: 2px solid white;
}

.social-list {
  justify-self: center;
  list-style: none;
  justify-content: space-between;
  display: flex;
  width: 75px;
  margin: .5em 0 0;
  padding: 0;
}

.social-link {
  color: #5bcbf0;
  opacity: .5;
}

.social-link:hover,
.social-link:focus {
  opacity: 1;
}

.bio-title {
  color: #0090D1;
  font-size: 1.25rem;
  // font-weight: 300;
  letter-spacing: 1px;
  text-transform: uppercase;
  line-height: 1;
  margin: 0;
}

.horizontal-center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
}

.vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

@media (min-width: 600px) {
  .card {
    text-align: left;
    grid-template-columns: 2fr 2fr;
    grid-template-areas:
      "image name"
      "image description"
  } 
  .profile-img{
    justify-self: right;
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);      
  margin-left: 35%;
  margin-right: auto;

  }
  .profile-name::after {
    margin-left: 0;
  }
}

</style>
<?php if ($misRecursos): ?>

<div class="w3-container w3-content" style="max-width:1400px;margin-top:90px">  
            <input type="text" id="userid" hidden="" value="<?php echo Yii::$app->user->identity->id ?>">
            <input type="text" id="useridFollow" hidden="" value="<?php echo $userid ?>">    
<div class="card">


        <?php 
          echo $user->getAvatarImg("perfil2");
        ?>           



    <h1 style="font-weight:bold;color:#00CC99" class="profile-name">
    <?php echo $user->nombre; ?>        
    </h1>

    <div class="profile-info">
            <h4 style="font-weight:normal;margin-top: 5px"  id="countfollowers" >    
            <h4 style="font-weight:normal;"  id="countfollowers" ><?php echo $countFollowers;?><?php if($countFollowers>1||$countFollowers==0){ echo " seguidores";} else{ echo " seguidor";} ?> </h4> 
            <h4 style="font-weight:normal;" ><?php echo $countResources ?> recursos</h4>     
            <?php if($userid!=Yii::$app->user->identity->id){?>     
                <?php if($countCollections>0){                   
                  $text = '<h4  style="font-weight:normal;">' . $countCollections . ' colecciones</h4>';
                  $url = "/recursos/recursos/mis-colecciones?Autor=" . Html::encode($userid);
                  echo Html::a($text, Url::to([$url])); 
                } else{?>
<!--                   <h4 style="font-weight:normal;"  style="text-align: left;">Sin colecciones</h4> -->
                <?php }?>     

            <?php } else{
                if($countCollections>0){
                    $text = '<h4  style="font-weight:normal">' . $countCollections . ' colecciones</h4>';
                    $url = "/recursos/recursos/mis-colecciones";
                    echo Html::a($text, Url::to([$url])); 
                } else{?>
<!--                     <h4  style="font-weight:normal;text-align: left;">Sin colecciones</h4> -->
                <?php }?>                                                 
            <?php } ?>
        <button id="btn_seguir" name="btn_seguir" style="font-size: 16px;margin-top: 10px;padding-left: 30px; padding-right: 30px" class="w3-button_green  w3-round-xxlarge" param="<?php echo $resp; ?>" onclick="favoritos(this)"><?php echo $textS; ?></button>            
        </div>               
    </div>
</div>    



<div class="w3-contentnomargins w3-marginleft">
   
  <div class="wf-container">         
    <div class="w3-container"> <h1 style="color:white">.</h1> </div>
    <h2 style="font-weight: bold;">Recursos</h2>      
    <div class="w3-container"> <h1 style="color:white">.</h1> </div>   
      <?php foreach ($misRecursos as $recurso): ?>
        <?php
          echo $this->render("/../widgets/views/content/recurso-relacionado", ["model" => $recurso]);
        ?>
      <?php endforeach; ?>
    </div>
</div>

<?php endif; ?>
<script type="text/javascript">
  ruta = "<?php echo Yii::$app->getUrlManager()->createUrl('/recursos/recursos/autor-favorito'); ?>";
  function favoritos(fieldbutton){
    var param = fieldbutton.getAttribute("param");
    autor1 = document.getElementById("useridFollow").value;
    autor2 = document.getElementById("userid").value;    

    $.ajax({
        url: ruta,
        type: "POST",
        data:{
          AutorFollow : autor1,
          AutorFan : autor2,
          param  : param
        },
        success: function (data) {
          button = document.getElementById("btn_seguir");
          follower = document.getElementById("follower");
          if(param=="Si"){
            button.setAttribute("param", "No");
            button.innerHTML  = "Dejar de seguir";
            follower.value = "Eres seguidor";
          }
          else{        
            button.setAttribute("param", "Si");
            button.innerHTML  = "Seguir";
            follower.value = "No lo sigues";
          }
        }
    });      
  }
</script>
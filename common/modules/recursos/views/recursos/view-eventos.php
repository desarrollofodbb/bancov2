<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    public function actionViewEventos() {
     $Autor = $_POST['Autor'];
     $query = (new Query)->select('TEventos.rIdUsuarios, user.nombre')
     ->from('TEventos')
     ->where(['Autor' => $Autor])
     ->andWhere(['Revisado' => NULL])
     ->leftJoin('user','TEventos.CreadoPor = user.id');

     $FollowQuery = (new Query)->select('rIdUsuarios')
     ->from('RAutoresSeguidores')
     ->where(['rIdSeguidores' => $Autor]);
     
     $EventsQuery = (new Query)->select('idEventos')
     ->from('TEventosSeguidores')
     ->where(['Autor' => $Autor]);

     $query2 = (new Query)->select('TEventos.rIdUsuarios, user.nombre')
     ->from('TEventos')
     ->where(['Autor' => $FollowQuery])
     ->andWhere(['not in', 'idEventos', $EventsQuery])

    $query->union($query2);
    $count=0;
    foreach ($query as $eventos) {
      $count++;
      $output .= '
  <li>
  <a href="#">
  <strong>'.$eventos["rIdUsuarios"].'</strong><br />
  <small><em>'.$row["nombre"].'</em></small>
  </a>
  </li>
';
    }
    if($count==0){
      $output .= '<li><a href="#" class="text-bold text-italic">No hay nuevas notificaciones</a></li>';      
    }
$data = array(
   'mensajes' => $output,
   'cantidad'  => $count
);
echo json_encode($data);

    return json_encode($data);
    }
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use common\widgets\Alert;
use pendalf89\tinymce\TinyMce;
use common\modules\recursos\models\CriteriosDeEvaluacion;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
?>


<div class="recursos-form">
    <br>
    <?php echo Alert::widget() ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'Nombre')->textInput(
                    [
//                            "placeholder" => "Nombre*",
                        "class" => "form-control round-form-control"
            ])->label("Nombre*");
            ?>
            <?php
            echo $form->field($model, 'ObjetivoDelRecurso')->textarea(
                    [
                        "rows" => 8,
                        "class" => "form-control round-form-control"
            ])->label("Objetivo del recurso*");
            ?>
            <fieldset>
                <?php
                $modelArchivoRelated = $model->archivo;
                $autor = $model->isNewRecord ? User::getCurrentUser() : $model->autor;
                echo $form->field($model, "Tipo")->dropDownList([
                    $model::TYPE_FILE => Yii::t("app", "Subir archivo"),
                    $model::TYPE_URL => Yii::t("app", "Agregar una URL"),
                    $model::TYPE_EMBED => Yii::t("app", "Embed"),
                        ], [
                    "prompt" => Yii::t("app", "Seleccione la fuente del recurso"),
                    "onchange" => ""
                    . "jQuery('div[id^=\"tipo-recurso-\"]').addClass('hidden');"
                    . "jQuery('#tipo-recurso-'+jQuery(this).val()).removeClass('hidden');",
//                    'class' => ""
                ])->hint(Yii::t("app", "Su espacio disponible para subir archivos es de: {tamano}", [
                            "tamano" => $autor->getUserRemainingSpace($modelArchivoRelated ? $modelArchivoRelated->Tamano : null)
                ]));
                ?>

                <div id="tipo-recurso-<?php echo $model::TYPE_FILE; ?>" class="<?php echo $model->Tipo == $model::TYPE_FILE ? "" : "hidden"; ?>">

                    <?php
                    $pluginOptions = [
                        'showRemove' => false,
                        'showUpload' => false,
                        'mainClass' => 'input-group-sm'
                    ];
                    if (!$model->isNewRecord && ($model->Tipo == $model::TYPE_FILE)) {
                        $pluginOptions['initialCaption'] = $model->Recurso;
//                                [
////                    'initialPreview' => [
////                        $model->getRecursoUrl(),
////                    ],
////                    'initialPreviewAsData' => true,
////                    'overwriteInitial' => true,
//                            
//                        ];
                    }
                    ?> 
                    <?php if (!$model->isNewRecord && ($model->Tipo == $model::TYPE_FILE)): ?>
                        <?php
                        echo $form->field($model, "Recurso")->hiddenInput()->label(false);
                        echo Html::a(Yii::t("app", "Descargar archivo actual"), $model->getRecursoUrl(), ["class" => "btn btn-sm btn-default", "target" => "_blank", "download" => ""]);
                        ?> 
                    <?php endif; ?> 
                    <?php
                    echo $form->field($modelRecurso, 'recursoFile')->widget(FileInput::classname(), [
                        'options' => [
                            'multiple' => true,
                        ],
                        'pluginOptions' => $pluginOptions
                    ])->label(false);
                    ?>  
                </div>

                <div id="tipo-recurso-<?php echo $model::TYPE_URL; ?>" class="<?php echo $model->Tipo == $model::TYPE_URL ? "" : "hidden"; ?>">
                    <?php
                    echo $form->field($model, 'url_recurso')->textInput(["class" => "form-control round-form-control"]);
                    ?>
                </div>

                <div id="tipo-recurso-<?php echo $model::TYPE_EMBED; ?>" class="<?php echo $model->Tipo == $model::TYPE_EMBED ? "" : "hidden"; ?>">
                    <?php
                    echo
                    $form->field($model, 'embed_recurso')->widget(TinyMce::className(), [
                        'clientOptions' => [
                            'language' => 'es',
                            'selector' => 'textarea.tinymce',
                            'plugins' => [
                                'media noneditable',
                            ],
                            'noneditable_editable_class' => 'mce-content-body',
                            'menubar' => false,
//                        'height' => 300,                       
                            'toolbar' => 'media',
                            'media_live_embeds' => true,
                            'media_alt_source' => false,
                            'media_poster' => false,
                            'media_dimensions' => false,
                            'valid_elements' => 'iframe[src|width|height|name|align]',
                            'extended_valid_elements' => 'iframe[src|width|height|name|align]',
                            'invalid_elements' => 'strong,b,em,i,p',
                        ],
                    ])->hint(Yii::t("app", "Asegurese de agregar un código embed válido"));
                    ?>
                </div>
            </fieldset>

            <?php
            echo
            $form->field($model, 'autores_ids')->widget(Select2::className(), [
                'model' => $model,
                'attribute' => 'autores_ids',
                'language' => 'es',
                'showToggleAll' => false,
                'data' => ArrayHelper::map(User::findByRole("autor"), 'id', 'username'),
                'options' => [
                    'multiple' => true,
                    "class" => "form-control round-form-control",
//                        "placeholder" => "Seleccione el autor o los autores",
                ],
                'pluginOptions' => [
//                'allowClear' => true,
                ],
                "changeOnReset" => false
            ])->label("Seleccione el autor o los autores");
            ?>
        </div> 
    </div>
    <div class="row">
        <?php
        foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
            ?> 
            <div class="col-xs-12 col-sm-4 col-ms-4 col-lg-4">
                <?php
                $valor_key = explode("-", $caracteristica_key);
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
  
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo
                $form->field($model, $caracteristica_field)->widget(Select2::className(), [
                    'model' => $model,
                    'attribute' => $caracteristica_field,
                    'language' => 'es',
                    'showToggleAll' => false,
                    'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                    'options' => [
                        'multiple' => $caracteristica_tipoSeleccion,
                        "class" => "form-control round-form-control",
                        "placeholder" => $placeholder,
                    ],
                    'pluginOptions' => [
//                'allowClear' => true,
                    ],
                ])->label($caracteristica_nombre);
                ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row criterios-de-evaluacion banco-ckeckboxlist"> 
        <div class="col col-md-12">
            <?php
            echo $form->field($model, "criterios_ids")
                    ->checkboxList(ArrayHelper::map(CriteriosDeEvaluacion::find()->select(["Nombre", "idCriteriosDeEvaluacion"])->all(), "idCriteriosDeEvaluacion", "Nombre"));
            ?>
        </div>
    </div>

    <?php
    echo Html::submitButton($model->isNewRecord ? $module->params["a_publish"] : $module->params["a_update"], [
        'class' => $model->isNewRecord ? 'btn btn-default' : 'btn btn-primary',
    ])
    ?>

    <?php ActiveForm::end(); ?>
</div>

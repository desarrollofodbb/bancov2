<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */

    /**
     * The user is follower yet
     * @param 
     * @return boolean
     */
    public function isFollower($AutorFollow, $AutorFan) {

     $count = (new Query())->select('rIdUsuarios')
    ->from('RAutoresSeguidores')
    ->where(['rIdUsuarios' =>$AutorFollow])
    ->andWhere(['rIdSeguidores' => $AutorFan])
    ->count();

        return $count == 1;
    }

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarFavoritos($AutorFollow, $AutorFan) {
      if(!$this->isFollower($AutorFan,$AutorFollow)){
        return Yii::$app->db->createCommand()->insert('RAutoresSeguidores', [
                 'rIdUsuarios' => $AutorFollow,
                 'rIdSeguidores' => $AutorFan,
               ])->execute();        
      }
      else{
        return true;
      }
    }
    public function actionAutorFavorito($AutorFollow, $AutorFan, $param) {
      if ($param=="Si"){
        $this->agregarFavoritos($AutorFollow,$AutorFan);     
      }
    }
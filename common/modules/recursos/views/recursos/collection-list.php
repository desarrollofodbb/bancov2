<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use common\modules\recursos\models\Colecciones
use common\models\User;;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    public function actionCollectionList($q = null, $id = null) {
        $Autor = Yii::$app->user->identity->id;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '0', 'text' => 'Crear colección']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('idColeccion AS id, Nombre AS text')
                ->from('TColecciones')
                ->where(['like', 'Nombre', $q])
                ->andWhere(["[TColecciones].[Autor]" => $Autor])
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        else {
            $query = new Query;
            $query->select('idColeccion AS id, Nombre AS text')
                ->from('TColecciones')
                ->andWhere(["[TColecciones].[Autor]" => $Autor])
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);

        }
    
        return $out;
    }
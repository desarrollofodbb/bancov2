<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use common\modules\recursos\widgets\RecursosRelacionados;
use common\modules\recursos\models\Favoritos;
use common\modules\recursos\models\Denuncias;
use common\modules\recursos\models\Recursos;
use common\widgets\Alert;
use common\modules\yii2comments\widgets\Comment;
use kartik\rating\StarRating;
use yii\widgets\ActiveForm;

$this->title = $model->Nombre;
/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */
/* @var $form yii\widgets\ActiveForm */
$user = $model->autor;  
$userid = Yii::$app->user->identity->id;
if(isset($_GET["Autor"])){
  $userid = $_GET["Autor"];    
}      
$countResources = $user->countResources($userid,$model->Slug);
$titleresources = $countResources . " recursos";
if($countResources==1){
  $titleresources = "1 recurso";
}
$actual_link = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
echo $actual_link;
?>
<style>
.container {
  height: 150px;
  position: relative;
}
.center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
* {
  box-sizing: border-box;
}

body {
  font-family: 'Fira Sans', sans-serif;
  line-height: 1.6;
  min-height: 100vh;
  display: grid;
  place-items: center;
}

.card {
  // box-shadow: 0 0 20px rgba(0,0,0,.2);
  width: 80%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  max-width: 600px;
  background: white;
  flex-basis: 250px;
  color: black;
  padding: 2em;
    border:none;
  text-align: center;
    border-radius: 5%;
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 2em;
  grid-template-areas: 
    "description"
    "image"
    "name";
}


.profile-name     { grid-area: name; }
.profile-info     { grid-area: description; }
.profile-img      { grid-area: image; }

.profile-info {
  font-weight: 300;

}

.profile-name {
  grid-area: name;
  letter-spacing: 1px;
/*  font-size: 2rem;*/
  margin: .75em 0 0;
  line-height: 1;
}

/*.profile-name::after {
  content: '';
  display: block;
  width: 2em;
  height: 1px;
  background: #5bcbf0;
  margin: .5em auto .65em;
  opacity: .25;
}*/

.profile-position {
  text-transform: uppercase;
  font-size: .875rem;
  letter-spacing: 3px;
  margin: 0 0 .5vem;
  line-height: 1;
  color: #5bcbf0;
}

.profile-img {
  justify-self: center;
  max-width: 100%;
  border-radius: 50%;
  border: 2px solid white;
}

.social-list {
  justify-self: center;
  list-style: none;
  justify-content: space-between;
  display: flex;
  width: 75px;
  margin: .5em 0 0;
  padding: 0;
}

.social-link {
  color: #5bcbf0;
  opacity: .5;
}

.social-link:hover,
.social-link:focus {
  opacity: 1;
}

.bio-title {
  color: #0090D1;
  font-size: 1.25rem;
  // font-weight: 300;
  letter-spacing: 1px;
  text-transform: uppercase;
  line-height: 1;
  margin: 0;
}

.horizontal-center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
}

.vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
#socialShare {
  width: 100%;
  margin-top: 15px;
  text-align: center;
}

#socialShare a,
#socialShare > .socialBox {
  position: relative;
  float: none;
  display: inline-block;
  color: #fff;
  font-size: 20px;
  padding: 5px;
  background-color: transparent;
  width: 40px;
  height: 40px;
  line-height: 40px;
  text-align: center;
  border-radius: 50%;
}

#socialShare  a {
  background-color: rgba(0, 0, 0, 0.2);
}

#socialShare > * > span {
  background-color: rgba(0, 0, 0, 0.6);
  box-shadow: 0 0 0 5px rgba(0, 0, 0, 0.15);
  display: block;
  color: #fff;
  font-size: 16px;
  padding: 8px;
  width: 24px;
  height: 24px;
  line-height: 24px;
  text-align: center;
  border-radius: 50%;
  -webkit-transform: scale(1);
  -moz-transform: scale(1);
  transform: scale(1);
  -webkit-transition: all .35s ease-in-out;
  -moz-transition: all .35s ease-in-out;
  transition: all .35s ease-in-out;
}
#socialShare > * > span:hover,
#socialShare > .open > span {
  -webkit-transform: scale(1.25);
  -moz-transform: scale(1.25);
  transform: scale(1.25);
  -webkit-transition: all .15s ease-in-out;
  -moz-transition: all .15s ease-in-out;
  transition: all .15s ease-in-out;
}

#socialGallery {
  left: 50%;
  margin: 0 auto 0;
  position: absolute;
  top: 60px;
  transform: translate(-50%, 0);
  visibility: hidden;
  width: 400px;
}

#socialGallery a {
  visibility: hidden;
  opacity: 0;
  margin: 5px 2px;
  background-color: rgba(0, 0, 0, 0.6);
  position: relative;
  top: 10px;
}

#socialGallery a > span {
  position: relative;
  top: 4px;
  left: 4px;
  border-radius: 50%;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
}

.socialToolBox {
  cursor: default;
}

.pointer {
  cursor: pointer
}

.facebook:hover {
  background: #3b5998 !important
}

.google:hover {
  background: #dd4b39 !important
}

.instagram:hover {
  background: #517fa4 !important
}

.tumblr:hover {
  background: #32506d !important
}

.twitter:hover {
  background: #00aced !important
}

.youtube:hover {
  background: #bb0000 !important
}

@media (min-width: 600px) {
  .card {
    text-align: left;
    grid-template-columns: 2fr 2fr;
    grid-template-areas:
      "image name"
      "image description"
  } 
  .profile-img{
    justify-self: right;
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);      
  margin-left: 45%;
  margin-right: auto;

  }
  .profile-name::after {
    margin-left: 0;
  }
</style>

<div class="w3-container">
  <div id="id05" class="w3-modal">
    <div class="w3-modal-content w3-animate-zoom w3-round-xlarge" style="width:375px;max-height:775px;">
      <div class="w3-container w3-center w3-padding">
      <span style="color: #00CC99" onclick="cierra('id05')" class="w3-button w3-xlarge w3-hover-opacity w3-display-topright" title="Cerrar / Cancelar"><i style="color: black;font-size: 20px">&times;</i></span>
        <h2 style="color:#666666;font-weight: bold;margin-top: 20px;margin-bottom: 20px  ">¿Desea eliminar la colección?</h2>
      </div>        
      <div class="w3-container w3-center">
          <button id="btn_cancelar" name="btn_cancelar" style="font-size: 15px;padding-bottom:5px;padding-top:5px;padding-right: 25px;padding-left: 25px" class="w3-button_grayligth w3-round-xxlarge w3-border-0" onclick="cierra('id05')"> Cancelar</button>
          <button id="btn_eliminacoleccion" name="btn_eliminacoleccion" class="w3-button_green w3-round-xxlarge w3-border-0" style="font-size: 15px;padding-bottom:5px;padding-top:5px;padding-right: 25px;padding-left: 25px" onclick="eliminacoleccion()"> Aceptar</button>
      </div>
          <input class="w3-round-xxlarge" style="outline:none;height:35px; border:none;width: 100%;background-color: #EBEDEC;margin-left: 0px;color:#333333;padding-left: 5px;padding-right: 5px;font-size: 15px; " maxlength="50"; type="text" id="idEliColeccion" value="<?php echo $model->Slug ?>" hidden>
          <br><br>      
    </div>
  </div>     
</div>

<div class="w3-container w3-content" style="max-width:1400px;margin-top:90px">  
<div class="w3-quarter">
  .
</div>
<div class="w3-half">
  <div class="w3-center w3-content">
    <div class="w3-dropdown-hover w3-hide-small">
      <div class="row">
        <h2 class="w3-padding" style="font-weight:bold">
          <?php echo $model->Nombre; 
          $len = strlen($model->Nombre);
          if($len > 15){
            $left = 245 + $len;
          }
          else{
            $left = 145 + $len;
          }
          ?>  
        </h2> 
        <h2>
        <button id="zoom-but4" onclick="myClick(this)" class="w3-button w3-round-xxlarge" title="Compartir">
          <i style="color:#717070" class="fa fa-share-alt"></i>
        </button>
        </h2>   
      
      <div id="zoom2" class="w3-dropdown-content w3-bar-block w3-card-4 w3-animate-zoom w3-round-xxlarge " style="height: 50px; margin-bottom: 10px;left:<?php echo $left .'px'; ?>;z-index: 1; ">
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px; margin-top: 10px;background-color:#00CC99" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link ?>"><i class="fa fa-facebook-f" style="color:white;font-size: 25px"></i></a>
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://api.whatsapp.com/send?text=<?php echo "Una colección interesante -> " . $actual_link ?>"><i class="fa fa-whatsapp" style="color:white;font-size: 25px"></i></a>
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://twitter.com/intent/tweet?text= Una colección interesante -> &url=<?php echo $actual_link ?>"><i class="fa fa-twitter" style="color:white;font-size: 25px"></i></a>          
        </div> 
        </div> 
  </div>
  <div class="w3-dropdown-hover w3-hide-medium w3-hide-large">
  <h2 style="margin-top:5px;font-weight:bold;"><?php echo $model->Nombre; ?>    <button id="zoom-but4" onclick="myClick(this)" class="w3-button w3-round-xxlarge" title="Compartir"><i style="color:#717070" class="fa fa-share-alt"></i></button></h2> 
        <div id="zoom2" class="w3-dropdown-content w3-bar-block w3-card-4 w3-animate-zoom w3-round-xxlarge " style="height: 50px; margin-bottom: 10px;left:240px">
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link ?>" target="_blank"><i class="fa fa-facebook-f" style="color:white;font-size: 25px"></i></a>
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://api.whatsapp.com/send?text=<?php echo "Una colección interesante -> " . $actual_link ?>" target="_blank"><i class="fa fa-whatsapp" style="color:white;font-size: 25px"></i></a>
          <a class="btn btn-success btn-circle btn-sm" style="margin-right:5px;margin-left:5px;margin-top: 10px;background-color:#00CC99" href="https://twitter.com/intent/tweet?text= Una colección interesante -> &url=<?php echo $actual_link ?>" target="_blank"><i class="fa fa-twitter" style="color:white;font-size: 25px"></i></a>          
        </div>  
  </div>
  </div>
  <div class="w3-center w3-content w3-padding w3-hide-small">
    

    
    <div class="w3-row">  
    <div class="col-sm-2"> 
    </div>  
    <div class="col-sm-4">  
    <h4 style="text-align: right;"><?php echo $titleresources ?></h4>
    </div>
    <div class="col-sm-4">  
    <h4 style="text-align: left;"><a onclick="document.getElementById('id05').style.display='block';" href="#">Eliminar</a></h4>
    </div>
    <div class="col-sm-2"> 
    </div>      
    </div>

     
  </div>
  <div class="w3-center w3-content w3-padding w3-hide-medium w3-hide-large">
    <div class="col-sm-2" style="color:white">x</div>  
    <div class="col-sm-4" ><h4><?php echo $titleresources ?></h4></div>
    <div class="col-sm-4" onclick="document.getElementById('id05').style.display='block';"><h4><a href="#">Eliminar colección</a></h4></div>
  
    <div class="col-sm-2" style="color:white">x</div>  
  </div>
  <div class="col-sm-12 w3-center w3-content w3-padding" style="margin-top: 10px">
    <?php 
      echo $user->getAvatarImg("perfil3");
    ?>  
  </div>   
  <div class="w3-center w3-content w3-padding">
  <h3 style="font-weight:bold;color: #666666" class="profile-name"><?php echo $user->nombre; ?></h3>  
  </div>
</div>
<div class="w3-content">
      <a id="miscolecciones" class="w3-bar-item w3-button fa fa-user fa-2x  w3-round-xxlarge"  style="text-decoration: none;color:#717070" data-toggle="tooltip" title = "Mis Colecciones" href="/recursos/recursos/mis-colecciones" hidden="">
      </a> 
</div>



</div>

</div>

                <?php echo Alert::widget() ?>
                   
                <div class="resultados-tool-bar">

                    <?php if (Yii::$app->user->can("/recursos/recursos/actualizar-coleccion", ["coleccion" => $model->Slug])): ?>
                        <a href="<?php echo Url::to(["/recursos/recursos/actualizar-coleccion", "coleccion" => $model->Slug]) ?>" class="fucsia-link sm-link">
                            <span class="glyphicon glyphicon-pencil"></span>
                            <?php echo Yii::t("app", "Editar recurso"); ?>
                        </a> 

                    <?php endif; ?>
                    <?php if (Yii::$app->user->can("/recursos/recursos/delete_coleccion", ["coleccion" => $model->Slug])): ?>

                        <?php
                        $eliminarText = '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', "Eliminar coleccion");
                        echo
                        Html::a($eliminarText, ['/recursos/recursos/delete_coleccion', "coleccion" => $model->Slug], [
                            'data' => [
                                'method' => 'post',
                                "confirm" => Yii::t("app", "¿Está seguro que desea eliminar la coleccion?"),
                                'params' =>
                                [
                                    'recurso' => $model->Slug,
                                ],
                            ],
                            "class" => 'fucsia-link sm-link',
                        ]);
                        ?>

                    <?php endif; ?> 
                </div> 


<div class="w3-content" style="margin-left: 15px"> 
  <h4 style="padding-top: 15px; padding-bottom: 15px; font-weight: bold; color:#666666">Recursos de la colección</h4>   
        <?php

        echo RecursosRelacionados::widget(["coleccion" => $model]);
        ?> 
</div>

<script type="text/javascript">

function cierra(param){
  document.getElementById(param).style.display='none'    
}  
  function eliminacoleccion(){

    var Slug  = document.getElementById("idEliColeccion").value;  
    var Autor = <?php echo Yii::$app->user->identity->id ?>;    

    rutacol = "/recursos/recursos/elimina-coleccion";
    $.ajax({        
      url: rutacol,
      type: "POST",
      data:{
        Slug : Slug,
        Autor : Autor
      },
      success: function (data) {
        alert("Colección eliminada");
        document.getElementById("miscolecciones").click();
      }
    });      
  }
</script>
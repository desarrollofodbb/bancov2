<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\RecursosAdvancedSearch */
/* @var $form yii\widgets\ActiveForm */

$module = \Yii::$app->controller->module;
?>

<div class="advanced-search-box">
    <h2 class="top-space">
        <?php Yii::t("app", "¿Qué desea encontrar?"); ?>
    </h2>
    <?php
    $form = ActiveForm::begin([
                'action' => ['/recursos/recursos/resultados-de-busqueda'],
                'method' => 'get',
    ]);
    ?>
    <div class="row adv-search-form">
        <?php
        foreach ($model->getValoresPorCaracteristicas() as $caracteristica_key => $valoresDeCaracteristica):
            ?>
            <div class="col-xs-12 col-sm-4 col-ms-4 col-lg-4">
                <div class="form-group"> 
                    <?php
                    $valor_key = explode("-", $caracteristica_key);
                    $caracteristica_tipoSeleccion = false;
                    $caracteristica_field = $valor_key[1];
                    $caracteristica_nombre = $valor_key[2];
                    $placeholder = "Seleccione " . strtolower(str_replace("_", " ", $caracteristica_nombre));
                    ?>
                    <?php
                    $placeholder = "Seleccione " . strtolower(str_replace("_", " ", $caracteristica_nombre));
                    echo
                    $form->field($model, $caracteristica_field)->widget(Select2::className(), [
                        'model' => $model,
                        'attribute' => $caracteristica_field,
                        'language' => 'es',
                        'showToggleAll' => false,
                        'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                        'options' => [
                            'multiple' => $caracteristica_tipoSeleccion,
//                            'class' => "selectpicker search-input form-control",
                            "placeholder" => $placeholder,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label($caracteristica_nombre);
                    ?>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="col-xs-12 form-group adv-srch-input">
            <?php
            $options = [
                "placeholder" => Yii::t("app", "Escriba el nombre del recurso que busca..."),
                "class" => "form-control search-input",
            ];

            echo $form->field($model, "Nombre")->textInput($options)->label(Html::encode(Yii::t("app", "Buscar")));
            ?>
        </div>

        <div class="col-xs-12">
            <?php echo Html::submitButton($module->params["a_search"], ['class' => 'btn btn-default form-group']) ?>
            <?php // echo Html::resetButton($module->params["a_reset"], ['class' => 'btn btn-primary form-group']) ?>
        </div> 
    </div>
    <?php ActiveForm::end(); ?>
</div>

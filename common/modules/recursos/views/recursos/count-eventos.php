<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    public function actionCountEventos() {
      $Autor = $_POST['Autor'];
     $count = (new Query())->select('rIdUsuarios')
    ->from('TEventos')
    ->where(['Autor' => $Autor])
    ->andWhere(['Revisado' => NULL])
    ->count();

     $FollowQuery = (new Query)->select('rIdUsuarios')
     ->from('RAutoresSeguidores')
     ->where(['rIdSeguidores' => $Autor]);
     
     $EventsQuery = (new Query)->select('idEventos')
     ->from('TEventosSeguidores')
     ->where(['Autor' => $Autor]);

     $count2 = (new Query())->select('rIdUsuarios')
    ->from('TEventos')
    ->where(['Autor' => $FollowQuery])

    ->count();     
        
    return $count + $count2;
    }
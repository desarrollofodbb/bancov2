<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
$userid = Yii::$app->user->identity->id;


if(isset($_GET["Autor"])){
  $userid = $_GET["Autor"];    
}
$user = new User();
$user = $user->findIdentity($userid);   
$resp = "No";
$textS = "Dejar de seguir";
$class = "";
$follower = "Lo sigues";
if(!$user->isFollower($userid,Yii::$app->user->identity->id)){
  $resp = "Si";
  $textS = "Seguir";
  $class = "";
  $follower = "No lo sigues";
}
if($userid==Yii::$app->user->identity->id){
  $follower = "";
}
$countCollections = $user->countCollections($userid);
$countResources = $user->countResources($userid);
$countFollowers = $user->countFollowers($userid);

?>
<!-- <link rel="stylesheet" href="css/demo.css"> -->
<style type="text/css">

.container {
  height: 150px;
  position: relative;
}
.center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

* {
  box-sizing: border-box;
}

body {
  font-family: "Source Sans Pro", sans-serif;
  line-height: 1.6;
  min-height: 100vh;
  display: grid;
  place-items: center;
}

.card {
  // box-shadow: 0 0 20px rgba(0,0,0,.2);
  width: 80%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  max-width: 600px;
  background: white;
  flex-basis: 250px;
  color: black;
  padding: 2em;
    border:none;
  text-align: center;
    border-radius: 5%;
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 2em;
  grid-template-areas: 
    "image"  
    "name"
    "description";
}


.profile-name     { grid-area: name; }
.profile-info     { grid-area: description; }
.profile-img      { grid-area: image; }




.profile-info {
  font-weight: 300;

}

.profile-name {
  grid-area: name;
  letter-spacing: 1px;
/*  font-size: 2rem;*/
  margin: .75em 0 0;
  line-height: 1;
}

/*.profile-name::after {
  content: '';
  display: block;
  width: 2em;
  height: 1px;
  background: #5bcbf0;
  margin: .5em auto .65em;
  opacity: .25;
}*/

.profile-position {
  text-transform: uppercase;
  font-size: .875rem;
  letter-spacing: 3px;
  margin: 0 0 .5vem;
  line-height: 1;
  color: #5bcbf0;
}

.profile-img {
  justify-self: center;
  max-width: 100%;
  border-radius: 50%;
  border: 2px solid white;
}

.social-list {
  justify-self: center;
  list-style: none;
  justify-content: space-between;
  display: flex;
  width: 75px;
  margin: .5em 0 0;
  padding: 0;
}

.social-link {
  color: #5bcbf0;
  opacity: .5;
}

.social-link:hover,
.social-link:focus {
  opacity: 1;
}

.bio-title {
  color: #0090D1;
  font-size: 1.25rem;
  // font-weight: 300;
  letter-spacing: 1px;
  text-transform: uppercase;
  line-height: 1;
  margin: 0;
}

.horizontal-center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
}

.vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

@media (min-width: 600px) {
  .card {
    text-align: left;
    grid-template-columns: 2fr 2fr;
    grid-template-areas:
      "image name"
      "image description"
  } 
  .profile-img{
    justify-self: right;
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);      
  margin-left: 35%;
  margin-right: auto;

  }
  .profile-name::after {
    margin-left: 0;
  }
}

</style>
<?php if ($misRecursos): ?>

<div class="w3-container w3-content" style="max-width:1400px;margin-top:90px">    <div class="w3-hide-large" style="margin-top:25px"></div>  
            <input type="text" id="userid" hidden="" value="<?php echo Yii::$app->user->identity->id ?>">
            <input type="text" id="useridFollow" hidden="" value="<?php echo $userid ?>">    
<div class="card">


        <?php 
          echo $user->getAvatarImg("perfil2");
        ?>           

    <h1 style="font-weight:bold;color:#00CC99" class="profile-name">
    <?php echo utf8_decode($user->nombre); ?>        
    </h1>

    <div class="profile-info">
            <h3 style="font-weight:normal;margin-top: 5px"  id="countfollowers" ></h3>    
            <h3 style="font-weight:normal;"  id="countfollowers" ><?php echo $countFollowers;?><?php if($countFollowers>1||$countFollowers==0){ echo " seguidores";} else{ echo " seguidor";} ?> </h3> 
            <h3 style="font-weight:normal;" ><?php echo $countResources ?> recursos</h3>     
            <?php if($userid!=Yii::$app->user->identity->id){?>     
                <?php if($countCollections>0){                   
                  $text = '<h3  style="font-weight:normal;">' . $countCollections . ' colecciones</h3>';
                  $url = "/recursos/recursos/mis-colecciones?Autor=" . Html::encode($userid);
                  echo Html::a($text, Url::to([$url])); 
                } else{?>
<!--                   <h4 style="font-weight:normal;"  style="text-align: left;">Sin colecciones</h4> -->
                <?php }?>     

            <?php } else{
                if($countCollections>0){
                    $text = '<h3  style="font-weight:normal">' . $countCollections . ' colecciones</h3>';
                    $url = "/recursos/recursos/mis-colecciones";
                    echo Html::a($text, Url::to([$url])); 
                } else{?>
<!--                     <h4  style="font-weight:normal;text-align: left;">Sin colecciones</h4> -->
                <?php }?>                                                 
            <?php } ?>
        <button id="btn_seguir" name="btn_seguir" style="font-size: 16px;margin-top: 10px;padding-left: 30px; padding-right: 30px" class="w3-button_green  w3-round-xxlarge" param="<?php echo $resp; ?>" onclick="favoritos(this)"><?php echo $textS; ?></button>            
        </div>               
    </div>
</div>    



<div class="w3-contentnomargins w3-marginleft" >
  <h2 style="font-weight: bold;margin-left: 15px; margin-top:20px; margin-bottom: 20px">Recursos</h2>    
  <div class="wf-container" id="mis-recursos" style="padding-block-end: 150px;">         


<!--        <?php foreach ($misRecursos as $recurso): ?> -->
        <?php
           echo $this->render("/../widgets/views/content/recurso-relacionado", ["model" => $recurso]);
        ?>
<!--       <?php endforeach; ?>  -->
    </div>
</div>

<?php endif; ?>

<div id="modal01" class="w3-modal" onclick="this.style.display='none'">
  <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
  <div class="w3-modal-content w3-animate-zoom">
    <img id="img01" style="width:100%" class="w3-round-xxlarge">
  </div>
</div>
<div id="snackbar"></div>
<script>
function onZoom(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
}
</script>
<script type="text/javascript">
//*******


  newsearch = 1;
  offset = 0;
  limit = 10;
  morescroll = 0;
  user = <?php echo $userid?>;
  inscroll = 0;


  $( document ).ready(function() {
   
     resultadomisrecursos(user);

  });

$(window).on('mousewheel', function(){
    ignoreNextScrollEvent = false;  
});
 
  window.onscroll = function() {
    if(ignoreNextScrollEvent==true){
      ignoreNextScrollEvent = false;
      // console.log("ignoring scroll SP:");
      return;
    }
    var totalPageHeight = document.body.scrollHeight - 10; 
    var scrollPoint = window.scrollY + window.innerHeight;
// console.log(scrollPoint + " tP " + totalPageHeight + " MS " + morescroll + " IS " + inscroll + " INS " + ignoreNextScrollEvent + " offset " + offset + " totalregs " + totalregs)
    if(scrollPoint >= totalPageHeight && morescroll==1 && inscroll==0 && ignoreNextScrollEvent==false && offset <= totalregs && totalregs > 0){
      inscroll=1;
// console.log("entro");
      idbusqueda = document.getElementById("mis-recursos");
      rutadata2 = "/recursos/recursos/ver-busquedas";
       
        // alert(buscar_nombre);
      clearInterval(veravi);

      $.ajax({
          url: rutadata2,
          type: "POST",
          data:{
            Nombre : "MR:" + user,
            Tag : "",
            Tipo : "",
            anio1 : "",
            anio2 : "",
            Autor : "",
            Dirigido : "",
            Nivel : "",
            Asignatura : "",
            Idioma : "",
            Usos : "",
            limit : 15,
            offset : offset
          },
          success: function (datacontent) {
            var obj = JSON.parse(datacontent);
            offset = offset + obj.cantidad;
            if (obj.cantidad>0){
              totalregs = obj.total;
              var waterfall = new Waterfall({ minBoxWidth: 200 });
              for (i in obj.resultados) {
                box = nuevoNodo(obj.resultados[i].preview,obj.resultados[i].tipo,obj.resultados[i].text,obj.resultados[i].url);   
                waterfall.addBox(box);                        
                $('#mis-recursos').append(waterfall);  
              } 
         
              // console.log("agregadas");
              if(obj.cantidad < 15){
                mensajefade = "Mostrando últimos " + obj.cantidad + " recursos encontrados.";
              }
              else{
                mensajefade = "Mostrando " + obj.cantidad + " recursos adicionales de un total de " + totalregs + " disponibles";
              }

              fademessage(mensajefade);
              var elmnt = document.getElementById("mis-recursos");
              elmnt.scrollIntoView(false);
              elmnt.scrollTop = elmnt.scrollHeight;
              ignoreNextScrollEvent = true;
              inscroll=0; 

              // veravi = VerificarAvisos(30000);
              if(offset==0){
                offset = obj.cantidad;   
              }
              if(obj.cantidad < 15){
                morescroll = 0;       
                totalregs = 0;       
              }         
            }
            else{
              inscroll=0;
              offset = 0;
              totalregs = 0;
              // veravi = VerificarAvisos(30000);
              morescroll = 0;
              mensajefade = "No se encontraron más coincidencias";
              fademessage(mensajefade);
            }
             
          },
          error: function (x,z,t){
            morescroll = 0;
            inscroll=0;
            // veravi = VerificarAvisos(30000);            
          }
      }); 
    }
    else{
      // console.log("")
    }
    
  };



function fademessage(text) {
  // Get the snackbar DIV
  var x = document.getElementById("snackbar");

  // Add the "show" class to DIV
  x.innerHTML  = text;
  x.className = "show";


  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

  function nuevoNodo(preview,tipo,text,url) {
    var box = document.createElement('div');
    box.className = 'wf-box';  
    box.style = 'margin-top:15px';          
    var divimage = document.createElement('div');
    divimage.className = 'image';
    var image = document.createElement('img');
    image.className = "w3-border w3-round-xlarge w3-hover-opacity";
    image.style = "width:100%;height:100%; border-radius:10%;cursor:pointer";

//     image.addEventListener("click", function(){
//       onZoom(this);
// });
    image.src = preview;

    var aTag = document.createElement('a');
    aTag.setAttribute('href', url);
    aTag.setAttribute('target','_self');

    
    aTag.appendChild(image);    
    // divimage.appendChild(image);            

    divimage.appendChild(aTag);            
    box.appendChild(divimage);
    var content = document.createElement('div');
    content.className = 'content';
    content.style = "margin-top:10px;margin-bottom:10px";
    var title = document.createElement('h3');
    title.style = "color:#999999;font-family: 'Source Sans Pro', sans-serif;font-size:18px";
    title.appendChild(document.createTextNode(tipo));
    content.appendChild(title);
    len = text.length;
    text = text.substring(0, 50);
    if(len>50){
      text = text + "...";  
    }
    var p = document.createElement('h4');
    p.style = "color:black;font-family: 'Source Sans Pro', sans-serif;font-size:16px";
    var aTag = document.createElement('a');
    aTag.setAttribute('href', url);
    aTag.setAttribute('target','_self');         
    aTag.appendChild(document.createTextNode(text));
    p.appendChild(aTag);
    content.appendChild(p);
    box.appendChild(content);    

    return box;
  }  

  function resultadomisrecursos(user){

    idbusqueda = document.getElementById("mis-recursos");
    rutadata1 = "/recursos/recursos/ver-busquedas";



    clearInterval(veravi);
    $.ajax({
        url: rutadata1,
        type: "POST",
        data:{
          Nombre : "MR:" + user,
          Tag : "",
          Tipo : "",
          anio1 : "",
          anio2 : "",
          Autor : "",
          Dirigido : "",
          Nivel : "",
          Asignatura : "",
          Idioma : "",
          Usos : "",
          limit : 15,
          offset : 0
        },
        success: function (datacontent) {
          var obj = JSON.parse(datacontent);

          if (obj.cantidad>0){
            totalregs = obj.total;
            $('#mis-recursos').empty();
            var waterfall = new Waterfall({ minBoxWidth: 200 });
            for (i in obj.resultados) {
              box = nuevoNodo(obj.resultados[i].preview,obj.resultados[i].tipo,obj.resultados[i].text,obj.resultados[i].url);   
              waterfall.addBox(box);        
               $('#mis-recursos').append(waterfall);  
            }  

            // veravi = VerificarAvisos(30000);
            offset = obj.cantidad;  
            morescroll = 1;        
            if(obj.cantidad < 15){
               morescroll = 0;
            }               
          }
          else{
            offset = 0;
            // veravi = VerificarAvisos(30000);
            morescroll = 0;
            // alert("No se encontraron coincidencias");
        
          }
         
        //  $('#mis-recursos').html(obj.mensajes);
        },
        error: function (x,z,t){

          morescroll = 0;
          // veravi = VerificarAvisos(30000);            
         //   debugger;
        }
    }); 

  }
//*******
  ruta = "<?php echo Yii::$app->getUrlManager()->createUrl('/recursos/recursos/autor-favorito'); ?>";
  function favoritos(fieldbutton){
    var param = fieldbutton.getAttribute("param");
    autor1 = document.getElementById("useridFollow").value;
    autor2 = document.getElementById("userid").value;    

    $.ajax({
        url: ruta,
        type: "POST",
        data:{
          AutorFollow : autor1,
          AutorFan : autor2,
          param  : param
        },
        success: function (data) {
          button = document.getElementById("btn_seguir");
          follower = document.getElementById("follower");
          if(param=="Si"){
            button.setAttribute("param", "No");
            button.innerHTML  = "Dejar de seguir";
            follower.value = "Eres seguidor";
          }
          else{        
            button.setAttribute("param", "Si");
            button.innerHTML  = "Seguir";
            follower.value = "No lo sigues";
          }
        }
    });      
  }
</script>
<?php

return [
    'components' => [
    // list of component configurations
    ],
    'params' => [
        // list of actions parameters
        "a_create" => Yii::t('app', 'Crear'),
        "a_publish" => Yii::t('app', 'Publicar'),
        "a_update" => Yii::t('app', 'Actualizar'),
        "a_delete" => Yii::t('app', 'Eliminar'),
        "a_search" => Yii::t('app', 'Buscar'),
        "a_send" => Yii::t('app', 'Enviar'),
        "a_reset" => Yii::t('app', 'Restablecer valores'),
        // list of titles parameters
        "t_add_new" => Yii::t('app', 'Agregar nuevo recurso'),
        "t_c_add_new" => Yii::t('app', 'Agregar nueva colección'),
        "t_update_page" => Yii::t('app', 'Actualizar recurso'),
        "t_image_featured" => Yii::t('app', 'Imagen destacada'),
        // list of message parameters
        "m_set_image" => Yii::t('app', 'Seleccionar la imagen destacada'),
        "m_delete_confirm" => Yii::t('app', 'Una vez eliminado el recurso todos sus datos asociados desapareceran permanentemente,Está seguro que desea eliminar el recurso?'),
        "m_seleccione_tipo_de_licencia" => Yii::t('app', 'Seleccione un valor'),
    ],
];


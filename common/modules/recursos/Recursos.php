<?php

namespace common\modules\recursos;

/**
 * recursos module definition class
 */
class Recursos extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\recursos\controllers';

    /**
     * @var string module name
     */
    public static $name = 'recursos';

    /**
     * @var string the class name of the recomendaciones model object, by default its common\modules\recursos\models\recomendaciones
     */
    public $recomendacionesClass = 'common\modules\recursos\models\recomendaciones';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

}

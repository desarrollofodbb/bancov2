<?php

namespace common\modules\recursos\controllers;

use Yii;
use yii\web\Controller;
use common\modules\recursos\models\Favoritos;
use yii\filters\VerbFilter;
use common\modules\recursos\models\Recursos;
use common\modules\reportes\models\Eventos;

/**
 * RecursosController implements the CRUD actions for Recursos model.
 */
class FavoritosController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'eliminar-de-mi-mochila' => ['POST'],
                    'agregar-recurso-mochila' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Deletes an existing favorite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEliminarDeMiMochila($rIdRecursos, $rIdUsuarios, $recurso) {
        if ($this->findModel($rIdRecursos, $rIdUsuarios)->delete()) {
            Yii::$app->session->setFlash("success", Yii::t("app", "El recurso se ha sido eliminado de su mochila correctamente"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "El recurso no se ha podido eliminar de su mochila, por favor refresque la página e intente nuevamente"));
        }
        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

    /**
     * Add an existing favorite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $recursoId
     * @return mixed
     */
    public function actionAgregarRecursoMochila() {
        $recursoId = Yii::$app->request->post("recursoId");
        $model = Recursos::findOne($recursoId);
        $recurso = Yii::$app->request->post("recurso");
        $favorito = new Favoritos();

        $favorito->rIdRecursos = $recursoId;
        $favorito->rIdUsuarios = Yii::$app->user->identity->id;
        if ($favorito->save()) {
          Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::LIKE_RECURSO, "Le gusta tú recurso", $recursoId, $model->Slug, $model->Autor);         
          Yii::$app->session->setFlash("success", Yii::t("app", "El recurso se ha  agregado a su mochila correctamente"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "El recurso no se ha podido agregar a su mochila, por favor refresque la página e intente nuevamente"));
        }
        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

    /**
     * Finds the Recursos model based on its primary slug value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($recursoId, $rIdUsuarios) {
        $model = Favoritos::find()
                ->where(["rIdRecursos" => $recursoId, "rIdUsuarios" => $rIdUsuarios])
                ->one();
        if ($model == null) {
            throw new NotFoundHttpException('La página solicitada no existe');
        } else {
            return $model;
        }
    }

}

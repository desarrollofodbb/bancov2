<?php

namespace common\modules\recursos\controllers;

use Yii;
use yii\web\Controller;
use common\modules\recursos\models\Denuncias;
use common\modules\recursos\models\DenunciasSearch;
use yii\filters\VerbFilter;
use common\modules\recursos\models\Recursos;
use yii\web\NotFoundHttpException;
use common\modules\reportes\models\Eventos;
use common\models\User;
/**
 * RecursosController implements the CRUD actions for Recursos model.
 */
class DenunciasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'eliminar-recurso' => ['POST'],
                    'agregar-denuncia' => ['POST'],
                    'rechazar-denuncia' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recursos models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DenunciasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Denuncia model.
     * @param integer $rIdRecursos
     * @param integer $rIdUsuarios
     * @return mixed
     */
    public function actionVerDenuncia($rIdRecursos, $rIdUsuarios) {
        return $this->render('ver-denuncia', [
                    'model' => $this->findModel($rIdRecursos, $rIdUsuarios),
        ]);
    }

    /**
     * Deletes an existing favorite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idRecurso
     * @return mixed
     */
    public function actionEliminarRecurso($rIdRecursos, $rIdUsuarios) {
        $recurso = $this->findRecursoModel($rIdRecursos);
        $denuncia = $this->findModel($rIdRecursos, $rIdUsuarios);
        if ($recurso) {
            $denuncia->Estado = Denuncias::STATUS_APPROVED;
            $denuncia->save();
        }

        if ($recurso->softDelete()) {
            Yii::$app->session->setFlash("success", Yii::t("app", "El recurso ha sido eliminado  correctamente"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "El recurso no se ha podido eliminar , por favor refresque la página e intente nuevamente"));
        }
        return $this->redirect(["index"]);
    }

    /**
     * Deletes an existing favorite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idRecurso
     * @return mixed
     */
    public function actionRechazarDenuncia($rIdRecursos, $rIdUsuarios) {
        $denuncia = $this->findModel($rIdRecursos, $rIdUsuarios);
        $denuncia->Estado = Denuncias::STATUS_REJECTED;
        $denuncia->save();
        Yii::$app->session->setFlash("success", Yii::t("app", "La denuncia ha sido rechazada"));

        return $this->redirect(["index"]);
    }

    /**
     * Add an existing favorite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $recursoId
     * @return mixed
     */
    public function actionAgregarDenuncia() {
        $denunciasParams = Yii::$app->request->post("Denuncias");
        $rIdRecursos = $denunciasParams["rIdRecursos"];
        $rIdUsuarios = $denunciasParams["rIdUsuarios"];
        $Motivo = $denunciasParams["Motivo"];
        $recurso = Yii::$app->request->post("recurso");
        $denuncia = new Denuncias();

        $denuncia->rIdRecursos = $rIdRecursos;
        $denuncia->rIdUsuarios = $rIdUsuarios;
        $denuncia->Motivo = $Motivo;
        if ($denuncia->save()) {
            $model = Recursos::find()->andFilterWhere(['idRecursos' => $rIdRecursos ])->one();     
            Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::REPORT_RECURSO, "", $rIdRecursos, $model->Slug, $model->Autor);     
            $denuncia->notifyComplaint(); //nofity to admin
            Yii::$app->session->setFlash("success", Yii::t("app", "La denuncia ha sido enviado correctamente"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "No se ha podido enviar correctamente la denuncia, por favor refresque la página e intente nuevamente"));
        }
        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

    /**
     * Finds the Recursos model based on its primary slug value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($rIdRecursos, $rIdUsuarios) {
        $model = Denuncias::find()
                ->where(["rIdRecursos" => $rIdRecursos, "rIdUsuarios" => $rIdUsuarios])
                ->one();
        if ($model == null) {
            throw new NotFoundHttpException('La página solicitada no existe');
        } else {
            return $model;
        }
    }

    /**
     * Finds the Recursos model based on its primary id value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idRecurso
     * @return Recursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRecursoModel($idRecurso) {
        $model = Recursos::find()->approved()
                ->andWhere(["idRecursos" => $idRecurso])
                ->one();
        if ($model == null) {
            throw new NotFoundHttpException('La página solicitada no existe');
        } else {
            return $model;
        }
    }

}

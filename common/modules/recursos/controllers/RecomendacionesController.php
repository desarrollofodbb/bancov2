<?php

namespace common\modules\recursos\controllers;

use Yii;
use common\modules\recursos\models\Recomendaciones;
use common\modules\recursos\models\RecomendacionesSearch;
use common\modules\recursos\models\Recursos;
use common\models\Configuraciones;
use yii2mod\moderation\enums\Status;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii2mod\moderation\ModerationBehavior;

/**
 * RecomendacionesController implements the CRUD actions for Recomendaciones model.
 */
class RecomendacionesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                    'delete' => ['post', 'delete'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomendaciones models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RecomendacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomendaciones model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomendaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        $requestParams = Yii::$app->request->post("Recomendaciones");
        $rIdRecursos = $requestParams["rIdRecursos"];
        $rIdUsuarios = $requestParams["rIdUsuarios"];
        $Titulo = $requestParams["Titulo"];
        $Descripcion = $requestParams["Descripcion"];
        $Url = $requestParams["Url"];
        $recurso = Yii::$app->request->post("recurso");
        $recomendacion = new Recomendaciones();

        $recomendacion->rIdRecursos = $rIdRecursos;
        $recomendacion->rIdUsuarios = $rIdUsuarios;
        $recomendacion->Titulo = $Titulo;
        $recomendacion->Descripcion = $Descripcion;
        $recomendacion->Url = $Url;

        if ($recomendacion->save()) {
            $recomendacion->notifyToAuthor(); //nofity to admin
            Yii::$app->session->setFlash("success", Yii::t("app", "La recomendación ha sido enviada, y debe ser aprobada por el usuario autor del recurso"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "No se ha podido enviar correctamente la recomendación, por favor refresque la página e intente nuevamente"));
        }

        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

    /**
     * Updates an existing Recomendaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash("success", Yii::t('app', 'La recomendación ha sido actualizada correctamente.'));

            return $this->redirect(['/recursos/recursos/ver-recurso', 'recurso' => $model->recurso->Slug]);
        } else {
            return $this->render('editar-recomendacion', [
                        'recomendacionesModel' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Recomendaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $urlredirect) {
        $this->findModel($id)->markRejected();
        Yii::$app->session->setFlash("success", Yii::t('app', 'La recomendación ha sido eliminada.'));
        return $this->redirect([$urlredirect]);
    }

    /**
     * Aprobar an existing Recomendaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionApprove($id, $urlredirect) {
        $this->findModel($id)->markApproved();
        Yii::$app->session->setFlash("success", Yii::t('app', 'La recomendación ha sido aprobada correctamente.'));
        return $this->redirect([$urlredirect]);
    }

    /**
     * Aprobar an existing Recomendaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionReject($id, $urlredirect) {
        $this->findModel($id)->markRejected();
        Yii::$app->session->setFlash("success", Yii::t('app', 'La recomendación ha sido rechazada correctamente.'));
        return $this->redirect([$urlredirect]);
    }

    /**
     * Finds the Recomendaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomendaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Recomendaciones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *  List all resources with status pending
     * @return mixed
     */
    public function actionAprobarRecomendaciones($recurso) {

        $recurso = $this->findRecursosModel($recurso);

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $dataProvider = new ActiveDataProvider([
            'query' => Recomendaciones::find()->pending()->where(
                    [
                        "rIdRecursos" => $recurso->idRecursos,
                    ]
            ),
            'pagination' => [
                'pageSize' => Yii::$app->params["recomendacionesPerPage"],
            ],
        ]);

        return $this->render('aprobar-recomendaciones', [
                    'recurso' => $recurso,
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
        ]);
    }

    protected function findRecursosModel($slug) {
        if (($model = Recursos::find()->approved()->andWhere('Slug = :Slug', [':Slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('El recurso al que está tratando de acceder no existe.');
        }
    }

}

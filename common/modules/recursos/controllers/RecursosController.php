<?php

namespace common\modules\recursos\controllers;

use Yii;
use common\modules\recursos\models\Recursos;
use common\modules\recursos\models\Colecciones;
use common\modules\recursos\models\ApprovalForm;
use common\modules\recursos\models\UploadForm;
use common\modules\recursos\models\Archivos;
use common\modules\recursos\models\RecursosSearch;
use common\modules\recursos\models\RecursosAdvancedSearch;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use common\modules\recursos\models\Votaciones;
use common\modules\recursos\models\Denuncias;
use common\models\Configuraciones;
use backend\modules\caracteristicas\models\Caracteristicas;
use common\modules\recursos\models\CriteriosDeValidacion;
use common\modules\reportes\models\PalabrasBuscadas;
use common\modules\reportes\models\BusquedasPalabras;
use common\modules\reportes\models\BusquedasFiltros;
use common\modules\reportes\models\Eventos;
use common\models\User;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\UnauthorizedHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use yii\db\Transaction;
use common\modules\recursos\models\Recomendaciones;
use common\modules\recursos\models\RecomendacionesSearch;
use common\modules\reportes\models\Busquedas;
use yii\db\Query;
use yii\db\Expression;

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\imagine\Image;
/**
 * RecursosController implements the CRUD actions for Recursos model.
 */
class RecursosController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'descargar-recurso' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recursos models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RecursosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recursos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//        $this->layout = "layouts/main.php";

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recursos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Recursos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->idRecursos]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Recursos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', yii::t('app', 'El recurso se actualizó correctamente'));
            return $this->redirect(['update', 'id' => $model->idRecursos]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Recursos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($recurso) {
        $this->findModelBySlug($recurso)->softDelete();
        Yii::$app->session->setFlash("success", "El recurso se ha eliminado correctamente");
        return $this->redirect(["/"]);
       
    }

    /**
     * Finds the Recursos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Recursos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

    //--------- Here start the public site functions -------------

    /**
     * Displays a single Status model.
     * @param string $recurso
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionVerRecurso($recurso,$idEvento="") {
        if (Yii::$app->user->isGuest){
            throw new UnauthorizedHttpException('Este recurso puede visualizarse sólo si está logueado en el sistema.  Favor ingrese sus credenciales.');
        }
        if($idEvento==""){
            if(isset($_GET['evento'])){
              $idEvento = $_GET['evento'];    
            }
            
        }
      
        $model = Recursos::find()->andFilterWhere(['Slug' => $recurso])->one();
        if (is_null($model)) {
            throw new NotFoundHttpException('La página solicitada no existe');
        } else if (($model->isPending() || $model->isRejected()) &&
                (!Yii::$app->user->can(Yii::$app->params["verificador_default_role"]))
        ) {
            throw new ForbiddenHttpException('El recurso está bajo revisión, si es el autor del recurso se le notificará cuando se apruebe o rechace el recurso.');
        } else {


            $modelDenuncia = new Denuncias();
            $modelVotacion = Votaciones::getCurrentUserVotacion($model->idRecursos);
            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
            $this->view->params['configuraciones_generales'] = $configuraciones;

            //------------ recomendaciones -----
            $recomendacionesModel = new Recomendaciones();
            $RecomendacionesSearchModel = new RecomendacionesSearch();
            $RecomendacionesSearchModel->rIdRecursos = $model->idRecursos;
            $recomendacionesDataProvider = $RecomendacionesSearchModel->search(Yii::$app->request->queryParams);

            //------------ recomendaciones -----
            //registrar evento
            if($model->Autor != Yii::$app->user->identity->id){
              Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::VER_RECURSO, "Recurso fue visto", $model->idRecursos, $recurso, $model->Autor);
            }
            if($idEvento!=""){
              Yii::$app->db->createCommand()->update('TEventos', [
                 'Revisado' => 1,
                 'FechaDeModificacion' => new Expression("GETDATE()")],    
                 'idEventos='.$idEvento)->execute();
            }
             
            return $this->render('ver-recurso', [
                        'recomendacionesModel' => $recomendacionesModel,
                        'recomendacionesDataProvider' => $recomendacionesDataProvider,
                        'model' => $model,
                        'modelDenuncia' => $modelDenuncia,
                        'modelVotacion' => $modelVotacion,
            ]);
        }
    }

    /**
     * Displays a single Status model.
     * @param string $recurso
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionVerColeccion($coleccion) {
        $model = Colecciones::find()->andFilterWhere(['Slug' => $coleccion])->one();

        if (is_null($model)) {
            throw new NotFoundHttpException('La página solicitada no existe');
        } 
        else {


            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
            $this->view->params['configuraciones_generales'] = $configuraciones;

            $modelRecursos = new Recursos();
            return $this->render('ver-coleccion', [
                        'model' => $model,
                        'modelRecursos' => $modelRecursos,
            ]);
        }
    }


    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarEvento($Slug, $evento, $redsocial) {

      $model = Recursos::find()->andFilterWhere(['Slug' => $Slug])->one();
      // if($model->Autor != Yii::$app->user->identity->id){
        Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::SHARE_RECURSO, "Recurso fue compartido en " . $redsocial, $model->idRecursos, $Slug, $model->Autor);
        return true;
      // }

      return "";

    }

  public function actionCrearEvento() {
    $Slug = $_POST["Slug"];
        
    $evento = $_POST["evento"];  
    $redsocial = $_POST["redsocial"];  
    $this->agregarEvento($Slug,$evento,$redsocial);     
  }



    /**
     * Creates a new Recursos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCrearPerfil() {
      $user = new User();

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;
        $caracteristicasDeRecursos = ValoresDeCaracteristicas::getValoresPorCaracteristicas();

        if ($user->load(Yii::$app->request->post())) {

            $transaction = Recursos::getDb()->beginTransaction(Transaction::SERIALIZABLE);
            try {

            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el perfil, por favor verifique los campos e intente nuevamente"));
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el perfil, por favor verifique los campos e intente nuevamente"));
                if (isset($model->idRecursos) && $model->idRecursos) {
                    $model->deleteRecursoFile();
                }
                $transaction->rollBack();
                throw $e;
            }
        } elseif (!\Yii::$app->request->isPost) {

        }
        $Recursos = new Recursos();
        return $this->render('crear-perfil', [
                    'model' => $Recursos,
        ]);
    }

    /**
     * Creates a new Recursos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCrearRecurso() {

        $model = new Recursos();
        $modelRecurso = new UploadForm();
        $modelRecurso->scenario = UploadForm::SCENARIO_CREATE;

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;
        $caracteristicasDeRecursos = ValoresDeCaracteristicas::getValoresPorCaracteristicas();


        if ($model->load(Yii::$app->request->post())) {

            $modelRecurso->recursoFile = UploadedFile::getInstance($modelRecurso, 'recursoFile');
            $model->recursoFileTemp = $modelRecurso->recursoFile;
            $transaction = Recursos::getDb()->beginTransaction(Transaction::SERIALIZABLE);
            try {

                $archivoRecurso = new Archivos();
                $archivoSaveResult = true;
                if($model->url_recurso==""){
                  $model->url_recurso="https://www.archivosbancorecursos.com";                  
                }
                $model->Tipo = 2;
                switch ($model->Tipo) {
                    case Recursos::TYPE_FILE:
                        if (!$archivoRecurso->saveRecursoFile($modelRecurso->recursoFile)) {
                            $archivoSaveResult = false;
                        }
                        $model->rIdArchivos = $archivoRecurso->idArchivo;
                        $model->Recurso = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $modelRecurso->recursoFile->baseName) . '.' . $modelRecurso->recursoFile->extension;
                        break;
                    case Recursos::TYPE_URL:
                        $model->Recurso = $model->url_recurso ? $model->url_recurso : $model->Recurso;
                        break;
                    case Recursos::TYPE_EMBED:
                        $model->Recurso = $model->embed_recurso ? $model->embed_recurso : $model->Recurso;
                        break;
                }
              
                $model->Autor = Yii::$app->user->identity->id;
                $usermodel = new user();
                $user = $usermodel->getUserById(Yii::$app->user->identity->id);
                if ($user->UserHasRole(Yii::$app->params["administrador_general_default_role"]) || $user->UserHasRole(Yii::$app->params["administrador_de_plataforma_default_role"])) {
                  $model->Estado = 1;
                }                
                $modelResult = $model->save();
                $rutaarchivos =  '../../server/uploads/' . $model->Autor . '/' . $model->idtime;
 
              if(file_exists($rutaarchivos)){                   
                if (!$archivoRecurso->saveRecursoFileNew($rutaarchivos,$model->Autor,$model->idtime,$model->idRecursos)) {
                  $archivoSaveResult = false;
                }                
              }                  
                $modelRecursoResult = $modelResult ? ($modelRecurso->recursoFile ? $modelRecurso->upload($model->Autor, $archivoRecurso->idArchivo) : true) : false;
                if ($modelResult && $modelRecursoResult && $archivoSaveResult) {
                  // $model->createRelation($model->Autor,$model->idRecursos);
                  $eventomsg = "Recurso en revisión";
                  if ($user->UserHasRole(Yii::$app->params["administrador_general_default_role"]) || $user->UserHasRole(Yii::$app->params["administrador_de_plataforma_default_role"])) {
                    Yii::$app->session->setFlash('success', yii::t('app', 'El recurso se ha creado correctamente y fue aprobado por ser usted un administrador del sistema'));
                    $eventomsg = "Recurso aprobado automáticamente";
                  }                      
                  else{
                    Yii::$app->session->setFlash('success', yii::t('app', 'El recurso se ha creado correctamente y debe ser sometido a un proceso de validación por parte de los administradores del sistema, se le notificará cuando el recurso sea aprobado o rechazo.'));
                  }

                    //Everything is ok!
                    $transaction->commit();
                    Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::REVIEW_RECURSO, $eventomsg, $model->idRecursos, $model->Slug,$model->Autor);                    
                    //notify to resources validator
                    $model->sendEmailToResourcesValidator();
                    return $this->redirect(['crear-recurso', 'recurso' => $model->Slug]);
                } else {
                    if ($modelRecursoResult) {//si el archivo se guardó, eliminarlo 
                        $model->deleteRecursoFile();
                    }
                    Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el recurso, por favor verifique los campos e intente nuevamente"));
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el recurso, por favor verifique los campos e intente nuevamente"));
                if (isset($model->idRecursos) && $model->idRecursos) {
                    $model->deleteRecursoFile();
                }
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el recurso, por favor verifique los campos e intente nuevamente"));
                if (isset($model->idRecursos) && $model->idRecursos) {
                    $model->deleteRecursoFile();
                }
                $transaction->rollBack();
                throw $e;
            }
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());

            //            --Load autores
            $model->autores_ids = ArrayHelper::map($model->autores, 'id', 'id');

            //            --Load valores de caracteristicas
            $caracteristicas = Caracteristicas::find()->all();
            foreach ($caracteristicas as $caracteristica) {
                $valor_field = str_replace("-", "_", $caracteristica->Slug);
                $model->$valor_field = ArrayHelper::map($model->valoresDeCaracteristicas, 'idValoresDeCaracteristicas', 'idValoresDeCaracteristicas');
            }

            // --Load criterios de evaluacion
            $model->criterios_ids = ArrayHelper::map($model->criteriosDeEvaluacion, 'idCriteriosDeEvaluacion', 'idCriteriosDeEvaluacion');
        }

        return $this->render('crear-recurso', [
                    'model' => $model,
                    'modelRecurso' => $modelRecurso,
                    'caracteristicasDeRecursos' => $caracteristicasDeRecursos,
        ]);
    }

    /**
     * Updates an existing Recursos model.
     * If update is successful, the browser will be redirected to the 'actualizar-recurso' page.
     * @param integer $slug
     * @return mixed
     */
    public function actionActualizarRecurso($recurso) {
    
 
        $model = $this->findModelBySlug($recurso);
        $model->id = $model->idRecursos;
        $currentRecurso = $model->Recurso;
        $currentTipo = $model->Tipo;
        $modelRecurso = new UploadForm();
        $modelRecurso->scenario = UploadForm::SCENARIO_UPDATE;

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $caracteristicasDeRecursos = ValoresDeCaracteristicas::getValoresPorCaracteristicas();

        if ($model->load(Yii::$app->request->post())) {

            $modelRecurso->recursoFile = UploadedFile::getInstance($modelRecurso, 'recursoFile');
            $model->recursoFileTemp = $modelRecurso->recursoFile;

            $modelRecurso->recursoFile = UploadedFile::getInstance($modelRecurso, 'recursoFile');
            $transaction = Recursos::getDb()->beginTransaction(Transaction::SERIALIZABLE);

            try {
                $archivoRecurso = $model->archivo ? $model->archivo : new Archivos(); //get the current file
                $archivoSaveResult = true;
                               
                $model->tempCurrentFile = $model->archivo;
                $model->Tipo = 2;
                switch ($model->Tipo) {
                    case Recursos::TYPE_FILE:
                        if ($modelRecurso->recursoFile) {//eliminar el archivo actual y el registro
                            if (!$archivoRecurso->saveRecursoFile($modelRecurso->recursoFile)) {
                                $archivoSaveResult = false;
                            }else{
                                $currentFileToDelete = $currentTipo == Recursos::TYPE_FILE ? $currentRecurso : null;
                                $model->deleteRecursoFile($currentFileToDelete);
                            }
                            $model->rIdArchivos = $archivoRecurso->idArchivo;
                        }
                        $model->Recurso = $modelRecurso->recursoFile ? preg_replace('/[^a-zA-Z0-9-_\.]/', '', $modelRecurso->recursoFile->baseName) . '.' . $modelRecurso->recursoFile->extension : $currentRecurso;
                        break;
                    case Recursos::TYPE_URL:
                        $model->rIdArchivos = NULL;
                        $model->Recurso = isset(Yii::$app->request->post("Recursos")["url_recurso"]) ? Yii::$app->request->post("Recursos")["url_recurso"] : $model->Recurso;
                        break;
                    case Recursos::TYPE_EMBED:
                        $model->rIdArchivos = NULL;
                        $model->Recurso = isset(Yii::$app->request->post("Recursos")["embed_recurso"]) ? Yii::$app->request->post("Recursos")["embed_recurso"] : $model->Recurso;
                        break;
                }
                // $model->Autor = Yii::$app->user->identity->id;
                $modelResult = $model->save();
$rutaarchivos =  '../../server/uploads/' . $model->Autor . '/' . $model->idtime;

              if(file_exists($rutaarchivos)){                   

                if (!$archivoRecurso->saveRecursoFileNew($rutaarchivos,$model->Autor,$model->idtime,$model->idRecursos)) {
                  $archivoSaveResult = false;
                }                
              }     

                // if ($model->Tipo != Recursos::TYPE_FILE) {
                //     $deleteResult = $archivoRecurso ? $archivoRecurso->delete() : "";
                // }

                $modelRecursoResult = $modelResult ? ($modelRecurso->recursoFile ? $modelRecurso->upload($model->Autor, $archivoRecurso->idArchivo) : true) : false;
 
                if ($modelResult && $modelRecursoResult && $archivoSaveResult) {

                    Yii::$app->session->setFlash('success', yii::t('app', 'El recurso se actualizó correctamente'));
                    //Everything is ok!
                    $transaction->commit();
                    return $this->redirect(['actualizar-recurso', 'recurso' => $model->Slug]);
                } else {
                    if ($modelRecursoResult && isset($currentFileToDelete)) {//si el archivo se guardó, eliminarlo
                        $model->deleteRecursoFile($currentFileToDelete);
                    }
                    Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el recurso, por favor verifique los campos e intente nuevamente"));
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el recurso, por favor verifique los campos e intente nuevamente"));
                if (isset($model->idRecursos) && $model->idRecursos && isset($currentFileToDelete)) {
                    $model->deleteRecursoFile($currentFileToDelete);
                }
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                Yii::$app->session->setFlash('error', Yii::t("app", "Algo ha salido mal al guardar el recurso, por favor verifique los campos e intente nuevamente"));
                if (isset($model->idRecursos) && $model->idRecursos) {
                    $model->deleteRecursoFile($currentFileToDelete);
                }
                $transaction->rollBack();
            }
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());
//            --Load autores
            $model->autores_ids = ArrayHelper::map($model->autores, 'id', 'id');
//            --Load valores de caracteristicas
            $caracteristicas = Caracteristicas::find()->all();
            foreach ($caracteristicas as $caracteristica) {
                $valor_field = str_replace("-", "_", $caracteristica->Slug);
                $model->$valor_field = ArrayHelper::map($model->valoresDeCaracteristicas, 'idValoresDeCaracteristicas', 'idValoresDeCaracteristicas');
            }
            // --Load criterios de evaluacion
            $model->criterios_ids = ArrayHelper::map($model->criteriosDeEvaluacion, 'idCriteriosDeEvaluacion', 'idCriteriosDeEvaluacion');

            switch ($model->Tipo) {
                case Recursos::TYPE_URL:
                    if($model->Recurso == "https://www.archivosbancorecursos.com"){
                      $model->url_recurso = "";
                    }              
                    else{
                      $model->url_recurso = $model->Recurso;    
                    }                    
                    break;
                case Recursos::TYPE_EMBED:
                    $model->embed_recurso = $model->Recurso;
                    break;
            }
        }

        return $this->render('actualizar-recurso', [
                    'model' => $model,
                    'modelRecurso' => $modelRecurso,
                    'caracteristicasDeRecursos' => $caracteristicasDeRecursos,
        ]);
    }

    /**
     * Finds the Recursos model based on its primary slug value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModelBySlug($slug,$onerror="") {     
        $model = Recursos::find()
                ->andWhere(['Slug' => $slug])
                ->one();
        if ($model == null) {  
          if($onerror=""){
            throw new NotFoundHttpException('La página solicitada no existe');
          }
          else{
            return "notfound";
          }  
        } else {
            return $model;
        }
    }

    public function actionAprobarRecurso($recurso) {
        $model = ApprovalForm::find()->where(['Slug' => $recurso])->one();
        if (is_null($model)) {//if the resource does not exist
            throw new NotFoundHttpException('La página solicitada no existe');
        } else if (($model->isPending() || $model->isRejected()) &&
                (!Yii::$app->user->can(Yii::$app->params["verificador_default_role"]))
        ) {//if the resource exist but the user does not have permissions to access
            throw new ForbiddenHttpException('El recurso está bajo revisión, si es el autor del recurso se le notificará cuando se apruebe o rechace el recurso.');
        } else {//si el recurso existe y tiene permisos de verificador (aprobar recursos)
            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
            $this->view->params['configuraciones_generales'] = $configuraciones;

            if ($model->load(Yii::$app->request->post())) {
                $postRequestRecursos = Yii::$app->request->post("ApprovalForm");

                if ($this->isApproved($postRequestRecursos)) {//if the resource has been approved
                    //save recurso, and update status to approved
                    $model->markApproved();
                    if ($model->save()) {
                        Yii::$app->session->setFlash("success", Yii::t("app", "El recurso ha sido aprobado y se notificará al usuario."));
                        $model->notifyApproved(); //notify to user that the resource has been approved
                        Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::PUBLIC_RECURSO, "Recurso aprobado", $model->idRecursos, $model->Slug,$model->Autor);
                        return $this->redirect(['aprobar-recurso', 'recurso' => $model->Slug]);
                    } else {
                        Yii::$app->session->setFlash("error", Yii::t("app", "Algo ha salido mal, por favor refresque la página e intente nuevamente"));
                    }
                } else {//if the resource has been rejected
                    //save recurso, and update status to rejected
                    $model->markRejected();
                    if ($model->save()) {
                        Yii::$app->session->setFlash("success", Yii::t("app", "El recurso ha sido rechazado y se notificará al usuario."));
                        $model->notifyRejected(); //notify to user that the resourse has been rejected
                        Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::REJECT_RECURSO, "Recurso Rechazado", $model->idRecursos, $model->Slug, $model->Autor);
                        return $this->redirect(['aprobar-recurso', 'recurso' => $model->Slug]);
                    } else {
                        Yii::$app->session->setFlash("error", Yii::t("app", "Algo ha salido mal, por favor refresque la página e intente nuevamente"));
                    }
                }
            } else {
                $model->load(Yii::$app->request->get());
                $model->criterios_validacion_calidad_ids = ArrayHelper::map($model->getCriteriosDeValidacionCalidad(), 'idCriteriosDeValidacion', 'idCriteriosDeValidacion');
                $model->criterios_validacion_educacion_ids = ArrayHelper::map($model->getCriteriosDeValidacionEducacion(), 'idCriteriosDeValidacion', 'idCriteriosDeValidacion');
            }
            return $this->render('aprobar-recurso', ['model' => $model,]);
        }
    }

    /**
     * Return is resource is approved or not
     * @return boolean
     */
    protected function isApproved($postRequestRecursos) {
        $criterios_validacion_calidad_ids = is_array($postRequestRecursos["criterios_validacion_calidad_ids"]) ? count($postRequestRecursos["criterios_validacion_calidad_ids"]) : 0;
        $criterios_validacion_educacion_ids = is_array($postRequestRecursos["criterios_validacion_educacion_ids"]) ? count($postRequestRecursos["criterios_validacion_educacion_ids"]) : 0;
        $countCriterios = $criterios_validacion_calidad_ids + $criterios_validacion_educacion_ids;
        $allCriteriosCount = CriteriosDeValidacion::find()->count();

        return $countCriterios >= $allCriteriosCount;
    }

    /**
     *  List all resources with status pending
     * @return mixed
     */
    public function actionAprobarRecursos() {

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $dataProvider = new ActiveDataProvider([
            'query' => Recursos::find()->pending()->orderBy("FechaDeCreacion DESC"),
            'pagination' => [
                'pageSize' => Yii::$app->params["recursosPerPage"],
            ],
        ]);

        return $this->render('aprobar-recursos', [
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
        ]);
    }

    /**
     *  List all resources with status approved and belongs to the current user
     * @return mixed
     */
    public function actionMisRecursosAjax($Autor="",$offset=0,$limit=0) {
        if($Autor==""){
          $Autor = Yii::$app->user->identity->id;
        }
        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;
  
       $misRecursos = (new Query)->select("TRecursos.idRecursos", "TRecursos.Nombre", "TRecursos.Slug", "TRecursos.Autor")
       ->from('TRecursos')
       ->where(["[TRecursos].[Autor]" => $Autor])
       ->orderBy("[idRecursos] DESC")  ;           
        if($limit>0){  
          $query->limit($limit)->offset($offset);      
        }
     $count=0;

     $options = [];
     $rows = $query->all();
     foreach ($rows as $registro) {       
      $recursoObject = $this->findModelBySlug($registro["Slug"]);
      if($recursoObject =="notfound"){
        continue;
      }

      $ValoresTipo = $recursoObject->getTipoValores()->All();
      $text = Html::encode($registro["Nombre"]);
      $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $registro["Slug"]]);
      $TipoRec = "";
      foreach ($ValoresTipo as $valor) {
        $TipoRec =  $valor->Nombre;
      }      

      $count++;
      //$preview = $recursoObject->getPreview($registro["Autor"],$registro["idRecursos"],"menu");

      $output[] = array("preview" => $recursoObject->getPreview($registro["Autor"],$registro["idRecursos"],"simplezoom"),
                    "tipo" => $TipoRec,
                    "text" => $text,
                    "url" => $url); 
     }
     if($count==0){
      $output[] = array("preview" => "",
                    "tipo" => "Sin Resultados",
                    "text" => "",
                    "url" => "");     
     }
     $data = array(
       'resultados' => $output,
       'tags' => $tags,
       'cantidad'  => $count
     );

     return json_encode($data);                    
    }

    /**
     *  List all resources with status approved and belongs to the current user
     * @return mixed
     */
    public function actionMisRecursos($Autor="") {
        if($Autor==""){
          $Autor = Yii::$app->user->identity->id;
        }
        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;        
        $misRecursos = Recursos::find()
                ->select(["TRecursos.idRecursos", "TRecursos.Nombre", "TRecursos.Slug", "TRecursos.Autor"])
                ->andWhere(["[TRecursos].[Autor]" => $Autor])   //solo los eventos de recursos
          //      ->limit(Yii::$app->params["mostViewedResourcesLimit"]) //filtrar por el top
                ->orderBy("[idRecursos] DESC")
                ->all();

        return $this->render('mis-recursos', [
                    "misRecursos" => $misRecursos,
                    'configuraciones' => $configuraciones,
        ]);                    
        // $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        // $this->view->params['configuraciones_generales'] = $configuraciones;

        // $dataProvider = new ActiveDataProvider([
        //     'query' => Recursos::find()->approved()->orderBy("FechaDeModeracion DESC")->andFilterWhere([
        //         "Autor" => Yii::$app->user->identity->id
        //     ]),
        //     'pagination' => [
        //         'pageSize' => Yii::$app->params["recursosPerPage"],
        //     ],
        // ]);

        // return $this->render('mis-recursos', [
        //             'dataProvider' => $dataProvider,
        //             'configuraciones' => $configuraciones,
        // ]);
    }

    /**
     *  List all resources with status approved and belongs to the current user
     * @return mixed
     */
    public function actionMisColecciones($Autor="") {
        if($Autor==""){
          $Autor = Yii::$app->user->identity->id;
        }
        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;        
        $misColecciones = Colecciones::find()
                ->select(["TColecciones.idColeccion", "TColecciones.Nombre", "TColecciones.Slug", "TColecciones.Autor"])
                ->andWhere(["[TColecciones].[Autor]" => $Autor])   //solo los eventos de recursos
                ->limit(Yii::$app->params["mostViewedResourcesLimit"]) //filtrar por el top
                ->orderBy("[idColeccion] DESC")
                ->all();
        return $this->render('mis-colecciones', [
                    "misColecciones" => $misColecciones,
                    'configuraciones' => $configuraciones,
        ]);

    }

    public function actionCollectionList($q = null, $id = null) {
        $Autor = Yii::$app->user->identity->id;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out1 = ['id' => '0', 'text' => 'Crear colección +'];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('idColeccion AS id, Nombre AS text')
                ->from('TColecciones')
                ->where(['like', 'Nombre', $q])
                ->andWhere(["[TColecciones].[Autor]" => $Autor])
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        else {
            $query = new Query;
            $query->select('idColeccion AS id, Nombre AS text')
                ->from('TColecciones')
                ->andWhere(["[TColecciones].[Autor]" => $Autor])
                ->limit(100);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        $out3 = ["-1" => $out1];
        $out2 = array_merge($out3,$data);
        $out['results'] = $out2;
        
        return $out;
    }

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarColeccion($Nombre, $Autor) {

        if(Yii::$app->db->createCommand()->insert('TColecciones', [
                 'Nombre' => $Nombre,
                 'Autor' => $Autor,
                 'FechaDeCreacion' => new Expression("GETDATE()"),    
               ])->execute()){

            return Yii::$app->db->getLastInsertID();
        }
        return "No se pudo crear colección";

    }

    public function actionCrearColeccion() {
      $Nombre = $_POST["Nombre"];
      $Autor = $_POST["Autor"];       
      $Coleccion = new Colecciones();
      $Coleccion->Nombre = $Nombre;
      $Coleccion->Autor = $Autor;
      $Coleccion->Estado = 1;
      $Coleccion->save();       
      return Yii::$app->db->getLastInsertID();
      return true;
      $this->agregarColeccion($Nombre,$Autor);     
    }

    public function eliminaColeccion($Slug, $Autor) {

        if(Yii::$app->db->createCommand()->delete('TColecciones', [
                 'Slug' => $Slug,
                 'Autor' => $Autor,
               ])->execute()){
            return "Coleccion eliminada correctamente";
        }
        return "No se pudo eliminar colección";

    }

    public function actionEliminaColeccion() {
      $Slug = $_POST["Slug"];
      $Autor = $_POST["Autor"];            
      $this->eliminaColeccion($Slug,$Autor);   
      $this->actionMisColecciones($Autor);
    }

    /**
     * The user is follower yet
     * @param 
     * @return boolean
     */
    public function isFollower($AutorFollow, $AutorFan) {

     $count = (new Query())->select('rIdUsuarios')
    ->from('RAutoresSeguidores')
    ->where(['rIdUsuarios' =>$AutorFollow])
    ->andWhere(['rIdSeguidores' => $AutorFan])
    ->count();

        return $count == 1;
    }

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarFavoritos($AutorFollow, $AutorFan) {
      if(!$this->isFollower($AutorFan,$AutorFollow)){
        Eventos::addEvent(Eventos::CATEGORY_AUTORES, Eventos::FOLLOW_USER, "Le gusta tu recurso", "", "", $AutorFollow);   
        return Yii::$app->db->createCommand()->insert('RAutoresSeguidores', [
                 'rIdUsuarios' => $AutorFollow,
                 'rIdSeguidores' => $AutorFan,
               ])->execute();        
      }
      else{
        return true;
      }
    }

    /**
     * The user is a not more follower
     * @param 
     * @return boolean
     */
    public function eliminarFavoritos($AutorFollow, $AutorFan) {

        return Yii::$app->db->createCommand()->delete('RAutoresSeguidores', [
                 'rIdUsuarios' => $AutorFollow,
                 'rIdSeguidores' => $AutorFan,
               ])->execute();        

    }

    public function actionDeleteFile() {

if(isset($_POST['key'])){
    $key=$_POST['key'];
}
if($key=="" && isset($_GET['key'])){
    $key=$_GET['key'];
}
$idArchivo=$key;

      $entro = 0;
      $query = (new Query)->select('idArchivo,idRecursos,CreadoPor,Nombre')
       ->from('TArchivos')
       ->where(['idArchivo' => $idArchivo]);      
      $rows = $query->all();
      foreach ($rows as $registro) {  
        $idRecursos = $registro['idRecursos'];
        $recursoObject = $this->findModel($idRecursos);  
        $Autor = $recursoObject->Autor; 
        $entro = 1;
        $idArchivo = $registro['idArchivo'];
        $CreadoPor = $registro['CreadoPor'];
        $Nombre = $registro['Nombre'];
        Yii::$app->db->createCommand()->delete('TArchivos', [
          'idArchivo' => $idArchivo,
        ])->execute(); 
        $folder = Yii::$app->params["pathRecursos"] . '/' . $Autor . '/' . $idArchivo . '_' .$Nombre;
        unlink($folder);
      } 
      if($entro==0){
        unlink($key);
      }
      return true;
    }

    public function actionAutorFavorito() {
      $AutorFollow = $_POST["AutorFollow"];
      $AutorFan = $_POST["AutorFan"];
      $param  = $_POST["param"];
      if ($param=="Si"){
        $this->agregarFavoritos($AutorFollow,$AutorFan);     
      }
      else{
        $this->eliminarFavoritos($AutorFollow,$AutorFan);      
      }
      return true;
    }

    public function actionLastEventos() {
      $Autor = $_POST["Autor"];
      $user = new User();
      return $user->LastEventos($Autor);
    }    

    public function actionViewEventos() {
      $Autor = $_POST['Autor'];
      $user = new User();
      return $user->ViewEventos($Autor);

    }

    public function ViewBusquedas($Tag,$Nombre,$Autor,$Tipo,$Anio1,$Anio2,$Dirigido,$Nivel,$Asignatura,$Idioma,$Usos,$limit,$offset){

      if($offset==0 && substr($Nombre,0,3)!="MR:"){
        $query = (new Query)->select('RRecursosValoresDeCaracteristicas.rIdValores, TvaloresDeCaracteristicas.Nombre,count(*)')
        ->from('TRecursos')
        ->where(['Estado' => 1])
        ->innerJoin('RRecursosValoresDeCaracteristicas', 'RRecursosValoresDeCaracteristicas.rIdRecursos = TRecursos.idRecursos')
        ->innerJoin('TvaloresDeCaracteristicas', 'RRecursosValoresDeCaracteristicas.rIdValores = TvaloresDeCaracteristicas.idValoresDeCaracteristicas');
        $autrec=0;
        if($Nombre!=""){
          $query->andWhere(['like', 'TRecursos.Nombre', $Nombre]);
          $event = new Busquedas();
          $event->Keyword = $Nombre;
          $resultbus = $event->save(); //guardar la cabecera de la búsqueda
          $words = Yii::$app->CustomFunctionsHelper->extractKeywords($Nombre, 4, 1, true);
          foreach ($words as $word) {
            if (!PalabrasBuscadas::find()->where(["Nombre" => $word])->exists()) {//si no existe aún la palabra en la lista de palabras buscadas
                $newWord = new PalabrasBuscadas();
                $newWord->Nombre = $word;
                $newWord->save();
                $wordId = $newWord->idPalabrasBuscadas;
            } else {
                $wordId = PalabrasBuscadas::find()->where(["Nombre" => $word])->one()->idPalabrasBuscadas;
            }
            $newBusquedaPalabra = new BusquedasPalabras();
            $newBusquedaPalabra->rIdBusquedas = $event->idBusquedas;
            $newBusquedaPalabra->rIdPalabrasBuscadas = $wordId;
            $newBusquedaPalabra->save();
          }               
        }
        if($Autor!=""){
          $query->andWhere(['like', 'TRecursos.autorRecurso', $Autor]);
          $event = new Busquedas();
          $event->Keyword = $Autor;
          $resultbus = $event->save(); //guardar la cabecera de la búsqueda             
        }
        if($Tipo!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Tipo]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorTipoRecurso";
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }
          if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Tipo;
            $newBusquedaFiltro->save();            
          }          
        } 
        if($Tag!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Tag]);
         if($Nombre==""){
           $event = new Busquedas();
           $event->Keyword = "PorTag";
           $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
         }
         if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Tag;
            $newBusquedaFiltro->save();            
         }           
        }        
        if($Dirigido!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Dirigido]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorDirigido";
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }
          if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Dirigido;
            $newBusquedaFiltro->save();            
          }            
        }   
        if($Nivel!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Nivel]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorNivel";
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }
          if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Nivel;
            $newBusquedaFiltro->save();            
          }             
        }      
        if($Asignatura!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Asignatura]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorAsignatura";
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }
          if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Asignatura;
            $newBusquedaFiltro->save();            
          }             
        }           
        if($Idioma!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Idioma]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorIdioma";
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }
          if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Idioma;
            $newBusquedaFiltro->save();            
          }            
        }                  
        if($Usos!=""){
          $query->andWhere(['RRecursosValoresDeCaracteristicas.rIdValores' => $Usos]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorUsos";
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }
          if($resultbus){
            $newBusquedaFiltro = new BusquedasFiltros();
            $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
            $newBusquedaFiltro->rIdFiltro = $Usos;
            $newBusquedaFiltro->save();            
          }            
        }      
        if($Anio1!="" && $Anio2!=""){
          $query->andWhere(['between','TRecursos.anioRecurso',$Anio1,$Anio2]);
          if($Nombre==""){
            $event = new Busquedas();
            $event->Keyword = "PorAnio " . $Anio1 . " " . $Anio2;
            $resultbus = $event->save(); //guardar la cabecera de la búsqueda            
          }          
        }      
 
        $query->groupBy('RRecursosValoresDeCaracteristicas.rIdValores, TvaloresDeCaracteristicas.Nombre')
        ->orderBy('count(*) desc');
        
        $rows = $query->all();
        
        $counttags = 0;
        foreach ($rows as $registro) {
          $tags[] = array("rIdValores" => $registro['rIdValores'],
                     "Nombre" => $registro['Nombre']);   
          $counttags++;
          if($counttags>6){
             break;
          }
        }
        if($counttags==0){
          $tags[] = array("rIdValores" => 0,
                     "Nombre" => "Sin Tags");           
        }
      }
      else{
        $tags[] = array("rIdValores" => 0,
                     "Nombre" => "Sin Tags");         
      }
           

      $query="SELECT  tr.Autor, tr.idRecursos, tr.Slug, tr.Nombre,count(*) OVER() AS full_count,ta.idArchivo,ta.Nombre AS preview";
      $query.=" FROM TRecursos as tr";
      $where=" WHERE tr.estado=1 ";
      $query.=" LEFT JOIN TArchivos AS TA ON ta.idRecursos = tr.idRecursos AND ta.Nombre LIKE 'preview_%'";

 
      if($Nombre!=""){
          if(substr($Nombre,0,3)=="MR:"){
            $autrec = substr($Nombre,3);
            $query.= " INNER JOIN RAutores ON RAutores.rIdRecursos = TRecursos.idRecursos";
            $where.= "AND [RAutores].[rIdUsuarios]='$autrec'";    
          }
          else{
            $where.= "AND tr.Nombre LIKE '$Nombre'";
          }    
      }     
      if($Autor!=""){
        $where.=" AND tr.autorRecurso like '$Autor'";
      }   
      if($Tipo!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS rrt on rrt.rIdRecursos = tr.idRecursos";
        $where.=" AND rrt.rIdValores=$Tipo";
      }   
      
      if($Tag!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS rtg on rtg.rIdRecursos = tr.idRecursos";
        $where.=" AND rtg.rIdValores=$Tag";
      }     
      if($Dirigido!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS dir on dir.rIdRecursos = tr.idRecursos";
        $where.=" AND dir.rIdValores=$Dirigido";
      } 
      if($Nivel!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS nvl on nvl.rIdRecursos = tr.idRecursos";
        $where.=" AND nvl.rIdValores=$Nivel";
      }  
      if($Asignatura!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS asg on asg.rIdRecursos = tr.idRecursos";
        $where.=" AND asg.rIdValores=$Asignatura";
      }           
      if($Idioma!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS idm on idm.rIdRecursos = tr.idRecursos";
        $where.=" AND idm.rIdValores=$Idioma";
      }                                       
      if($Usos!=""){
        $query.=" INNER JOIN RRecursosValoresDeCaracteristicas AS uso on uso.rIdRecursos = tr.idRecursos";
        $where.=" AND uso.rIdValores=$Usos";
      }      
      if($Anio1!="" && $Anio2!=""){
        $where.=" AND tr.anioRecurso between($Anio1,$Anio2)";
      }     
  
      if($limit>0){                             
       // $query->limit($limit)->offset($offset);   
        $where.=" order by  idRecursos desc" ;
      }
      $query.=$where;
      $count=0;
      $total = 0;
      $options = [];
      $rows= Yii::$app->db->createCommand($query)->queryAll();
     
       if($Nombre!=""){                     
         $resultadoArray=$this->recursosPorEtiqueta($Nombre);
         foreach($resultadoArray as $etiquetaR){
           $flagTag=false;
           for($i=0;$i<count($rows);$i++){
               if($rows[$i]["idRecursos"]===$etiquetaR["idRecursos"])
                 $flagTag=true;
           }
           if(!$flagTag)
             array_push($rows,$etiquetaR);    
         }
       }  
       foreach ($rows as $registro) {       
         $recursoObject = $this->findModelBySlug($registro["Slug"]);
         if($recursoObject =="notfound"){
           $count++;     
           continue;
         }
 
       $total = $registro["full_count"];
        
        $row = (new Query)->select('TvaloresDeCaracteristicas.Nombre')
        ->from('TvaloresDeCaracteristicas')
        ->where(['Caracteristicas_id' => 4])
        ->Join("INNER JOIN","RRecursosValoresDeCaracteristicas", "RRecursosValoresDeCaracteristicas.rIdValores = TvaloresDeCaracteristicas.idValoresDeCaracteristicas")
        ->Join("INNER JOIN","TRecursos", "TRecursos.idRecursos = RRecursosValoresDeCaracteristicas.rIdRecursos AND TRecursos.idRecursos = " . $registro['idRecursos'])
        ->one()  ;       
 
        $TipoRec = $row['Nombre'];
       // $ValoresTipo = $recursoObject->getTipoValores()->All();
       $text = Html::encode($registro["Nombre"]);
       $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $registro["Slug"]]);
       // $TipoRec = "";
       // foreach ($ValoresTipo as $valor) {
       //   $TipoRec =  $valor->Nombre;
       //   break;
       // }      
 
       $count++;                      
       //$preview = $recursoObject->getPreview($registro["Autor"],$registro["idRecursos"],"menu");
 
       $output[] = array("preview" => $recursoObject->getPreviewBusqueda($registro["Autor"],$registro["idArchivo"],$registro["preview"]),
                     "tipo" => $TipoRec,
                     "text" => $text,
                     "url" => $url); 
      }
      if($count==0){
       $output[] = array("preview" => "",
                     "tipo" => "Sin Resultados",
                     "text" => "",
                     "url" => "");     
      }
      $data = array(
        'resultados' => $output,
        'tags' => $tags,
        'cantidad'  => $count,
        'total' => $total
      );
 
      return json_encode($data);        
      }
     
    
     /**
      * Busca recursos por etiquetas
      * @author Daniel CM
      */
    public function recursosPorEtiqueta($Nombre){
      $query = (new Query)->select('TRecursos.Autor, TRecursos.idRecursos, TRecursos.Slug, TRecursos.Nombre,count(*) OVER() AS full_count,TArchivos.idArchivo,TArchivos.Nombre AS preview')
        ->from('TRecursos')
        ->where(['Estado' => 1])
        ->leftJoin("TArchivos", "TArchivos.idRecursos = TRecursos.idRecursos AND TArchivos.Nombre LIKE 'preview_%'");
      $query->innerJoin('RRecursosValoresDeCaracteristicas', 'RRecursosValoresDeCaracteristicas.rIdRecursos = TRecursos.idRecursos')
        ->innerJoin('TvaloresDeCaracteristicas', 'RRecursosValoresDeCaracteristicas.rIdValores = TvaloresDeCaracteristicas.idValoresDeCaracteristicas');
      $query->where(['like', 'TvaloresDeCaracteristicas.Nombre', $Nombre]);
      $resultado = $query->all();
    return $resultado;
    }
    public function actionVerBusquedas() {
        
      $Tag = "";
      $Nombre = ""; 
      $Autor = "";
      $Tipo = "";
      $anio1 = "";
      $anio2 = "";
      $Dirigido = "";
      $Nivel = "";
      $Asignatura = "";
      $Idioma = "";
      $Usos = "";
      $limit = 0; //Cantidad de Registros 
      $offset = 0; //Registros a saltar

      if(isset($_POST['limit'])){
        $limit = $_POST['limit']; 
      }
      if(isset($_POST['offset'])){
        $offset = $_POST['offset']; 
      }
      if(isset($_POST['Tag'])){
        $Tag = $_POST['Tag']; 
      }     
     if(isset($_POST['Nombre'])){
       $Nombre = $_POST['Nombre']; 
     }
     if(isset($_POST['Autor'])){
       $Autor = $_POST['Autor']; 
     }
     if(isset($_POST['Tipo'])){
       $Tipo = $_POST['Tipo']; 
     }     
     if(isset($_POST['anio1'])){
       $anio1 = $_POST['anio1']; 
     }          
     if(isset($_POST['anio2'])){
       $anio2 = $_POST['anio2']; 
     }   
     if(isset($_POST['Dirigido'])){
       $Dirigido = $_POST['Dirigido']; 
     }  
     if(isset($_POST['Nivel'])){
       $Nivel = $_POST['Nivel']; 
     }  
     if(isset($_POST['Asignatura'])){
       $Asignatura = $_POST['Asignatura']; 
     }  
     if(isset($_POST['Idioma'])){
       $Idioma = $_POST['Idioma']; 
     }       
     if(isset($_POST['Usos'])){
       $Usos = $_POST['Usos']; 
     }  
    
      return $this->ViewBusquedas($Tag,$Nombre,$Autor,$Tipo,$anio1,$anio2,$Dirigido,$Nivel,$Asignatura,$Idioma,$Usos,$limit,$offset);

    }    


    /**
     * The user is follower yet
     * @param 
     * @return boolean
     */
    public function isRecursoColeccion($idColeccion, $idRecursos) {

     $count = (new Query())->select('idColeccion')
    ->from('TColeccionesRecursos')
    ->where(['idColeccion' =>$idColeccion])
    ->andWhere(['idRecursos' => $idRecursos])
    ->count();

        return $count == 1;
    }



    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarRecursoColeccion($idColeccion, $idRecursos) {
      if(!$this->isRecursoColeccion($idColeccion,$idRecursos)){
        if(Yii::$app->db->createCommand()->insert('TColeccionesRecursos', [
                 'idColeccion' => $idColeccion,
                 'idRecursos' => $idRecursos,
                 'FechaDeCreacion' => new Expression("GETDATE()"),                 
               ])->execute()){
            $model = Recursos::find()->andFilterWhere(['idRecursos' => $idRecursos])->one();
            if($model->Autor != Yii::$app->user->identity->id){
              Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::COLLECTION_RECURSO, "Recurso agregado a colección", $model->idRecursos, 
                $model->Slug, $model->Autor);  
            }
            return "Recurso agregado correctamente a la Colección";
        }
        else{
          return "Recurso no se pudo agregar a la Colección";  
        }
      }
      else{
        return "Recurso ya estaba en esta Colección";
      }
    }

    public function actionRecursoColeccion() {
      $idColeccion = $_POST["idColeccion"];
      $idRecursos = $_POST["idRecursos"];        

      return $this->agregarRecursoColeccion($idColeccion,$idRecursos);         
    }


    /**
     *  List all resources with status approved and belongs to the current user
     * @return mixed
     */
    public function actionMiMochila() {

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $dataProvider = new ActiveDataProvider([
            'query' => Recursos::find()->approved()->
                    leftJoin("RFavoritos", "TRecursos.idRecursos = RFavoritos.rIdRecursos")
                    ->where([//dejar el filtro como where unicamente para listar los que han sido eliminados tambien
                        "rIdUsuarios" => Yii::$app->user->identity->id
                    ]),
            'pagination' => [
                'pageSize' => Yii::$app->params["recursosPerPage"],
            ],
        ]);

        return $this->render('mi-mochila', [
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
        ]);
    }

    /**
     *  Most recent resources list
     * @return mixed
     */
    public function actionMasRecientes() {

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $dataProvider = new ActiveDataProvider([
            'query' => Recursos::getRecursosMasRecientes(),
            'pagination' => [
                'pageSize' => Yii::$app->params["recursosPerPage"],
            ],
        ]);

        return $this->render('mas-recientes', [
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
        ]);
    }

    /**
     * Return the recursos list when the user clic on all recursos or when the users
     * search by Busqueda avanzada form.
     * 
     *   
     * @return mixed
     */
    public function actionResultadosDeBusqueda() {
        $searchModel = new RecursosAdvancedSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $valoresRequested = isset($queryParams["RecursosAdvancedSearch"]) ? $this->getValoresPost($queryParams["RecursosAdvancedSearch"]) : array();
        //agregar evento para guardar la busqueda  
        Busquedas::addNewSearch($valoresRequested, isset($queryParams["RecursosAdvancedSearch"]["Nombre"]) ? $queryParams["RecursosAdvancedSearch"]["Nombre"] : null);

        return $this->render('resultados-de-busqueda', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
                    'queryParams' => $queryParams,
        ]);
    }

    public function actionTags($tag) {

    /*    $modelTag = ValoresDeCaracteristicas::findOne(["Slug" => $tag]);

        if (!is_null($modelTag)) {
            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
            $this->view->params['configuraciones_generales'] = $configuraciones;

            $dataProvider = new ActiveDataProvider([
                'query' => $modelTag->getRecursos(),
                'pagination' => [
                    'pageSize' => Yii::$app->params["recursosPerPage"],
                ],
            ]);

            return $this->render('tags', [
                        'dataProvider' => $dataProvider,
                        'configuraciones' => $configuraciones,
                        'tagNombre' => $modelTag->Nombre,
            ]);
        } else {*/
            throw new NotFoundHttpException('La página solicitada no existe');
    }

    public function actionDescargarRecurso($recurso,$idArchivo="",$idRecursos="",$Nombre="",$file="") {
        if($idRecursos==""){
          $recursoObject = $this->findModelBySlug($recurso);    
          $path = $recursoObject->getRecursoPath();
        }
        else{
          $recursoObject = $this->findModel($idRecursos);  
          $Autor = $recursoObject->Autor; 
          $rutaorg = Yii::getAlias('@recursos') . '/' . $Autor . '/'; 
          $path = $rutaorg . $idArchivo . '_' . $Nombre;
        }
        if (!file_exists($path)) {
          $path = $rutaorg . $idArchivo . '_' . $file;
        }
        


        if (file_exists($path)) {
            //registrar evento
            if($recursoObject->Autor != Yii::$app->user->identity->id){
              Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::DESCARGAR_RECURSO, "Recurso descargado", $recursoObject->idRecursos, $recurso,  $recursoObject->Autor);
            }

            return Yii::$app->response->sendFile($path);
        }

        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

    protected function getValoresPost($params) {

        $valoresRequested = [];
        foreach ($params as $key => $valor) {
            if ($valor && ($key != "Nombre")) {
                $valoresRequested[] = $valor;
            }
        }
        return $valoresRequested;
    }

}
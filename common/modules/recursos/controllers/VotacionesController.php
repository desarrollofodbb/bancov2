<?php

namespace common\modules\recursos\controllers;

use Yii;
use yii\web\Controller;
use common\modules\recursos\models\Votaciones;
use common\modules\reportes\models\Eventos;
use yii\filters\VerbFilter;

/**
 * RecursosController implements the CRUD actions for Recursos model.
 */
class VotacionesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'votar-recurso' => ['POST'],
                ],
            ],
        ];
    }

    /**
     *  
     * Votar recurso
     * @param integer $id
     * @return mixed
     */
    public function actionVotarRecurso() {
        $votacionesParams = Yii::$app->request->post("Votaciones");
        $rIdRecursos = $votacionesParams["rIdRecursos"];
        $rIdUsuarios = $votacionesParams["rIdUsuarios"];
        $votacionValor = $votacionesParams["Votacion"];
        $recurso = Yii::$app->request->post("recurso");

        $votacion = Votaciones::getCurrentUserVotacion($rIdRecursos);
        $votacion->rIdRecursos = $rIdRecursos;
        $votacion->rIdUsuarios = $rIdUsuarios;
        $votacion->Votacion = $votacionValor;
        $votacion->save(); //se guarda la votacion
        //registrar evento
        Eventos::addEvent(Eventos::CATEGORY_RECURSOS, Eventos::VOTAR_RECURSO, "", $rIdRecursos,$recurso);

        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

}

<?php

namespace common\modules\recursos\controllers;

use Yii;
use yii\web\Controller;
use common\modules\recursos\models\Colecciones;
use yii\filters\VerbFilter;
use common\modules\recursos\models\Recursos;
use common\models\Configuraciones;

/**
 * RecursosController implements the CRUD actions for Coleccioneso model.
 */
class ColeccionesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'eliminar-de-mi-coleccion' => ['POST'],
                    'agregar-recurso-coleccion' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Colecciones models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ColeccionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Colecciones model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//        $this->layout = "layouts/main.php";
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Colecciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Colecciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->idColeccion]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Colecciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCrearColeccion() {
        $model = new Colecciones();
        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $model->Estado = true;
        $this->view->params['configuraciones_generales'] = $configuraciones;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['crear-coleccion', 'id' => $model->idColeccion]);
        } else {
       ;
            return $this->render('crear-coleccion', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Add an existing colecction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $recursoId
     * @return mixed
     */
    public function actionAgregarRecursoColeccion() {
        $recursoId = Yii::$app->request->post("recursoId");
        $coleccionId = Yii::$app->request->post("coleccionId");
        $recurso = Yii::$app->request->post("recurso");
        $favorito = new Favoritos();

        $favorito->rIdRecursos = $recursoId;
        $favorito->rIdUsuarios = Yii::$app->user->identity->id;
        if ($favorito->save()) {
            Yii::$app->session->setFlash("success", Yii::t("app", "El recurso se ha  agregado a su mochila correctamente"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "El recurso no se ha podido agregar a su mochila, por favor refresque la página e intente nuevamente"));
        }
        return $this->redirect(["/recursos/recursos/ver-recurso", "recurso" => $recurso]);
    }

    /**
     * Updates an existing Colecciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', yii::t('app', 'La colección se actualizó correctamente'));
            return $this->redirect(['update', 'id' => $model->idColeccion]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Recursos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($idColeccion) {
        $this->findModelBySlug($idColeccion)->softDelete();
        Yii::$app->session->setFlash("success", "La colección se ha eliminado correctamente");
        return $this->redirect(["resultados-de-busqueda-coleccion"]);
    }

    /**
     * Finds the Colecciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {  
        if (($model = Colecciones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

}

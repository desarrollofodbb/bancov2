<?php

namespace common\modules\recursos;

/**
 * recursos module definition class
 */
class Colecciones extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\recursos\controllers';

    /**
     * @var string module name
     */
    public static $name = 'colecciones';



    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    public function behaviors() {
        return [
            [//SluggableBehavior automatically fills the specified attribute with a value that can be used a slug in a URL.
                'class' => SluggableBehavior::className(),
                'attribute' => 'Nombre',
                'slugAttribute' => 'Slug',
//                'immutable' => true,
                'ensureUnique' => true,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
                'value' => function ($event) {
                    return new Expression("GETDATE()");
                },
            ],
        ];
    }    

}

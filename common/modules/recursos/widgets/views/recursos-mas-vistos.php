<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<!-- <link rel="stylesheet" href="css/demo.css"> -->
<style type="text/css">

/*.w3-marginleft {
  margin-left: 12px;
}*/
</style>
<?php if ($RecursosMasVistos): ?>


<!--         <h1><?php echo Yii::t("app", "Los más vistos"); ?> <span class="icon icon-fod-icon-cloud"></span></h1> -->
        <?php


        $index = 0;
        $iterador = 1;
     //   $recursosCount = count($RecursosMasVistos);
        ?> 
<div class="w3-contentnomargins w3-marginleft">    
  <div id="relacionados" class="wf-container" hidden>           
    <?php foreach ($RecursosMasVistos as $recurso): ?>
      <?php
        echo $this->render("content/recurso-relacionado", ["model" => $recurso]);
      ?>
    <?php endforeach; ?>
  </div>
</div>


<?php endif; ?>
<script>
		$(window).load(function() {
			// Animate loader off screen
      $("#relacionados").show();
		});
</script>
<!--     <script src="js/responsive_waterfall.js"></script>
    <script src="js/app.js"></script> -->
<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\imagine\Image;
?>
<style type="text/css">
body {
  font-family: "Source Sans Pro", sans-serif;
  line-height: 1.6;
  min-height: 100vh;
  display: grid;
  place-items: center;
}  
</style>
<div class="wf-box">
<?php    
  if (!Yii::$app->user->isGuest) {   
    $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $model->Slug]);          
    echo '<a href="'.$url.'">';
} ?>
        <?php
          $TipoRec = "";
          echo $model->getPreview($model->Autor,$model->idRecursos,"simplezoom",Yii::$app->user->isGuest);
          $ValoresTipo = $model->getTipoValores()->All();
          foreach ($ValoresTipo as $valor) {
            $TipoRec =  $valor->Nombre;
          }
        ?>
<?php    
  if (!Yii::$app->user->isGuest) {           
echo '</a>';
} ?>

    <div class="content" style="margin-top:10px;margin-bottom:10px">
        <h3 onclick='document.getElementById("id01").style.display="block"' class="no-margin" style="color:#999999;font-size: 18px"  ><?php echo $TipoRec; ?></h3>
        <?php
        $len = strlen($model->Nombre);
        $text = Html::encode($model->Nombre);
        $text = substr($text, 0,50);
        if($len>50){
          $text = $text . "...";  
        }
        if (!Yii::$app->user->isGuest) { 
          $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $model->Slug]);
        }
        else{
          $url = "<p style = 'font-size: 18px' onclick='document.getElementById('id01').style.display='block'>".$text."</p>";
        }
        $options = ["style" => "font-size: 16px"];
        ?> 
        <h4>
        <?php    
        if (!Yii::$app->user->isGuest) { 
          echo Html::a($text, $url, $options);
        } else{
        ?>
        <p onclick='document.getElementById("id01").style.display="block"'><?php echo $text ?></p>
        <?php } ?>
        </h4>
<!--     <p>
        <?php
        $the_content = $model->ObjetivoDelRecurso;
        echo Yii::$app->CustomFunctionsHelper->getTheExcerpt($the_content, Yii::$app->params["excerptLengthRecursosListados"]);
        ?>   
    </p>  -->       
  </div>
</div>
 

    <?php
    $text = '<span class="icon icon-fod-icon-plus"></span>';
    $url = Url::to(["/recursos/recursos/ver-recurso", "recurso" => $model->Slug]);
    $options = ["class" => "md-link fucsia-link"];
    // echo Html::a($text, $url, $options);
    ?> 

<?php

use yii\helpers\Html;
?>
<!-- <link rel="stylesheet" href="css/demo.css"> -->
<style type="text/css">
wf-container.menos:style{
  padding-left: 10px;
}
body {
  font-family: "Source Sans Pro", sans-serif;
  line-height: 1.6;
  min-height: 100vh;
  display: grid;
  place-items: center;
}
</style>
<?php if ($RecursosRelated): ?>



        <?php
        $index = 0;
        $iterador = 1;
        $recursosCount = count($RecursosRelated);
        ?>
    
  <div class="wf-container">    

        <?php foreach ($RecursosRelated as $recurso): ?> 
                    <?php
                    echo $this->render("content/recurso-relacionado", [
                        "model" => $recurso
                    ]);
                    ?>
        <?php endforeach; ?>
    </div>



<?php endif; ?>
<!--     <script src="../responsive_waterfall.js"></script>
    <script src="app.js"></script> -->

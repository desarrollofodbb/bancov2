<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use common\modules\recursos\widgets\RecursosMasVistos;
use kartik\select2\Select2;
use kartik\alert\Alert;
use yii\helpers\ArrayHelper;
use common\models\User;
//  $log_filename = $_SERVER['DOCUMENT_ROOT'] . '/log';

//      if (!file_exists($log_filename))
//      {
//          // create directory/folder uploads.
//          mkdir($log_filename, 0777, true);
//      }
//      $log_file_data = $log_filename.'/log.log';     
// $t=time();
// $hora = date("H:i:s",$t);       
//      file_put_contents($log_file_data, "S-BP-" . $hora . "\n", FILE_APPEND);  

$VdC = new ValoresDeCaracteristicas();
$tag = "";
if(isset($_GET['tag'])){
  $tag = $_GET['tag'];
}

$buscar_nombre = "";
if(isset($_GET['buscar_nombre'])){
  $buscar_nombre = $_GET['buscar_nombre'];
}

$logueado = 0;
$iduser = 0;
if(isset(Yii::$app->user->identity->id)){
  $logueado = 1;
  $user = new User();
  $user = $user->findIdentity(Yii::$app->user->identity->id);    
  $iduser = Yii::$app->user->identity->id;   
}

?>
<style type="text/css">


.boton {
  border: none;
  border-bottom-left-radius: 30px; 
  border-top-left-radius: 30px;
  background-color: #666666;
  color: white;
  padding: 2px 2px;
  cursor: pointer;
}
.boton:hover{
  background-color: #00CC99;
}
.boton:focus{
  outline:none;    
}

        .btn-circle.btn-sm { 
            top: 1px;
            right: 1px;
            width: 32px; 
            height: 32px; 

            border-bottom-right-radius: 30px;
    border-bottom-left-radius: 30px;
    border-top-right-radius: 30px;
    border-top-left-radius: 30px;
            font-size: 8px; 
            text-align: center; 
        } 
        .btn-circle.btn-md { 
            width: 50px; 
            height: 50px; 
            padding: 7px 10px; 
            border-radius: 25px; 
            font-size: 10px; 
            text-align: center; 
        } 
        .btn-circle.btn-xl { 
            width: 70px; 
            height: 70px; 
            padding: 10px 16px; 
            border-radius: 35px; 
            font-size: 12px; 
            text-align: center; 
        } 
body{
  margin-top: 60px;
} 

</style>

<div id="rowpadding" class="w3-row" style="width:100%;top:50px">
    <div class="w3-threequarter">
      <div class="w3-hide-large w3-hide-medium w3-container" style="margin-top:20px"><p style="color:white">X</p></div>
      <div class="w3-bar"  style="margin-left: 20px;margin-bottom: 1px;margin-top: 30px;" id="searchlabels"> 
      <?php 
        if(isset(Yii::$app->params["autorProfuturo"])){
          echo '<a href="/recursos/recursos/mis-colecciones?Autor='.Yii::$app->params["autorProfuturo"].'">';
          echo '<img style="width:15%;font-size:12px;margin-bottom:5px" class="w3-round-xlarge w3-hover-opacity" src="' . Yii::$app->params["urlBack"] . "/uploads/etiqueta-profuturo666666.png" . '">';
          echo '</a>';
        }
      ?>

      </div>
    </div>
    <div class="w3-quarter">   
      <div class="w3-dropdown-click w3-block w3-right w3-border-0 w3-hide-small" style="margin-bottom: 25px;margin-top: 40px;font-size: 15px">
        <button id="zoom-button" class="boton w3-block w3-padding-small w3-border-0" onclick="myClick2(this)" > <h3 ><i class="fa fa-filter"></i>  Filtrar búsqueda</h3>  </button>
        <div id= "zoom" class="w3-dropdown-content w3-card-4 w3-bar-block dropdown-menu-right"  style="z-index:2;width:325px;height:400px; overflow:hidden; overflow-y:scroll;background-color: #EBEDEC;">
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
          <?php
          $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(4);
          foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
            $valor_key = explode("-", $caracteristica_key);               
            $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
            $caracteristica_field = $valor_key[1];
            $caracteristica_nombre = $valor_key[2];
            echo "<h3 class='TitulosFiltroBuqueda' style='margin-top: 10px'>" . $caracteristica_nombre . "</h3>";

            $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
            echo Select2::widget([
                'model' => $model,
                'attribute' => $caracteristica_field,
                'language' => 'es',
                'showToggleAll' => false,
                'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                'options' => [
                'multiple' => false,
                'class' => 'form-control; border-radius:60px;',
                'placeholder' => $placeholder,
                ],
                'pluginOptions' => [
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                    'maximumSelectionLength' => 7,
                ],
                'pluginEvents' => [
                   "change" => "function() { // function to make ajax call here 
                  resultadobusqueda('',this.value,'','','','','','','','');
                  // document.getElementById('zoom-button').click();
                 }",
                ],                  
            ]);
          endforeach; 
          ?>                 
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;color:black;margin-bottom: 10px">
          <h3 class='TitulosFiltroBuqueda'>Año</h3>
          <div class="InputRadioFiltroBusqueda">
            <input id = "rangoanio1"  type="radio" name="check" onclick="onlyOne(this,this.name)"> 2005 - 2009</input><br>
            <input id = "rangoanio2"  type="radio" name="check" onclick="onlyOne(this,this.name)"> 2010 - 2015</input><br>            
            <input id = "rangoanio3"  type="radio" name="check" onclick="onlyOne(this,this.name)"> 2016 - 2021</input><br><br>  
        </div>              
            <div class="w3-row">
              <div class="w3-third w3-padding-small">
                <input id="anio1" class="w3-input  w3-border-0 w3-round-xxlarge" style="padding: 5px"type="text" placeholder="Desde">
              </div>
              <div class="w3-third w3-padding-small">
                <input id="anio2" class="w3-input w3-border-0 w3-round-xxlarge" style="padding: 5px" type="text" placeholder="Hasta">
              </div>
              <div class="w3-third w3-padding-small">
                <button class="btn btn-success btn-circle btn-sm w3-text-white w3-center w3-padding-small w3-border-0" style="background-color:#00CC99; font-size: 16px;
  font-weight: normal; " onclick="buscaanios('bp')">Ir</button>  
              </div>
            </div>          
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
          <h3 class='TitulosFiltroBuqueda'>Autor</h3>
            <div class="w3-row">
              <div class="w3-twothird w3-padding-small">    
                <input id="autor" class="w3-input w3-border-0 w3-round-xxlarge" style="padding: 5px" type="text" placeholder="Buscar Autor">   
              </div>
              <div class="w3-third w3-padding-small">
                <button class="btn btn-success btn-circle btn-sm w3-text-white w3-center w3-padding-small w3-border-0"  style="background-color:#00CC99; font-size: 16px;
  font-weight: normal;  " onclick="buscaautor('bp')">Ir</button>  
              </div>              
            </div>
          </a>
          <a href="#" class="w3-bar-item w3-button" style="color: black; text-decoration: none;margin-bottom: 10px">
            <h3>Dirigido a</h3>
            <div class="InputRadioFiltroBusqueda">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(5);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                foreach ($valoresDeCaracteristica as $detalle_caracteristica => $key):
                  echo '<input name = "dirigido" id = "' . $detalle_caracteristica . '" type="radio" name="check1" onclick="onlyOne(this,this.name)">' . '  ' . $key['Nombre'] . '</input><br>';
                endforeach;
              endforeach; 
            ?>    
            </div>     
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(9);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                echo "<h3 class='TitulosFiltroBuqueda'>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo Select2::widget([
                          'model' => $model,
                          'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ],
                          'pluginEvents' => [
                             "change" => "function() { // function to make ajax call here 
                            resultadobusqueda('','','','','','',this.value,'','','');
                            // document.getElementById('zoom-button').click();
                           }",
                          ],                           
                        ]);
              endforeach; 
            ?>        
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(8);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                echo "<h3 class='TitulosFiltroBuqueda'>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo Select2::widget([
                          'model' => $model,
                          'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ],
                          'pluginEvents' => [
                             "change" => "function() { // function to make ajax call here 
                            resultadobusqueda('','','','','','','',this.value,'','');
                            // document.getElementById('zoom-button').click();            
                           }",                
                           ]           
                        ]);
              endforeach; 
            ?>        
          </a>    
          <a href="#" class="w3-bar-item w3-button" style="color: black; text-decoration: none;margin-bottom: 10px">
          <h3 class='TitulosFiltroBuqueda'>Idioma</h3>
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(2);
              
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                foreach ($valoresDeCaracteristica as $detalle_caracteristica => $key):
                  echo '<input name = "idioma" id = "' . $detalle_caracteristica . '" type="radio" name="check1" onclick="onlyOne(this,this.name)">' . '  ' . $key['Nombre'] . '</input><br>';
                endforeach;
              endforeach; 
            ?>  
               
          </a>    
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(6);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                $caracteristica_nombre = "Usos poderosos de la tecnologia";
                echo "<h3 class='TitulosFiltroBuqueda'>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo Select2::widget([
                          'model' => $model,
                          'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ],
                          'pluginEvents' => [
                             "change" => "function() { // function to make ajax call here 
                            resultadobusqueda('','','','','','','','','',this.value);
                            // document.getElementById('zoom-button').click();
                           }",                
                           ]                              
                ]);
              endforeach; 
            ?>        
          </a>
        </div>
      </div>     
    </div> 

  </div>




<div class="w3-contentnomargins w3-marginleft" style="padding-top: 1px">

    <div class="wf-container" id="resultado-busquedas">
    <?php
// $t=time();
// $hora = date("H:i:s",$t);       
//      file_put_contents($log_file_data, "S-BP1-" . $hora . "\n", FILE_APPEND);      
    if($tag == "" &&  $buscar_nombre==""){
      echo RecursosMasVistos::widget();        
    }
// $t=time();
// $hora = date("H:i:s",$t);       
//      file_put_contents($log_file_data, "S-BP2-" . $hora . "\n", FILE_APPEND);      
    ?>
    </div>
</div>

<div id="modal01" class="w3-modal" onclick="this.style.display='none'">
  <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
  <div class="w3-modal-content w3-animate-zoom">
    <img id="img01" style="width:100%" class="w3-round-xxlarge">
  </div>
</div>

<script>
function onZoom(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
}
</script>


<script type="text/javascript">
  logueado = <?php echo $logueado ?>;  
  $( document ).ready(function() {
   idsearch = document.getElementById("rowpadding");
   if(logueado==0){
    idsearch.style.display = 'none';
   }
   else{
    idsearch.style.display = 'block';
   }

  });
function myClick2(id) {
  var x = document.getElementById("zoom");    
  var y = document.getElementById("zoom-button");  
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    y.style = "background-color:#00CC99";
  } else { 
    x.className = x.className.replace(" w3-show", "");
    y.style = "background-color:#666666";
    y.className = "boton w3-block w3-padding-small w3-border-0";
  }
}
function buscaautor(param){
  autor = document.getElementById("autor").value;
  resultadobusqueda('','','','',autor,'','','','','');   
  if(param=="as"){
    // document.getElementById('zoom-but3').click();
  }
  else{
    // document.getElementById('zoom-button').click();
  }
}
function buscaanios(param){
  anio1 = document.getElementById("anio1").value;
  anio2 = document.getElementById("anio2").value;
  resultadobusqueda('','',anio1,anio2,'','','','','','');
  if(param=="as"){
    // document.getElementById('zoom-but3').click();
  }
  else{
    // document.getElementById('zoom-button').click();
  }  
}
function onlyOne(checkbox,namecheck="") {
    var checkboxes = document.getElementsByName(namecheck)
    checkboxes.forEach((item) => {
        if (item !== checkbox) item.checked = false
    })
    // $('#alert-id').fadeIn(500);
    if(checkbox.id=="rangoanio1"){
      resultadobusqueda('','',2005,2010,'','','','','','');
    }
    if(checkbox.id=="rangoanio2"){
      resultadobusqueda('','',2011,2020,'','','','','','');
    }
    if(checkbox.name=="dirigido"){
      resultadobusqueda('','','','','',checkbox.id,'','','','');
    }
    if(checkbox.name=="idioma"){
      resultadobusqueda('','','','','','','','',checkbox.id,'');
    }                
}    
</script>
<?php
$actual_uri = $_SERVER['REQUEST_URI'];

use yii\widgets\ActiveForm;
use common\models\User;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Configuraciones;
use common\widgets\Alert;

$logueado = 0;
$iduser = 0;
$VdC = new ValoresDeCaracteristicas();
if (isset(Yii::$app->user->identity->id)) {
    $logueado = 1;
    $user = new User();
    $user = $user->findIdentity(Yii::$app->user->identity->id);
    $iduser = Yii::$app->user->identity->id;
}

$autorProfuturo = 0;
$imageProfuturo = "";
$linkProfuturo = "";
if (isset(Yii::$app->params["autorProfuturo"])) {
    $autorProfuturo = Yii::$app->params["autorProfuturo"];
    $imageProfuturo = Yii::$app->params["urlBack"] . "/uploads/etiqueta-profuturo666666.png";
    $linkProfuturo = "/recursos/recursos/mis-colecciones?Autor=" . Yii::$app->params["autorProfuturo"];
}

$tag = "''";
$nombre = "''";
if (isset($_GET['tag'])) {
    $tag = $_GET['tag'];
}
if (isset($_GET['buscar_nombre'])) {
    $nombre = "'" . $_GET['buscar_nombre'] . "'";
}
$padding = 16;
if (Yii::$app->user->isGuest) {
    $padding = 8;
}
?>
<style type="text/css">
    * {
        box-sizing: border-box;
    }

    .boton-reg {
        border: 2px solid;
        border-color: #00CC99;
        background-color: #EBEDEC;
        color: #00CC99;
        padding: 3px 3px;
        font-size: 12px;
        font-weight: bold;
        font-color:#00CC99;
        cursor: pointer;
        padding-left:20px;
        padding-right:20px;

    }

    #link { 
        color: #00CC99; 
        text-decoration: none;
    } /* CSS link color */

    #link:hover { 
        color: white; 
        text-decoration: none;
    } /* CSS link color */
    /* Green */
    .exito-reg {
        border-color: #00CC99;
        color: #00CC99;
    }

    .exito-reg:hover {
        background-color: #666666;
        color: white;
    }


    .w3-boton{
        border:none;
        display:inline-block;
        font-weight: bold;
        font-size:12px;
        padding:8px 16px;
        margin-right:10px;
        vertical-align:middle;
        overflow:hidden;
        text-decoration:none;
        color:inherit;
        background-color:#B6B6B6;
        text-align:center;
        cursor:pointer;
        white-space:nowrap
    }
    .w3-boton:focus{
        outline:none;    
    }
    .w3-exito{
        border-color: #B6B6B6;
        color: #B6B6B6;    
    }
    .w3-exito:hover{
        background-color: #666666;
        color: white;    
    }
    .w3-input{
        width:75%;
        border: none;
    }
    .align-depends{
        float:right!important
    }
    .w3-inputnew{
        width:60%;
        margin-left: 5px;
        outline:none; 
        border: none;
    }


    @media screen  and  (max-width: 996px) {
        .w3-inputnew{
            width:45%;
        }
        .align-depends{
            width: 85%;
            text-align:center!important;
        }
    }

    @media screen  and  (max-width: 739px) {
        .w3-half{width:100%}    
        .w3-inputnew{
            width:65%;
        }
        .align-depends{
            width: 75%;
            text-align:center!important;
        }
    }

    @media screen  and  (max-width: 545px) {
        .w3-half{width:100%}    
        .w3-inputnew{
            width:54%;
        }
        .align-depends{
            margin: auto;
            width: 101%;
            padding: 1px;
        }
    }

    @media screen  and  (max-width: 479px) {
        .w3-half{width:100%}       
        .align-depends{
            text-align:center!important;
        }
    }

    .boton {
        border: none;
        border-bottom-left-radius: 30px; 
        border-top-left-radius: 30px;
        background-color: #666666;
        color: white;
        padding: 2px 2px;
        cursor: pointer;
    }
    .boton:hover{
        background-color: #00CC99;
    }
    .boton:focus{
        outline:none;    
    }

    .btn-circle.btn-sm { 
        top: 1px;
        right: 1px;
        width: 32px; 
        height: 32px; 
        border:none;
        border-bottom-right-radius: 30px;
        border-bottom-left-radius: 30px;
        border-top-right-radius: 30px;
        border-top-left-radius: 30px;
        font-size: 8px; 
        text-align: center; 
    } 
    .btn-circle.btn-md { 
        width: 50px; 
        height: 50px; 
        padding: 7px 10px; 
        border-radius: 25px; 
        font-size: 10px; 
        text-align: center; 
    } 
    .btn-circle.btn-xl { 
        width: 70px; 
        height: 70px; 
        padding: 10px 16px; 
        border-radius: 35px; 
        font-size: 12px; 
        text-align: center; 
    } 
    body {
        font-family: "Source Sans Pro", sans-serif;
        line-height: 1.6;
        min-height: 100vh;
        display: grid;
        place-items: center;
    }

    /* The snackbar - position it at the bottom and in the middle of the screen */
    #snackbar {
        visibility: hidden; /* Hidden by default. Visible on click */
        min-width: 250px; /* Set a default minimum width */
        margin-left: -125px; /* Divide value of min-width by 2 */
        background-color: #00CC99; /* Black background color */
        color: white; /* White text color */
        text-align: center; /* Centered text */
        border-radius: 2px; /* Rounded borders */
        padding: 16px; /* Padding */
        position: fixed; /* Sit on top of the screen */
        z-index: 1; /* Add a z-index if needed */
        left: 50%; /* Center the snackbar */
        bottom: 90px; /* 30px from the bottom */
        font-size: 14px;
    }

    /* Show the snackbar when clicking on a button (class added with JavaScript) */
    #snackbar.show {
        visibility: visible; /* Show the snackbar */
        /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
        However, delay the fade out process for 2.5 seconds */
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    /* Animations to fade the snackbar in and out */
    @-webkit-keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {bottom: 30px; opacity: 1;}
    }

    @keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {bottom: 30px; opacity: 1;}
    }

    @-webkit-keyframes fadeout {
        from {bottom: 30px; opacity: 1;}
        to {bottom: 0; opacity: 0;}
    }

    @keyframes fadeout {
        from {bottom: 30px; opacity: 1;}
        to {bottom: 0; opacity: 0;}
    }
</style>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<div class="w3-container">
    <div class="w3-modal" id="m-nosearch" tabindex="-1" role="dialog" style="display:none">
        <div class="w3-modal-content w3-animate-zoom w3-round-xlarge" style="width:375px;max-height:775px;" role="document">
            <div class="modal-content w3-animate-zoom w3-round-xlarge"  >
                <div class="w3-container w3-center w3-padding">
                    <h2 style="color:black;font-weight: bold;margin-top: 20px">Búsqueda finalizada</h2>
                </div>    
                <div class="modal-body" style="text-align: center;">
                    <h1>¡Disculpe!</h1>
                    No se encontraron coincidencias.</br>
                    Reintente con otros criterios.</br>
                </div>
                <br><br>
                <div class="w3-container w3-center w3-padding">          
                    <div class="w3-third">                    
                        <p style="color:white">A</p>
                </div>
                <div class="w3-third">          
                        <button id="btn_finalizar" name="btn_creacoleccion" class="w3-button_green w3-round-xxlarge w3-border-0" style="font-size: 15px;padding-bottom:5px;padding-top:5px;padding-right: 25px;padding-left: 25px" onclick="cierrawin('m-nosearch')"> Aceptar</button>
                 </div>      
                    <div class="w3-third">                    
                        <p style="color:white">B</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w3-container">
    <div class="w3-modal" id="m-search" tabindex="-1" role="dialog" aria-labelledby="Spinner" style="display:none" >
        <div class="w3-modal-content w3-animate-zoom w3-round-xlarge" role="document"  style="width:375px;max-height:775px;">
            <div class="modal-content">
                <div class="modal-body">
                    <h1>Se está realizando la búsqueda</h1>
                    <p>Esto puede tardar unos segundos.</p>
                    <p>
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Cargando...</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="w3-top"> 
    <div class="w3-white w3-border w3-row-padding w3-padding-<?php echo $padding ?>"> 
        <!-- LOGIN / USER STATUS -->
        <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-md-6 col-xs-12 w3-left-align">
                <form action="<?php echo Yii::$app->homeUrl ?>" method="get">    
                    <div class="input-group">
                        <?php
                        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
                        if ($configuraciones["generalHeaderLogo"]) {
                            ?>
                            <a  href="<?php echo Yii::$app->homeUrl; ?>">         
                                <div class="w3-hide-small">
                                    <?php
                                    echo Html::img(Yii::$app->params["urlBack"] . $configuraciones["generalHeaderLogo"]->getThumbUrl('small'), ["alt" => Yii::$app->name, 'class' => "pull-left"]);
                                    ?>
                                </div>
                                <div class="w3-hide-medium w3-hide-large">
                                    <?php
                                    echo Html::img(Yii::$app->params["urlBack"] . $configuraciones["generalHeaderLogo"]->getThumbUrl('thumbnail'), ["alt" => Yii::$app->name, 'class' => "pull-left"]);
                                    ?>
                                </div>            
                            </a>                         

                        <?php } else { ?>
                            <a href="#" class="w3-bar-item w3-button w3-round-xxlarge"><i class="fa fa-home"></i></a>
                        <?php } ?>                
                        <input class="w3-inputnew" name = "buscar_nombre" id="buscar_nombre" type="text"  placeholder="Buscar" style="border-radius: 15px 0px 0px 15px;background-color: #EAECEB;padding-left: 10px;font-size:15px;margin-left:15px"></input> 
                        <div class="input-group-append"> 
                            <button type="submit" class="w3-bar-item w3-button" id="butbusca" style="border-radius: 0px 15px 15px 0px;background-color: #EAECEB"><i style="color:#717070" class="fa fa-search fa-2x" ></i></button>  
                        </div>
                    </div>
                </form>        
            </div>
            <div class="col-md-6 col-xs-12 w3-center">         
                <div class="align-depends">
                    <div class="w3-bar">
                        <a class="w3-bar-item w3-button fa fa-play fa-2x   w3-round-xxlarge" style="text-decoration: none;color:#717070" data-toggle="tooltip" title = "Video Introductorio" href="<?php echo Yii::$app->params["videolink"] ?>" target="_blank">
                        </a> 
                        <a class="w3-bar-item w3-button fa fa-upload fa-2x  w3-round-xxlarge" style="text-decoration: none;color:#717070" data-toggle="tooltip" title = "Subir Recursos" href="/recursos/recursos/crear-recurso">
                        </a>  
                        <a class="w3-bar-item w3-button fa fa-user fa-2x  w3-round-xxlarge"  style="text-decoration: none;color:#717070" data-toggle="tooltip" title = "Mis Colecciones" href="/recursos/recursos/mis-colecciones">
                        </a> 
                        <div class="w3-dropdown-click">
                            <button id="zoom-but1" onclick="myClick(this)" class="w3-button w3-round-xxlarge" title="Avisos Recibidos">
                                <span id="count" class="w3-badge w3-right w3-small count-value" style="background-color:#F29222; border-radius:15px;"></span>
                                <i style="color:#717070" class="fa fa-bell fa-2x"></i>
                            </button> 
                            <div id="zoom1" class="w3-dropdown-content dropdown-menu-value" style="width:300px; max-height:300px ; overflow:hidden; overflow-y:scroll; right:0">
                            </div>
                        </div>   
                        <?php
                        if ($_SERVER["REQUEST_URI"] != "/recursos/recursos/crear-recurso") {
                            ?>      
                            <div class="w3-dropdown-click w3-hide-medium w3-hide-large">
                                <button id="zoom-but3" onclick="myClick(this)" class="w3-button w3-round-xxlarge" title="Búsqueda Avanzada"><i style="color:#717070" class="fa fa-filter fa-2x"></i></button> 
                                <div id="zoom3" class="w3-dropdown-content w3-card-4 w3-bar-block dropdown-menu-right" style="z-index:2; height:400px; overflow:hidden; overflow-y:scroll;background-color: #EBEDEC;">
                                    <?php
                                    include('advancedsearch.php');
                                    ?>
                                </div>
                            </div>  
                        <?php } ?>      
                        <div class="w3-dropdown-hover w3-hide-small" >
                            <button id="zoom-but2" onclick="myClick(this)" class="w3-button w3-round-xxlarge" title="Opciones Adicionales"><i style="color:#717070" class="fa fa-bars fa-2x"></i></button>     
                            <div id="zoom2" class="w3-dropdown-content w3-bar-block w3-card-4" style="width:300px; right:0">
                                <a href="/quienes-somos" style="text-decoration: none;font-size: 12px" class="w3-bar-item w3-button">¿Quiénes somos?</a>
                                <?php if (Yii::$app->user->can(Yii::$app->params['verificador_default_role'])): ?>
                                    <a class="w3-bar-item w3-button" style="text-decoration: none;font-size: 12px" href="<?php echo \yii\helpers\Url::to(["/recursos/recursos/aprobar-recursos"]) ?>"><?php echo Yii::t("app", "Aprobar recursos"); ?></a>
                                <?php endif; ?> 
                                <a href="/sobre-el-banco-de-recursos" style="text-decoration: none;font-size: 12px" class="w3-bar-item w3-button">Sobre Banco Recursos</a>
                                <a href="/contactenos" style="text-decoration: none;font-size: 12px" class="w3-bar-item w3-button">Contacto</a>
                                <a href="/preguntas-frecuentes" style="text-decoration: none;font-size: 12px" class="w3-bar-item w3-button">Preguntas Frecuentes</a>
                                <?=
                                Html::a(
                                        Yii::t("app", 'Cerrar sesión'), ['/site/logout'], ['data-method' => 'post', 'class' => 'w3-bar-item w3-button', 'style' => 'text-decoration: none;font-size: 12px']
                                )
                                ?>
                            </div>
                        </div>  


                    </div>
                </div>
            </div>
        <?php } else { ?>

            <?php
            $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());

            if ($configuraciones["generalHeaderLogo"]) {
                ?>
                <div class="w3-half w3-container">  
                    <div class="w3-hide-small">     
                        <a  href="<?php echo Yii::$app->homeUrl; ?>">         
                            <?php
                            echo Html::img(Yii::$app->params["urlBack"] . $configuraciones["generalHeaderLogo"]->getThumbUrl('small'), ["alt" => Yii::$app->name, 'class' => "pull-left", 'style' => 'margin-top: 10px;margin-bottom:10px']);
                            ?>
                        </a>    
                    </div>   
                    <div class="w3-hide-medium w3-hide-large"> 
                        <a  href="<?php echo Yii::$app->homeUrl; ?>">         
                            <?php
                            echo Html::img(Yii::$app->params["urlBack"] . $configuraciones["generalHeaderLogo"]->getThumbUrl('thumbnail'), ["alt" => Yii::$app->name, 'class' => "pull-left", 'style' => 'margin-top: 10px;margin-bottom:10px']);
                            ?>
                        </a>    
                    </div>                                     
                <?php } else { ?>
                    <a href="#" class="w3-bar-item w3-button"><i class="fa fa-home"></i></a>
    <?php } ?>  
                <div class="w3-hide-medium w3-hide-large">
                    <h2 style="color:#00CC99;margin-top: 15px;margin-bottom:10px"><b>Banco de Recursos</b></h2>                
                </div>

                <?php } ?>
            <div class="error-login-wrap">
            <?php echo Alert::widget(["options" => ["class" => "error-login"]]) ?>
            </div>
            <?php
            $loginform = new common\models\LoginForm();
            $form = ActiveForm::begin(["action" => Url::to(['/site/login']), 'id' => 'login-form']);
            ?>
        </div>
        <div id="loginarea" class="w3-half w3-container w3-hide-small w3-hide-medium" style="display:none"> 
            <div class="form-inline" class="col-lg-6 col-md-6 col-sm-6">
                <div class="w3-third w3-padding-small">
<?php echo $form->field($loginform, 'username')->textInput(["class" => "w3-input w3-border-0", "style" => "border-radius: 15px 15px 15px 15px;background-color: #EAECEB;margin-top: 10px;margin-bottom:10px", "placeholder" => "Usuario"])->label(false); ?>

                    <p style="margin-left: 5px">
<?php echo Html::a(Yii::t("app", "Registrarme"), Yii::$app->params["perfilUsuarioCrearCuenta"], ["target" => "_blank", "style" => "color:#00CC99"]) ?>             
                    </p>

                </div>
                <div class="w3-third w3-padding-small">
                        <?php echo $form->field($loginform, 'password')->passwordInput(["class" => "w3-input w3-border-0", "style" => "border-bottom:none;border-radius: 15px 15px 15px 15px;background-color: #EAECEB; margin-bottom:10px;margin-top: 10px", "placeholder" => "Contraseña"])->label(false); ?>
                    <p style="margin-left: 5px">
<?php echo Html::a(Yii::t("app", "Olvidé mi contraseña"), Yii::$app->params["perfilUsuarioEditarCuenta"], ["target" => "_blank", "style" => "color:#00CC99"]) ?>
                    </p>             

                </div>
                <div class="w3-third w3-padding-small w3-right">
                    <!--             <?php echo $form->field($loginform, 'rememberMe')->checkbox()->label("Recordarme"); ?> -->
<?php echo Html::submitButton('Ingresar', ['class' => 'btn btn-default w3-round-xxlarge w3-border-0', 'style' => 'padding-left:30px; padding-right:30px;padding-top:5px;padding-bottom:5px;font-size:15px', 'name' => 'login-button']) ?>
                    <p style="color:white">xxxxxxxxx</p>

                </div>
            </div>

<?php ActiveForm::end(); ?>    
        </div>
    </div>                             
</div>
<div class="w3-container">

    <?php
    $loginform = new common\models\LoginForm();
    $form = ActiveForm::begin(["action" => Url::to(['/site/login']), 'id' => 'login-form']);
    ?>
    <div id="id01" class="w3-modal">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom w3-round-xlarge" style="max-width:330px;margin-bottom: 20px; ">
            <div style="margin-top: 15px">.</div>
            <div class="w3-center">
                <span style="color: #00CC99" onclick="document.getElementById('id01').style.display = 'none'" class="w3-button w3-xlarge w3-hover-opacity w3-display-topright" title="Cerrar"><i style="color: #00CC99">&times;</i></span>
                <h2 style="color:#00CC99">Ingresar a su cuenta</h2>
            </div>

            <div class="w3-section w3-padding-small w3-round-xxlarge">
                <div>
                    <label style="margin-left: 40px"><b style="color:#666666; font-weight:bold;">Nombre de usuario</b></label>
<?php echo $form->field($loginform, 'username')->textInput(["class" => "w3-input", "style" => "border-radius: 15px 15px 15px 15px;background-color: #EAECEB;width:75%;margin:auto", "placeholder" => "Usuario"])->label(false); ?>
                </div>
                <div>
                    <label style="margin-left: 40px"><b style="color:#666666; font-weight:bold;">Contraseña</b></label>
                    <?php echo $form->field($loginform, 'password')->passwordInput(["class" => "w3-input", "style" => "border-radius: 15px 15px 15px 15px;background-color: #EAECEB;width:75%;margin:auto", "placeholder" => "Contraseña"])->label(false); ?>
<?php echo Html::a(Yii::t("app", "¿Olvidé mi contraseña?"), Yii::$app->params["perfilUsuarioEditarCuenta"], ["target" => "_blank", "style" => "color:#00CC99;margin-left:40px"]) ?>     
                </div>     
                <div class="w3-center">
<?php echo Html::submitButton('Ingresar', ['class' => 'btn btn-default w3-round-xxlarge w3-border-0', 'name' => 'login-button', 'style' => 'font-size:12px;padding-left:35px;padding-right:35px;margin-left:0px;margin-top:10px;padding-top:9px;padding-bottom:9px']) ?>
                </div>
            </div>


            <div class="w3-container w3-padding-32 w3-light-grey w3-center " style="padding-bottom: 30px;border-bottom-left-radius: 30px; border-bottom-right-radius: 30px;">
                <!--         <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-button w3-red w3-round-xxlarge">Cancelar</button> -->
                <h5 style="color:#00CC99">¿Aún no tiene una cuenta en el Banco de Recursos?</h5>
                <h5 >Solo necesita una cuenta</h5>
                <h6 style="color:#666666;margin-bottom: 20px">Acceda a todas las plataformas FOD con una cuenta única</h6>
<?php echo Html::a(Yii::t("app", "Registrarse"), Yii::$app->params["perfilUsuarioEditarCuenta"], ["target" => "_blank", "id" => "link", "class" => "boton-reg exito-reg w3-round-xxlarge", 'style' => 'font-size:12px;padding-left:20px;padding-right:20px;margin-left:0px;margin-top:10px;padding-top:7px;padding-bottom:7px']) ?>
            </div>

        </div>
    </div>
<?php ActiveForm::end(); ?>     
</div>
<div id="snackbar"></div>
<?php
//  $t=time();
//  $hora = date("H:i:s",$t);       
//       file_put_contents($log_file_data, "END-BG-" . $hora . "\n", FILE_APPEND);  
if (Yii::$app->user->isGuest) {
    ?>
    <div style="margin-top: 50px"></div> 
<?php } ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
                    var path = window.location.pathname;
                    var page = path.split("/").pop();
                    logueado = <?php echo $logueado ?>;
                    newsearch = 1;
                    offset = 0;
                    limit = 10;
                    morescroll = 0;
                    gtag = "";
                    gtipo = "";
                    ganio1 = "";
                    ganio2 = "";
                    gautor = "";
                    gdirigido = "";
                    gnivel = "";
                    gasignatura = "";
                    gidioma = "";
                    gusos = "";
                    CurrentEvents = 0;
                    totalregs = 0;
                    tag = <?php echo $tag ?>;
                    nombre = <?php echo $nombre ?>;
                    autorProfuturo = '<?php echo $autorProfuturo; ?>';
                    imageProfuturo = '<?php echo $imageProfuturo; ?>';
                    linkProfuturo = '<?php echo $linkProfuturo; ?>';
                    inscroll = 0;
                    inEvents = 0;
                    var ignoreNextScrollEvent = false;
                    var veravi = "";
                    $(document).ready(function () {
                        idlogin = document.getElementById("loginarea");
                        if (logueado > 0) {
                            verificarAvisosUsr();
//                            VerificarAvisos(10000);
                            
                            if (idlogin) {
                                idlogin.style.display = 'none';
                            }
                                
//                            window.onload = function () {
//                                console.log("DOM fully loaded and parsed");
////                                VerificarAvisos(10000);
//
//                                if (idlogin) {
//                                    idlogin.style.display = 'none';
//                                }
//
//                            };
                        } else {
                            idlogin.style.display = 'block';
                        }


                        if (tag != "") {
                            resultadobusqueda(tag, '', '', '', '', '', '', '', '', '');
                        }
                        if (nombre != "") {
                            resultadobusqueda('', '', '', '', '', '', '', '', '', '');
                        }
                    });

                    $(window).on('mousewheel', function () {

                        ignoreNextScrollEvent = false;
                    });

                    window.onscroll = function () {


                        if (ignoreNextScrollEvent == true) {
                            ignoreNextScrollEvent = false;
                            // console.log("ignoring scroll SP:");
                            return;
                        }
                        var totalPageHeight = document.body.scrollHeight - 10;
                        var scrollPoint = window.scrollY + window.innerHeight;

// console.log("offset:" + offset + " totalregs " + totalregs + " morescroll " + morescroll);
                        if (scrollPoint >= totalPageHeight && morescroll == 1 && inscroll == 0 && ignoreNextScrollEvent == false && offset <= totalregs && totalregs > 0) {
// console.log("entro a scroll");

                            inscroll = 1;
                            buscar_nombre = document.getElementById("buscar_nombre").value;
                            if (buscar_nombre == "") {
                                buscar_nombre = nombre;
                            }
                            idbusqueda = document.getElementById("resultado-busquedas");
                            rutadata2 = "/recursos/recursos/ver-busquedas";
                            tag = gtag;
                            tipo = gtipo;
                            anio1 = ganio1;
                            anio2 = ganio2;
                            autor = gautor;
                            dirigido = gdirigido;
                            nivel = gnivel;
                            asignatura = gasignatura;
                            idioma = gidioma;
                            usos = gusos;
                            // alert(buscar_nombre);
                            clearInterval(veravi);
                            $.ajax({
                                url: rutadata2,
                                type: "POST",
                                data: {
                                    Nombre: buscar_nombre,
                                    Tag: tag,
                                    Tipo: tipo,
                                    anio1: anio1,
                                    anio2: anio2,
                                    Autor: autor,
                                    Dirigido: dirigido,
                                    Nivel: nivel,
                                    Asignatura: asignatura,
                                    Idioma: idioma,
                                    Usos: usos,
                                    limit: 15,
                                    offset: offset
                                },
                                success: function (datacontent) {
                                    var obj = JSON.parse(datacontent);
                                    offset = offset + obj.cantidad;
                                    // console.log("Cantidad encontrada " + obj.cantidad + " " + offset);
                                    if (obj.cantidad > 0) {
                                        totalregs = obj.total;
                                        var waterfall = new Waterfall({minBoxWidth: 200});
                                        for (i in obj.resultados) {
                                            box = nuevoNodo(obj.resultados[i].preview, obj.resultados[i].tipo, obj.resultados[i].text, obj.resultados[i].url);
                                            waterfall.addBox(box);
                                            $('#resultado-busquedas').append(waterfall);
                                        }
                                        // console.log("agregadas");
                                        if (obj.cantidad < 10) {
                                            mensajefade = "Mostrando últimos " + obj.cantidad + " recursos encontrados en esta búsqueda.";
                                        } else {
                                            mensajefade = "Mostrando " + obj.cantidad + " recursos adicionales de un total de " + totalregs + " encontrados";
                                        }

                                        fademessage(mensajefade);
                                        var elmnt = document.getElementById("resultado-busquedas");
                                        elmnt.scrollIntoView(false);
                                        elmnt.scrollTop = elmnt.scrollHeight;
                                        ignoreNextScrollEvent = true;
                                        inscroll = 0;
                                        if (offset == 0) {
                                            offset = obj.cantidad;
                                        }
                                        if (obj.cantidad < 10) {
                                            morescroll = 0;
                                            totalregs = 0;
                                        }
                                    } else {

                                        inscroll = 0;
                                        offset = 0;
                                        totalregs = 0;
                                        morescroll = 0;
                                        mensajefade = "No se encontraron más coincidencias";
                                        fademessage(mensajefade);
                                    }

                                },
                                error: function (x, z, t) {
                                    morescroll = 0;
                                    inscroll = 0;
                                }
                            });
                        }

                    };

                    function cierrawin(param) {

                        document.getElementById(param).style.display = 'none'
                    }

                    function fademessage(text) {
                        // Get the snackbar DIV
                        var x = document.getElementById("snackbar");

                        // Add the "show" class to DIV
                        x.innerHTML = text;
                        x.className = "show";


                        // After 3 seconds, remove the show class from DIV
                        setTimeout(function () {
                            x.className = x.className.replace("show", "");
                        }, 10000);
                    }

                    function nuevoNodo(preview, tipo, text, url) {
                        var box = document.createElement('div');
                        box.className = 'wf-box';
                        box.style = 'margin-top:15px';
                        var divimage = document.createElement('div');
                        divimage.className = 'image';
                        var image = document.createElement('img');
if(text!="Sin Resultados"){                         
                        image.className = "w3-border w3-round-xlarge w3-hover-opacity";
}                        
                        image.style = "width:100%;height:100%; border-radius:10%;cursor:pointer";

//     image.addEventListener("click", function(){
//       onZoom(this);
// });
                        image.src = preview;

                        var aTag = document.createElement('a');
if(text!="Sin Resultados"){                        
                        aTag.setAttribute('href', url);
                        aTag.setAttribute('target', '_self');
}                        

                        aTag.appendChild(image);

                        // divimage.appendChild(image);            

                        divimage.appendChild(aTag);

                        box.appendChild(divimage);
                        var content = document.createElement('div');
                        content.className = 'content';
                        content.style = "margin-top:10px;margin-bottom:10px";
                        var title = document.createElement('h3');
                        title.style = "color:#999999;font-family: 'Source Sans Pro', sans-serif;font-size:18px";
                        title.appendChild(document.createTextNode(tipo));
                        content.appendChild(title);
                        len = text.length;
                        text = text.substring(0, 50);
                        if (len > 50) {
                            text = text + "...";
                        }
                        var p = document.createElement('h4');
                        p.style = "color:black;font-family: 'Source Sans Pro', sans-serif;font-size:16px";
                        var aTag = document.createElement('a');
if(text!="Sin Resultados"){                           
                        aTag.setAttribute('href', url);
                        aTag.setAttribute('target', '_self');
}                        
else{
  text = "";
}
                        aTag.appendChild(document.createTextNode(text));
                        p.appendChild(aTag);
                        content.appendChild(p);
                        box.appendChild(content);

                        return box;
                    }

                    function resultadobusqueda(tag, tipo, anio1, anio2, autor, dirigido, nivel, asignatura, idioma, usos) {

                        document.getElementById('m-search').style.display = 'block';

                        buscar_nombre = document.getElementById("buscar_nombre").value;
                        idbusqueda = document.getElementById("resultado-busquedas");
                        rutadata1 = "/recursos/recursos/ver-busquedas";
                        if (buscar_nombre == "" && nombre != "") {
                            buscar_nombre = nombre;
                            document.getElementById("buscar_nombre").value = nombre;
                        }

                        // alert(buscar_nombre);
                        if (newsearch == 1) {
                            gtag = tag;
                            gtipo = tipo;
                            ganio1 = anio1;
                            ganio2 = anio2;
                            gautor = autor;
                            gdirigido = dirigido;
                            gnivel = nivel;
                            gasignatura = asignatura;
                            gidioma = idioma;
                            gusos = usos;
                        } else {
                            if (tag != "") {
                                gtag = tag;
                            }
                            if (tipo != "") {
                                gtipo = tipo;
                            }
                            if (anio1 != "") {
                                ganio1 = anio1;
                            }
                            if (anio2 != "") {
                                ganio2 = anio2;
                            }
                            if (autor != "") {
                                gautor = autor;
                            }
                            if (dirigido != "") {
                                gdirigido = dirigido;
                            }
                            if (nivel != "") {
                                gnivel = nivel;
                            }
                            if (asignatura != "") {
                                gasignatura = asignatura;
                            }
                            if (idioma != "") {
                                gidioma = idioma;
                            }
                            if (usos != "") {
                                gusos = usos;
                            }
                        }

                        newsearch = 0;

                        clearInterval(veravi);
                        $.ajax({
                            url: rutadata1,
                            type: "POST",
                            data: {
                                Nombre: buscar_nombre,
                                Tag: gtag,
                                Tipo: gtipo,
                                anio1: ganio1,
                                anio2: ganio2,
                                Autor: gautor,
                                Dirigido: gdirigido,
                                Nivel: gnivel,
                                Asignatura: gasignatura,
                                Idioma: gidioma,
                                Usos: gusos,
                                limit: 15,
                                offset: 0
                            },
                            success: function (datacontent) {
                                if(datacontent!==""){
                                        var obj =JSON.parse(datacontent.toString('utf8'));
                                    }
                                    else{
                                        var obj="";
                                        obj.cantidad=0;
                                    }
                                // console.log("primera busqueda " + obj.cantidad + " " + obj.total);
                                if (obj.cantidad > 0 && obj.cantidad <= 8) {
                                    //1
                                    totalregs = obj.total;
                                    document.getElementById('m-search').style.display = 'none';
                                    //$("#m-search").hide();
                                    $('#resultado-busquedas').empty();
                                    var waterfall = new Waterfall({minBoxWidth: 200});
                                    for (i in obj.resultados) {
                                        box = nuevoNodo(obj.resultados[i].preview, obj.resultados[i].tipo, obj.resultados[i].text, obj.resultados[i].url);
                                        waterfall.addBox(box);
                                        $('#resultado-busquedas').append(waterfall);
                                    }
                                    var charactercount = 0;
                                    $('#searchlabels').empty();
                                    var lenadi = 0;
                                    if (autorProfuturo > 0) {
                                        var image = document.createElement("img");
                                        image.className = "w3-round-xlarge w3-hover-opacity";
                                        image.style = "width:15%;font-size:12px;margin-bottom:5px; margin-right:10px;";
                                        image.src = imageProfuturo;
                                        var aTag = document.createElement('a');
                                        aTag.setAttribute('href', linkProfuturo);
                                        aTag.setAttribute('target', '_self');
                                        aTag.appendChild(image);
                                        var page = document.getElementById("searchlabels");
                                        page.appendChild(aTag);
                                        lenadi = 14;
                                    }
                                    entro = 0;
                                    for (i in obj.tags) {
                                        entro = 1;
                                        var text = obj.tags[i].Nombre + " ";
                                        charactercount = charactercount + text.length;
                                        if (charactercount > 85 - lenadi) {
                                            break;
                                        }
                                        var element = document.createElement("button");
                                        var textlabel = document.createTextNode(text);
                                        var i_element = document.createElement("i");
                                        i_element.className = "fa fa-plus";
                                        element.appendChild(textlabel);
                                        element.appendChild(i_element);
                                        element.className = 'w3-boton w3-exito w3-round-xxlarge w3-text-white ';
                                        element.style = "font-size:12px;margin-bottom:5px";
                                        element.id = obj.tags[i].rIdValores;
                                        element.onclick = function () {
                                            resultadobusqueda(obj.tags[i].rIdValores, '', '', '', '', '', '', '', '', '');
                                        };
                                        var page = document.getElementById("searchlabels");
                                        page.appendChild(element);
                                    }

                                    var waterfall = new Waterfall({minBoxWidth: 200});
                                    for (var o = 1; o < 18; o++) {
                                        box = nuevoNodo("../../recursosthumbnail/0.png", "", "Sin Resultados", "");
                                        waterfall.addBox(box);
                                        $('#resultado-busquedas').append(waterfall);

    
                                    }                                  
                        
                                    if(entro == 0){
                                    
                                    }
                                    offset = obj.cantidad;
                                    morescroll = 1;
                                    if (obj.cantidad < 10) {
                                        morescroll = 0;
                                        mensajefade = "1 Mostrando " + obj.cantidad + " recursos encontrados en esta búsqueda.";
                                    } else {
                                        mensajefade = "1 Mostrando " + obj.cantidad + " recursos de un total de " + totalregs + " encontrados";
                                    }
                                    fademessage(mensajefade);

                                } else if (obj.cantidad > 8) {
                                    //2
                                    totalregs = obj.total;
                                    document.getElementById('m-search').style.display = 'none';
                                    //$("#m-search").hide();
                                    $('#resultado-busquedas').empty();
                                    var waterfall = new Waterfall({minBoxWidth: 200});
                                    for (i in obj.resultados) {
                                        box = nuevoNodo(obj.resultados[i].preview, obj.resultados[i].tipo, obj.resultados[i].text, obj.resultados[i].url);
                                        waterfall.addBox(box);
                                        $('#resultado-busquedas').append(waterfall);
                                    }
                                    var charactercount = 0;
                                    $('#searchlabels').empty();
                                    var lenadi = 0;
                                    if (autorProfuturo > 0) {
                                        var image = document.createElement("img");
                                        image.className = "w3-round-xlarge w3-hover-opacity";
                                        image.style = "width:15%;font-size:12px;margin-bottom:5px; margin-right:10px;";
                                        image.src = imageProfuturo;
                                        var aTag = document.createElement('a');
                                        aTag.setAttribute('href', linkProfuturo);
                                        aTag.setAttribute('target', '_self');
                                        aTag.appendChild(image);
                                        var page = document.getElementById("searchlabels");
                                        page.appendChild(aTag);
                                        lenadi = 14;
                                    }
                                    entro = 0;
                                    for (i in obj.tags) {
                                        entro = 1;
                                        var text = obj.tags[i].Nombre + " ";
                                        charactercount = charactercount + text.length;
                                        if (charactercount > 85 - lenadi) {
                                            break;
                                        }
                                        var element = document.createElement("button");
                                        var textlabel = document.createTextNode(text);
                                        var i_element = document.createElement("i");
                                        i_element.className = "fa fa-plus";
                                        element.appendChild(textlabel);
                                        element.appendChild(i_element);
                                        element.className = 'w3-boton w3-exito w3-round-xxlarge w3-text-white ';
                                        element.style = "font-size:12px;margin-bottom:5px";
                                        element.id = obj.tags[i].rIdValores;
                                        element.onclick = function () {
                                            resultadobusqueda(obj.tags[i].rIdValores, '', '', '', '', '', '', '', '', '');
                                        };
                                        var page = document.getElementById("searchlabels");
                                        page.appendChild(element);
                                    }
                        
                                    if(entro == 0){
                                    
                                    }
                                    offset = obj.cantidad;
                                    morescroll = 1;
                                    if (obj.cantidad < 10) {
                                        morescroll = 0;
                                        mensajefade = "Mostrando " + obj.cantidad + " recursos encontrados en esta búsqueda.";
                                    } else {
                                        mensajefade = "Mostrando " + obj.cantidad + " recursos de un total de " + totalregs + " encontrados";
                                    }
                                    fademessage(mensajefade);

                                } else {
                                    //3
                                    offset = 0;
                                    morescroll = 0;
                                    // alert("No se encontraron coincidencias");
                                    document.getElementById('m-search').style.display = 'none';
                                    //$("#m-search").hide();
                                    document.getElementById('m-nosearch').style.display = 'block';

                                    //$("#m-search").hide();
                                    $('#resultado-busquedas').empty();

                                    var charactercount = 0;
                                    $('#searchlabels').empty();
                                    var lenadi = 0;                                    
                                    if (autorProfuturo > 0) {
                                        var image = document.createElement("img");
                                        image.className = "w3-round-xlarge w3-hover-opacity";
                                        image.style = "width:15%;font-size:12px;margin-bottom:5px; margin-right:10px;";
                                        image.src = imageProfuturo;
                                        var aTag = document.createElement('a');
                                        aTag.setAttribute('href', linkProfuturo);
                                        aTag.setAttribute('target', '_self');
                                        aTag.appendChild(image);
                                        var page = document.getElementById("searchlabels");
                                        page.appendChild(aTag);
                                        lenadi = 14;
                                    }            
                                    var waterfall = new Waterfall({minBoxWidth: 200});
                                    for (var i = 1; i < 24; i++) {
                                    

                                        box = nuevoNodo("../../recursosthumbnail/0.png", "", "Sin Resultados", "");
                                        waterfall.addBox(box);
                                        $('#resultado-busquedas').append(waterfall);

    
                                    }                                  
                                    //$("#m-nosearch").show();          
                                }

                                //  $('#resultado-busquedas').html(obj.mensajes);
                            },
                            error: function (x, z, t) {
                                document.getElementById('m-search').style.display = 'none';
                                //$("#m-search").hide();
                                morescroll = 0;
                            }
                        });

                    }

                    function obtener_avisos_session(fecha) {
                        if (window.sessionStorage != null) {
                            //Recuperar claves y escribirlas en los input correspondientes      
                            var fechaguardada = sessionStorage["fecha"];
                            // console.log(fecha + " " + fechaguardada);
                            if (fechaguardada == fecha) {
                                var avisos = sessionStorage.getItem("avisos");
                                var cantidad = sessionStorage.getItem("cantidad");
                                return cantidad + "|" + avisos;
                            }
                            return "";
                        }
                        return "";
                    }

                    function guardar_avisos_session(fecha, avisos, cantidad) {
                        if (window.sessionStorage != null) {
                            //Guardar avisos con los valores devueltos
                            // console.log("Guardando datos de Avisos");
                            sessionStorage.setItem("fecha", fecha);
                            sessionStorage.setItem("avisos", avisos);
                            sessionStorage.setItem("cantidad", cantidad);
                        }
                    }

                    function VerificarAvisos(ms) {
                        if (inEvents == 1) {
                            return;
                        }
                        if (ms > 0 && !$.trim($('.dropdown-menu-value').html())) {
                            inEvents = 1;
                            rutadate = "/recursos/recursos/last-eventos";
                            rutadata = "/recursos/recursos/view-eventos";
                            if (logueado > 0) {
                                Autor = <?php echo $iduser ?>;
                                inEvents = 1;
                                $.ajax({
                                    url: rutadate,
                                    type: "POST",
                                    data: {
                                        'Autor': Autor
                                    },
                                    success: function (data) {
                                        var lastdate_event = data;
                                        var avisos = obtener_avisos_session(lastdate_event);
                                        if (avisos != "") {
                                            var array = avisos.split("|");
                                            $('.dropdown-menu-value').html(array[1]);
                                            $('.count-value').html(array[0]);
                                            inEvents = 0;
                                        } else {
                                            $.ajax({
                                                url: rutadata,
                                                type: "POST",
                                                data: {
                                                    Autor: Autor
                                                },
                                                success: function (datacontent) {
                                                    var obj = JSON.parse(datacontent);
                                                    inEvents = 0;
                                                    $('.dropdown-menu-value').html(obj.mensajes);
                                                    $('.count-value').html(obj.cantidad);
                                                    guardar_avisos_session(obj.fecha, obj.mensajes, obj.cantidad);
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        
                        return setInterval(function () {
                            rutadate = "/recursos/recursos/last-eventos";
                            rutadata = "/recursos/recursos/view-eventos";
                            if (logueado > 0 && inEvents == 0) {
                                Autor = <?php echo $iduser ?>;
                                inEvents = 1;
                                $.ajax({
                                    url: rutadate,
                                    type: "POST",
                                    data: {
                                        'Autor': Autor
                                    },
                                    success: function (data) {
                                        // console.log("Terminó de verificar eventos interno");
                                        var lastdate_event = data;
                                        var avisos = obtener_avisos_session(lastdate_event);
                                        if (avisos != "") {
                                            var array = avisos.split("|");
                                            $('.dropdown-menu-value').html(array[1]);
                                            $('.count-value').html(array[0]);
                                            inEvents = 0;
                                            console.log("Using events saved!");
                                        } else {
                                            $.ajax({
                                                url: rutadata,
                                                type: "POST",
                                                data: {
                                                    Autor: Autor
                                                },
                                                success: function (datacontent) {
                                                    // console.log("Terminó de verificar eventos interno");
                                                    var obj = JSON.parse(datacontent);
                                                    inEvents = 0;
                                                    $('.dropdown-menu-value').html(obj.mensajes);
                                                    $('.count-value').html(obj.cantidad);
                                                    guardar_avisos_session(obj.fecha, obj.mensajes, obj.cantidad);
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                // console.log("Breaking Events Internal");
                            }

                        }, ms);

                    }
                    
                    
                    /**
                    * @author Dennis Madriz
                    */
                    function verificarAvisosUsr(){
                        rutadate = "/recursos/recursos/last-eventos";
                        rutadata = "/recursos/recursos/view-eventos";
                        Autor = <?php echo $iduser ?>;
                        $.ajax({
                            url: rutadate,
                            type: "POST",
                            data: {
                                'Autor': Autor
                            },
                            success: function (data) {
                                var lastdate_event = data;
                                var avisos = obtener_avisos_session(lastdate_event);
                                if (avisos != "") {
                                    var array = avisos.split("|");
                                    $('.dropdown-menu-value').html(array[1]);
                                    $('.count-value').html(array[0]);
                                } else {
                                    $.ajax({
                                        url: rutadata,
                                        type: "POST",
                                        data: {
                                            Autor: Autor
                                        },
                                        success: function (datacontent) {
                                            if(datacontent !=  ""){
                                                var obj = JSON.parse(datacontent);
                                                $('.dropdown-menu-value').html(obj.mensajes);
                                                $('.count-value').html(obj.cantidad);
                                                guardar_avisos_session(obj.fecha, obj.mensajes, obj.cantidad);
                                            }
//                                            }else{
//                                                $('.dropdown-menu-value').html("");
//                                                $('.count-value').html(0);
////                                                guardar_avisos_session(0, obj.mensajes, obj.cantidad);
//                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                    
                    
                    
                    function myClick(id) {
                        var x = document.getElementById("zoom1");
                        if (id.id == "zoom-but2") {
                            var x = document.getElementById("zoom2");
                        }
                        if (id.id == "zoom-but3") {
                            var x = document.getElementById("zoom3");
                        }
                        if (x.className.indexOf("w3-show") == -1) {
                            x.className += " w3-show";
                        } else {
                            x.className = x.className.replace(" w3-show", "");
                        }
                    }
</script>
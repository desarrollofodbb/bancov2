<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use common\modules\recursos\widgets\RecursosMasVistos;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\User;
// use kartik\alert\Alert;
$VdC = new ValoresDeCaracteristicas();
$tag = "''";
if(isset($_GET['tag'])){
  $tag = $_GET['tag'];
}

$logueado = 0;
$iduser = 0;
if(isset(Yii::$app->user->identity->id)){
  $logueado = 1;
  $user = new User();
  $user = $user->findIdentity(Yii::$app->user->identity->id);    
  $iduser = Yii::$app->user->identity->id;   
}

?>
<style type="text/css">
.boton {
  border: none;
  border-bottom-left-radius: 30px; 
  border-top-left-radius: 30px;
  background-color: #666666;
  color: white;
  padding: 2px 2px;
  cursor: pointer;
}
.boton:hover{
  background-color: #00CC99;
}
.boton:focus{
  outline:none;    
}

.btn-circle.btn-sm { 
  top: 1px;
  right: 1px;
  width: 32px; 
  height: 32px; 
  border-bottom-right-radius: 30px;
  border-bottom-left-radius: 30px;
  border-top-right-radius: 30px;
  border-top-left-radius: 30px;
  font-size: 8px; 
  text-align: center; 
} 
.btn-circle.btn-md { 
  width: 50px; 
  height: 50px; 
  padding: 7px 10px; 
  border-radius: 25px; 
  font-size: 10px; 
  text-align: center; 
} 
.btn-circle.btn-xl { 
  width: 70px; 
  height: 70px; 
  padding: 10px 16px; 
  border-radius: 35px; 
  font-size: 12px; 
  text-align: center; 
} 	
</style>
<?php 
// echo Alert::widget([
//     'type' => Alert::TYPE_INFO,
//     'title' => 'Heads up!',
//     'icon' => 'fas fa-info-circle',
//     'options'=>['id'=>'alert-id'],
//     'body' => 'This alert needs your attention, but it\'s not super important.',
//     'showSeparator' => true,
//     'delay' => false
// ]);
?>            
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
          <?php
          $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(4);
          foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
            $valor_key = explode("-", $caracteristica_key);               
            $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
            $caracteristica_field = $valor_key[1];
            $caracteristica_nombre = $valor_key[2];
            echo "<h3>" . $caracteristica_nombre . "</h3>";

            $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
            echo Select2::widget([
                'model' => $model,
                'attribute' => $caracteristica_field,
                'language' => 'es',
                'showToggleAll' => false,
                'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                'options' => [
                  'id' => '4',
                  'multiple' => false,
                  'class' => 'form-control; border-radius:60px;',
                  'placeholder' => $placeholder,
                ],
                'pluginOptions' => [
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                    'maximumSelectionLength' => 7,
                ],
                'pluginEvents' => [
                   "change" => "function() { // function to make ajax call here 
                  resultadobusqueda('',this.value,'','','','','','','','');
                  // document.getElementById('zoom-but3').click();
                 }",
                ],                  
            ]);
          endforeach; 
          ?>                 
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;color:black;margin-bottom: 10px; font-size:15px !important;">
            <h3>Año</h3>
            <input id = "rangoanio1" class="w3-radio" type="radio" name="check" onclick="onlyOne(this,this.name)"> 2005 - 2010</input><br>
            <input id = "rangoanio2" class="w3-radio" type="radio" name="check" onclick="onlyOne(this,this.name)"> 2011 - 2020</input><br><br>              
            <div class="w3-row">
              <div class="w3-third w3-padding-small" style="width: 45%">
                <input id="anio1" class="w3-input  w3-border-0 w3-round-xxlarge" style="padding: 5px"type="text" placeholder="Desde">
              </div>
              <div class="w3-third w3-padding-small" style="width: 45%">
                <input id="anio2" class="w3-input w3-border-0 w3-round-xxlarge" style="padding: 5px" type="text" placeholder="Hasta">
              </div>
              <div class="w3-third" style="width: 10%;padding-left: 10px">
                <button class="btn btn-success btn-circle btn-sm w3-text-white w3-center w3-padding-small w3-border-0" style="background-color:#00CC99; font-size: 16px;
  font-weight: normal; " onclick="buscaanios('as')">Ir</button>  
              </div>
            </div>          
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <h3>Autor</h3>
            <div class="w3-row">
              <div class="w3-twothird w3-padding-small" style="width: 90%">    
              <input id="autor" class="w3-input w3-border-0 w3-round-xxlarge" style="padding: 5px; font-size:15px !important;" type="text" placeholder="Buscar Autor">
              </div>
              <div class="w3-third" style="width: 10%;padding-left: 10px">
                <button class="btn btn-success btn-circle btn-sm w3-text-white w3-center w3-padding-small w3-border-0"  style="background-color:#00CC99; font-size: 16px;
  font-weight: normal;  " onclick="buscaautor('as')">Ir</button>  
              </div>              
            </div>
          </a>
          <a href="#" class="w3-bar-item w3-button" style="color: black; text-decoration: none;margin-bottom: 10px; font-size:15px !important;">
            <h3>Dirigido a</h3>
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(5);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                foreach ($valoresDeCaracteristica as $detalle_caracteristica => $key):
                  echo '<input name = "dirigido" id = "' . $detalle_caracteristica . '" class="w3-radio" type="radio" name="check1" onclick="onlyOne(this,this.name,"as")">' . '  ' . $key['Nombre'] . '</input><br>';
                endforeach;
              endforeach; 
            ?>         
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(9);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                echo "<h3>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo Select2::widget([
                          'model' => $model,
                          'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                          	'id' => '9',
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ],
                          'pluginEvents' => [
                             "change" => "function() { // function to make ajax call here 
                            resultadobusqueda('','','','','','',this.value,'','','');
                            // document.getElementById('zoom-but3').click();
                           }",
                          ],                           
                        ]);
              endforeach; 
            ?>        
          </a>
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(8);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                echo "<h3>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo Select2::widget([
                          'model' => $model,
                          'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                          	'id' => '8',
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ],
                          'pluginEvents' => [
                             "change" => "function() { // function to make ajax call here 
                            resultadobusqueda('','','','','','','',this.value,'','');
                            // document.getElementById('zoom-but3').click();      
                           }",                
                           ]           
                        ]);
              endforeach; 
            ?>        
          </a>    
          <a href="#" class="w3-bar-item w3-button" style="color: black; text-decoration: none;margin-bottom: 10px; font-size:15px !important;">
            <h3>Idioma</h3>
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(2);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                foreach ($valoresDeCaracteristica as $detalle_caracteristica => $key):

                  echo '<input name = "idioma" id = "' . $detalle_caracteristica . '" class="w3-radio" type="radio" name="check1" onclick="onlyOne(this,this.name)">' . '  ' . $key['Nombre'] . '</input><br>';
                endforeach;
              endforeach; 
            ?>            
          </a>    
          <a href="#" class="w3-bar-item w3-button" style="text-decoration: none;margin-bottom: 10px">
            <?php
              $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(6);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                $caracteristica_nombre = "Usos poderosos de la tecnologia";
                echo "<h3>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                echo Select2::widget([
                          'model' => $model,
                          'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                          	'id' => '6',
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ],
                          'pluginEvents' => [
                             "change" => "function() { // function to make ajax call here 
                            resultadobusqueda('','','','','','','','','',this.value);
                            // document.getElementById('zoom-but3').click();
                           }",                
                           ]                              
                ]);
              endforeach; 
            ?>        
          </a>
     
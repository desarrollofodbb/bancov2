<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dmstr\widgets\Menu;
use yii\helpers\Url;
use common\modules\recursos\widgets\BusquedaGeneral;
use common\components\AutoLoginFODWidget;

?>
<div id="header-search1" class="header-search1">
    <?php
    $form = ActiveForm::begin([
                'action' => ['/recursos/recursos/resultados-de-busqueda'],
                'method' => 'get',
    ]);
    ?>
    <?php
    echo $form->field($model, "Nombre")->textInput(["placeholder" => Yii::t("app", "Buscar")])->label(false);
    ?>
    <span class="input-group-btn">
        <?php echo Html::submitButton('<span class="icon icon-fod-icon-glass"></span>', ['class' => 'btn1 btn-default']) ?>
    </span>
    <span class="input-group-btn">
        <?php echo Html::submitButton('<span class="fa fa-play"></span>', ['class' => 'btn btn-default']) ?>
    </span>   
    <span class="input-group-btn">
        <?php echo Html::submitButton('<span class="fa fa-upload"></span>', ['class' => 'btn btn-default']) ?>
    </span>       
    <span class="input-group-btn">
        <?php echo Html::submitButton('<span class="fa fa-folder"></span>', ['class' => 'btn btn-default']) ?>
    </span>    
    <span class="input-group-btn">
        <?php echo Html::submitButton('<span class="fa fa-bell"></span>', ['class' => 'btn btn-default']) ?>
    </span>    
    <span class="input-group-btn">        
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <?php echo Html::submitButton('<span class="fa fa-bars"></span>', ['class' => 'btn btn-default']) ?></a>
<!--                          <?php
                        $query_params = Yii::$app->request->queryParams;
                        $current_page = isset($query_params["slug"]) ? $query_params["slug"] : "";

                        echo
                        Menu::widget(
                                [
                                    'options' => ['class' => 'nav navbar-nav'],
                                    'items' => [
                                        [
                                            'label' => Yii::t("app", "¿Quiénes somos?"),
                                            'icon' => '',
                                            'url' => '/quienes-somos',
                                            'active' => $current_page == "quienes-somos" ? true : false
                                        ],
                                        [
                                            'label' => Yii::t("app", "Sobre el banco de recursos"),
                                            'icon' => '',
                                            'url' => '/sobre-el-banco-de-recursos',
                                            'active' => $current_page == "sobre-el-banco-de-recursos" ? true : false
                                        ],
                                        [
                                            'label' => Yii::t("app", "Preguntas Frecuentes"),
                                            'icon' => '',
                                            'url' => '/preguntas-frecuentes',
                                            'active' => $current_page == "preguntas-frecuentes" ? true : false
                                        ],
                                        // [
                                        //     'label' => Yii::t("app", "Contáctenos"),
                                        //     'icon' => '',
                                        //     'url' => '/contactenos',
                                        //     'active' => $current_route == "paginas/paginas/contactenos" ? true : false
                                        // ],
                                    ],
                                ]
                        )
                        ?>     -->                        
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right">
                                    <a class="dropdown-item" href="/quienes-somos">
                                        ¿Quiénes somos?
                                    </a>
                                    <div class="dropdown-divider">
                                        
                                    </div>
                                    <a class="dropdown-item" href="/sobre-el-banco-de-recursos">
                                        Sobre el Banco de Recursos
                                    </a>
                                    <a class="dropdown-item" href="/sobre-el-banco-de-recursos"> 
                                        Contacto
                                    </a>
                                    <a class="dropdown-item" href="/contactenos">
                                        FAQ
                                    </a>
                                    <div class="dropdown-divider">
                                        
                                    </div>
                                    <a class="dropdown-item" href="login.html">
                                    Logout
                                    </a>
                                </div>
                            </div>
                        </li>    
    </span>     
    <?php ActiveForm::end(); ?>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
.dropdown-divider {
  height: 0;
  margin: 0.5rem 0;
  overflow: hidden;
  border-top: 1px solid #98A4B8; }
.dropdown-item {
  display: block;
  width: 100%;
  padding: 0.25rem 1.5rem;
  clear: both;
  font-weight: 400;
  font-size: 11px;
  font-style: "Sans Pro Bold";
  color: #2A2E30;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0; }
  .dropdown-item:hover, .dropdown-item:focus {
    color: #1e2122;
    text-decoration: none;
    background-color: #EAECEB; }
  .dropdown-item.active, .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: #6967ce; }
  .dropdown-item.disabled, .dropdown-item:disabled {
    color: #6b6f80;
    pointer-events: none;
    background-color: transparent; }

.dropdown-menu.show {
  display: block; }

.dropdown-header {
  display: block;
  padding: 0.5rem 1.5rem;
  margin-bottom: 0;
  font-size: 0.875rem;
  color: #6b6f80;
  white-space: nowrap; }

.dropdown-item-text {
  display: block;
  padding: 0.25rem 1.5rem;
  color: #2A2E30; }      
#header-search1 {
    position: absolute;
    top: 15px;
    left: 75px;
}
#header-search1 form {
    display: table;
}    
#header-search1 .form-control {

    resize: horizontal;
    height: 18px;
    width: 276px;
    font-size: 11px;
    color: #EAECEB;
    background-color:  #EAECEB;
    border: none;
    border-radius: 0;
}    
#header-search1 .input-group-addon, #header-search1 .input-group-btn {
    width: 26px;
    vertical-align: middle;
    white-space: nowrap;
}
#header-search1 .btn {
    width: 26px;
    height: 18px;
    padding: 4px;
    border: none;
    font-size: 11px;
    line-height: 1;
    background-color: white;
    border: none;
    color: #717070;
}
#header-search1 .btn1 {
    width: 26px;
    height: 18px;
    padding: 4px;
    border: none;
    font-size: 11px;
    line-height: 1;
    background-color: #EAECEB;
    border: none;
    color: #717070;
}
#header-search1 .btn:hover {
  background-color: #EAECEB;
}
#header-search1 .help-block {
    display: none;
}
#header-search1 .form-group {
    margin-bottom: 0;
}

</style>
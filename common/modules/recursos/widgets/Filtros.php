<?php
namespace common\modules\recursos\widgets;

use yii\base\Widget;
use Yii;
use common\modules\recursos\models\FiltrosModel;
use common\models\Configuraciones;

class Filtros extends Widget {

    /**
     * @inheritdoc
     */
    public function init() {      
      parent::init();
    }

    /**
     * Runs the widget.
     */
    public function run() {
      /*
      $searchModel = new RecursosAdvancedSearch();
      $queryParams = Yii::$app->request->queryParams;
      $dataProvider = $searchModel->search($queryParams);
      */
      $this->view->params['palabra'] = "palabra";
    }

}

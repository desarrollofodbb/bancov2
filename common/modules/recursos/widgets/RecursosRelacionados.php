<?php

namespace common\modules\recursos\widgets;

use yii\base\Widget;
use common\modules\recursos\models\Recursos;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class RecursosRelacionados extends Widget {

    /**
     * @var int Optional, if set, show more than 3 items
     */
    public $recurso;
    public $coleccion;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }

    /**
     * Runs the widget.
     */
    public function run() {

        $recurso = $this->recurso;
        if(!$recurso){
          $recurso = new Recursos();  
          $RecursosRelated = $recurso->getRecursosRelacionados($this->coleccion->idColeccion);
        }
        else{
          $RecursosRelated = $recurso->getRecursosRelacionados();    
        }
        

        return$this->render('recursos-relacionados', [
                    "RecursosRelated" => $RecursosRelated
        ]);
    }

}

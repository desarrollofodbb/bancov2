<?php

namespace common\modules\recursos\widgets;

use yii\base\Widget;
use Yii;
use common\modules\recursos\models\RecursosAdvancedSearch;
use common\models\Configuraciones;

class BusquedaGeneral extends Widget {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }

    /**
     * Runs the widget.
     */
    public function run() {
  
         $searchModel = new RecursosAdvancedSearch();
         $queryParams = Yii::$app->request->queryParams;
         $dataProvider = $searchModel->search($queryParams);

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());

        $this->view->params['configuraciones_generales'] = $configuraciones;

        return$this->render('busqueda-general', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
        ]);
    }

}

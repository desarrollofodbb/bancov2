<?php

namespace common\modules\recursos\widgets;

use yii\base\Widget;
use common\modules\recursos\models\Recursos;

class RecursosMasVistos extends Widget {

    /**
     * @var int Optional, if set, show more than 3 items
     */
    public $items = '';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }

    /**
     * Runs the widget.
     */
    public function run() {
        $RecursosMasVistos = Recursos::getRecursosMasVistos();

        return$this->render('recursos-mas-vistos', [
                    "RecursosMasVistos" => $RecursosMasVistos
        ]);
    }

}

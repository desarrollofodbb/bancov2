<?php

namespace common\modules\recursos\widgets;

use yii\base\Widget;
use Yii;
use common\modules\recursos\models\RecursosAdvancedSearch;
use common\models\Configuraciones;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;

class BusquedaPersonaje extends Widget {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }

    /**
     * Runs the widget.
     */
    public function run() {

        $searchModel = new RecursosAdvancedSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings(), Configuraciones::contactoSettings());
        $this->view->params['configuraciones_generales'] = $configuraciones;

        $valores = ValoresDeCaracteristicas::find()->joinWith("caracteristicas")->where(["TCaracteristicas.Slug" => "tags"])->all();

        $tags = $valores ? \yii\helpers\ArrayHelper::map($valores, "idValoresDeCaracteristicas", "Nombre") : array();


        return$this->render('busqueda-personaje', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'configuraciones' => $configuraciones,
                    'tags' => $tags,
        ]);
    }

}

(function (w, d, $) {

    w.addImageToGallery = function (checkboxcontainerId, imageContainer, data) {
        var input_checkbox = $('<input type="checkbox" name="ProductosConTerminos[media_ids][]" value="' + data["id"] + '" checked="">');
        var input_image = $('<img src="" alt="" data-value="' + data["id"] + '">');
        var content_image = $('<div data-value="' + data["id"] + '"><i onclick="deleteImageFromGallery(this,\'' + checkboxcontainerId + '\',' + data["id"] + ')" class="icon-trash icon-white"></i></div>');

        if ($("" + checkboxcontainerId + " input[value='" + data["id"] + "']").length <= 0) {
            $("" + checkboxcontainerId).append(input_checkbox);
        }

        if ($("" + imageContainer + " img[data-value='" + data["id"] + "']").length <= 0) {
            $(input_image).attr("src", data["url"]);
            $(content_image).append(input_image);
            $("" + imageContainer).append(content_image);
        }


    };

    w.deleteImageFromGallery = function (element, checkboxcontainerId, dataId) {
        $(element).parent().remove();
        console.log(element, checkboxcontainerId, dataId);
        $("" + checkboxcontainerId + " input[value='" + dataId + "']").remove();

    };

})(window, document, jQuery);



<?php

namespace common\modules\filemanager\assets;

use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle {

    public $sourcePath = '@common/modules/filemanager/assets/source';
    public $js = [
        'js/fileinput.js',
        'js/gallery.js',
    ];
    public $css = [
        'css/gallery.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
        'common\modules\filemanager\assets\ModalAsset',
    ];

}

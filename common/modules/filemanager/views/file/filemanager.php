<?php

use common\modules\filemanager\assets\FilemanagerAsset;
use common\modules\filemanager\Module;
use common\modules\filemanager\models\Tag;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\filemanager\models\MediafileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['moduleBundle'] = FilemanagerAsset::register($this);
?>

<header id="header"><span class="glyphicon glyphicon-picture"></span> <?= Module::t('main', 'File manager') ?></header>

<div id="filemanager" data-url-info="<?= Url::to(['file/info']) ?>">
    <div class="row">
        <div class="col-md-8">
            <p><?= Html::a('<span class="glyphicon glyphicon-upload"></span> ' . Module::t('main', 'Agregar archivo'), ['file/uploadmanager'], ['class' => 'btn btn-default'])
?></p>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '<div class="items">{items}</div>{pager}',
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(
                            Html::img($model->getDefaultThumbUrl($this->params['moduleBundle']->baseUrl))
                            . '<span class="checked glyphicon glyphicon-check"></span>', '#mediafile', ['data-key' => $key]
            );
        },
            ])
            ?>
        </div>
        <div class="col-md-4">

            <div class="dashboard"> 
                <div id="fileinfo">

                </div>
            </div>
        </div>
    </div>
</div>

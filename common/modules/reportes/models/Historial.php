<?php

namespace common\modules\reportes\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * User model
 *
 * @property integer $idHistorial
 * @property integer $Ip
 * @property date $FechaDeLogin
 * @property integer $idUsuarios
 * 
 * @property User $usuario
 */
class Historial extends ActiveRecord {

    CONST STATUS_ERROR = 0;
    CONST STATUS_EXITO = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%THistorial}}';
    }

    public function behaviors() {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'Ip',
                ],
                'value' => function ($event) {
            return Yii::$app->getRequest()->getUserIP();
        },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'FechaDeLogin',
                ],
                'value' => function ($event) {
            return new Expression("GETDATE()");
        },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'idUsuarios',
                ],
                'value' => function ($event) {
            if (Yii::$app->user->isGuest) {
                return null;
            } else {
                $user = Yii::$app->get('user', false);
                return $user->id;
            }
        },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'Estado',
                ],
                'value' => function ($event) {
            if (Yii::$app->user->isGuest) {
                return self::STATUS_ERROR; //error
            } else {
                return self::STATUS_EXITO; //exito
            }
        },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            ['status', 'default', 'value' => self::STATUS_ACTIVE],
//            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

}

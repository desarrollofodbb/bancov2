<?php

namespace common\modules\reportes\models;

use Yii;

/**
 * This is the model class for table "RBusquedasPalabras".
 *
 * @property int $rIdBusquedas
 * @property int $rIdPalabrasBuscadas
 *
 * @property TBusquedas $rIdBusquedas0
 * @property TPalabrasBuscadas $rIdPalabrasBuscadas0
 */
class BusquedasPalabras extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'RBusquedasPalabras';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rIdBusquedas', 'rIdPalabrasBuscadas'], 'required'],
            [['rIdBusquedas', 'rIdPalabrasBuscadas'], 'integer'],
            [['rIdBusquedas'], 'exist', 'skipOnError' => true, 'targetClass' => Busquedas::className(), 'targetAttribute' => ['rIdBusquedas' => 'idBusquedas']],
            [['rIdPalabrasBuscadas'], 'exist', 'skipOnError' => true, 'targetClass' => PalabrasBuscadas::className(), 'targetAttribute' => ['rIdPalabrasBuscadas' => 'idPalabrasBuscadas']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rIdBusquedas' => Yii::t('app', 'R Id Busquedas'),
            'rIdPalabrasBuscadas' => Yii::t('app', 'R Id Palabras Buscadas'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRIdBusquedas0() {
        return $this->hasOne(Busquedas::className(), ['idBusquedas' => 'rIdBusquedas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRIdPalabrasBuscadas0() {
        return $this->hasOne(PalabrasBuscadas::className(), ['idPalabrasBuscadas' => 'rIdPalabrasBuscadas']);
    }

}

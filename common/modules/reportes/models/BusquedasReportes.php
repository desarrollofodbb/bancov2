<?php

namespace common\modules\reportes\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class BusquedasReportes extends Model {

    public $year;
    public $month;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                ['year', 'month'],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function BusquedasPorMes($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select("MONTH([FechaDeCreacion]) AS 'm_date', COUNT(*) AS 'total'");
        $query->from(["TBusquedas"]);

// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['MONTH([FechaDeCreacion])', 'COUNT(*)'],
                'defaultOrder' => ['COUNT(*)' => SORT_DESC]
            ]
        ]);


        $query->andFilterWhere(["YEAR([FechaDeCreacion])" => $this->year]); //solo los login que han sido exitosos
        $query->groupBy("MONTH([FechaDeCreacion])");
//        $query->orderBy("COUNT(*) DESC");

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function filtrosDeBusquedas($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select(["[TValoresDeCaracteristicas].[Nombre] AS 'nombre_filtro'", "[TCaracteristicas].[Nombre] AS 'nombre_tipo'", "COUNT(*) AS 'total'"]);
        $query->from(["TBusquedas"]);
        $query->innerJoin("RBusquedasFiltros", "TBusquedas.idBusquedas = RBusquedasFiltros.rIdBusqueda");
        $query->innerJoin("TValoresDeCaracteristicas", "RBusquedasFiltros.rIdFiltro = TValoresDeCaracteristicas.idValoresDeCaracteristicas");
        $query->innerJoin("TCaracteristicas", "TValoresDeCaracteristicas.Caracteristicas_id = TCaracteristicas.idCaracteristicas");

// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ["[TCaracteristicas].[Nombre]", '[TValoresDeCaracteristicas].[Nombre]', 'COUNT(*)'],
                'defaultOrder' => ['COUNT(*)' => SORT_DESC]
            ]
        ]);


        $query->andFilterWhere(["YEAR([TBusquedas].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TBusquedas].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->groupBy(["[TCaracteristicas].[Nombre]", "TValoresDeCaracteristicas.Nombre"]);
//        $query->orderBy("COUNT(*) DESC");

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function cantidadDePalabrasPorMes($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select(["[TPalabrasBuscadas].[Nombre] AS 'nombre_palabra'", "COUNT(*) AS 'total'"]);
        $query->from(["TBusquedas"]);
        $query->innerJoin("RBusquedasPalabras", "TBusquedas.idBusquedas = RBusquedasPalabras.rIdBusquedas");
        $query->innerJoin("TPalabrasBuscadas", "RBusquedasPalabras.rIdPalabrasBuscadas = TPalabrasBuscadas.idPalabrasBuscadas");

// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ["[TPalabrasBuscadas].[Nombre]", 'COUNT(*)'],
                'defaultOrder' => ['COUNT(*)' => SORT_DESC]
            ]
        ]);


        $query->andFilterWhere(["YEAR([TBusquedas].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TBusquedas].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->groupBy(["TPalabrasBuscadas.Nombre"]);
//        $query->orderBy("COUNT(*) DESC");

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }


        return $dataProvider;
    }

}

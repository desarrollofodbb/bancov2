<?php

namespace common\modules\reportes\models;

use Yii;
use yii\base\Model;
use yii2mod\moderation\enums\Status;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\modules\reportes\models\Eventos;


class RecursosReportes extends Model {

    public $year;
    public $materia;
    public $palabras;
    public $filtro;
    public $month;
    public $status;
    public $user;
    public $top; //necesario para recursos más vistos

    /**
     * @inheritdoc
     */

    public function rules() {
        return [
            [
                [
                    'year',
                    'materia',
                    'palabras',
                    'filtro',
                    'month',
                    'status',
                    'user',
                    'top',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recursosPorUsuario($params) {
        $pending = Status::PENDING;
        $approved = Status::APPROVED;
        $rejected = Status::REJECTED;

        $this->load($params);

        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select("username, COUNT(*) as 'total', "
                . "  SUM(CASE WHEN [TRecursos].[Estado] = '$pending' THEN 1 ELSE 0 END) AS 'pending', "
                . "  SUM(CASE WHEN [TRecursos].[Estado] = '$approved' THEN 1 ELSE 0 END) AS 'approved', "
                . "  SUM(CASE WHEN [TRecursos].[Estado] = '$rejected' THEN 1 ELSE 0 END) AS 'rejected' "
        );
        $query->from(["[dbo].[user]"]);
        $query->innerJoin("[dbo].[TRecursos]", "[dbo].[user].[id] = [dbo].[TRecursos].[Autor]");

// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'username',
                    'SUM(CASE WHEN [TRecursos].[Estado] = 0 THEN 1 ELSE 0 END)',
                    'SUM(CASE WHEN [TRecursos].[Estado] = 1 THEN 1 ELSE 0 END)',
                    'SUM(CASE WHEN [TRecursos].[Estado] = 2 THEN 1 ELSE 0 END)',
                    'COUNT(*)'],
                'defaultOrder' => ['COUNT(*)' => SORT_DESC]
            ]
        ]);

        $query->groupBy(["username"]);
        $query->andFilterWhere(["isDeleted" => false]); //solo los recursos que no esten eliminados
        $query->andFilterWhere(["YEAR([TRecursos].[FechaDeCreacion])" => $this->year]); //solo los login que han sido exitosos 
        $query->andFilterWhere(["MONTH([TRecursos].[FechaDeCreacion])" => $this->month]); //solo los login que han sido exitosos 
        $query->andFilterWhere(["TRecursos.Estado" => $this->status]); //filtrar por el estado del recurso 
        $query->andFilterWhere(["[dbo].[user].[id]" => $this->user]); //filtrar por usuario


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recursosPublicadosPorMes($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select(["MONTH([TRecursos].[FechaDeModeracion]) AS 'm_date', COUNT(*) AS 'total'"]);
        $query->from(["TRecursos"]);


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ["MONTH([TRecursos].[FechaDeModeracion])", 'COUNT(*)'],
                'defaultOrder' => ['COUNT(*)' => SORT_DESC]
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }

        $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados
        $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados
        $query->andFilterWhere(["YEAR([TRecursos].[FechaDeModeracion])" => $this->year]); //filtrar por año
        $query->groupBy(["MONTH([TRecursos].[FechaDeModeracion])"]);



        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recursosMasVistos($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");
        $this->top = $this->top ? $this->top : 10; //por defecto mostrar el top 10

        $query = new Query();
        $query->select(["[TRecursos].[Slug]", "[TRecursos].[Nombre]", "COUNT(*) AS 'total'"]);
        $query->from(["TEventos"]);
        $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]");


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }
        $query->andFilterWhere(["[TEventos].[Categoria]" => Eventos::CATEGORY_RECURSOS]); //solo los eventos de recursos
        $query->andFilterWhere(["[TEventos].[Accion]" => Eventos::VER_RECURSO]); //solo los eventos de ver recurso
        $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados
        $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados
        $query->andFilterWhere(["YEAR([TEventos].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TEventos].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->limit($this->top); //filtrar por el top
        $query->groupBy(["[TRecursos].[Nombre]", "[TRecursos].[Slug]"]);
        $query->orderBy("COUNT(*) DESC");

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function usuariosMasRecursosVistos($params) {
        

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");
        $this->top = $this->top ? $this->top : 10; //por defecto mostrar el top 10
        $this->materia = $this->materia ? $this->materia : 0;
        $this->palabras = $this->palabras ? $this->palabras : 0;
        $this->filtro = $this->filtro ? $this->filtro : 0;

        $query = new Query();
        $query->select(["[user].[nombre]", "COUNT(*) AS 'total'"]);
        $query->from(["TEventos"]);
        // $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]");
        $query->innerJoin("[user]", "[TEventos].[CreadoPor] = [user].[id]");


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }
        $query->andFilterWhere(["[TEventos].[Categoria]" => Eventos::CATEGORY_RECURSOS]); //solo los eventos de recursos
        $query->andFilterWhere(["[TEventos].[Accion]" => Eventos::VER_RECURSO]); //solo los eventos de ver recurso
   //     $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados
    //    $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados

        if($this->filtro == 1 && $this->materia>0){

          $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]"); 
          $query->innerJoin("[RRecursosValoresDeCaracteristicas]", "[TRecursos].[idRecursos] = [RRecursosValoresDeCaracteristicas].[rIdRecursos]");
          $query->innerJoin("[TvaloresDeCaracteristicas]", "[RRecursosValoresDeCaracteristicas].[rIdValores] = [TvaloresDeCaracteristicas].[idValoresDeCaracteristicas]");
          $query->andFilterWhere(["[TvaloresDeCaracteristicas].[Caracteristicas_id]" => "8"]);
          $query->andFilterWhere(["[RRecursosValoresDeCaracteristicas].[rIdValores]" => $this->materia]);
            
        }

        if($this->filtro == 2 && $this->palabras>0){

          $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]"); 
          $query->innerJoin("[RRecursosValoresDeCaracteristicas]", "[TRecursos].[idRecursos] = [RRecursosValoresDeCaracteristicas].[rIdRecursos]");
          $query->innerJoin("[TvaloresDeCaracteristicas]", "[RRecursosValoresDeCaracteristicas].[rIdValores] = [TvaloresDeCaracteristicas].[idValoresDeCaracteristicas]");
          $query->andFilterWhere(["[TvaloresDeCaracteristicas].[Caracteristicas_id]" => "10"]);
          $query->andFilterWhere(["[RRecursosValoresDeCaracteristicas].[rIdValores]" => $this->palabras]);
            
        }

        $query->andFilterWhere(["YEAR([TEventos].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TEventos].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->limit($this->top); //filtrar por el top
        $query->groupBy(["[user].[nombre]"]);
        $query->orderBy("COUNT(*) DESC");
// var_dump($query->createCommand()->sql); 
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recursosMasVotados($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");
        $this->top = $this->top ? $this->top : 10; //por defecto mostrar el top 10

        $query = new Query();
        $query->select(["[TRecursos].[Slug]", "[TRecursos].[Nombre]", "COUNT(*) AS 'total'"]);
        $query->from(["TEventos"]);
        $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]");


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }
        $query->andFilterWhere(["[TEventos].[Categoria]" => Eventos::CATEGORY_RECURSOS]); //solo los eventos de recursos
        $query->andFilterWhere(["[TEventos].[Accion]" => Eventos::VOTAR_RECURSO]); //solo los eventos de ver recurso
        $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados
        $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados
        $query->andFilterWhere(["YEAR([TEventos].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TEventos].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->limit($this->top); //filtrar por el top
        $query->groupBy(["[TRecursos].[Nombre]", "[TRecursos].[Slug]"]);
        $query->orderBy("COUNT(*) DESC");

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recursosMasDescargados($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");
        $this->top = $this->top ? $this->top : 10; //por defecto mostrar el top 10

        $query = new Query();
        $query->select(["[TRecursos].[Slug]", "[TRecursos].[Nombre]", "COUNT(*) AS 'total'"]);
        $query->from(["TEventos"]);
        $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]");


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }
        $query->andFilterWhere(["[TEventos].[Categoria]" => Eventos::CATEGORY_RECURSOS]); //solo los eventos de recursos
        $query->andFilterWhere(["[TEventos].[Accion]" => Eventos::DESCARGAR_RECURSO]); //solo los eventos de ver recurso
        $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados
        $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados
        $query->andFilterWhere(["YEAR([TEventos].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TEventos].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->limit($this->top); //filtrar por el top
        $query->groupBy(["[TRecursos].[Nombre]", "[TRecursos].[Slug]"]);
        $query->orderBy("COUNT(*) DESC");

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function recursosMasCompartidos($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");
        $this->top = $this->top ? $this->top : 10; //por defecto mostrar el top 10

        $query = new Query();
        $query->select(["[TRecursos].[Slug]", "[TRecursos].[Nombre]", "COUNT(*) AS 'total'"]);
        $query->from(["TEventos"]);
        $query->innerJoin("[TRecursos]", "[TEventos].[Valor] = [TRecursos].[idRecursos]");


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }
        $query->andFilterWhere(["[TEventos].[Categoria]" => Eventos::CATEGORY_RECURSOS]); //solo los eventos de recursos
        $query->andFilterWhere(["[TEventos].[Accion]" => Eventos::SHARE_RECURSO]); //solo los eventos de compartir recurso
    //    $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados

        if($this->filtro == 1 && $this->materia>0){

          $query->innerJoin("[RRecursosValoresDeCaracteristicas]", "[TRecursos].[idRecursos] = [RRecursosValoresDeCaracteristicas].[rIdRecursos]");
          $query->innerJoin("[TvaloresDeCaracteristicas]", "[RRecursosValoresDeCaracteristicas].[rIdValores] = [TvaloresDeCaracteristicas].[idValoresDeCaracteristicas]");
          $query->andFilterWhere(["[TvaloresDeCaracteristicas].[Caracteristicas_id]" => "8"]);
          $query->andFilterWhere(["[RRecursosValoresDeCaracteristicas].[rIdValores]" => $this->materia]);
            
        }

        if($this->filtro == 2 && $this->palabras>0){

          $query->innerJoin("[RRecursosValoresDeCaracteristicas]", "[TRecursos].[idRecursos] = [RRecursosValoresDeCaracteristicas].[rIdRecursos]");
          $query->innerJoin("[TvaloresDeCaracteristicas]", "[RRecursosValoresDeCaracteristicas].[rIdValores] = [TvaloresDeCaracteristicas].[idValoresDeCaracteristicas]");
          $query->andFilterWhere(["[TvaloresDeCaracteristicas].[Caracteristicas_id]" => "10"]);
          $query->andFilterWhere(["[RRecursosValoresDeCaracteristicas].[rIdValores]" => $this->palabras]);
            
        }


        $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados
        $query->andFilterWhere(["YEAR([TEventos].[FechaDeCreacion])" => $this->year]); //filtrar por año
        $query->andFilterWhere(["MONTH([TEventos].[FechaDeCreacion])" => $this->month]); //filtrar por mes
        $query->limit($this->top); //filtrar por el top
        $query->groupBy(["[TRecursos].[Nombre]", "[TRecursos].[Slug]"]);
        $query->orderBy("COUNT(*) DESC");

        return $dataProvider;
    }

}

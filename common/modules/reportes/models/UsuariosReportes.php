<?php

namespace common\modules\reportes\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\modules\reportes\models\Historial;

class UsuariosReportes extends Model {

    public $year;
    public $adumen;
    public $tipo;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                ['year','adumen'],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function logInUsuarios($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select("MONTH([FechaDeLogin]) AS 'm_date', COUNT(*) AS 'total'");
        $query->from(["THistorial"]);

// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['MONTH([FechaDeLogin])', 'COUNT(*)'],
                'defaultOrder' => ['COUNT(*)' => SORT_DESC]
            ]
        ]);


        $query->andFilterWhere(["Estado" => Historial::STATUS_EXITO]); //solo los login que han sido exitosos 
        $query->andFilterWhere(["YEAR([FechaDeLogin])" => $this->year]); //solo los login que han sido exitosos 
        $query->groupBy("MONTH([FechaDeLogin])");

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function usuariosQuePublicanRecursos($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : date("Y");

        $query = new Query();
        $query->select(["MONTH([TRecursos].[FechaDeCreacion]) AS 'm_date', COUNT(DISTINCT Autor) AS 'total'"]);
        $query->from(["[dbo].[user]"]);
        $query->innerJoin("TRecursos", "[dbo].[user].id = [TRecursos].[Autor]");


// add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ["MONTH([TRecursos].[FechaDeCreacion])", 'COUNT(DISTINCT Autor)'],
                'defaultOrder' => ['COUNT(DISTINCT Autor)' => SORT_DESC]
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//            $query->andWhere('0=1');
//            return $dataProvider;
        }

        $query->andFilterWhere(['not', "[idRecursos]" => null]);
        $query->andFilterWhere(["[TRecursos].[isDeleted]" => false]); //solo los recursos que no esten eliminados
//        $query->andFilterWhere(["Estado" => Status::APPROVED]); //solo los recursos que han sido aprobados
        $query->andFilterWhere(["YEAR([TRecursos].[FechaDeCreacion])" => $this->year]); //solo los login que han sido exitosos 
        $query->groupBy(["MONTH([TRecursos].[FechaDeCreacion])"]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function listadoUsuarios($params) {

        $this->load($params);
        $this->year = $this->year ? $this->year : 0;
// var_dump($this->year);
        $query = new Query();
        $query->select("id, nombre, username, esmenor, email, identificacion ");
        $query->from(["user"]);

        if($this->year==1){
          $query->where(["[user].[esmenor]" => 0,"[user].[esmenor]" => NULL])  ;
        }
        if($this->year==2){
          $query->where(["[user].[esmenor]" => 1])  ;
        }        

//         $rows = $query->all();
//         foreach ($rows as $registro) {
//           $username = $registro['username'];
//           $urlws = Yii::$app->params["urlWS"] 
//                   . "/usuario/getPersonaByUsername/" . $username;
//    var_dump($username);
//           $ch = curl_init($urlws);
//           curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//           $result = curl_exec($ch);
//           curl_close($ch);
//           $resultadologin = json_decode($result);        
//         }
// //


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['nombre', 'username', 'esmenor', 'Identificacion'],
                'defaultOrder' => ['nombre' => SORT_DESC]
            ]
        ]);



        return $dataProvider;
    }

}

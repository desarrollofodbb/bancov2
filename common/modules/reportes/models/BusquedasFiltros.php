<?php

namespace common\modules\reportes\models;

use Yii;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;

/**
 * This is the model class for table "RBusquedasFiltros".
 *
 * @property int $rIdBusqueda
 * @property int $rIdFiltro
 *
 * @property TBusquedas $rIdBusqueda0
 * @property TvaloresDeCaracteristicas $rIdFiltro0
 */
class BusquedasFiltros extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'RBusquedasFiltros';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rIdBusqueda', 'rIdFiltro'], 'required'],
            [['rIdBusqueda', 'rIdFiltro'], 'integer'],
            [['rIdBusqueda'], 'exist', 'skipOnError' => true, 'targetClass' => Busquedas::className(), 'targetAttribute' => ['rIdBusqueda' => 'idBusquedas']],
            [['rIdFiltro'], 'exist', 'skipOnError' => true, 'targetClass' => ValoresDeCaracteristicas::className(), 'targetAttribute' => ['rIdFiltro' => 'idValoresDeCaracteristicas']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rIdBusqueda' => Yii::t('app', 'R Id Busqueda'),
            'rIdFiltro' => Yii::t('app', 'R Id Filtro'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRIdBusqueda0() {
        return $this->hasOne(Busquedas::className(), ['idBusquedas' => 'rIdBusqueda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRIdFiltro0() {
        return $this->hasOne(ValoresDeCaracteristicas::className(), ['idValoresDeCaracteristicas' => 'rIdFiltro']);
    }

}

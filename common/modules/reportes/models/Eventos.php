<?php

namespace common\modules\reportes\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;

/**
 * This is the model class for table "TEventos".
 *
 * @property int $idEventos
 * @property int $Categoria
 * @property int $Accion
 * @property string $Etiqueta
 * @property string $Valor
 * @property string $FechaDeCreacion
 * @property string $FechaDeModificacion
 * @property int $CreadoPor
 * @property int $ActualizadoPor
 *
 * @property User $actualizadoPor
 * @property User $creadoPor
 */
class Eventos extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TEventos';
    }

    //costantes de categorías
    const CATEGORY_BUSQUEDAS = 1;
    const CATEGORY_RECURSOS = 2;
    const CATEGORY_AUTORES = 3;
    //constantes de acciones de recursos
    const VER_RECURSO = 1;
    const DESCARGAR_RECURSO = 2;
    const VOTAR_RECURSO = 3;
    const REVIEW_RECURSO = 4;
    const PUBLIC_RECURSO = 5;
    const REJECT_RECURSO = 6;
    const LIKE_RECURSO = 7;
    const COMMENT_RECURSO = 8;
    const TAG_RECURSO = 9;
    const ADMIN_RECURSO = 10;  // FOR ALL USERS
    const COLLECTION_RECURSO = 11;
    const DOWN_RECURSO = 12;
    const REPORT_RECURSO = 13;
    const FOLLOW_USER = 14;  //CATEGORY_AUTORES
    const SHARE_RECURSO = 15;  //CATEGORY_AUTORES
    //constantes de acciones de filtros de búsquedas
    const BUSCAR_RECURSO = 1;

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
                'value' => function ($event) {
                    return new Expression("GETDATE()");
                },
            ],
            [//BlameableBehavior automatically fills the specified attributes with the current user ID.
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CreadoPor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Categoria', 'Accion'], 'required'],
            [['Categoria', 'Accion'], 'integer'],
            [['Etiqueta', 'Valor'], 'string'],
            [['ActualizadoPor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ActualizadoPor' => 'id']],
            [['CreadoPor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['CreadoPor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idEventos' => Yii::t('app', 'Id Eventos'),
            'Categoria' => Yii::t('app', 'Categoria'),
            'Accion' => Yii::t('app', 'Accion'),
            'Etiqueta' => Yii::t('app', 'Etiqueta'),
            'Valor' => Yii::t('app', 'Valor'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha De Creacion'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Modificacion'),
            'CreadoPor' => Yii::t('app', 'Creado Por'),
            'ActualizadoPor' => Yii::t('app', 'Actualizado Por'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualizadoPor() {
        return $this->hasOne(User::className(), ['id' => 'ActualizadoPor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreadoPor() {
        return $this->hasOne(User::className(), ['id' => 'CreadoPor']);
    }

    public static function addEvent($Category, $Action, $Label, $Value, $Slug = "", $Autor = "") {
  $log_filename = $_SERVER['DOCUMENT_ROOT'] . '/log';
      if (!file_exists($log_filename))
      {
          // create directory/folder uploads.
          mkdir($log_filename, 0777, true);
      }
    $log_file_data = $log_filename.'/log.log';     
 // $t=time();
 // $hora = date("H:i:s",$t);       
      file_put_contents($log_file_data, "Start-Evento-". "\n", FILE_APPEND);  
        if(!isset(Yii::$app->user->identity->id)){
file_put_contents($log_file_data, "Salio Evento 1 -". "\n", FILE_APPEND);  
            return;
        }
        if($Autor!=""){
// No registrar eventos del mismo usuario en su mismo recurso, excepto que sea el de subir un recurso nuevo. GAMF 05/11/2020  

          // if($Autor==Yii::$app->user->identity->id && $Action != self::REVIEW_RECURSO){
          //   return TRUE;
          // }
        }
        if ($Action == self::VER_RECURSO) {
            $cookies = Yii::$app->request->cookies;
            $currentUrl = str_replace(["/", "?", "=", "-", " "], "_", Yii::$app->request->getUrl());

//            $cookies = Yii::$app->response->cookies;
//            $cookies->remove($currentUrl);

            if ($cookies->has($currentUrl)) {
file_put_contents($log_file_data, "Salio Evento 2 -". "\n", FILE_APPEND);                  
                return; //si ya se registró una visita en la sessión para el recurso no se registra más
            } else {
                $cookies = Yii::$app->response->cookies;

                // add a new cookie to the response to be sent
                $cookies->add(new \yii\web\Cookie([
                    'name' => $currentUrl,
                    'value' => time(),
                    'expire' => time() + (60 * 30), //cada 30 minutos
                ]));
            }
        }

        $event = new Eventos();
        $event->Categoria = $Category;
        $event->Accion = $Action;
        $event->Etiqueta = $Label;
        $event->Valor = '' . $Value;
        $event->Slug = $Slug;
        $event->Autor = $Autor;
        $result = $event->save();
file_put_contents($log_file_data, "Evento Registrado -". "\n", FILE_APPEND);  
        return $result;
    }

}

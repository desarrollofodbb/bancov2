<?php

namespace common\modules\reportes\models;

use Yii;

/**
 * This is the model class for table "TPalabrasBuscadas".
 *
 * @property int $idPalabrasBuscadas
 * @property string $Nombre
 *
 * @property RBusquedasPalabras[] $rBusquedasPalabras
 * @property TBusquedas[] $rIdBusquedas
 */
class PalabrasBuscadas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TPalabrasBuscadas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPalabrasBuscadas' => Yii::t('app', 'Id Palabras Buscadas'),
            'Nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRBusquedasPalabras()
    {
        return $this->hasMany(RBusquedasPalabras::className(), ['rIdPalabrasBuscadas' => 'idPalabrasBuscadas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRIdBusquedas()
    {
        return $this->hasMany(TBusquedas::className(), ['idBusquedas' => 'rIdBusquedas'])->viaTable('RBusquedasPalabras', ['rIdPalabrasBuscadas' => 'idPalabrasBuscadas']);
    }
}

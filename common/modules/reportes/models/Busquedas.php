<?php

namespace common\modules\reportes\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\User;

/**
 * This is the model class for table "TBusquedas".
 *
 * @property int $idBusquedas
 * @property string $Keyword
 * @property int $CreadoPor
 * @property int $ActualizadoPor
 * @property string $FechaDeCreacion
 * @property string $FechaDeModificacion
 *
 * @property RBusquedasFiltros[] $rBusquedasFiltros
 * @property TvaloresDeCaracteristicas[] $rIdFiltros
 * @property User $actualizadoPor
 * @property User $creadoPor
 */
class Busquedas extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TBusquedas';
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
                'value' => function ($event) {
                    return new Expression("GETDATE()");
                },
            ],
            [//BlameableBehavior automatically fills the specified attributes with the current user ID.
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CreadoPor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Keyword'], 'string'],
            [['ActualizadoPor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ActualizadoPor' => 'id']],
            [['CreadoPor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['CreadoPor' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idBusquedas' => Yii::t('app', 'Id Busquedas'),
            'Keyword' => Yii::t('app', 'Keyword'),
            'CreadoPor' => Yii::t('app', 'Creado Por'),
            'ActualizadoPor' => Yii::t('app', 'Actualizado Por'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha De Creacion'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha De Modificacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusquedaFiltros() {
        return $this->hasMany(BusquedasFiltros::className(), ['rIdBusqueda' => 'idBusquedas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiltros() {
        return $this->hasMany(TvaloresDeCaracteristicas::className(), ['idValoresDeCaracteristicas' => 'rIdFiltro'])->viaTable('RBusquedasFiltros', ['rIdBusqueda' => 'idBusquedas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualizadoPor() {
        return $this->hasOne(User::className(), ['id' => 'ActualizadoPor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreadoPor() {
        return $this->hasOne(User::className(), ['id' => 'CreadoPor']);
    }

    public static function addNewSearch($filters = array(), $Keyword = null) {
        if ((count($filters) <= 0) && !$Keyword) {
            return; //si no se selecciona ning'un filtro ni se ingresa un keyword no se registra como busqueda
        }
//        var_dump(Yii::$app->CustomFunctionsHelper->extractKeywords($Keyword, 4, 1, true));
//        die;
        //--- validar si no es la misma busqueda
        $cookies = Yii::$app->request->cookies;

        $search = $filters;
        $search[] = $Keyword;
        $currentSearch = serialize($search);


//            $cookies = Yii::$app->response->cookies;
//            $cookies->remove($currentUrl);

        if ($cookies->has("currentSearch") && ($cookies->getValue('currentSearch') == $currentSearch)) {
            return; //si ya se registró una visita en la sessión para el recurso no se registra más
        } else {
            $cookies = Yii::$app->response->cookies;

            // add a new cookie to the response to be sent
            $cookies->add(new \yii\web\Cookie([
                'name' => "currentSearch",
                'value' => $currentSearch,
                'expire' => time() + (60 * 30), //cada 30 minutos
            ]));
        }
//        var_dump($currentSearch, $cookies->getValue('currentSearch'));
//        die;
        //----------

        $event = new Busquedas();
        $event->Keyword = $Keyword;
        $result = $event->save(); //guardar la cabecera de la búsqueda

        if ($filters && $result) {
            foreach ($filters as $filter) {//guardar los filtros relacionados a valores de caracteristicas
                $newBusquedaFiltro = new BusquedasFiltros();
                $newBusquedaFiltro->rIdBusqueda = $event->idBusquedas;
                $newBusquedaFiltro->rIdFiltro = $filter;
                $newBusquedaFiltro->save();
            }
        }
        if ($Keyword) {//guardar los keywords de la búsquedas
            $words = Yii::$app->CustomFunctionsHelper->extractKeywords($Keyword, 4, 1, true);
            foreach ($words as $word) {
                if (!PalabrasBuscadas::find()->where(["Nombre" => $word])->exists()) {//si no existe aún la palabra en la lista de palabras buscadas
                    $newWord = new PalabrasBuscadas();
                    $newWord->Nombre = $word;
                    $newWord->save();
                    $wordId = $newWord->idPalabrasBuscadas;
                } else {
                    $wordId = PalabrasBuscadas::find()->where(["Nombre" => $word])->one()->idPalabrasBuscadas;
                }
                $newBusquedaPalabra = new BusquedasPalabras();
                $newBusquedaPalabra->rIdBusquedas = $event->idBusquedas;
                $newBusquedaPalabra->rIdPalabrasBuscadas = $wordId;
                $newBusquedaPalabra->save();
            }
        }

        return $result;
    }

}

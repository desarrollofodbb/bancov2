<?php

namespace common\modules\reportes\controllers;

use Yii;
use yii\web\Controller;
use common\modules\reportes\models\BusquedasReportes;
use common\modules\reportes\models\RecursosReportes;
use common\modules\reportes\models\UsuariosReportes;
use common\models\User;
use yii\db\Query;
use yii\db\Expression;
/**
 * Default controller for the `Reportes` module
 */
class ReportesController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionOverview() {
        return $this->render('overview');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogInUsuarios() {
        $searchModel = new UsuariosReportes();
        $dataProvider = $searchModel->logInUsuarios(Yii::$app->request->queryParams);


        return $this->render('log-in-usuarios', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUsuariosQuePublicanRecursos() {
        $searchModel = new UsuariosReportes();
        $dataProvider = $searchModel->usuariosQuePublicanRecursos(Yii::$app->request->queryParams);


        return $this->render('usuarios-que-publican-recursos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionBusquedasPorMes() {
        $searchModel = new BusquedasReportes();
        $dataProvider = $searchModel->BusquedasPorMes(Yii::$app->request->queryParams);


        return $this->render('busquedas-por-mes', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionFiltrosDeBusquedas() {
        $searchModel = new BusquedasReportes();
        $dataProvider = $searchModel->filtrosDeBusquedas(Yii::$app->request->queryParams);


        return $this->render('filtros-de-busquedas', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCantidadDePalabrasPorMes() {
        $searchModel = new BusquedasReportes();
        $dataProvider = $searchModel->cantidadDePalabrasPorMes(Yii::$app->request->queryParams);


        return $this->render('cantidad-de-palabras-por-mes', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRecursosPublicadosPorMes() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->recursosPublicadosPorMes(Yii::$app->request->queryParams);


        return $this->render('recursos-publicados-por-mes', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRecursosMasVistos() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->recursosMasVistos(Yii::$app->request->queryParams);

        return $this->render('recursos-mas-vistos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUsuariosMasRecursosVistos() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->usuariosMasRecursosVistos(Yii::$app->request->queryParams);

        return $this->render('usuarios-mas-recursos-vistos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLosReportes() {
        $searchModel = new RecursosReportes();
        // $dataProvider = $searchModel->recursosMasVistos(Yii::$app->request->queryParams);

        return $this->render('los-reportes', [
                    'searchModel' => "",
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRecursosMasDescargados() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->recursosMasDescargados(Yii::$app->request->queryParams);

        return $this->render('recursos-mas-descargados', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRecursosMasCompartidos() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->recursosMasCompartidos(Yii::$app->request->queryParams);

        return $this->render('recursos-mas-compartidos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRecursosMasVotados() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->recursosMasVotados(Yii::$app->request->queryParams);

        return $this->render('recursos-mas-votados', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionListadoUsuarios() {
        $searchModel = new UsuariosReportes();
        $dataProvider = $searchModel->listadoUsuarios(Yii::$app->request->queryParams);

        return $this->render('listado-usuarios', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionRecursosPorUsuario() {
        $searchModel = new RecursosReportes();
        $dataProvider = $searchModel->recursosPorUsuario(Yii::$app->request->queryParams);
        $userList = User::findByRole("autor");
        $userList = \yii\helpers\ArrayHelper::map($userList, "id", "username");

        return $this->render('recursos-por-usuario', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'userList' => $userList,
        ]);
    }

    public function ViewReportes($tiprep,$mes,$anio,$fecini,$fecfin,$etiqueta,$asignatura,$autor,$periodo,$toprep,$profuturo){

      switch ($tiprep) {
        case "usrper":
          $current_date = date("d-m-Y");
          $current_year = date("Y");
          $current_month = date("m");          
          $current_fecha = date("Y") . date("m"); 
          $initial_date = date("d-m-Y", strtotime($current_date." -11 months"));
          $initial_year = date("Y", strtotime($current_date." -11 months"));
          $initial_month = date("m", strtotime($current_date." -11 months"));
          $semester_fecha = $current_year . date("m", strtotime($current_date." -5 months"));
          $trimester_fecha = $current_year . date("m", strtotime($current_date." -2 months"));
          $datos = [];
          $newmes = 0;
          $newtri = 0;
          $newsem = 0;
          $newyear = 0;

          $newmesmay = 0;
          $newtrimay = 0;
          $newsemmay = 0;
          $newyearmay = 0;

          $newmesmen = 0;
          $newtrimen = 0;
          $newsemmen = 0;
          $newyearmen = 0;

          $select = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) AS fecha, ISNULL(esmenor,0) AS esmenor,count(*) AS total");
          $where = "DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970'))) >= DATEADD(m,-11,dateadd(mm, datediff(mm, 0, GetDate()), 0))";
          // if($anio>0){
            $where = " YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) = " . $current_year . "";
          // }
          $groupby = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) ,ISNULL(esmenor,0)");
          $query = (new Query)->select($select)
          ->from('user')
          ->where($where)
          ->groupby($groupby)
          ->orderby('fecha');      
//var_dump($query->createCommand()->sql);           
          $rows = $query->all();
          foreach ($rows as $registro) {  
            $newyear = $newyear + $registro['total'];
            if($registro['esmenor']==0){
              $newyearmay = $newyearmay + $registro['total'];
            }
            else{
              $newyearmen = $newyearmen + $registro['total'];
            }
            $fecha = $registro['fecha'];
            if($fecha==$current_fecha){
              $newmes = $newmes + $registro['total'];
              if($registro['esmenor']==0){
                $newmesmay = $registro['total'];
              }
              else{
                $newmesmen = $registro['total'];
              }              
            }
            if($fecha>=$trimester_fecha){
              $newtri = $newtri + $registro['total'];
              if($registro['esmenor']==0){
                $newtrimay = $newtrimay + $registro['total'];
              }
              else{
                $newtrimen = $newtrimen + $registro['total'];
              }                   
            }            
            if($fecha>=$semester_fecha){
              $newsem = $newsem + $registro['total'];
              if($registro['esmenor']==0){
                $newsemmay = $newsemmay + $registro['total'];
              }
              else{
                $newsemmen = $newsemmen + $registro['total'];
              }                                 
            }

          }      
          $count = (new Query())->select('id')
          ->from('User')
          ->count();          

          $labels= array('Mes','Trimestre','Semestre','Año');
          
          $returnData = new \stdClass();
          $returnData->line = new \stdClass();
          $returnData->line->type = 'bar';
          $returnData->line->title = 'Usuarios nuevos del año actual resumido por período';
          $returnData->line->labels = $labels;
          $returnData->line->datasets = new \stdClass();   

          $dataset = array();
//Segundo          
          $colorbackground = array("rgba(0, 115, 174, 0.6)");
          $colorborder = array("rgba(0, 115, 174, 1)");
          $datagraph = array($newmes, $newtri, $newsem, $newyear); 
          $datasets = new \stdClass();
          $datasets->data = $datagraph;
          $datasets->backgroundColor = $colorbackground;
          $datasets->borderColor = $colorbackground;
          $datasets->hoverBackgroundColor = $colorbackground;
          $datasets->hoverBorderColor = $colorborder;
          $datasets->hoverBorderWidth = 2;
          $datasets->label = "Total";
          $dataset[] = $datasets;
//Tercero          
          $colorbackground = array("rgba(212, 20, 90, 0.6)");
          $colorborder = array("rgba(212, 20, 90, 1)");
          $datagraph = array($newmesmay, $newtrimay, $newsemmay, $newyearmay); 

          $datasets = new \stdClass();
          $datasets->data = $datagraph;
          $datasets->backgroundColor = $colorbackground;
          $datasets->borderColor = $colorbackground;
          $datasets->hoverBackgroundColor = $colorbackground;
          $datasets->hoverBorderColor = $colorborder;
          $datasets->hoverBorderWidth = 2;
          $datasets->label = "Adultos";
          $dataset[] = $datasets;          

//Cuarto             
          $colorbackground = array("rgba(0, 204, 153, 0.6)");
          $colorborder = array("rgba(0, 204, 153, 1)");
          $datagraph = array($newmesmen, $newtrimen, $newsemmen, $newyearmen); 
          $datasets = new \stdClass();
          $datasets->data = $datagraph;
          $datasets->backgroundColor = $colorbackground;
          $datasets->borderColor = $colorbackground;
          $datasets->hoverBackgroundColor = $colorbackground;
          $datasets->hoverBorderColor = $colorborder;
          $datasets->hoverBorderWidth = 2;
          $datasets->label = "Menores";
          $dataset[] = $datasets;            
          

          $returnData->line->datasets = $dataset;
          return json_encode($returnData);
          
          break;
        case "usrmes":
          $months = array(
            'Ene', 
            'Feb', 
            'Mar', 
            'Abr', 
            'May', 
            'Jun', 
            'Jul', 
            'Ago', 
            'Sep', 
            'Oct', 
            'Nov', 
            'Dic'
          );    
          $months_full = array(
            'Enero', 
            'Febrero', 
            'Marzo', 
            'Abril', 
            'Mayo', 
            'Junio', 
            'Julio', 
            'Agosto', 
            'Septiembre', 
            'Octubre', 
            'Noviembre', 
            'Diciembre'
          );            
          $months_tri = array(
            'Ene-Feb-Mar', 
            'Abr-May-Jun', 
            'Jul-Ago-Set', 
            'Oct-Nov-Dic'
          );                    
          $months_six = array(
            'Ene-Feb-Mar-Abr-May-Jun', 
            'Jul-Ago-Set-Oct-Nov-Dic'
          );                              
          $current_date = date("d-m-Y");
          $current_year = date("Y");
          $current_month = date("m");          
          $current_fecha = date("Y") . date("m"); 
          $initial_date = date("d-m-Y", strtotime($current_date." -11 months"));
          $initial_year = date("Y", strtotime($current_date." -11 months"));
          $initial_month = date("m", strtotime($current_date." -11 months"));
          $semester_fecha = date("Y", strtotime($current_date." -5 months")) . date("m", strtotime($current_date." -5 months"));
          $trimester_fecha = date("Y", strtotime($current_date." -2 months")) . date("m", strtotime($current_date." -2 months"));
          $datos = [];
          $title = 'Usuarios nuevos por mes del año ' . $anio;
          if($mes>0){
            $title = $title . ' y el mes ' . $months_full[$mes - 1];
          }
          $select = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) AS fecha, ISNULL(esmenor,0) AS esmenor,count(*) AS total");
          $where = "DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970'))) >= DATEADD(m,-11,dateadd(mm, datediff(mm, 0, GetDate()), 0))";
          $groupby = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) ,ISNULL(esmenor,0)");
       
          // if($periodo=="mes"){
         
          $where = "YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) = " . $anio;
          if($periodo=="mes" && $mes > 0){
         
            $where = $where . " AND MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) =" . $mes . "";
          }
          
          // }
          if($periodo=="trimes"){
            $title = 'Usuarios nuevos por trimestre del año ' . $anio;
            $select = new Expression("CONCAT(
              YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  
              
              CASE WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 4
                           THEN '01'
                 WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 7
                           THEN '02'
                   WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 10
                           THEN '03'
                   WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 13
                           THEN '04'
                   END 
              
              )  AS fecha , 
              ISNULL(esmenor,0) AS esmenor,
              count(*) as total");
            
            $groupby = new Expression("CONCAT(
              YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  
              
              CASE WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 4
                           THEN '01'
                 WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 7
                           THEN '02'
                   WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 10
                           THEN '03'
                   WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 13
                           THEN '04'
                   END 
              
              ),
              ISNULL(esmenor,0)");  
          }       
          if($periodo=="sixmes"){
            $title = 'Usuarios nuevos por semestre del año ' . $anio;
            $select = new Expression("CONCAT(
              YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  
              
              CASE WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 7
                           THEN '01'
                 WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 13
                           THEN '02'
              END 
              
              )  AS fecha , 
              ISNULL(esmenor,0) AS esmenor,
              count(*) as total");
            
            $groupby = new Expression("CONCAT(
              YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  
              
              CASE WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 7
                           THEN '01'
                 WHEN MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < 13
                           THEN '02'
              END 
              
              ),
              ISNULL(esmenor,0)");  
          }   
          if($periodo=="anio"){
            $title = 'Usuarios nuevos del año ' . $anio;
            $select = new Expression("CONCAT(
              YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  
              
              CASE WHEN YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < YEAR(GetDate())
                           THEN '01'
                           ELSE '02'
              END 
              
              )  AS fecha , 
              ISNULL(esmenor,0) AS esmenor,
              count(*) as total");
          //  $where = "YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) >= YEAR(GetDate()) - 1";
            $groupby = new Expression("CONCAT(
              YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  
              
              CASE WHEN YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) < YEAR(GetDate())
                           THEN '01'
                           ELSE '02'
              END 
              
              ),
              ISNULL(esmenor,0)");  
          }             
          $query = (new Query)->select($select)
          ->from('user')
          ->where($where)
          ->groupby($groupby)
          ->orderby('fecha');      
 // var_dump($query->createCommand()->sql);  

          $rows = $query->all();
          $labels = array();
          $nuevos_arr = array();
          $mayores_arr = array();
          $menores_arr = array();
          $lastdate = "";
          $last_igual = 0;
          $nuevos = 0;
          $mayores = 0;
          $menores = 0;
          $month=0;
          $fecha = "";
          $last_fecha = "";
          $last_fecha_ant = "";
          foreach ($rows as $registro) {  
            
            $fecha = $registro['fecha'];
// var_dump($fecha . " * " . $registro['total'] . " * " . $registro['esmenor']);            
            $month = substr($fecha,4,2) - 0;
            $month_ori = $month;
            $month = $month - 1;

            $year = substr($fecha,0,4);
            if((($periodo=="mes" && $mes == 0) || $periodo=="trimes" || $periodo=="sixmes") && $last_fecha==""){
              
              for ($x = 1; $x <= $month; $x++) {
              
                if($periodo=="mes"){
                  $name_month = substr($months[$x - 1], 0, 3);   
                }
                if($periodo=="trimes"){
                  $name_month = $months_tri[$x - 1];                    
                }
                if($periodo=="sixmes"){
                  $name_month = $months_six[$x - 1];                    
                }                
                $label = $year . "-" . $name_month;
                
                array_push($labels,$label);  
                array_push($nuevos_arr,0);                           
                array_push($mayores_arr,0); 
                array_push($menores_arr,0);   
              }              
            }       
            if($periodo!="mes"){
              
              switch ($periodo) {
                case "trimes":
                  $month = $month + 0;
                  $name_month = $months_tri[$month];
                  break;
                case "sixmes":
                  $month = $month + 0;
                  $name_month = $months_six[$month];
                  break;
                case "anio":
                  $name_month = $year;
                  break;

                }
            }
            else{
              $name_month = $months[$month];
            }
            $label = $year . "-" . $name_month;
          
            if($periodo=="anio"){
              $label = $year;
            }     
            if($lastdate==""){
   
              $last_fecha = $fecha;
              $last_fecha_ant = $fecha;
              $lastdate = $label;
              array_push($labels,$label);    
              if($registro['esmenor']==0){
                $nuevos = $registro['total'];
                $mayores = $registro['total']; 
                $menores = 0;
              }
              else{
                $nuevos = $registro['total'];
                $mayores = 0; 
                $menores = $registro['total'];
              }                  
//var_dump("IF " . $fecha . " " . $nuevos . " " . $mayores . " " .$menores );              
            }
            else{

              if($lastdate==$label){
// var_dump("ELSE1 * 1 ")                ;                
                $in_labels = in_array($label,$labels);
                if(!$in_labels){
                  array_push($labels,$label);  
                }
                $nuevos = $nuevos + $registro['total'];
                $menores = $registro['total'];
                array_push($nuevos_arr,$nuevos);                           
                array_push($mayores_arr,$mayores); 
                array_push($menores_arr,$menores); 
                $nuevos = 0;
                $mayores = 0;
                $menores = 0;
              }
              else{
 //var_dump("ELSE * 2 " . $fecha . " ... " . $last_fecha)                ;                
                $despues = 0;
                if(($periodo=="mes" || $periodo=="trimes" ||  $periodo=="anio") && $last_fecha != "" && $fecha!=$last_fecha){
                  $year2 = substr($last_fecha,0,4);
                  $month2 = substr($last_fecha,4,2) + 0;              
                  $year3 = substr($fecha,0,4);
                  $month3 = substr($fecha,4,2) + 0;
                
                  if($month3>$month2 + 1){                
                    array_push($nuevos_arr,$nuevos);                           
                    array_push($mayores_arr,$mayores); 
                    array_push($menores_arr,$menores);

                    $despues = 1;
                  }

                }
                if($despues==0){       
                  array_push($nuevos_arr,$nuevos);                           
                  array_push($mayores_arr,$mayores); 
                  array_push($menores_arr,$menores);                                
                   $in_labels = in_array($label,$labels);
                   if(!$in_labels){
                     array_push($labels,$label);  
                   }

                  if($registro['esmenor']==0){
                    $nuevos = $registro['total'];
                    $mayores = $registro['total']; 
                    $menores = 0;
                  }
                  else{
                    $nuevos = $registro['total'];
                    $mayores = 0; 
                    $menores = $registro['total'];
                  }    

                  array_push($nuevos_arr,$nuevos);                           
                  array_push($mayores_arr,$mayores); 
                  array_push($menores_arr,$menores);                  
                }      
                else{
                  if($registro['esmenor']==0){
                    $nuevos = $registro['total'];
                    $mayores = $registro['total']; 
                    $menores = 0;
                  }
                  else{
                    $nuevos = $registro['total'];
                    $mayores = 0; 
                    $menores = $registro['total'];
                  }     
//echo "PUSH " . $nuevos . " " . $mayores . " " . $menores . " " . $fecha;
                }            
                
  
              }
           
              $last_fecha_ant = $last_fecha;
              if($last_fecha==""){
                $last_fecha_ant = $fecha;
              }
              $last_fecha = $fecha;
              $lastdate = $label;

            } 
//**** */
// var_dump("lo que sigue");
if($periodo=="anio"){
  $year2 = substr($last_fecha_ant,0,4);
  $month2 = substr($last_fecha_ant,4,2) + 0;              
  $year3 = substr($fecha,0,4);
  $month3 = substr($fecha,4,2) + 0;    

}            
if($periodo=="mes" && $last_fecha_ant != "" && $fecha!=$last_fecha_ant){
  $year2 = substr($last_fecha_ant,0,4);
  $month2 = substr($last_fecha_ant,4,2) + 0;              
  $year3 = substr($fecha,0,4);
  $month3 = substr($fecha,4,2) + 0;

// var_dump($periodo . " " . $last_fecha_ant . " * " . $fecha . " Y2: " . $year2 . " M2: " . $month2 . " Y3: " . $year3 . " M3: " . $month3);

// var_dump($nuevos_arr);  
  if($year2==$year3 && $month3>$month2 + 1){

    $month2++;
    $label2 = "";
    for ($x = $month2; $x < $month3; $x++) {
      $name_month = $months[$x - 1];
        $label2 = $year2 . "-" . substr($name_month,0,3);
        array_push($labels,$label2);  
        array_push($nuevos_arr,0);                           
        array_push($mayores_arr,0); 
        array_push($menores_arr,0);   
    }                
  }


  if($despues==1){
    $in_labels = in_array($label,$labels);
    if(!$in_labels){
      array_push($labels,$label);  
    }
               
    if($registro['esmenor']==0){
      $nuevos = $registro['total'];
      $mayores = $registro['total']; 
      $menores = 0;
    }
    else{
      $nuevos = $registro['total'];
      $mayores = 0; 
      $menores = $registro['total'];
    }           
    array_push($nuevos_arr,$nuevos);                           
    array_push($mayores_arr,$mayores); 
    array_push($menores_arr,$menores);       
    $despues=0;
  }
}

if($periodo=="trimes" && $last_fecha_ant != "" && $fecha!=$last_fecha_ant){
  $year2 = substr($last_fecha_ant,0,4);
  $month2 = substr($last_fecha_ant,4,2) + 0;              
  $year3 = substr($fecha,0,4);
  $month3 = substr($fecha,4,2) + 0;
 // var_dump($year2 . " - " . $month2 . " * " . $year3 . " - " . $month3);
  // var_dump("==>" . $fecha . " " . $nuevos . " " . $mayores . " " . $menores);
  // var_dump($nuevos_arr);
  // var_dump($mayores_arr);
  // var_dump($menores_arr);
  if($year2==$year3 && $month3>$month2 + 1){
//var_dump("A");
    $month2++;
    $label2 = "";
    for ($x = $month2; $x < $month3; $x++) {
      $name_month = $months_tri[$x - 1];
      if($label2==""){
        $label2 = $year2 . "-" . substr($name_month,0,3);
      }
      else{
        $label2 = $label2 . "-" . substr($name_month,0,3);
      }
      if($x == $month3 - 1){
        array_push($labels,$label2);  
        array_push($nuevos_arr,0);                           
        array_push($mayores_arr,0); 
        array_push($menores_arr,0);                     
      }
    }                
  }
  if($year3>$year2 && $month3>0){
//var_dump("B");    
    $month2++;
    $label2="";
    for ($x = $month2; $x < 5; $x++) {
      $name_month = $months_tri[$x - 1];
      if($label2==""){
        $label2 = $year2 . "-" . $name_month;
      }
      else{
        $label2 = $year3 . "-" . $name_month;
      }
      if($x == 4){
        // $label2 = $label2 . "-" . substr($name_month,0,3);
  //      var_dump($label2);
        array_push($labels,$label2);  
        array_push($nuevos_arr,0);                           
        array_push($mayores_arr,0); 
        array_push($menores_arr,0);                     
      }     
    }  
    if($month3>1){
      $label2="";        
      for ($x = 1; $x < $month3; $x++) {
        $name_month = $months_tri[$x - 1];
        if($label2==""){
          $label2 = $year3 . "-" . $name_month;
        }
        else{
          $label2 = $label2 . "-" . $name_month;
        }
        if($x == $month3 - 1){
        // $label2 = $label2 . "-" . substr($name_month,0,3);
  //      var_dump($label2);
          array_push($labels,$label2);  
          array_push($nuevos_arr,0);                           
          array_push($mayores_arr,0); 
          array_push($menores_arr,0);                     
        }     
      }          
    }
    
             
  }
  if($despues==1){
    $in_labels = in_array($label,$labels);
    if(!$in_labels){
      array_push($labels,$label);  
    }
    array_push($nuevos_arr,$nuevos);                           
    array_push($mayores_arr,$mayores); 
    array_push($menores_arr,$menores);                  
    if($registro['esmenor']==0){
      $nuevos = $registro['total'];
      $mayores = $registro['total']; 
      $menores = 0;
    }
    else{
      $nuevos = $registro['total'];
      $mayores = 0; 
      $menores = $registro['total'];
    }           
    $despues=0;
  }
}
//**** */ 
//var_dump($last_fecha_ant. "--" . $fecha);
//var_dump($nuevos_arr);

//**** */                
          }
          if(!isset($label)){
            if($periodo=="mes" && $mes > 0){
              $name_month = $months[$mes - 1];
              $label = $anio . "-" . $name_month;
              array_push($labels,$label);  
              array_push($nuevos_arr,0);                           
              array_push($mayores_arr,0); 
              array_push($menores_arr,0);                
            }
          }
          $in_labels = in_array($label,$labels);
          if(!$in_labels){
            array_push($labels,$label);  
          }
          array_push($nuevos_arr,$nuevos);                           
          array_push($mayores_arr,$mayores); 
          array_push($menores_arr,$menores);     
               
          $count = (new Query())->select('id')
          ->from('User')
          ->count();          

          $returnData = new \stdClass();
          $returnData->line = new \stdClass();
          $returnData->line->type = 'bar';
          $returnData->line->title = $title;
          $returnData->line->labels = $labels;
          $returnData->line->datasets = new \stdClass();   
          
          $returnData->line->datasets = new \stdClass();   

          $dataset = array();
          $datagraph = array();
//Segundo          
          $colorbackground = array("rgba(0, 115, 174, 0.6)");
          $colorborder = array("rgba(0, 115, 174, 1)");
          $datagraph = $nuevos_arr; 
          $datasets = new \stdClass();
          $datasets->data = $datagraph;
          $datasets->backgroundColor = $colorbackground;
          $datasets->borderColor = $colorbackground;
          $datasets->hoverBackgroundColor = $colorbackground;
          $datasets->hoverBorderColor = $colorborder;
          $datasets->hoverBorderWidth = 2;
          $datasets->label = "Total";
          $dataset[] = $datasets;
//Tercero          
          $colorbackground = array("rgba(212, 20, 90, 0.6)");
          $colorborder = array("rgba(212, 20, 90, 1)");
          $datagraph = $mayores_arr; 

          $datasets = new \stdClass();
          $datasets->data = $datagraph;
          $datasets->backgroundColor = $colorbackground;
          $datasets->borderColor = $colorbackground;
          $datasets->hoverBackgroundColor = $colorbackground;
          $datasets->hoverBorderColor = $colorborder;
          $datasets->hoverBorderWidth = 2;
          $datasets->label = "Adultos";
          $dataset[] = $datasets;          

//Cuarto             
          $colorbackground = array("rgba(0, 204, 153, 0.6)");
          $colorborder = array("rgba(0, 204, 153, 1)");
          $datagraph = $menores_arr; 
          $datasets = new \stdClass();
          $datasets->data = $datagraph;
          $datasets->backgroundColor = $colorbackground;
          $datasets->borderColor = $colorbackground;
          $datasets->hoverBackgroundColor = $colorbackground;
          $datasets->hoverBorderColor = $colorborder;
          $datasets->hoverBorderWidth = 2;
          $datasets->label = "Menores";
          $dataset[] = $datasets;            
          

          $returnData->line->datasets = $dataset;
          return json_encode($returnData);
          break;
          case "usrtot":
            $months = array(
              'Ene', 
              'Feb', 
              'Mar', 
              'Abr', 
              'May', 
              'Jun', 
              'Jul', 
              'Ago', 
              'Sep', 
              'Oct', 
              'Nov', 
              'Dic'
            );          
            $current_date = date("d-m-Y");
            $current_year = date("Y");
            $current_month = date("m");          
            $current_fecha = date("Y") . date("m"); 
            $initial_date = date("d-m-Y", strtotime($current_date." -11 months"));
            $initial_year = date("Y", strtotime($current_date." -11 months"));
            $initial_month = date("m", strtotime($current_date." -11 months"));
            $semester_fecha = date("Y", strtotime($current_date." -5 months")) . date("m", strtotime($current_date." -5 months"));
            $trimester_fecha = date("Y", strtotime($current_date." -2 months")) . date("m", strtotime($current_date." -2 months"));
            $datos = [];
  
            $count = (new Query())->select('id')
            ->from('User')
            ->where("DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970'))) < DATEADD(m,-11,dateadd(mm, datediff(mm, 0, GetDate()), 0))") 
            ->count();    

            $select = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) AS fecha,count(*) AS total");
            $groupby = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00'))");
            $query = (new Query)->select($select)
            ->from('user')
            ->where("DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970'))) >= DATEADD(m,-11,dateadd(mm, datediff(mm, 0, GetDate()), 0))")

            ->groupby($groupby)
            ->orderby('fecha');      
            $rows = $query->all();
            $labels = array();
            $nuevos_arr = array();
            $lastdate = "";
            $nuevos = 0;
            foreach ($rows as $registro) {  
              $fecha = $registro['fecha'];
              $month = substr($fecha,4,2) - 1;
              $year = substr($fecha,0,4);
              $name_month = $months[$month];
              $label = $year . "-" . $name_month;
              array_push($labels,$label);  
              $nuevos = $nuevos + $registro['total'];
              array_push($nuevos_arr,$nuevos + $count);
            }   
  
            $returnData = new \stdClass();
            $returnData->line = new \stdClass();
            $returnData->line->type = 'line';
            $returnData->line->title = 'Total de usuarios del banco de recursos';
            $returnData->line->labels = $labels;
            $returnData->line->datasets = new \stdClass();   
            
            $returnData->line->datasets = new \stdClass();   
  
            $dataset = array();
            $datagraph = array();

            $colorbackground = array("rgba(0, 115, 174, 0.6)");
            $colorborder = array("rgba(0, 115, 174, 1)");
            $datagraph = $nuevos_arr; 
            $datasets = new \stdClass();
            $datasets->data = $datagraph;
            $datasets->backgroundColor = $colorbackground;
            $datasets->borderColor = $colorborder;
            $datasets->label = "Total usuarios";
            $datasets->type = "line";
            $dataset[] = $datasets;            
  
            $returnData->line->datasets = $dataset;
            return json_encode($returnData);
            break;   

            case "usrtotadumen":
      
              $current_date = date("d-m-Y");
              $current_year = date("Y");
              $current_month = date("m");          
              $current_fecha = date("Y") . date("m"); 
              $initial_date = date("d-m-Y", strtotime($current_date." -11 months"));
              $initial_year = date("Y", strtotime($current_date." -11 months"));
              $initial_month = date("m", strtotime($current_date." -11 months"));
              $semester_fecha = date("Y", strtotime($current_date." -5 months")) . date("m", strtotime($current_date." -5 months"));
              $trimester_fecha = date("Y", strtotime($current_date." -2 months")) . date("m", strtotime($current_date." -2 months"));
              $datos = [];
    
              $select = new Expression("esmenor,count(*) AS total");
              $groupby = new Expression("esmenor");
              $query = (new Query)->select($select)
              ->from('user')
              ->groupby($groupby);      
              $rows = $query->all();
              $labels = array();
              $mayores_arr = array();
              $menores_arr = array();
              $nuevos_arr = array();
              $lastdate = "";
              $nuevos = 0;
              $mayores = 0;
              $menores = 0;
              foreach ($rows as $registro) {  
                if($registro['esmenor']==0){
                  $mayores = $mayores + $registro['total'];
                }
                else{
                  $menores = $menores + $registro['total'];
                }                
              }   
              
              array_push($labels,"Total");  
              array_push($nuevos_arr,$mayores + $menores);    

              array_push($mayores_arr,$mayores);    

              array_push($menores_arr,$menores);                                
              $returnData = new \stdClass();
              $returnData->line = new \stdClass();
              $returnData->line->type = 'bar';

              $returnData->line->title = 'Comparativo total usuarios mayores y menores de edad ';
              $returnData->line->labels = $labels;
              $returnData->line->datasets = new \stdClass();   
              
              $returnData->line->datasets = new \stdClass();   
    
              $dataset = array();
              $datagraph = array();
  
    //Segundo          
              $colorbackground = array("rgba(0, 115, 174, 0.6)");
              $colorborder = array("rgba(0, 115, 174, 1)");
              $datagraph = $nuevos_arr; 
              $datasets = new \stdClass();
              $datasets->data = $datagraph;
              $datasets->backgroundColor = $colorbackground;
              $datasets->type = "bar";
              $datasets->label = "Total";
              $dataset[] = $datasets;
    //Tercero          
              $colorbackground = array("rgba(212, 20, 90, 0.6)");
              $colorborder = array("rgba(212, 20, 90, 1)");
              $datagraph = $mayores_arr; 
    
              $datasets = new \stdClass();
              $datasets->data = $datagraph;
              $datasets->backgroundColor = $colorbackground;
              $datasets->borderColor = $colorbackground;
              $datasets->type = "bar";
              $datasets->label = "Adultos";
              $dataset[] = $datasets;          
    
    //Cuarto             
              $colorbackground = array("rgba(0, 204, 153, 0.6)");
              $colorborder = array("rgba(0, 204, 153, 1)");
              $datagraph = $menores_arr; 
              $datasets = new \stdClass();
              $datasets->data = $datagraph;
              $datasets->backgroundColor = $colorbackground;
              $datasets->borderColor = $colorbackground;
              $datasets->type = "bar";
              $datasets->label = "Menores";
              $dataset[] = $datasets;     

         
    
              $returnData->line->datasets = $dataset;
              return json_encode($returnData);
              break;   
  
            
            case "usrnew":
              $months = array(
                'Enero', 
                'Febrero', 
                'Marzo', 
                'Abril', 
                'Mayo', 
                'Junio', 
                'Julio', 
                'Agosto', 
                'Septiembre', 
                'Octubre', 
                'Noviembre', 
                'Diciembre'
              );          
              $current_date = date("d-m-Y");
              $current_year = date("Y");
              $current_month = date("m");          
              $current_fecha = date("Y") . date("m"); 
              $initial_date = date("d-m-Y", strtotime($current_date." -11 months"));
              $initial_year = date("Y", strtotime($current_date." -11 months"));
              $initial_month = date("m", strtotime($current_date." -11 months"));
              $semester_fecha = date("Y", strtotime($current_date." -5 months")) . date("m", strtotime($current_date." -5 months"));
              $trimester_fecha = date("Y", strtotime($current_date." -2 months")) . date("m", strtotime($current_date." -2 months"));
              $datos = [];
    
              $count = 0;    
  
              $select = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) AS fecha, ISNULL(esmenor,0) AS esmenor,count(*) AS total");
              $groupby = new Expression("CONCAT(YEAR(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,  FORMAT(MONTH(DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970')))) ,'00')) ,ISNULL(esmenor,0)");
              $query = (new Query)->select($select)
              ->from('user')
              ->where("DATEADD(hour,-0,(dateadd(second ,[created_at], '1/1/1970'))) >= DATEADD(m,-11,dateadd(mm, datediff(mm, 0, GetDate()), 0))")
              ->groupby($groupby)
              ->orderby('fecha');      
//var_dump($query->createCommand()->sql);   
              $rows = $query->all();
              $labels = array();
              $nuevos_arr = array();
              $mayores_arr = array();
              $menores_arr = array();              
              $lastdate = "";
              $nuevos = 0;
              $mayores = 0;
              $menores = 0;
              foreach ($rows as $registro) {  
                $fecha = $registro['fecha'];
                $month = substr($fecha,4,2) - 1;
                $year = substr($fecha,0,4);
                $name_month = $months[$month];
                $label = $year . "-" . $name_month;
                if($lastdate==""){
                  $lastdate = $label;
                  array_push($labels,$label);    
                  if($registro['esmenor']==0){
                    $nuevos = $registro['total'];
                    $mayores = $registro['total']; 
                    $menores = 0;
                  }
                  else{
                    $nuevos = $registro['total'];
                    $mayores = 0; 
                    $menores = $registro['total'];
                  }                  
                }
                else{
                  if($lastdate==$label){
                    $in_labels = in_array($label,$labels);
                    if(!$in_labels){
                      array_push($labels,$label);  
                    }
                    $nuevos = $nuevos + $registro['total'];
                    $menores = $registro['total'];
                    array_push($nuevos_arr,$nuevos);                           
                    array_push($mayores_arr,$mayores); 
                    array_push($menores_arr,$menores); 
                    $nuevos = 0;
                    $mayores = 0;
                    $menores = 0;
                  }
                  else{
                    $in_labels = in_array($label,$labels);
                    if(!$in_labels){
                      array_push($labels,$label);  
                    }
                
                    if($registro['esmenor']==0){
                      $nuevos = $registro['total'];
                      $mayores = $registro['total']; 
                      $menores = 0;
                    }
                    else{
                      $nuevos = $registro['total'];
                      $mayores = 0; 
                      $menores = $registro['total'];
                    }         
                    array_push($nuevos_arr,$nuevos);                           
                    array_push($mayores_arr,$mayores); 
                    array_push($menores_arr,$menores);                      
      
                  }
                  $lastdate = $label;
    
                }           
              }   
              $in_labels = in_array($label,$labels);
              if(!$in_labels){
                array_push($labels,$label);  
              }
              array_push($nuevos_arr,$nuevos);                           
              array_push($mayores_arr,$mayores); 
              array_push($menores_arr,$menores);   
    
              $returnData = new \stdClass();
              $returnData->line = new \stdClass();
              $returnData->line->type = 'line';
              $returnData->line->title = 'Crecimiento de usuarios nuevos en último año';
              $returnData->line->labels = $labels;
              $returnData->line->datasets = new \stdClass();   
              
              $returnData->line->datasets = new \stdClass();   
    
              $dataset = array();
              $datagraph = array();
    //Segundo          
              $colorbackground = array("rgba(0, 115, 174, 0.6)");
              $colorborder = array("rgba(0, 115, 174, 1)");
              $datagraph = $nuevos_arr; 
              $datasets = new \stdClass();
              $datasets->data = $datagraph;
              $datasets->backgroundColor = $colorbackground;
              $datasets->type = "line";
              $datasets->label = "Total";
              $dataset[] = $datasets;
    //Tercero          
              $colorbackground = array("rgba(212, 20, 90, 0.6)");
              $colorborder = array("rgba(212, 20, 90, 1)");
              $datagraph = $mayores_arr; 
    
              $datasets = new \stdClass();
              $datasets->data = $datagraph;
              $datasets->backgroundColor = $colorbackground;
              $datasets->borderColor = $colorbackground;
              $datasets->type = "line";
              $datasets->label = "Adultos";
              $dataset[] = $datasets;          
    
    //Cuarto             
              $colorbackground = array("rgba(0, 204, 153, 0.6)");
              $colorborder = array("rgba(0, 204, 153, 1)");
              $datagraph = $menores_arr; 
              $datasets = new \stdClass();
              $datasets->data = $datagraph;
              $datasets->backgroundColor = $colorbackground;
              $datasets->borderColor = $colorbackground;
              $datasets->type = "line";
              $datasets->label = "Menores";
              $dataset[] = $datasets;             
    
              $returnData->line->datasets = $dataset;
              return json_encode($returnData);
              break;          
  
              case "recvismat" || "recvispal" || "recvisaut" || "recvotmat" || "recvotpal" || "recvotaut" || "recshamat" || "recshapal" || "recshaaut" || "reclikmat" || "reclikpal" || "reclikaut":
                $current_date = date("d-m-Y");
                $current_year = date("Y");
                $current_month = date("m");          
                $current_fecha = date("Y") . date("m"); 
                $initial_date = date("d-m-Y", strtotime($current_date." -11 months"));
                $initial_year = date("Y", strtotime($current_date." -11 months"));
                $initial_month = date("m", strtotime($current_date." -11 months"));
                $semester_fecha = date("Y", strtotime($current_date." -5 months")) . date("m", strtotime($current_date." -5 months"));
                $trimester_fecha = date("Y", strtotime($current_date." -2 months")) . date("m", strtotime($current_date." -2 months"));
                $datos = [];
                $rectot = 0;
                $recmay = 0;
                $recmen = 0;
                $recurso = "";
                $label = "";
                $labels = array();
                $rectot_arr = array();
                $recmay_arr = array();
                $recmen_arr = array();
                $tabla = "TEventos";
                if($tiprep=="reclikmat" || $tiprep=="reclikpal" || $tiprep=="reclikaut"){
                  $tabla = "RFavoritos";
                }
                $select = new Expression("TOP($toprep) c.Nombre, ISNULL(b.esmenor,0) AS esmenor, count(*) AS total");
                $from = "$tabla a, User b, TvaloresDeCaracteristicas c, RRecursosValoresDeCaracteristicas d  ";
                if($profuturo=="true"){
                  $from = "$tabla a, User b, TvaloresDeCaracteristicas c, RRecursosValoresDeCaracteristicas d, TvaloresDeCaracteristicas e, RRecursosValoresDeCaracteristicas f  ";    
                }
                $groupby = new Expression("c.Nombre, ISNULL(b.esmenor,0)");
                if($tiprep=="recvismat"){
                  $title = 'Recursos más vistos Top '. $toprep .' x Materias';
                  $where = "a.Accion = 1 AND a.CreadoPor = b.id AND c.Caracteristicas_id = 8 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor ";
                
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.Valor";
                  }
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;
                  // if($anio!=0){
                  //   $where = $where . " AND YEAR(a.FechaDeCreacion)= ".$anio;
                  //   $title = $title . " del año " . $anio ;
                  // }
                  // if($mes>0){
                  //   $where = $where . " AND MONTH(a.FechaDeCreacion) = " . $mes;
                  //   $title = $title . " del mes " . $mes;
                  // }  
                }
                if($tiprep=="reclikmat"){
                  $title = 'Recursos más gustados Top '. $toprep .' x Materias';
                  $where = "a.rIdUsuarios = b.id AND c.Caracteristicas_id = 8 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.rIdRecursos ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.rIdRecursos";
                  }                 
                  $where = $where . " AND DATEADD(hour,-0,(dateadd(second ,[a].[FechaDeCreacion], '1/1/1970'))) >= ' ".$fecini."' AND DATEADD(hour,-0,(dateadd(second ,[a].[FechaDeCreacion], '1/1/1970'))) <= '".$fecfin."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;            
                }                
                if($tiprep=="recvotmat"){
                  $title = 'Recursos más votados Top '. $toprep .' x Materias';
                  $where = "a.Accion = 3 AND a.CreadoPor = b.id AND c.Caracteristicas_id = 8 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.Valor";
                  }                  
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                               
                }
                if($tiprep=="recshamat"){
                  $title = 'Recursos más compartidos Top '. $toprep .' x Materias';
                  $where = "a.Accion = 15 AND a.CreadoPor = b.id AND c.Caracteristicas_id = 8 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.Valor";
                  }                  
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                                
                }                
                if($tiprep=="recvispal"){
                  $title = 'Recursos más vistos Top '. $toprep .' x Palabras Clave';
                  $where = "a.Accion = 1 AND a.CreadoPor = b.id AND c.Caracteristicas_id = 10 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.Valor";
                  }                  
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                                
                }
                if($tiprep=="recvotpal"){
                  $title = 'Recursos más votados Top '. $toprep .' x Palabras Clave';
                  $where = "a.Accion = 3 AND a.CreadoPor = b.id AND c.Caracteristicas_id = 10 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.Valor";
                  }                  
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                               
                }
                if($tiprep=="recshapal"){
                  $title = 'Recursos más compartidos Top '. $toprep .' x Palabras Clave';
                  $where = "a.Accion = 15 AND a.CreadoPor = b.id AND c.Caracteristicas_id = 10 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.Valor";
                  }                  
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                             
                }  
                if($tiprep=="reclikpal"){
                  $title = 'Recursos más gustados Top '. $toprep .' x Palabras Clave';
                  $where = "a.rIdUsuarios = b.id AND c.Caracteristicas_id = 8 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.rIdRecursos ";
                  if($profuturo=="true"){         
                    $where = $where . " AND e.Caracteristicas_id = 4 AND e.idValoresDeCaracteristicas = 188 AND e.idValoresDeCaracteristicas = f.rIdValores AND f.rIdRecursos = a.rIdRecursos";
                  }                  
                  $where = $where . " AND DATEADD(hour,-0,(dateadd(second ,[a].[FechaDeCreacion], '1/1/1970'))) >= ' ".$fecini."' AND DATEADD(hour,-0,(dateadd(second ,[a].[FechaDeCreacion], '1/1/1970'))) <= '".$fecfin."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                     
                }                
                              
                if($tiprep=="recvisaut"){
                  $title = 'Recursos más vistos Top '. $toprep .' x Autor';
                  $select = new Expression("TOP($toprep) b.nombre AS Nombre, ISNULL(b.esmenor,0) AS esmenor, count(*) AS total");
                  $from = "TEventos a, User b ";
                  $where = "a.Accion = 1 AND a.CreadoPor = b.id ";
                  if($profuturo=="true"){       
                    $from = "TEventos a, User b, TvaloresDeCaracteristicas c, RRecursosValoresDeCaracteristicas d ";  
                    $where = $where . " AND c.Caracteristicas_id = 4 AND c.idValoresDeCaracteristicas = 188 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor";
                  }                                    
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                                 
                  $groupby = new Expression("b.nombre, ISNULL(b.esmenor,0)");
                }
                if($tiprep=="recvotaut"){
                  $title = 'Recursos más votados Top '. $toprep .' x Autor';
                  $select = new Expression("TOP($toprep) b.nombre AS Nombre, ISNULL(b.esmenor,0) AS esmenor, count(*) AS total");
                  $from = "TEventos a, User b ";
                  $where = "a.Accion = 3 AND a.CreadoPor = b.id ";
                  if($profuturo=="true"){       
                    $from = "TEventos a, User b, TvaloresDeCaracteristicas c, RRecursosValoresDeCaracteristicas d ";  
                    $where = $where . " AND c.Caracteristicas_id = 4 AND c.idValoresDeCaracteristicas = 188 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor";
                  }                      
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                              
                  $groupby = new Expression("b.nombre, ISNULL(b.esmenor,0)");
                }                

                if($tiprep=="recshaaut"){
                  $title = 'Recursos más compartidos Top '. $toprep .' x Autor';
                  $select = new Expression("TOP($toprep) b.nombre AS Nombre, ISNULL(b.esmenor,0) AS esmenor, count(*) AS total");
                  $from = "TEventos a, User b ";
                  $where = "a.Accion = 15 AND a.CreadoPor = b.id ";
                  if($profuturo=="true"){       
                    $from = "TEventos a, User b, TvaloresDeCaracteristicas c, RRecursosValoresDeCaracteristicas d ";  
                    $where = $where . " AND c.Caracteristicas_id = 4 AND c.idValoresDeCaracteristicas = 188 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.Valor";
                  }                      
                  $where = $where . "  AND CAST(a.FechaDeCreacion AS DATE)  >= '" . $fecini . "'  AND CAST(a.FechaDeCreacion AS DATE)  <= '" . $fecfin ."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                                 
                  $groupby = new Expression("b.nombre, ISNULL(b.esmenor,0)");
                }                                

                if($tiprep=="reclikaut"){
                  $title = 'Recursos más gustados Top '. $toprep .' x Autor';
                  $select = new Expression("TOP($toprep) b.nombre AS Nombre, ISNULL(b.esmenor,0) AS esmenor, count(*) AS total");
                  $from = "RFavoritos a, User b ";
                  $where = "a.rIdUsuarios = b.id ";
                  if($profuturo=="true"){       
                    $from = "RFavoritos a, User b, TvaloresDeCaracteristicas c, RRecursosValoresDeCaracteristicas d ";  
                    $where = $where . " AND c.Caracteristicas_id = 4 AND c.idValoresDeCaracteristicas = 188 AND c.idValoresDeCaracteristicas = d.rIdValores AND d.rIdRecursos = a.rIdRecursos";
                  }                      
                  $where = $where . " AND DATEADD(hour,-0,(dateadd(second ,[a].[FechaDeCreacion], '1/1/1970'))) >= ' ".$fecini."' AND DATEADD(hour,-0,(dateadd(second ,[a].[FechaDeCreacion], '1/1/1970'))) <= '".$fecfin."'";
                  $title = $title . " del " . $fecini . " al " . $fecfin ;                    
                  $groupby = new Expression("b.nombre, ISNULL(b.esmenor,0)");
                }                
                
                $orderby = new Expression("count(*) desc,  ISNULL(b.esmenor,0) asc");
                $query = (new Query)->select($select)
                ->from($from)
                ->where($where)
                ->groupby($groupby)
                ->orderby($orderby);      
                $rows = $query->all();
// var_dump($query->createCommand()->sql);                
                foreach ($rows as $registro) {  
                  if($recurso!=""){
                    if($recurso!=$registro['Nombre']){
                      $rec = $recurso;
                      $in_labels = in_array($rec,$labels);
                      if(!$in_labels){
                        array_push($labels,$rec);  
                      }      
                      array_push($rectot_arr,$recmay + $recmen);                           
                      array_push($recmay_arr,$recmay); 
                      array_push($recmen_arr,$recmen);   
                      if($registro['esmenor']==0){
                        $recmay = $registro['total'] + 0;
                      }
                      else{
                        $recmen = $registro['total'] + 0;
                      }
    
                    }
                    else{
                      if($registro['esmenor']==0){
                        $recmay = $recmay + $registro['total'];
                      }
                      else{
                        $recmen = $recmen + $registro['total'];
                      }
                    }
                  }
                  else{
                    if($registro['esmenor']==0){
                      $recmay = $registro['total'] + 0;
                    }
                    else{
                      $recmen = $registro['total'] + 0;
                    }
                  }
                  $recurso = $registro['Nombre'];
      
                }      
                $in_labels = in_array($recurso,$labels);
                if(!$in_labels){
                  array_push($labels,$recurso);  
                }      
                array_push($rectot_arr,$recmay + $recmen);                           
                array_push($recmay_arr,$recmay); 
                array_push($recmen_arr,$recmen);   


 
                $returnData = new \stdClass();
                $returnData->line = new \stdClass();
                $returnData->line->type = 'bar';
                $returnData->line->title = $title;
                $returnData->line->labels = $labels;
                $returnData->line->datasets = new \stdClass();   
      
                $dataset = array();
      //Segundo          
                $colorbackground = array("rgba(0, 115, 174, 0.6)");
                $colorborder = array("rgba(0, 115, 174, 1)");
                $datagraph = $rectot_arr; 
                $datasets = new \stdClass();
                $datasets->data = $datagraph;
                $datasets->backgroundColor = $colorbackground;
                $datasets->borderColor = $colorbackground;
                $datasets->hoverBackgroundColor = $colorbackground;
                $datasets->hoverBorderColor = $colorborder;
                $datasets->hoverBorderWidth = 2;
                $datasets->label = "Total";
                $dataset[] = $datasets;
      //Tercero          
                $colorbackground = array("rgba(212, 20, 90, 0.6)");
                $colorborder = array("rgba(212, 20, 90, 1)");
                $datagraph = $recmay_arr; 
      
                $datasets = new \stdClass();
                $datasets->data = $datagraph;
                $datasets->backgroundColor = $colorbackground;
                $datasets->borderColor = $colorbackground;
                $datasets->hoverBackgroundColor = $colorbackground;
                $datasets->hoverBorderColor = $colorborder;
                $datasets->hoverBorderWidth = 2;
                $datasets->label = "Adultos";
                $dataset[] = $datasets;          
      
      //Cuarto             
                $colorbackground = array("rgba(0, 204, 153, 0.6)");
                $colorborder = array("rgba(0, 204, 153, 1)");
                $datagraph = $recmen_arr; 
                $datasets = new \stdClass();
                $datasets->data = $datagraph;
                $datasets->backgroundColor = $colorbackground;
                $datasets->borderColor = $colorbackground;
                $datasets->hoverBackgroundColor = $colorbackground;
                $datasets->hoverBorderColor = $colorborder;
                $datasets->hoverBorderWidth = 2;
                $datasets->label = "Menores";
                $dataset[] = $datasets;            
                
      
                $returnData->line->datasets = $dataset;
                return json_encode($returnData);
                
                break;
              default:
          echo "Your favorite color is neither red, blue, nor green!";
      }
 

      return json_encode($data);        
      }
    
    public function actionVerReportes() {
        
      $tiprep = "";
      $mes = 0; 
      $anio = 0;
      $fecini = "";
      $fecfin = "";
      $etiqueta = "";
      $asignatura = "";
      $autor = "";
      $periodo = "";
      $toprep = 10;
      $profuturo = TRUE;

      if(isset($_GET['profuturo'])){
        $profuturo = $_GET['profuturo']; 
      }
 
      if(isset($_GET['tiprep'])){
        $tiprep = $_GET['tiprep']; 
      }
      if(isset($_GET['toprep'])){
        $toprep = $_GET['toprep']; 
      }      
      if(isset($_GET['mes'])){
        $mes = $_GET['mes']; 
      }
      if(isset($_GET['anio'])){
        $anio = $_GET['anio']; 
      }     
      if(isset($_GET['fecha1'])){
        $fecini = $_GET['fecha1']; 
      }
      if(isset($_GET['fecha2'])){
        $fecfin = $_GET['fecha2']; 
      }
      if(isset($_GET['etiqueta'])){
        $etiqueta = $_GET['etiqueta']; 
      }     
      if(isset($_GET['asignatura'])){
        $asignatura = $_GET['asignatura']; 
      }          
      if(isset($_GET['autor'])){
        $autor = $_GET['autor']; 
      }   
      if(isset($_GET['periodo'])){
        $periodo = $_GET['periodo']; 
      }   
     
       return $this->ViewReportes($tiprep,$mes,$anio,$fecini,$fecfin,$etiqueta,$asignatura,$autor,$periodo,$toprep,$profuturo);
 
     }    


}

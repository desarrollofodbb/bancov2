<?php

return [
    'components' => [
    // list of component configurations
    ],
    'params' => [
        // list of actions parameters
//        "a_create" => Yii::t('app', 'Crear'), 
        // list of titles parameters
        "t_sesion_usuarios" => Yii::t('app', 'Sesión de usuarios'),
        "m_sesion_usuarios" => Yii::t('app', 'Cantidad de usuarios que han iniciado sesión por mes.'),
        "t_usuarios_que_publican_recursos" => Yii::t('app', 'Usuarios que publican recursos'),
        "m_usuarios_que_publican_recursos" => Yii::t('app', 'Cantidad de usuarios que publican recursos, (Se incluyen únicamente recursos con los estados "pendientes de validación", "aprobados" y "rechazados").'),
        "t_busquedas_por_mes" => Yii::t('app', 'Búsquedas por mes'),
        "m_busquedas_por_mes" => Yii::t('app', 'Cantidad de búsquedas realizadas por mes.'),
        "t_filtros_de_busquedas" => Yii::t('app', 'Filtros de búsquedas'),
        "m_filtros_de_busquedas" => Yii::t('app', 'Cantidad de búsquedas asociadas a cada tipo de filtro, sea por año o por mes.'),
        "t_cantidad_de_palabras_por_mes" => Yii::t('app', 'Cantidad de palabras buscadas'),
        "m_cantidad_de_palabras_por_mes" => Yii::t('app', ' Cantidad de palabras buscadas ordenadas de forma ascendente (de mayor a menor) con la posibilidad de filtrar por año y mes.'),
        "t_recursos_publicados_por_mes" => Yii::t('app', 'Recursos publicados por mes'),
        "m_recursos_publicados_por_mes" => Yii::t('app', 'Cantidad de recursos validados con estado aprobado y agrupados por mes con la posibilidad de filtrar por año.'),
        "t_recursos_mas_vistos" => Yii::t('app', 'Recursos más vistos'),
        "m_recursos_mas_vistos" => Yii::t('app', 'Cantidad de recursos  más visitados.'),
        "t_recursos_mas_descargados" => Yii::t('app', 'Recursos más descargados'),
        "m_recursos_mas_descargados" => Yii::t('app', 'Cantidad de recursos más descargados.'),
        "t_recursos_mas_votados" => Yii::t('app', 'Recursos más votados'),
        "m_recursos_mas_votados" => Yii::t('app', 'Cantidad de recursos más votados.'),
        "t_recursos_por_usuario" => Yii::t('app', 'Recursos por usuario'),
        "m_recursos_por_usuario" => Yii::t('app', 'Cantidad de recursos publicados por usuarios, con la posibilidad de filtrar por año, mes, estado de los recursos y por un usuario en específico, (No incluye los recursos eliminados).'),
    // list of message parameters
//       
    ],
];


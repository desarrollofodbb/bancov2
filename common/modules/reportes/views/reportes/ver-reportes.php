<?php

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\modules\recursos\models\Recursos */


    public function actionVerReportes() {
        
      $Tag = "";
      $Nombre = ""; 
      $Autor = "";
      $Tipo = "";
      $anio1 = "";
      $anio2 = "";
      $Dirigido = "";
      $Nivel = "";
      $Asignatura = "";
      $Idioma = "";
      $Usos = "";
      $limit = 0; //Cantidad de Registros 
      $offset = 0; //Registros a saltar
 
      if(isset($_POST['tiprep'])){
        $tiprep = $_POST['tiprep']; 
      }
      if(isset($_POST['mes'])){
        $mes = $_POST['mes']; 
      }
      if(isset($_POST['anio'])){
        $anio = $_POST['anio']; 
      }     
      if(isset($_POST['fecini'])){
        $fecini = $_POST['fecini']; 
      }
      if(isset($_POST['fecfin'])){
        $fecfin = $_POST['fecfin']; 
      }
      if(isset($_POST['etiqueta'])){
        $etiqueta = $_POST['etiqueta']; 
      }     
      if(isset($_POST['asignatura'])){
        $asignatura = $_POST['asignatura']; 
      }          
      if(isset($_POST['autor'])){
        $autor = $_POST['autor']; 
      }   

     
       return $this->ViewReportes($tiprep,$mes,$anio,$fecini,$fecfin,$etiqueta,$asignatura,$autor);
 
     }    
<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\User;

$module = Yii::$app->getModule("reportes");
$this->title = "Lista de Usuarios";
global $usuariosModel;
$usuariosModel = $searchModel;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\reportes\models\Usuarios */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<div class="reporte-view">

    <h1><?php echo Html::encode($this->title); ?></h1> 
    <article>
        <p><strong><?php echo Yii::t("app", "Detalle del reporte:"); ?></strong>
            <?php echo Yii::t("app", "Listado de Usuarios"); ?> </p>
    </article>
    <?php echo Alert::widget() ?>

    <?php $form = ActiveForm::begin(["method" => "GET", "action" => ["/reportes/reportes/listado-usuarios"]]); ?>

    <?php

    $btnBack = Html::a(Yii::t("app", "Volver al listado de reportes"), ["/reportes/reportes/overview"], ["class" => "btn btn-default"]);
    $btnSubmit = Html::submitButton(Yii::t('app', 'Generar'), ['class' => 'btn btn-primary']);

    ?>  
    <?php

      $data = [0 => "Todos", 1 => "Adultos", 2 => "Menores"]; 

      echo $form->field($searchModel, 'year')->label('Tipo persona') ->widget(Select2::classname(), [
      'data' => $data,
      'name' => 'year',
      'id' => 'year',
      'value' => [0], // initial value    
      'options' => ['placeholder' => 'Seleccione tipo ...'],

      ]);
        
      echo $btnBack;
      echo $btnSubmit;  

    ?>  
    <?php ActiveForm::end(); ?>

    <?php
    echo "<br>";
// Generate a bootstrap responsive striped table with row highlighted on hover
// var_dump($searchModel);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => '',
                'format' => 'raw',
                'label' => Yii::t("app", "Nombre"),

                'value' => function ($data) {
                   // $data = array_map('utf8_decode', $data);
                    $nombre = $data["nombre"];
                    //$nombre = htmlspecialchars($nombre, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8', false);
 
                    return $nombre;
                },
            ],
            [
                'attribute' => '',
                'format' => 'html',
                'label' => Yii::t("app", "Usuario"),
                'value' => function ($data) {
                  return $data["username"];
                },
            ],
            [
                'attribute' => '',
                'format' => 'html',
                'label' => Yii::t("app", "Correo"),
                'value' => function ($data) {
                  if($data['esmenor']==1){
                  	return "Persona Menor";
                  }
                  return $data["email"];
                },
            ],
            [
                'attribute' => '',
                'format' => 'html',
                'label' => Yii::t("app", "ID"),
                'value' => function ($data) {
                  if($data['esmenor']==1){
                  	return "Persona Menor";
                  }
                  if($data['identificacion']==null || $data['identificacion']==""){
                    $username = $data['username'];
                    $urlws = Yii::$app->params["urlWS"] 
                   . "/usuario/getPersonaByUsername/" . $username;
                    $ch = curl_init($urlws);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $resultadologin = json_decode($result);   
                    if(isset($resultadologin->infoPersona)){
                    $tuser = User::findOne($data['id']);
                    $tuser->identificacion = $resultadologin->infoPersona->Identificacion;
                    $tuser->save();                      
                      return $resultadologin->infoPersona->Identificacion;	
                    }
                  }
                  else{
                  	return $data['identificacion'];
                  }
                  return "No indicado";
                },
            ],                  
        ],             
        'toolbar' => [
          '{export}',
          '{toggleData}',
        ],  
    // set export properties
        'export' => [
          'fontAwesome' => true
        ],    
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;' . 'Lista de usuarios',
        ],    
        'responsive'=>true,
        'hover'=>true,
        'condensed' => true,
        'persistResize' => false,
        'exportContainer' => ['class' => 'btn-group-sm'],
        'exportConfig' => [
          GridView::HTML => ['label' => 'HTML'],
        // GridView::CSV => ['label' => 'CSV'],
        // GridView::TEXT  => ['label' => 'Text'],
          GridView::EXCEL => ['label' => 'Excel'],
        // GridView::PDF => ['label' => 'PDF'],
        // GridView::JSON => ['label' => 'JSON'],
        ],
        'showPageSummary' => true
    ]);
  ?>
</div>

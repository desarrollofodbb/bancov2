<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use kartik\select2\Select2;
$userid = Yii::$app->user->identity->id;


if(isset($_GET["Autor"])){
  $userid = $_GET["Autor"];    
}
$user = new User();
$user = $user->findIdentity($userid);   


?>
<!-- <link rel="stylesheet" href="css/demo.css"> -->



<div class="" style="">    
  <div class="w3-hide-large" style="margin-top:25px"></div>  
 
  <div class="w3-panel w3-card" style="background-color:#00CC99"><h4 style="color:white"><i class="fa fa-bar-chart"></i> Reportes Estadísticos</h4>
  </div>   

  <div class="" >
    <div class="w3-panel">
      <div class="w3-bar" style="background-color:#00CC99">
        <button id = "guser_but" class="w3-bar-item w3-button tablink" style="background-color:#F29222" onclick="openGraph(event,'guser')"><h5 style="color:white"><i class="fa fa-user"></i> Usuarios</h5></button>
        <button id = "grecurso_but" class="w3-bar-item w3-button tablink" onclick="openGraph(event,'grecurso')"><h5 style="color:white"><i class="fa fa-eye"></i> Recursos</h5></button>
      </div>    
    </div>

    <div id="guser" class="w3-container w3-border grafico">
      <div class="w3-container"  style="margin-top:15px">
    <?php
      $data = ['usrtot' => 'Total usuarios', 'usrper' => 'Nuevos resumido por período', 'usrmes' => 'Nuevos seleccionando período', 'usrnew' => 'Crecimiento de nuevos', 'usrtotadumen' => 'Usuarios total comparativo']; 
      echo Select2::widget([
      'name' => 'sel-repusr',
      'id' => 'sel-repusr',
      'value' => ['usrtot'], // initial value
      'data' => $data,
      'options' => [
        'placeholder' => 'Seleccione tipo de reporte ...',
        'options' => [
          
        ]
      ],
      'pluginEvents' => [
        "change" => "function() { // function to make ajax call here 
          openGraph(event,'guser');
       // document.getElementById('zoom-but3').click();
      }",
      ],      
      ]);
      ?>    
      </div>
      <div class="col-sm-12 chart-container">
        <div id="usr_opciones"  style="display:none">
        <h3 id="usr_title" style="font-weight: bold;margin-left: 15px; margin-top:10px; margin-bottom: 10px; color:#00CC99">
          <i class="fa fa-user-circle"></i> Nuevos por 
          <input class="w3-radio" type="radio" name="per" value="mes" checked onclick="refreshRep(this)">
          <label>Mes</label>
          <input class="w3-radio" type="radio" name="per" value="trimes" onclick="refreshRep(this)">
          <label>Trimestre</label>
          <input class="w3-radio" type="radio" name="per" value="sixmes" onclick="refreshRep(this)">
          <label>Semestre</label>
          <input class="w3-radio" type="radio" name="per" value="anio" onclick="refreshRep(this)">
          <label>Año</label>            
        </h3>  
        <h3 id="usr_title2" style="font-weight: bold;margin-left: 15px; margin-top:10px; margin-bottom: 10px; color:#00CC99">
          Seleccionar año
        </h3>
        <div id = "usr_year" class="w3-container w3-half">
    <?php
       $anio = date('Y');
       $data = [$anio => $anio, $anio - 1 => $anio - 1, $anio - 2 => $anio - 2, $anio - 3 => $anio - 3, $anio - 4 => $anio - 4, $anio - 5 => $anio - 5]; 
       echo Select2::widget([
       'name' => 'sel-anio',
       'id' => 'sel-anio',
       'value' => [$anio], // initial value
       'data' => $data,
       'options' => [
         'placeholder' => 'Seleccione año',
         'options' => [

         ]
       ],
       'pluginEvents' => [
         "change" => "function() { // function to make ajax call here 
           refreshRep('anio');
        // document.getElementById('zoom-but3').click();
       }",
       ],  
       ]);        
    ?>              
        </div>   
        <div id="usr_month" class="w3-container w3-half">
    <?php
       $mes = date('m');
       $data = ['0' => 'Todos los meses', 1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Setiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre']; 
       echo Select2::widget([
       'name' => 'sel-mes',
       'id' => 'sel-mes',
       'value' => ['0'], // initial value
       'data' => $data,
       'options' => [
         'placeholder' => 'Seleccione mes',
         'options' => [

         ]
       ],
       'pluginEvents' => [
         "change" => "function() { // function to make ajax call here 
           openGraph(event,'guser');
        // document.getElementById('zoom-but3').click();
       }",
       ],  
       ]);        
    ?>              
        </div> 
        </div>
        <div class="col-sm-12 chart-container">
        <canvas id="myChartUsuarios"></canvas>
        <div class="w3-center">
          <button id="download-pdf2" onclick="downloadPDF('usuarios')" class="w3-button w3-teal w3-border w3-border-amber w3-round-large">Descargar PDF</button>
        </div>
        </div>
      </div>     
    </div>
    
    <div id="grecurso" class="w3-container w3-border grafico" style="display:none">
      <div class="w3-container"  style="margin-top:15px">
      <input id="profuturo" class="w3-check" type="checkbox" checked="checked" onclick="profuturo()">
<label id="labelprofuturo">Sólo ProFuturo</label>
      </div>
      <div class="w3-row">
      <div class="w3-container w3-third"  style="margin-top:15px">
    
      <?php
      $data = ['recvis' => 'Más vistos', 'recvot' => 'Más votados', 'reclik' => 'Más me gusta', 'recsha' => 'Más compartidos']; 
      echo Select2::widget([
      'name' => 'sel-tiprep',
      'id' => 'sel-tiprep',
      'value' => ['recvis'], // initial value
      'data' => $data,
      'options' => [
        'placeholder' => 'Seleccione tipo de reporte ...',
        'options' => [
          
        ]
      ],
      'pluginEvents' => [
        "change" => "function() { // function to make ajax call here 
          openGraph(event,'grecurso');
       // document.getElementById('zoom-but3').click();
      }",
      ],      
      ]);
      ?>    
      </div>
      <div class="w3-container w3-third"  style="margin-top:15px">
      <?php
      $data = ['mat' => 'Por Materia', 'pal' => 'Por palabra clave', 'aut' => 'Por autor']; 
      echo Select2::widget([
      'name' => 'sel-tipfil',
      'id' => 'sel-tipfil',
      'value' => ['mat'], // initial value
      'data' => $data,
      'options' => [
        'placeholder' => 'Seleccione tipo de reporte ...',
        'options' => [

        ]
      ],
      'pluginEvents' => [
        "change" => "function() { // function to make ajax call here 
          openGraph(event,'grecurso');
       // document.getElementById('zoom-but3').click();
      }",
      ],  
      ]);        
    ?>    
    </div>
    <div class="w3-container w3-third"  style="margin-top:15px">
    <?php
      $data = ['5' => 'Top 5', '10' => 'Top 10', '15' => 'Top 15', '20' => 'Top 20']; 
      echo Select2::widget([
      'name' => 'sel-tiptop',
      'id' => 'sel-tiptop',
      'value' => ['10'], // initial value
      'data' => $data,
      'options' => [
        'placeholder' => 'Seleccione tipo de top',
        'options' => [

        ]
      ],
      'pluginEvents' => [
        "change" => "function() { // function to make ajax call here 
          openGraph(event,'grecurso');
       // document.getElementById('zoom-but3').click();
      }",
      ],  
      ]);        
    ?>       
    </div>
    </div>
    <div class="w3-row">
    <div class="w3-container w3-half"  style="margin-top:15px">
            <label id="labelfecha1" class="control-label" style="font-weight: bold;font-size:16px" for="fecha1">Fecha inicial</label>            
            <input id = "fecha1" style="padding:0px; height: 36px; color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="date" onchange="openGraph(event,'grecurso')"></input>   
   
    </div>    
    <div class="w3-container w3-half"  style="margin-top:15px">
      <label id="labelfecha2" class="control-label" style="font-weight: bold;font-size:16px" for="fecha2">Fecha final</label>            
      <input id = "fecha2" style="padding:0px; height: 36px; color: #666666;font-weight: normal; border: none;" class="w3-input w3-round-xlarge" type="date" onchange="openGraph(event,'grecurso')"></input>        
    <?php
      // $mes = date('m');
      // $data = ['0' => 'Todos los meses', 1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Setiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre']; 
      // echo Select2::widget([
      // 'name' => 'sel-mes',
      // 'id' => 'sel-mes',
      // 'value' => ['0'], // initial value
      // 'data' => $data,
      // 'options' => [
      //   'placeholder' => 'Seleccione mes',
      //   'options' => [

      //   ]
      // ],
      // 'pluginEvents' => [
      //   "change" => "function() { // function to make ajax call here 
      //     openGraph(event,'grecurso');
      //  // document.getElementById('zoom-but3').click();
      // }",
      // ],  
      // ]);        
    ?>       
    </div>  
    </div>   
    <div class="col-sm-12 chart-container">
      <canvas id="myChartRecursos"></canvas>
      <div class="w3-center">
        <button id="download-pdf" onclick="downloadPDF('recurso')" class="w3-button w3-teal w3-border w3-border-amber w3-round-large">Descargar PDF</button>
      </div>      
    </div>       
  </div> 

  </div>




</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.3.2/chart.min.js"></script>

<script>
var backgroundColor = 'white';
var elegido = "usrmes";
function profuturo(){
  // var rep = document.getElementById("profuturo").checked;
  // document.getElementById("labelprofuturo").innerHTML = "Todos los recursos del banco";
  // if(rep){
  //   document.getElementById("labelprofuturo").innerHTML = "Sólo etiquetados como Profuturo";
  // }
  openGraph('','grecurso');
}

function reportesinicio(tiprep){

  rutacol = "/reportes/reportes/ver-reportes";

  if(tiprep=='usrmes' || tiprep=='mes' || tiprep=='trimes' || tiprep=='sixmes' || tiprep=='anio'){
    usrmes(tiprep);
  }
  else{
    var anio = document.getElementById("sel-anio").value;
    var mes = document.getElementById("sel-mes").value;    
    $.ajax({        
      url: rutacol,
      type: "GET",
      data:{
        tiprep : tiprep,
        anio : anio,
        mes : mes,
        '_csrf': yii.getCsrfToken()
      },
      success: function (response) {


        datares = JSON.parse(response);
console.log(datares);

        $.each(datares, function (dataType, data) {
          var ctx = document.getElementById('myChartUsuarios').getContext('2d');
          if (window.myPie) {
            window.myPie.destroy();
          }          
          var config = {
                type: data.type,
                data: {
                    datasets: data.datasets,
                    labels: data.labels
                },
                options:  {
                    responsive: true,
                    plugins :{
                    title: {
                        display: true,
                        position: 'top',
                        text: data.title,                        
                        color : '#00CC99',
                        font:{
                          size : 20
                        }                        
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: '#333'
                        }
                    }
                    }
                }
            };          
console.log("key: " + dataType + " value: " + data);
window.myPie = new Chart(ctx, config);
        });        

        

      },
      error: function (response){
        console.log(response);

      }
    });   
  }
  
 
    
}

function openGraph(evt, graphName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("grafico");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].style = "";
  }
  document.getElementById(graphName).style.display = "block";
  
  
  // alert(graphName);
  if(graphName=="grecurso"){
    document.getElementById('usr_opciones').style.display = "none";  
    var prirep = document.getElementById("sel-tiprep").value;
    var segrep = document.getElementById("sel-tipfil").value;
    var toprep = document.getElementById("sel-tiptop").value;
    var anio = document.getElementById("sel-anio").value;
    var mes = document.getElementById("sel-mes").value;
    var f1 = new Date(document.getElementById("fecha1").value);

    var year = f1.getFullYear();
    var m = f1.getMonth() + 1
    var month = ("0" + m).slice(-2);
    var d = f1.getDate();
    var day = ("0" + d).slice(-2);

    fecha1 = year + "-" + month + "-" + day  ;

    var f2 = new Date(document.getElementById("fecha2").value);

    var year = f2.getFullYear();
    var m = f2.getMonth() + 1
    var month = ("0" + m).slice(-2);
    var d = f2.getDate();
    var day = ("0" + d).slice(-2);

    fecha2 = year + "-" + month + "-" + day  ;

    var rep = prirep.concat(segrep);

    //reportesrecursos(rep,toprep,anio,mes);
    reportesrecursos(rep,toprep,fecha1,fecha2,anio,mes);
  }
  else{
    var rep = document.getElementById("sel-repusr").value;

    if(rep=="usrmes" || rep=="usrper"){
      document.getElementById('usr_title').style.display = "block"; 
      document.getElementById('usr_title2').style.display = "none"; 
      document.getElementById('usr_opciones').style.display = "block";     
      document.getElementById('usr_year').style.display = "block";  
      document.getElementById('usr_month').style.display = "block"; 
      if(rep=="usrper") {
        document.getElementById('usr_year').style.display = "none";  
        document.getElementById('usr_title').style.display = "none"; 
        document.getElementById('usr_month').style.display = "none";
        document.getElementById('usr_title2').style.display = "none";    
      }
      
    }
    else{
      document.getElementById('usr_opciones').style.display = "none";      
    }
    reportesinicio(rep);    
  }
  // if(graphName=="grecursovotado"){
  //   reportesrecursosVot();
  // }  

  if(evt!=""){
    obj_but = graphName + "_but";
    document.getElementById(obj_but).style = "background-color:#F29222"; 
   // evt.currentTarget.style = "background-color:#F29222";  
  }
  
}

function usrmes(periodo) {
    var anio = document.getElementById("sel-anio").value;
    var mes = document.getElementById("sel-mes").value;

    if(periodo=='usrmes'){    
      periodo = "mes";
    }

  $.ajax({        
      url: rutacol,
      type: "GET",
      data:{
        tiprep : "usrmes",
        periodo : periodo,
        anio : anio,
        mes : mes
      },
      success: function (response) {
      

        datares = JSON.parse(response);
console.log(datares);

        $.each(datares, function (dataType, data) {
          var ctx = document.getElementById('myChartUsuarios').getContext('2d');
          if (window.myPie) {
            window.myPie.destroy();
          }
          var config = {
                type: data.type,
                data: {
                    datasets: data.datasets,
                    labels: data.labels
                },
                options:  {
                    responsive: true,
                    plugins :{
                    title: {
                        display: true,
                        position: 'top',
                        text: data.title,                        
                        color : '#00CC99',
                        font:{
                          size : 20
                        }                        
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: '#333'
                        }
                    }
                    }
                }
            };          
console.log("key: " + dataType + " value: " + data);

            
            window.myPie = new Chart(ctx, config);

        });                

      },
      error: function (response){
        console.log(response);

      }
    });      


}

function fillCanvasBackgroundWithColor(canvas, color) {
  
  const context = canvas.getContext('2d');

  context.save();
  
  context.globalCompositeOperation = 'destination-over';

  context.fillStyle = color;
  context.fillRect(0, 0, canvas.width, canvas.height);

  context.restore();
}

function refreshRep(elem) {
  var rep = document.getElementById("sel-repusr").value;
var radios = document.getElementsByTagName('input');
var value;
for (var i = 0; i < radios.length; i++) {
    if (radios[i].type === 'radio' && radios[i].checked) {
        // get value, set checked flag or do whatever you need to
        value = radios[i].value; 

    }
}

  if(value=="mes" || value=="trimes" || value=="sixmes" || value == "anio"){
    var checkBox = elem;
    elegido = checkBox.value;
    document.getElementById("sel-mes").disabled = false;
    if(value!="mes"){
      document.getElementById("sel-mes").disabled = true;
    }

      usrmes(value);

  }
  else{
    reportesinicio(value);
    if(rep=="usrper"){
      
    }
    // document.getElementById("sel-mes").disabled = false;
    // if(elegido!="mes"){
    //   document.getElementById("sel-mes").disabled = true;
    // }
    // usrmes(elegido);
  }

}

function reportesrecursos(rep,top,fecha1,fecha2,anio,mes){
  var profuturo = document.getElementById("profuturo").checked;

  $.ajax({        
      url: rutacol,
      type: "GET",
      data:{
        tiprep : rep,
        toprep : top,
        fecha1 : fecha1,
        fecha2 : fecha2,
        profuturo : profuturo,
        anio : anio,
        mes : mes
      },
      success: function (response) {


        datares = JSON.parse(response);
console.log(datares);

        $.each(datares, function (dataType, data) {
          var ctx = document.getElementById('myChartRecursos').getContext('2d');
          if (window.myPie) {
            window.myPie.destroy();
          }
          var config = {
                type: data.type,
                data: {
                    datasets: data.datasets,
                    labels: data.labels
                },
                options:  {
                    responsive: true,
                    plugins :{
                    title: {
                        display: true,
                        position: 'top',
                        text: data.title,                        
                        color : '#00CC99',
                        font:{
                          size : 20
                        }                        
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: '#333'
                        }
                    }
                    }
                }
            };          
console.log("key: " + dataType + " value: " + data);
// titlegraph = document.getElementById('titlegraph');
// titlegraph.innerHTML = data.title


window.myPie = new Chart(ctx, config);
        });        

        

      },
      error: function (response){
        console.log(response);

      }
    });   



}


//add event listener to button

// document.getElementById('download-pdf').addEventListener("click", downloadPDF);

//donwload pdf from original canvas
function downloadPDF(tiprep) {
  if(tiprep=="recurso"){
    var canvas = document.querySelector('#myChartRecursos');
  }
  else{
    var canvas = document.querySelector('#myChartUsuarios');
  }
  
  fillCanvasBackgroundWithColor(canvas, 'white');
	//creates image
	var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
  
	//creates PDF from img
	var doc = new jsPDF('landscape');
	doc.setFontSize(20);
  doc.setFillColor(255, 255,255,0);
	doc.text(15, 15, "Cool Chart");
  doc.setProperties({title: 'Some DocumentTitle'});
	doc.addImage(canvasImg, 'PNG', 0, 0 );
	doc.save('canvas.pdf');
}


$( document ).ready(function() {
  document.getElementById('fecha1').valueAsDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
  document.getElementById('fecha2').valueAsDate = new Date();  
  reportesinicio('usrtot');  
});
</script>



<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$module = Yii::$app->getModule("reportes");
$this->title = $module->params["t_recursos_mas_vistos"];
global $usuariosModel;
$usuariosModel = $searchModel;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\reportes\models\Usuarios */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="reporte-view">

    <h1><?php echo Html::encode($this->title); ?></h1> 
    <article>
        <p><strong><?php echo Yii::t("app", "Detalle del reporte:"); ?></strong>
            <?php echo Html::encode($module->params["m_recursos_mas_vistos"]); ?> </p>
    </article>
    <?php echo Alert::widget() ?>

    <?php $form = ActiveForm::begin(["method" => "GET", "action" => ["/reportes/reportes/recursos-mas-vistos"]]); ?>

    <?php
    $topField = $form->field($searchModel, 'top')->dropDownList([
                '10' => '10',
                '20' => '20',
                '30' => '30',
            ])
            ->label(Yii::t("app", "Seleccione el top"));

    $monthField = $form->field($searchModel, 'month')->dropDownList([
                '1' => 'Enero',
                '2' => 'Febrero',
                '3' => 'Marzo',
                '4' => 'Abril',
                '5' => 'Mayo',
                '6' => 'Junio',
                '7' => 'Julio',
                '8' => 'Agosto',
                '9' => 'Setiembre',
                '10' => 'Octubre',
                '11' => 'Noviembre',
                '12' => 'Diciembre',
                    ], ["prompt" => Yii::t("app", 'Todos los meses')])
            ->label(Yii::t("app", "Seleccione el mes"));






    $btnBack = Html::a(Yii::t("app", "Volver al listado de reportes"), ["/reportes/reportes/overview"], ["class" => "btn btn-default"]);
    $btnSubmit = Html::submitButton(Yii::t('app', 'Generar'), ['class' => 'btn btn-primary']);

    ?>  
    <?php
      $anio = date('Y');
      $data = [$anio => $anio, $anio - 1 => $anio - 1, $anio - 2 => $anio - 2, $anio - 3 => $anio - 3, $anio - 4 => $anio - 4, $anio - 5 => $anio - 5]; 

      echo $form->field($searchModel, 'year')->widget(Select2::classname(), [
      'data' => $data,
      'name' => 'sel-anio',
      'id' => 'sel-anio',
      'value' => [$anio], // initial value    
      'options' => ['placeholder' => 'Seleccione año ...'],
      ]);
      echo $monthField;
      echo $topField;
      echo $btnBack;
      echo $btnSubmit;  
    ?>  
    <?php ActiveForm::end(); ?>

    <?php
// Generate a bootstrap responsive striped table with row highlighted on hover
    echo "<br>";
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'columns' => [
            [
                'attribute' => '',
                'format' => 'raw',
                'label' => Yii::t("app", "Recurso"),
                'value' => function ($data) {
                    $recursoUrl = Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . $data["Slug"];
                    return Html::a($data["Nombre"], $recursoUrl, ['target' => '_blank']);
                },
                        'pageSummary' => function ($summary, $data, $widget) {
                    return "Total";
                }
                    ],
                    [
                        'attribute' => '',
                        'format' => 'html',
                        'label' => Yii::t("app", "Visitas"),
                        'value' => function ($data) {
                            return $data["total"];
                        },
                        'pageSummary' => function ($summary, $data, $widget) {

                            return $summary;
                        }
                    ],
                ],             
        'toolbar' => [
          '{export}',
          '{toggleData}',
        ],  
    // set export properties
        'export' => [
          'fontAwesome' => true
        ],    
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp;' . 'Recursos más vistos',
        ],    
        'responsive'=>true,
        'hover'=>true,
        'condensed' => true,
        'persistResize' => false,
        'exportContainer' => ['class' => 'btn-group-sm'],
        'exportConfig' => [
          GridView::HTML => ['label' => 'HTML'],
        // GridView::CSV => ['label' => 'CSV'],
        // GridView::TEXT  => ['label' => 'Text'],
          GridView::EXCEL => ['label' => 'Excel'],
        // GridView::PDF => ['label' => 'PDF'],
        // GridView::JSON => ['label' => 'JSON'],
        ],
        'showPageSummary' => true
            ]);
            ?>
</div>

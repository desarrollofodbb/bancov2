<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$module = Yii::$app->getModule("reportes");
$this->title = $module->params["t_recursos_por_usuario"];
global $usuariosModel;
$usuariosModel = $searchModel;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\reportes\models\Usuarios */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="reporte-view">

    <h1><?php echo Html::encode($this->title); ?></h1> 
    <article>
        <p><strong><?php echo Yii::t("app", "Detalle del reporte:"); ?></strong>
            <?php echo Html::encode($module->params["m_recursos_por_usuario"]); ?> </p>
    </article>
    <?php echo Alert::widget() ?>

    <?php $form = ActiveForm::begin(["method" => "GET", "action" => ["/reportes/reportes/recursos-por-usuario"]]); ?>

    <?php
    $monthField = $form->field($searchModel, 'month')->dropDownList([
                '1' => 'Enero',
                '2' => 'Febrero',
                '3' => 'Marzo',
                '4' => 'Abril',
                '5' => 'Mayo',
                '6' => 'Junio',
                '7' => 'Julio',
                '8' => 'Agosto',
                '9' => 'Setiembre',
                '10' => 'Octubre',
                '11' => 'Noviembre',
                '12' => 'Diciembre',
                    ], ["prompt" => Yii::t("app", 'Todos los meses')])
            ->label(Yii::t("app", "Seleccione el mes"));

    $statusField = $form->field($searchModel, 'status')->dropDownList([
                        '0' => 'Pendientes de validación',
                        '1' => 'Aprobados',
                        '2' => 'Rechazados',
                            ], ["prompt" => Yii::t("app", 'Todos los estados')])
                    ->label(Yii::t("app", "Filtrar por estado"))->hint("Hace referencia al estado del recurso");

    $userField = // Normal select with ActiveForm & model
            $form->field($searchModel, 'user')->widget(Select2::classname(), [
                'data' => $userList,
                'language' => 'es',
                'options' => ['placeholder' => Yii::t('app', 'Seleccione un usuario')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(Yii::t("app", "Filtrar por usuario"));


    $btnBack = Html::a(Yii::t("app", "Volver al listado de reportes"), ["/reportes/reportes/overview"], ["class" => "btn btn-default"]);
    $btnSubmit = Html::submitButton(Yii::t('app', 'Generar'), ['class' => 'btn btn-primary']);
 
    ?>  
    <?php
      $anio = date('Y');
      $data = [$anio => $anio, $anio - 1 => $anio - 1, $anio - 2 => $anio - 2, $anio - 3 => $anio - 3, $anio - 4 => $anio - 4, $anio - 5 => $anio - 5]; 

      echo $form->field($searchModel, 'year')->widget(Select2::classname(), [
      'data' => $data,
      'name' => 'sel-anio',
      'id' => 'sel-anio',
      'value' => [$anio], // initial value    
      'options' => ['placeholder' => 'Seleccione año ...'],
      ]);
      echo $monthField;
      echo $userField;
      echo $statusField;
      echo $btnBack;
      echo $btnSubmit;  
    ?>  
    <?php ActiveForm::end(); ?>

    <?php
// Generate a bootstrap responsive striped table with row highlighted on hover
    echo "<br>";
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'username',
                'label' => Yii::t("app", "Usuario"),
                'value' => function ($data) {
                    return $data["username"];
                },
                'pageSummary' => function ($summary, $data, $widget) {

                    return "Total";
                }
            ],
            [
                'attribute' => 'SUM(CASE WHEN [TRecursos].[Estado] = 0 THEN 1 ELSE 0 END)',
                'format' => 'html',
                'label' => Yii::t("app", "Pendientes de validación"),
                'visible' => ($usuariosModel->status == '0' || $usuariosModel->status == ''),
                'value' => function ($data) {
                    global $usuariosModel;
                    return $data["pending"];
                },
                'pageSummary' => function ($summary, $data, $widget) {

                    return $summary;
                }
            ],
            [
                'attribute' => 'SUM(CASE WHEN [TRecursos].[Estado] = 1 THEN 1 ELSE 0 END)',
                'format' => 'html',
                'label' => Yii::t("app", "Aprobados"),
                'visible' => ($usuariosModel->status == '1' || $usuariosModel->status == ''),
                'value' => function ($data) {
                    global $usuariosModel;
                    return $data["approved"];
                },
                'pageSummary' => function ($summary, $data, $widget) {

                    return $summary;
                }
            ],
            [
                'attribute' => 'SUM(CASE WHEN [TRecursos].[Estado] = 2 THEN 1 ELSE 0 END)',
                'format' => 'html',
                'label' => Yii::t("app", "Rechazados"),
                'visible' => ($usuariosModel->status == '2' || $usuariosModel->status == ''),
                'value' => function ($data) {
                    global $usuariosModel;
                    return $data["rejected"];
                },
                'pageSummary' => function ($summary, $data, $widget) {

                    return $summary;
                }
            ],
            [
                'attribute' => 'COUNT(*)',
                'format' => 'html',
                'label' => Yii::t("app", "Total"),
                'value' => function ($data) {
                    global $usuariosModel;
                    return $data["total"];
                },
                'pageSummary' => function ($summary, $data, $widget) {

                    return $summary;
                }
            ],
        ],             
        'toolbar' => [
          '{export}',
          '{toggleData}',
        ],  
    // set export properties
        'export' => [
          'fontAwesome' => true
        ],    
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-hand-right"></i>&nbsp;&nbsp;' . 'Recursos por usuario',
        ],    
        'responsive'=>true,
        'hover'=>true,
        'condensed' => true,
        'persistResize' => false,
        'exportContainer' => ['class' => 'btn-group-sm'],
        'exportConfig' => [
          GridView::HTML => ['label' => 'HTML'],
        // GridView::CSV => ['label' => 'CSV'],
        // GridView::TEXT  => ['label' => 'Text'],
          GridView::EXCEL => ['label' => 'Excel'],
        // GridView::PDF => ['label' => 'PDF'],
        // GridView::JSON => ['label' => 'JSON'],
        ],
        'showPageSummary' => true
    ]);
    ?>
</div>

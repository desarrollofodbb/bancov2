<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use yii\helpers\ArrayHelper;
use common\modules\recursos\models\RecursosAdvancedSearch;

$VdC = new ValoresDeCaracteristicas();
$module = Yii::$app->getModule("reportes");
$this->title = $module->params["t_recursos_mas_vistos"];
global $usuariosModel;
$usuariosModel = $searchModel;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\reportes\models\Usuarios */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>

<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<div class="reporte-view">

    <h1>Recursos compartidos por usuarios</h1> 
    <article>
        <p><strong><?php echo Yii::t("app", "Detalle del reporte:"); ?></strong>
            Detalle de recursos </p>
    </article>
    <?php echo Alert::widget() ?>

    <?php $form = ActiveForm::begin(["method" => "GET", "action" => ["/reportes/reportes/recursos-mas-compartidos"]]); ?>

    <?php
    $topField = $form->field($searchModel, 'top')->dropDownList([
                '10' => '10',
                '20' => '20',
                '30' => '30',
            ])
            ->label(Yii::t("app", "Seleccione el top"));

    $monthField = $form->field($searchModel, 'month')->dropDownList([
                '1' => 'Enero',
                '2' => 'Febrero',
                '3' => 'Marzo',
                '4' => 'Abril',
                '5' => 'Mayo',
                '6' => 'Junio',
                '7' => 'Julio',
                '8' => 'Agosto',
                '9' => 'Setiembre',
                '10' => 'Octubre',
                '11' => 'Noviembre',
                '12' => 'Diciembre',
                    ], ["prompt" => Yii::t("app", 'Todos los meses')])
            ->label(Yii::t("app", "Seleccione el mes"));

        $VdC = new ValoresDeCaracteristicas();
        $model = new RecursosAdvancedSearch();
        $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(8);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                // echo "<h3>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                $materias = $form->field($searchModel, 'materia')->widget(Select2::classname(),[
                          // 'model' => $model,
                          'name' => 'sel-materia',
                          'id' => 'sel-materia',                          
                          // 'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                            'id' => '8',
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'allowClear' => true,
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ]        
                        ]);
              endforeach; 

        $caracteristicasDeRecursos = $VdC->getValoresPorCaracteristicas(10);
              foreach ($caracteristicasDeRecursos as $caracteristica_key => $valoresDeCaracteristica):
                $valor_key = explode("-", $caracteristica_key);               
                $caracteristica_tipoSeleccion = $valor_key[0] == "0" ? false : true;
                $caracteristica_field = $valor_key[1];
                $caracteristica_nombre = $valor_key[2];
                // echo "<h3>" . $caracteristica_nombre . "</h3>";
                $placeholder = "Seleccione " . strtolower($caracteristica_nombre);
                $palabras = $form->field($searchModel, 'palabras')->widget(Select2::classname(),[
                          // 'model' => $model,
                          'name' => 'sel-palabras',
                          'id' => 'sel-palabras',                          
                          // 'attribute' => $caracteristica_field,
                          'language' => 'es',
                          'showToggleAll' => false,
                          'data' => ArrayHelper::map($valoresDeCaracteristica, "idValoresDeCaracteristicas", "Nombre"),
                          'options' => [
                            'id' => '10',
                            'multiple' => false,
                            'class' => 'form-control; border-radius:60px;',
                            'placeholder' => $placeholder,
                          ],
                          'pluginOptions' => [
                            'tokenSeparators' => [',', ' '],
                            'allowClear' => true,
                            'maximumInputLength' => 10,
                            'maximumSelectionLength' => 7,
                           ]        
                        ]);
              endforeach; 

    $btnBack = Html::a(Yii::t("app", "Volver al listado de reportes"), ["/reportes/reportes/overview"], ["class" => "btn btn-default"]);
    $btnSubmit = Html::submitButton(Yii::t('app', 'Generar'), ['class' => 'btn btn-primary']);

    ?>  
    <?php
      $anio = date('Y');
      $data = [$anio => $anio, $anio - 1 => $anio - 1, $anio - 2 => $anio - 2, $anio - 3 => $anio - 3, $anio - 4 => $anio - 4, $anio - 5 => $anio - 5]; 

      echo $form->field($searchModel, 'year')->widget(Select2::classname(), [
      'data' => $data,
      'name' => 'sel-anio',
      'id' => 'sel-anio',
      'value' => [$anio], // initial value    
      'options' => ['placeholder' => 'Seleccione año ...'],
      ]);



      echo $monthField;
      echo $topField;
      ?>
<h3>Filtro adicional</h3>       
      <?php
      $data = ['0' => 'Sin filtro adicional', '1' => 'Por materia', '2' => 'Por palabra clave']; 
      echo $form->field($searchModel, 'filtro')->widget(Select2::classname(), [
      'name' => 'sel-filadi',
      'id' => 'sel-filadi',
      'value' => ['0'], // initial value
      'data' => $data,
      'options' => [
        'placeholder' => 'Seleccione filtro adicional ...',
        'id' => 'sel-filadi',
        'options' => [
          
        ]
      ],
      'pluginEvents' => [
        "change" => "function() { // function to make ajax call here 
          showfiltro(this.value);
       // document.getElementById('zoom-but3').click();
      }",
      ],      
      ]);      
?>
<div id="sel1">
<?php      
      echo $materias;
?>
</div>
<div id="sel2">
<?php
      echo $palabras;
?>
</div>
<?php
      echo $btnBack;
      echo $btnSubmit;  
?>  
    <?php ActiveForm::end(); ?>

    <?php
// Generate a bootstrap responsive striped table with row highlighted on hover
    echo "<br>";
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'columns' => [
            [
                'attribute' => '',
                'format' => 'raw',
                'label' => Yii::t("app", "Recurso"),
                'value' => function ($data) {
                    //$recursoUrl = Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . $data["Slug"];
                    return $data["Nombre"];
                },
                        'pageSummary' => function ($summary, $data, $widget) {
                    return "Total";
                }
                    ],
                    [
                        'attribute' => '',
                        'format' => 'html',
                        'label' => Yii::t("app", "Compartidos"),
                        'value' => function ($data) {
                            return $data["total"];
                        },
                        'pageSummary' => function ($summary, $data, $widget) {

                            return $summary;
                        }
                    ],
                ],             
        'toolbar' => [
          '{export}',
          '{toggleData}',
        ],  
    // set export properties
        'export' => [
          'fontAwesome' => true
        ],    
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp;' . 'Cantidad veces compartidos',
        ],    
        'responsive'=>true,
        'hover'=>true,
        'condensed' => true,
        'persistResize' => false,
        'exportContainer' => ['class' => 'btn-group-sm'],
        'exportConfig' => [
          GridView::HTML => ['label' => 'HTML'],
        // GridView::CSV => ['label' => 'CSV'],
        // GridView::TEXT  => ['label' => 'Text'],
          GridView::EXCEL => ['label' => 'Excel'],
        // GridView::PDF => ['label' => 'PDF'],
        // GridView::JSON => ['label' => 'JSON'],
        ],
        'showPageSummary' => true
            ]);
            ?>
</div>
<script type="text/javascript">

    function showfiltro(filtro){

      if(filtro==0){
        $('#sel1').hide();
        $('#sel2').hide();
      }
      if(filtro==1){
        $('#sel1').show();
        $('#sel2').hide();        
      }      
      if(filtro==2){
        $('#sel1').hide();
        $('#sel2').show();        
      }      
    }
$( document ).ready(function() {
  elemento = document.getElementById("sel-filadi");
  showfiltro(elemento.value);  
});    
</script>
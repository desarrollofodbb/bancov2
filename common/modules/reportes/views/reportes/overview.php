<?php

use yii\helpers\Url;

$module = Yii::$app->getModule("reportes");
$this->title = Yii::t("app", "Reportes");
?>
<div class="reportes-overview">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo Yii::t("app", "Reportes de usuarios"); ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row"> 
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_sesion_usuarios"]; ?></h3>

                            <p>
                                <?php
                                echo $module->params["m_sesion_usuarios"];
                                ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/log-in-usuarios"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_usuarios_que_publican_recursos"]; ?></h3>

                            <p>
                                <?php
                                echo $module->params["m_usuarios_que_publican_recursos"];
                                ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/usuarios-que-publican-recursos"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div> 
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3>Listado de usuarios</h3>

                            <p>
                            Usuarios adultos y menores en la base de datos
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/listado-usuarios"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>                 
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo Yii::t("app", "Reportes de búsquedas"); ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row"> 
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_busquedas_por_mes"]; ?></h3>

                            <p>
                                <?php echo $module->params["m_busquedas_por_mes"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/busquedas-por-mes"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_filtros_de_busquedas"]; ?></h3>

                            <p>
                                <?php echo $module->params["m_filtros_de_busquedas"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/filtros-de-busquedas"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_cantidad_de_palabras_por_mes"]; ?></h3>

                            <p>
                                <?php echo $module->params["m_cantidad_de_palabras_por_mes"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/cantidad-de-palabras-por-mes"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div> 
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo Yii::t("app", "Reportes de recursos"); ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row"> 
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_recursos_publicados_por_mes"]; ?></h3>

                            <p>
                                <?php echo $module->params["m_recursos_publicados_por_mes"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/recursos-publicados-por-mes"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_recursos_mas_vistos"]; ?></h3>

                            <p>
                                <?php
                                echo $module->params["m_recursos_mas_vistos"];
                                ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/recursos-mas-vistos"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div> 
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3>Cantidad recursos vistos por usuario</h3>

                            <p>
                                Lista de usuarios y la cantidad de recursos vistos
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/usuarios-mas-recursos-vistos"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>                 
            </div>
            <div class="row"> 
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_recursos_mas_descargados"]; ?></h3>

                            <p>
                                <?php echo $module->params["m_recursos_mas_descargados"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/recursos-mas-descargados"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3>Recursos más compartidos</h3>

                            <p>
                                Los recursos que más se comparten
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/recursos-mas-compartidos"]); ?>" class="small-box-footer" style = "background-color: #00cc99" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>                
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_recursos_mas_votados"]; ?></h3>

                            <p>
                                <?php echo $module->params["m_recursos_mas_votados"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/recursos-mas-votados"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6"> 
                    <!-- small box -->
                    <div class="small-box bg-reportes-grey">
                        <div class="inner">
                            <h3><?php echo $module->params["t_recursos_por_usuario"]; ?></h3>
                            <p>
                                <?php echo $module->params["m_recursos_por_usuario"]; ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo Url::to(["/reportes/reportes/recursos-por-usuario"]); ?>" class="small-box-footer" style = "background-color: #00cc99">
                            <?php echo Yii::t("app", "Generar reporte"); ?> <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div> 
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</div>

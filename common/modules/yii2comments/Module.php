<?php

namespace common\modules\yii2comments;

use Yii;

/**
 * Class Module
 *
 * @package common\modules\yii2comments
 */
class Module extends \yii\base\Module {

    /**
     * @var string module name
     */
    public static $name = 'comment';

    /**
     * @var string the class name of the [[identity]] object
     */
    public $userIdentityClass;

    /**
     * @var string the class name of the comment model object, by default its common\modules\yii2comments\models\CommentModel
     */
    public $commentModelClass = 'common\modules\yii2comments\models\CommentModel';

    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'common\modules\yii2comments\controllers';

    /**
     * {@inheritdoc}
     */
    public function init() {
        parent::init();

        if ($this->userIdentityClass === null) {
            $this->userIdentityClass = Yii::$app->getUser()->identityClass;
        }
    }

}

<?php

namespace common\modules\yii2comments\events;

use yii\base\Event;
use common\modules\yii2comments\models\CommentModel;

/**
 * Class CommentEvent
 *
 * @package common\modules\yii2comments\events
 */
class CommentEvent extends Event
{
    /**
     * @var CommentModel
     */
    private $_commentModel;

    /**
     * @return CommentModel
     */
    public function getCommentModel()
    {
        return $this->_commentModel;
    }

    /**
     * @param CommentModel $commentModel
     */
    public function setCommentModel(CommentModel $commentModel)
    {
        $this->_commentModel = $commentModel;
    }
}

<?php

namespace common\modules\yii2comments;

use yii\web\AssetBundle;

/**
 * Class CommentAsset
 *
 * @package common\modules\yii2comments
 */
class CommentAsset extends AssetBundle {

    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@common/modules/yii2comments/assets';

    /**
     * {@inheritdoc}
     */
    public $js = [
        'js/comment.js',
    ];

    /**
     * {@inheritdoc}
     */
    public $css = [
        'css/comment.css',
    ];

    /**
     * {@inheritdoc}
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
    ];

}

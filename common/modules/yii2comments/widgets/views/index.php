<?php

use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $commentModel \yii2comments\comments\models\CommentModel */
/* @var $maxLevel null|integer comments max level */
/* @var $encryptedEntity string */
/* @var $pjaxContainerId string */
/* @var $formId string comment form id */
/* @var $commentDataProvider \yii\data\ArrayDataProvider */
/* @var $listViewConfig array */
/* @var $commentWrapperId string */
?>
<div class="comment-wrapper" id="<?php echo $commentWrapperId; ?>">
    <?php Pjax::begin(['enablePushState' => false, 'timeout' => 20000, 'id' => $pjaxContainerId]); ?>
    <div class="comments row">
        <div class="col-md-12 col-sm-12">
<!--             <div class="title-block clearfix"> 
                <h4 class="top-space underlined-title">
                    <?php echo Yii::t('yii2mod.comments', 'Comentarios ({0})', $commentModel->getCommentsCount()); ?>
                </h4>
                <div class="title-separator"></div>
            </div> -->

            <?php if (!Yii::$app->user->isGuest) : ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <?php
                        echo $this->render('_form', [
                            'commentModel' => $commentModel,
                            'formId' => $formId,
                            'encryptedEntity' => $encryptedEntity,
                        ]);
                        ?>
                    </div> 
                </div>
                <br>
            <?php endif; ?>
            <?php
            echo ListView::widget(ArrayHelper::merge(
                            [
                        'dataProvider' => $commentDataProvider,
                        'layout' => "{items}\n{pager}",
                        'itemView' => '_list',
                        'viewParams' => [
                            'maxLevel' => $maxLevel,
                        ],
                        'options' => [
                            'tag' => 'ol',
                            'class' => 'comments-list',
                        ],
                        'itemOptions' => [
                            'tag' => false,
                        ],
                            ], $listViewConfig
            ));
            ?> 
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>

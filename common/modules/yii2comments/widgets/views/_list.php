<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \yii2comments\comments\models\CommentModel */
/* @var $maxLevel null|integer comments max level */
?>
<li class="comment" id="comment-<?php echo $model->id; ?>">
    <div class="" data-comment-content-id="<?php echo $model->id ?>">
        <div class="row">
      <div class="col-xs-2">    
        <div class="comment-author-avatar">
            <?php echo Html::img($model->getAvatar()); ?>
        </div>        
      </div>
        <div class="col-xs-7">
            <div class="comment-author-name">
                <span style="font-weight: bold"><?php echo $model->getAuthorName(); ?></span>
                <?php echo Html::a($model->getPostedDate(), $model->getAnchorUrl(), ['class' => 'commeet-date']); ?>
            </div>
            <div class="comment-body">
                <?php echo $model->getContent(); ?>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="comment-action-buttons w3-center">
                <?php if (Yii::$app->getUser()->can('/comment/default/delete', ["id" => $model->id])) : ?>
                    <?php echo Html::a(Yii::t('yii2mod.comments', 'Eliminar'), '#', ['data' => ['action' => 'delete', 'url' => Url::to(['/comment/default/delete', 'id' => $model->id]), 'comment-id' => $model->id]]); ?>
                <?php endif; ?>
                <?php if (!Yii::$app->user->isGuest && ($model->level < $maxLevel || is_null($maxLevel)) && Yii::$app->getUser()->can('/comment/default/create')) : ?>
                    <?php echo Html::a("<span class='glyphicon glyphicon-share-alt'></span> " . Yii::t('yii2mod.comments', 'Responder'), '#', ['class' => 'comment-reply', 'data' => ['action' => 'reply', 'comment-id' => $model->id]]); ?>
                <?php endif; ?>
            </div>                
        </div>
        </div>
    </div>
    <hr style="color:#666666">
</li>
<?php if ($model->hasChildren()) : ?>
    <ul class="children">
        <?php foreach ($model->getChildren() as $children) : ?>
            <li class="comment" id="comment-<?php echo $children->id; ?>">
                <?php echo $this->render('_list', ['model' => $children, 'maxLevel' => $maxLevel]) ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

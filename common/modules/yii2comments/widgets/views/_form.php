<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this \yii\web\View */
/* @var $commentModel \yii2comments\comments\models\CommentModel */
/* @var $encryptedEntity string */
/* @var $formId string comment form id */
?>
<div class="comment-form-container">
    <?php
    $model = new User();
    $model = $model->findIdentity(Yii::$app->user->identity->id);  
    $form = ActiveForm::begin([
                'options' => [
                    'id' => $formId,
                    'class' => 'comment-box',
                ],
                'action' => Url::to(['/comment/default/create', 'entity' => $encryptedEntity]),
                'validateOnChange' => false,
                'validateOnBlur' => false,
    ]);
    ?>
    <div class="row">  
      <div class="col-xs-2">   
        <div class="comment-author-avatar">
            <?php echo Html::img($model->getAvatar()); ?>
        </div>        
      </div>
<!--     <div class="comment-details">   -->
<!--       <div class="w3-row">   -->
      <div class="col-xs-7">
    <?php echo $form->field($commentModel, 'content', ['template' => '{input}{error}'])->textarea(['placeholder' => Yii::t('yii2mod.comments', 'Añadir un comentario...'), 'rows' => 2, 'style' => 'border:none;font-size: 15px;','data' => ['comment' => 'content']]) ?>
    <?php echo $form->field($commentModel, 'parentId', ['template' => '{input}'])->hiddenInput(['data' => ['comment' => 'parent-id']]); ?>
      </div>  
      <div class="col-xs-3">    
            <?php echo Html::a(Yii::t('yii2mod.comments', 'Haga clic aquí para cancelar la respuesta.'), '#', ['id' => 'cancel-reply', 'class' => 'pull-right', 'data' => ['action' => 'cancel-reply']]); ?>
            <?php echo Html::submitButton(Yii::t('yii2mod.comments', 'Enviar'), ['class' => 'w3-button_green comment-submit w3-round-xxlarge w3-border-0','style' => 'font-size:15px']); ?>        
      </div>    
<!--   </div> -->
<!--     </div> -->

    <?php $form->end(); ?>
    </div>
    <hr style="color:#666666">
    <div class="clearfix"></div>
</div>

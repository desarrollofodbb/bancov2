<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\preguntas_frecuentes\models\PreguntasFrecuentesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Preguntas Frecuentes');

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
?>
<div class="preguntas_frecuentes-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a($module->params["t_add_new"], ['create'], ['class' => 'btn btn-success']); ?>
    </p>

    <?php
    echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'Titulo',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
//                    'view' => function ($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Yii::$app->params['urlFront'] . $model->Slug, ['target' => '_blank', 'title' => Yii::t('yii', 'View'),]);
//                    },
                    'delete' => function ($url, $model, $key) {
                        $data_confirm = Yii::t('app', 'Está seguro que desea eliminar la pregunta frecuente?');
                        $text = '<span class="glyphicon glyphicon-trash"></span>';
                        $toroute = Url::toRoute(['/preguntas-frecuentes/preguntas-frecuentes/delete', 'id' => $key]);
                        $aria_label = Yii::t('app', 'Eliminar');
                        return Html::a($text, $toroute, ['data-method' => "post", "data-confirm" => $data_confirm, "data-pjax" => "0", "aria-label" => $aria_label, "title" => $aria_label]);
                    },
                        ]
                    ]
                ],
            ]);
            ?>
</div>

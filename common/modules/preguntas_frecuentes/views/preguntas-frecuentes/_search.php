<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\preguntas_frecuentes\models\PreguntasFrecuentesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="preguntas_frecuentes-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>

    <?php echo $form->field($model, 'idPreguntasFrecuentes') ?>

    <?php echo $form->field($model, 'Titulo') ?>

    <?php echo $form->field($model, 'Slug') ?>

    <?php echo $form->field($model, 'Descripcion') ?>

    <?php // echo $form->field($model, 'idEstados')  ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

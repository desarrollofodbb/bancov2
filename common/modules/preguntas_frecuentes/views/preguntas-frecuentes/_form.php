<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use pendalf89\filemanager\widgets\TinyMCE;

/* @var $this yii\web\View */
/* @var $model common\modules\preguntas_frecuentes\models\PreguntasFrecuentes */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
?>

<div class="preguntas_frecuentes-form">

    <?php if (!$model->isNewRecord): ?>
        <p>
            <?php echo Html::a($module->params["t_add_new"], ['create'], ['class' => 'btn btn-success']); ?>
        </p>
    <?php endif; ?>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col col-md-12">
            <?php echo $form->field($model, 'Titulo')->textInput() ?>

            <?=
            $form->field($model, 'Descripcion')->widget(TinyMCE::className(), [
                'clientOptions' => [
                    'language' => 'es',
                    'menubar' => false,
                    'height' => 200,
                    'image_dimensions' => false,
                    'plugins' => [
                        'advlist autolink lists link charmap print preview anchor searchreplace visualblocks code contextmenu table',
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-12">
            <div class="form-inline">
                <div class="form-group">
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <div class="form-group">
                    <?php echo Html::a(Yii::t('app', 'Cancelar'), ['/preguntas-frecuentes/preguntas-frecuentes'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

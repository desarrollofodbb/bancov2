<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\preguntas_frecuentes\models\PreguntasFrecuentes */

$this->title = $model->idPreguntasFrecuentes;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'PreguntasFrecuentes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preguntas_frecuentes-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <p>
        <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idPreguntasFrecuentes], ['class' => 'btn btn-primary']) ?>
        <?php
        echo
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idPreguntasFrecuentes], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    echo
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPreguntasFrecuentes',
            'Titulo',
            'Slug',
            'Descripcion',
            'FechaDeCreacion',
            'FechaDeModificacion',
            'Autor',
        ],
    ])
    ?>

</div>

<?php

namespace common\modules\preguntas_frecuentes;

/**
 * preguntas_frecuentes module definition class
 */
class PreguntasFrecuentes extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\preguntas_frecuentes\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

}

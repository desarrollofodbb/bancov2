<?php

namespace common\modules\preguntas_frecuentes\controllers;

use Yii;
use common\modules\preguntas_frecuentes\models\PreguntasFrecuentes;
use common\modules\preguntas_frecuentes\models\PreguntasFrecuentesSearch;
use common\models\Configuraciones;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PreguntasFrecuentesController implements the CRUD actions for PreguntasFrecuentes model.
 */
class PreguntasFrecuentesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PreguntasFrecuentes models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PreguntasFrecuentesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings());

        $this->view->params['configuraciones_generales'] = $configuraciones;

        $requested_path = \Yii::getAlias('@webroot');

        if (strpos($requested_path, "frontend/web")) {
            return $this->render('@frontend/views/preguntas-frecuentes/index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PreguntasFrecuentes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//        $this->layout = "@frontend/views/layouts/main.php";

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PreguntasFrecuentes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new PreguntasFrecuentes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash("success", "La pregunta frecuente ha sido agregada correctamente.");
            return $this->redirect(['update', 'id' => $model->idPreguntasFrecuentes]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PreguntasFrecuentes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', yii::t('app', 'La pregunta frecuente se actualizó correctamente'));
            return $this->redirect(['update', 'id' => $model->idPreguntasFrecuentes]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PreguntasFrecuentes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PreguntasFrecuentes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PreguntasFrecuentes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PreguntasFrecuentes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

    /**
     * Displays a single Status model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSlug($slug) {
        $model = PreguntasFrecuentes::find()->where(['Slug' => $slug])->one();
        if (!is_null($model)) {
            return $this->render('@frontend/views/preguntas_frecuentes/view', ['model' => $model,]);
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

}

<?php

namespace common\modules\preguntas_frecuentes\controllers;

use yii\web\Controller;

/**
 * Default controller for the `preguntas_frecuentes` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

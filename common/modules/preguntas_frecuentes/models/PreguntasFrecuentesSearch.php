<?php

namespace common\modules\preguntas_frecuentes\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\preguntas_frecuentes\models\PreguntasFrecuentes;

/**
 * PreguntasFrecuentesSearch represents the model behind the search form of `common\modules\preguntas_frecuentes\models\PreguntasFrecuentes`.
 */
class PreguntasFrecuentesSearch extends PreguntasFrecuentes {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Titulo', 'Descripcion', 'FechaDeCreacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PreguntasFrecuentes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idPreguntasFrecuentes' => $this->idPreguntasFrecuentes,
            'FechaDeCreacion' => $this->FechaDeCreacion,
            'Autor' => $this->Autor
        ]);

        $query->andFilterWhere(['COLLATE Latin1_General_CI_AI like', 'Titulo', "%" . $this->Titulo . "%"]);
        $query->orderBy("FechaDeCreacion DESC");
        return $dataProvider;
    }

}

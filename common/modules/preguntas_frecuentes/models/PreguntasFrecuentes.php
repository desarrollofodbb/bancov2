<?php

namespace common\modules\preguntas_frecuentes\models;

use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "TPreguntasFrecuentes".
 *
 * @property integer $idPreguntasFrecuentes
 * @property string $Titulo
 * @property string $Slug
 * @property string $Descripcion
 * @property integer $FechaDeCreacion
 * @property integer $FechaDeModificacion
 * @property string $Autor
 * @property string $ActualizadoPor
 *
 * @property user $autor
 * @property user $actualizadoPor
 */
class PreguntasFrecuentes extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%TPreguntasFrecuentes}}';
    }

    public function behaviors() {
        return [
//            SluggableBehavior automatically fills the specified attribute with a value that can be used a slug in a URL.
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'Titulo',
                'slugAttribute' => 'Slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
//            TimestampBehavior automatically fills the specified attributes with the current timestamp.
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'FechaDeCreacion',
                'updatedAtAttribute' => 'FechaDeModificacion',
            ],
//            BlameableBehavior automatically fills the specified attributes with the current user ID.
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'Autor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Titulo', 'Descripcion'], 'required'],
            [['Titulo', 'Descripcion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idPreguntasFrecuentes' => Yii::t('app', 'Id'),
            'Titulo' => Yii::t('app', 'Título'),
            'Slug' => Yii::t('app', 'Slug'),
            'Descripcion' => Yii::t('app', 'Descripción'),
            'FechaDeCreacion' => Yii::t('app', 'Fecha de creación'),
            'FechaDeModificacion' => Yii::t('app', 'Fecha de modificación'),
            'Autor' => Yii::t('app', 'Author'),
            'ActualizadoPor' => Yii::t('app', 'Actualizado por')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutorRelated() {
        return $this->hasOne(User::className(), ['id' => 'Autor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualizadoPorRelated() {
        return $this->hasOne(User::className(), ['id' => 'ActualizadoPor']);
    }

    /**
     * Lists all Insteresting data models.
     * @return ActiveDataProvider
     */
    public static function getFrequentQuestions() {

        $model = PreguntasFrecuentes::find()->orderBy('Titulo ASC');

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => Yii::$app->params["preguntasFrecuentesPerPage"],
            ],
        ]);

        return $dataProvider;
    }

}

<?php

return [
//    'user.passwordResetTokenExpire' => 3600,//esto por si se activa un reset  para el proyecto local
    'contactFormEmail' => 'prueba01@fod.ac.cr',    
    'validatorEmail' => 'prueba01@fod.ac.cr', //correo general para la validación o aprobación de los recursos
    'newResourceNotificationSubject' => Yii::t('app', 'Nuevo recurso publicado y enviado a proceso de calidad.'),
    'approvedNotificationSubject' => Yii::t('app', 'Su recurso ha sido aprobado'),
    'rejectedNotificationSubject' => Yii::t('app', 'Su recurso no ha pasado el proceso de calidad.'),
    'complaintNotificationSubject' => Yii::t('app', 'Un usuario ha denunciado un recurso'), //subject para las denuncias
    'recommendationNotificationSubject' => Yii::t('app', 'Nueva recomendación agregada a su recurso'), //subject para las denuncias
    'adminEmail' => 'prueba01@fod.ac.cr', //correo general para denuncias
    'administradorEmail' => 'murillex1@hotmail.com', //correo al que llegaran copia de las autorizaciones y rechazos de Recursos
    'supportEmail' => 'daniel.cerdas@fod.ac.cr', //El setFrom de los correos
    'developerEmail' => 'prueba01@fod.ac.cr', //correo para el reporte de bugs
];

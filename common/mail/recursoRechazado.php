<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="nuevo-recurso">
    <p>Hola <?php echo Html::encode($recurso->autor->nombre); ?>, su recurso "<?php echo Html::encode($recurso->Nombre); ?>" no ha pasado el proceso de calidad</p>

<p> Verifique su recurso aquí </p>
        <a target="_blank" href="<?php echo Url::to(Yii::$app->params["urlFront"] . $linkrecurso); ?>">
            Link de su Recurso
        </a>
    <p>Para más detalles pongase en contacto con el administrador del sistema. O escríbanos  por medio de nuestro
        <a target="_blank" href="<?php echo Url::to(Yii::$app->params["urlFront"] . "paginas/paginas/contactenos"); ?>">
            Formulario de contacto
        </a>
    </p> 
</div>

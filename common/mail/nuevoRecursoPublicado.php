<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="nuevo-recurso">
    <p>Hola, un nuevo recurso ha sido publicado por el usuario <?php echo Html::encode($recurso->autor->username); ?>,</p>

    <p>Al iniciar sesión en el sistema con el rol de validador, podrá ver el recurso haciendo click</p>
    <a target="_blank" href="<?php echo Url::to(Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . Html::encode($recurso->Slug)); ?>">
        Aqu&iacute;
    </a>
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="nuevo-recurso">
    <p>Hola <?php echo Yii::$app->name ?>, el usuario "<?php echo Html::encode($denuncia->usuario->username); ?>" ha denunciado el recurso 
        "<?php echo Html::a($denuncia->recurso->Nombre, Url::to(Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . Html::encode($denuncia->recurso->Slug))); ?>"
    </p>
    <p><strong>Motivo: </strong> <?php echo $denuncia->Motivo; ?></p>


    <p><strong>Nota: </strong><?php echo "Para procesar la denuncia se debe dirigir a la sección de denuncias en la parte administrativa del sistema"; ?>
        <a target="_blank" href="<?php echo Url::to(Yii::$app->params["urlBack"] . "recursos/denuncias/index"); ?>">
            Haciendo click Aqu&iacute;
        </a>
    </p> 
</div>

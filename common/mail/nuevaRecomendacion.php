<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="nuevo-recurso">
    <p>Hola <?php echo $recurso->autor->username; ?>, el usuario "<?php echo Html::encode($recomendacion->usuario->username); ?>" ha publicado la siguiente recomendación: 
        "<?php echo $recomendacion->Titulo; ?>" para el recurso "<?php echo $recurso->Nombre; ?>"
    </p>

    <p><strong>Nota: </strong><?php echo "Para aprobar o rechazar las recomendaciones agregadas al recurso haga click"; ?>
        <a target="_blank" href="<?php echo Url::to(Yii::$app->params["urlFront"] . "recursos/recomendaciones/aprobar-recomendaciones?recurso={$recurso->Slug}"); ?>">
            Aqu&iacute;
        </a>
    </p> 
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="nuevo-recurso">
    <p>Hola <?php echo Html::encode($recurso->autor->nombre); ?>, su recurso "<?php echo Html::encode($recurso->Nombre); ?>" ha sido aprobado</p>

    <p>Podrá ver el recurso haciendo click</p>
    <a target="_blank" href="<?php echo Url::to(Yii::$app->params["urlFront"] . "recursos/recursos/ver-recurso?recurso=" . Html::encode($recurso->Slug)); ?>">
        Aqu&iacute;
    </a>
</div>

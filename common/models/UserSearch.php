<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;

/**
 * User represents the model behind the search form about common\models\User`.
 */
class UserSearch extends User {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[
            'id',
            'status',
            'created_at',
            'updated_at'
                ],
                'integer',
            ],
            [
                [
                    'username',
                    'auth_key',
                    'password_hash',
                    'password_reset_token',
                    'email'
                ],
                'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {



        $query = User::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if ($this->username) {//search by username in the API
            $this->getInfoPersonaBy("Username", $this->username);
        }
        if ($this->email) {
            $this->getInfoPersonaBy("Correo", $this->email);
        }

        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
//            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['COLLATE Latin1_General_CI_AI like', 'username', "%" . $this->username . "%"])
                ->andFilterWhere(['!=', 'username', "igd_admin"])//no mostrar el usuario igd_admin para evitar modificarlo por error
                ->andFilterWhere(['like', 'auth_key', $this->auth_key])
                ->andFilterWhere(['like', 'password_hash', $this->password_hash])
                ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
                ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

    /**
     * Get the info and compare with the local database, if the user does not exist
     * then the user is created or updated
     * @param type $tipo it can be, username or email 
     * @param type $dato the username or email 
     */
    public function getInfoPersonaBy($tipo, $dato) {
        $estado = false;
        $personaIsActive = false;

        //1- Get del servicio 
        $getInfoPersonaBy = Yii::$app->params["api"] .
                'persona/getInfoPersonaBy/' . $tipo
                . '/' . $dato;


        $jsonResponse = Json::decode(@file_get_contents($getInfoPersonaBy));


        //2- Si el servicio es true validar si el usuario esta activo 
        if ($jsonResponse) {
            if (isset($jsonResponse["estado"]) && ($jsonResponse["estado"])) {
                $estado = true;
            }
            if (isset($jsonResponse["persona"]["estado"]) && ($jsonResponse["persona"]["estado"])) {
                $personaIsActive = true;
            }
        }

        if ($estado) {
            $user = User::findOne(["username" => $jsonResponse["usuario"]["username"]]);
            $userByEmail = User::findOne(["email" => $jsonResponse["persona"]["CorreoElectronico"]]);

            if (!$user && $userByEmail) {
//si por usuario no existe pero por correo si quiere decir que hay dos usuarios con el mismo correo
                Yii::$app->session->setFlash("warning", Yii::t("app", "El usuario {usuario} ya tiene asignado el correo {correo}, no puede existir más de un usuario con el mismo correo", [
                            "usuario" => $userByEmail->username,
                            "correo" => $jsonResponse["persona"]["CorreoElectronico"]
                ]));
                return; //no hago nada m'as
            }
        }

        //        2.1 Si la persona esta inactiva actualizar en la base de datos local como inactiva
        if ($estado && $user && !$personaIsActive) {
//            $user->status = User::STATUS_DELETED;
//            $user->save();
        }

        if ($estado && $user) {//when user is created or not always the data is updated
            $user->foto = $jsonResponse["persona"]["Foto"];
            $user->nombre = $jsonResponse["persona"]["Nombre"];
            $user->save();
        }

//3 - Si esta activo verificar si existe el usuario localmente, de lo contrario agregarlo
        if ($estado && !$user) {
            $user = new User();
            $user->username = $jsonResponse["usuario"]["username"];
            $user->foto = $jsonResponse["persona"]["Foto"];
            $user->nombre = $jsonResponse["persona"]["Nombre"];
            $user->email = $jsonResponse["persona"]["CorreoElectronico"];
            $user->setPassword("user123"); //by default assign this password, when the user loggedin assign the correct
            $user->generateAuthKey();
            $user->generatePasswordResetToken();
            $user->save();
        }
    }

}

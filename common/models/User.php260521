<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\modules\recursos\models\Recursos;
use yii\helpers\Html;
use yii\db\Query;
use common\modules\reportes\models\Eventos;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    //insignias del usuario
    const INSIGNIA_NIVEL_1 = 1;
    const INSIGNIA_NIVEL_2 = 2;
    const INSIGNIA_NIVEL_3 = 3;
    const INSIGNIA_NIVEL_4 = 4;
    const INSIGNIA_NIVEL_5 = 5;
    const INSIGNIA_NIVEL_6 = 6;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => Yii::t('app', 'Usuario'),
            'email' => Yii::t('app', 'Correo'),
            'password' => Yii::t('app', 'Contraseña'),
            'status' => Yii::t('app', 'Estado'),
            'rememberMe' => Yii::t('app', 'Recordarme'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * Return user by name
     * 
     * @param string $id user
     * 
     * @return User a user model
     */
    public static function getUserById($id) {
        $user = User::find()->where(['id' => $id])->one();
        return $user;
    }

    /**
     * Finds all users by assignment role
     *
     * @param  \yii\rbac\Role $role
     * @return static|null
     */
    public static function findByRole($role) {
        return static::find()
                        ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = id')
                        ->where(['auth_assignment.item_name' => $role])
                        ->all();
    }

    public function UserHasRole($role) {
        $rolesByUser = Yii::$app->authManager->getRolesByUser($this->id);
        $autor_roles = $rolesByUser ? $rolesByUser : array();
        if (array_key_exists($role, $autor_roles)) {
            return true;
        }
        return false;
    }

    /**
     * set insignia depent on user published and approved recursos
     */
    public function setUserInsignia() {
        $recursos = Recursos::find()
                ->innerJoin("user", "[dbo].[user].id = TRecursos.Autor")
                ->andWhere(["TRecursos.Estado" => \yii2mod\moderation\enums\Status::APPROVED, "[dbo].[user].id" => $this->id])
                ->count();

        if (($recursos >= 0) && ($recursos < 5)) {
            $this->insignia = self::INSIGNIA_NIVEL_1;
        } else if (($recursos >= 5) && ($recursos < 10)) {
            $this->insignia = self::INSIGNIA_NIVEL_2;
        } else if (($recursos >= 10) && ($recursos < 15)) {
            $this->insignia = self::INSIGNIA_NIVEL_3;
        } else if (($recursos >= 15) && ($recursos < 20)) {
            $this->insignia = self::INSIGNIA_NIVEL_4;
        } else if (($recursos >= 20) && ($recursos < 25)) {
            $this->insignia = self::INSIGNIA_NIVEL_5;
        } elseif ($recursos >= 25) {
            $this->insignia = self::INSIGNIA_NIVEL_6;
        }
        $this->save();

        return $this->insignia;
    }

    public function getUserInsignia() {
        $insignia = $this->setUserInsignia();
        return Yii::$app->params["urlFront"] . "images/insignias/insignia_nivel_" . $insignia . ".png";
    }

    /**
     * get current loggedin user
     * @return user
     */
    public static function getCurrentUser() {
        return self::findOne(Yii::$app->user->identity->id);
    }

    /**
     * Get the filesizes that user has been used
     * @return boolean
     */
    public function getUserFileSizes() {

        $fileSizes = self::find()
                ->select(["TArchivos.Tamano"])
                ->innerJoin("TArchivos", "[dbo].[user].id = TArchivos.CreadoPor")
                ->andWhere(["[dbo].[user].id" => $this->id])->
                sum("cast(TArchivos.Tamano as bigint )");

        return $fileSizes;
    }

    /**
     * The user reched the limit
     * @param integer $currentFileSize if there is to validate the sum of curren file sizes plus current file
     * @return boolean
     */
    public function haveUserReachedTheLimit($currentFileSize, $newFileSize) {
        $fileSizes = $this->getUserFileSizes(); //it can be NULL if user has not files jet
        if (!$fileSizes) {
            return false;
        } else if ((($fileSizes - $currentFileSize) + $newFileSize) >= Yii::$app->params["spaceLimitPerUser"]) {
            return true;
        }

        return false;
    }

    /**
     * The user is follower yet
     * @param 
     * @return boolean
     */
    public function isFollower($AutorFollow, $AutorFan) {

     $count = (new Query())->select('rIdUsuarios')
    ->from('RAutoresSeguidores')
    ->where(['rIdUsuarios' =>$AutorFollow])
    ->andWhere(['rIdSeguidores' => $AutorFan])
    ->count();

        return $count == 1;
    }

    /**
     * The user has many followers
     * @param 
     * @return boolean
     */
    public function countFollowers($AutorFollow) {

     $count = (new Query())->select('rIdUsuarios')
    ->from('RAutoresSeguidores')
    ->where(['rIdUsuarios' =>$AutorFollow])
    ->count();

        return $count;
    }

    /**
     * The user has many followers
     * @param 
     * @return boolean
     */
    public function countEventos($Autor) {

     $count = (new Query())->select('rIdUsuarios')
    ->from('TEventos')
    ->where(['Autor' => $Autor])
    ->andWhere(['Revisado' => NULL])
    ->count();

     $FollowQuery = (new Query)->select('rIdUsuarios')
     ->from('RAutoresSeguidores')
     ->where(['rIdSeguidores' => $Autor]);
     
     $EventsQuery = (new Query)->select('idEventos')
     ->from('TEventosSeguidores')
     ->where(['Autor' => $Autor]);

     $count2 = (new Query())->select('rIdUsuarios')
    ->from('TEventos')
    ->where(['Autor' => $FollowQuery])
    ->andWhere(['not in', 'idEventos', $EventsQuery])
    ->count();     
        
    return $count + $count2;
    }    

    public function ViewEventos($Autor) {
     $Autor = json_decode($Autor);
     $query = (new Query)->select('TEventos.idEventos,TEventos.CreadoPor, TEventos.Etiqueta, TEventos.Slug, user.nombre, TEventos.valor,TEventos.FechaDeCreacion,TEventos.Revisado')
     ->from('TEventos')
     ->where(['Autor' => $Autor])
     // ->andWhere(['Revisado' => NULL])
     ->leftJoin('user','[TEventos].[CreadoPor] = [user].[id]')
     ->limit(99)
     ->orderBy(['FechaDeCreacion ' => SORT_DESC  ]);

     $FollowQuery = (new Query)->select('rIdUsuarios')
     ->from('RAutoresSeguidores')
     ->where(['rIdSeguidores' => $Autor]);
     
     $EventsQuery = (new Query)->select('idEventos')
     ->from('TEventosSeguidores')
     ->where(['Autor' => $Autor]);

     $query2 = (new Query)->select('TEventos.idEventos,TEventos.CreadoPor, TEventos.Etiqueta, TEventos.Slug, user.nombre, TEventos.valor, TEventos.FechaDeCreacion,TEventos.Revisado')
     ->from('TEventos')
     ->where(['Autor' => $FollowQuery])
     ->andWhere(['Accion' => Eventos::FOLLOW_USER])
     ->andWhere(['not in', 'idEventos', $EventsQuery])
     ->leftJoin('user','[TEventos].[CreadoPor] = [user].[id]')
     ->limit(99)
     ->orderBy(['FechaDeCreacion ' => SORT_DESC  ]);

    //$rows = $query->union($query2)->all();
    $qunion = (new yii\db\Query())
    ->select('*')
    ->from($query->union($query2))
    ->limit(99)
    ->orderBy(['FechaDeCreacion ' => SORT_DESC  ]);
    $rows = $qunion->all();     
    $count = 0;
    $output = "";
    $model = new Recursos();
    $user = new User();
    $color = "w3-light-gray";
    foreach ($rows as $eventos) { 
      $Slug = $eventos["Slug"];
      $queryRecursos = (new Query)->select('idRecursos, Autor')
      ->from('TRecursos')
      ->where(['Slug' => $Slug])
      ->all();
      $image = "";
      foreach ($queryRecursos as $valores){
        $image = $model->getPreview($valores['Autor'], $valores['idRecursos'],"menu") ;
      }
      if($image == ""){
        $userid = $eventos["CreadoPor"];
        $user = $user->findIdentity($userid);                    
        $image = $user->getAvatarImg("menu");
      }
      
      $colorfont = "#00CC99";
      if($eventos['Revisado']==1){
        $colorfont = "black";
      }
      else{
        $count++;
      }
      $etiqueta = $eventos["Etiqueta"];
      if(strlen($etiqueta)>27){
        $etiqueta = substr($etiqueta, 0, 24) . "...";
      }
      $output .= 
      '<a href="/recursos/recursos/ver-recurso?recurso='.$eventos["Slug"].'&evento='.$eventos["idEventos"].'">
        <div class="w3-bar '.$color.'">
        
 
          <img src="' . $image . '" class="w3-bar-item" style="border-radius:30%;width:95px" />       

          <div class="w3-bar-item" >             
             <h5 class="w3-small" style="color:'.$colorfont.'">'.$eventos["nombre"].'</h5>
             <h5 class="w3-small" style="color:'.$colorfont.'">'.$etiqueta.'</h5>           
             <h5 class="w3-small" style="color:'.$colorfont.'">'.$eventos["FechaDeCreacion"].'</h5> 
          </div>       
                                
        </div>
      </a>
      ';  
      if($color=="w3-light-gray"){
        $color="w3-white";
      }
      else{
        $color="w3-light-gray";
      }
      // $output .= 
      // '<li class="w3-bar">
      //    <div class="row">
      //      <div class="col-lg-3 col-sm-3 col-3 text-center">  
      //        <img src="' . $image . '" class="w3-bar-item w3-circle" style="width:85px" />       
      //      </div>
      //      <div class="w3-bar-item">
      //        <a href="/recursos/recursos/ver-recurso?recurso='. $eventos["Slug"].'">
      //          <span class="w3-large">'.$eventos["nombre"].'</span><br>
      //          <span class="w3-large">'.$eventos["Etiqueta"].'</span><br>
      //        </a>             
      //      </div>       
      //    </div>
      //  </li>';
    }
    if($count==0){
      $output .= '<li><a href="#" class="text-bold text-italic">No hay nuevas notificaciones</a></li>';      
    }
$data = array(
   'mensajes' => $output,
   'cantidad'  => $count
);


    return json_encode($data);
    }

    /**
     * The user has many collections
     * @param 
     * @return boolean
     */
    public function countCollections($AutorFollow) {

     $count = (new Query())->select('Autor')
    ->from('TColecciones')
    ->where(['Autor' =>$AutorFollow])
    ->count();

        return $count;
    }

    /**
     * The user has many resources
     * @param 
     * @return boolean
     */
    public function countResources($AutorFollow,$Slug="") {
    
      if($Slug==""){
        $count = (new Query())->select('Autor')
        ->from('TRecursos')
        ->where(['Autor' =>$AutorFollow])
        ->count();        
      }
      else{
        $count = (new Query)->select('TColecciones.idColeccion')
        ->from('TColecciones')
        ->where(['Autor' => $AutorFollow])
        ->andWhere(['Slug' => $Slug])
        ->innerJoin('TColeccionesRecursos', 'TColeccionesRecursos.idColeccion = TColecciones.idColeccion')        
        ->count();      
      }


      return $count;
    }

    /**
     * The user is a new follower
     * @param 
     * @return boolean
     */
    public function agregarFavoritos($AutorFollow, $AutorFan) {
      if(!$this->isFollower($AutorFan,$AutorFollow)){        
        Eventos::addEvent(Eventos::CATEGORY_AUTORES, Eventos::FOLLOW_USER, "LIKE", "0", "", $AutorFollow);          
        return Yii::$app->db->createCommand()->insert('RAutoresSeguidores', [
                 'rIdUsuarios' => $AutorFollow,
                 'rIdSeguidores' => $AutorFan,
               ])->execute();        
      }
      else{
        return true;
      }
    }

    /**
     * The user reched the limit
     * @param integer $currentFileSize if there is to validate the sum of curren file sizes plus current file
     * @return boolean
     */
    public function getUserRemainingSpace($currentFileSize = null) {
        $fileSizes = $this->getUserFileSizes(); //it can be NULL if user has not files jet

        if (!$fileSizes) {
            return $this->human_filesize(Yii::$app->params["spaceLimitPerUser"]);
        } else if ($currentFileSize) {
            return $this->human_filesize((Yii::$app->params["spaceLimitPerUser"] - ($fileSizes - $currentFileSize)));
        } else {
            return $this->human_filesize(Yii::$app->params["spaceLimitPerUser"] - $fileSizes);
        }
    }

    protected function human_filesize($bytes) {
        $i = floor(log($bytes, 1024));
        return round($bytes / pow(1024, $i), [0, 0, 2, 2, 3][$i]) . ['B', 'kB', 'MB', 'GB', 'TB'][$i];
    }

    /**
     * return user avatar image object
     * @return type
     */
    public function getAvatarImg($format = "") {
        if ($this->foto) {
            if($format=="menu"){
              return $this->foto;
            }
            if($format=="perfil2"){
              return Html::img($this->foto, ["alt" => $this->username, "class" => "profile-img"]);
            }
            if($format=="perfil3"){
              return Html::img($this->foto, ["alt" => $this->username, "class" => "user-image", "style" => "width:25%;border-radius:50%"]);
            }            
            if($format=="perfil"){
              return Html::img($this->foto, ["alt" => $this->username, "class" => "user-image", "style" => "border-radius:50%"]);
            }else{
              return Html::img($this->foto, ["alt" => $this->username, "class" => "user-image", "style" => "border-radius:50%"]);
            }
        } else {
            if($format=="menu"){
              return Yii::$app->params["urlBack"] . '/img/avatar.png';
            }            
            return Html::img(Yii::$app->params["urlBack"] . '/img/avatar.png', ["alt" => $this->username, "class" => "user-image", "style" => "border-radius:40%"]);
        }
    }

    /**
     * return user avatar image object
     * @return type
     */
    public function getAvatar() {
        if ($this->foto) {
            return $this->foto;
        } else {
            return Yii::$app->params["urlBack"] . '/img/avatar.png';
        }
    }

}

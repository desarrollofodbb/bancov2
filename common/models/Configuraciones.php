<?php

namespace common\models;

use Yii;
use common\modules\filemanager\models\Mediafile;

/**
 * This is the model class for table "TConfiguraciones".
 *
 * @property integer $idConfiguraciones
 * @property integer $general_header_logo
 * @property integer $general_header_image_featured
 * @property integer $general_footer_logo
 * @property string $general_footer_copyright
 * 
 * @property int $personaje_imagen
 * @property string $personaje_titulo
 * @property string $personaje_descripcion
 * @property string $personaje_link
 * 
 * @property string $contacto_mensaje_gracias
 * @property string $contacto_correo_info
 * @property string $contacto_telefono
 * @property string $contacto_direccion
 * 
 * @property Mediafile $generalHeaderLogo
 * @property Mediafile $GeneralHeaderImageFeatured
 * @property Mediafile $generalFooterLogo
 * @property Mediafile $personajeImagen
 * 

 */
class Configuraciones extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TConfiguraciones';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[
            "general_header_logo",
            "general_header_image_featured",
            "general_footer_logo",
            "general_footer_copyright",
            "personaje_imagen",
            "personaje_titulo",
            "personaje_descripcion",
            "personaje_link",
            "contacto_mensaje_gracias",
            "contacto_correo_info",
            "contacto_telefono",
            "contacto_direccion",
                ], "required", "message" => Yii::t("app", "El campo es requerido")],
            ['contacto_correo_info', 'email', "message" => Yii::t("app", "El correo no tiene un formato correcto")],
            ['contacto_correo_info', 'string', 'max' => 255, "message" => Yii::t("app", "El correo no debe exceder los 255 caracteres")],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idConfiguraciones' => 'Id Configuraciones',
            'general_header_logo' => 'Logo banco de recursos',
            'general_header_image_featured' => 'Imagen destacada del home',
            'general_footer_logo' => 'Logo Fundación Omar Dengo',
            'general_footer_copyright' => 'Copyright del footer',
            'personaje_titulo' => 'Título principal',
            'personaje_descripcion' => 'Descripción',
            'personaje_link' => 'Url del call to action del personaje',
            'contacto_mensaje_gracias' => 'Mensaje de gracias del formulario de contacto',
            'contacto_correo_info' => 'Correo de contacto',
            'contacto_telefono' => 'Teléfono de contacto',
            'contacto_direccion' => 'Dirección de contacto',
        ];
    }

    public function saveConfiguraciones($model_value, $post_fields) {
        foreach ($post_fields as $key => $value) {
            $model_value->$key = $value;
        }
        $model_value->update();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralHeaderLogo() {
        return $this->hasOne(Mediafile::className(), ['id' => 'general_header_logo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralHeaderImageFeatured() {
        return $this->hasOne(Mediafile::className(), ['id' => 'general_header_image_featured']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralFooterLogo() {
        return $this->hasOne(Mediafile::className(), ['id' => 'general_footer_logo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajeImagen() {
        return $this->hasOne(Mediafile::className(), ['id' => 'personaje_imagen']);
    }

    public static function generalSettings() {
        $model_configuraciones = Configuraciones::find()->where("idConfiguraciones=:id", [":id" => 1])->one();

        $Configuraciones = array();
        $Configuraciones["generalHeaderLogo"] = $model_configuraciones->generalHeaderLogo;
        $Configuraciones["generalHeaderImageFeatured"] = $model_configuraciones->generalHeaderImageFeatured;
        $Configuraciones["generalFooterLogo"] = $model_configuraciones->generalFooterLogo;
        $Configuraciones["general_footer_copyright"] = $model_configuraciones->general_footer_copyright;

        return $Configuraciones;
    }

    public static function personajeSettings() {
        $model_configuraciones = Configuraciones::find()->where("idConfiguraciones=:id", [":id" => 1])->one();

        $Configuraciones = array();
        $Configuraciones["personajeImagen"] = $model_configuraciones->personajeImagen;
        $Configuraciones["personaje_titulo"] = $model_configuraciones->personaje_titulo;
        $Configuraciones["personaje_descripcion"] = $model_configuraciones->personaje_descripcion;
        $Configuraciones["personaje_link"] = $model_configuraciones->personaje_link;

        return $Configuraciones;
    }

    public static function contactoSettings() {
        $model_configuraciones = Configuraciones::find()->where("idConfiguraciones=:id", [":id" => 1])->one();

        $Configuraciones = array();
        $Configuraciones["contacto_mensaje_gracias"] = $model_configuraciones->contacto_mensaje_gracias;
        $Configuraciones["contacto_correo_info"] = $model_configuraciones->contacto_correo_info;
        $Configuraciones["contacto_telefono"] = $model_configuraciones->contacto_telefono;
        $Configuraciones["contacto_direccion"] = $model_configuraciones->contacto_direccion;

        return $Configuraciones;
    }

}

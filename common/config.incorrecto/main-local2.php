<?php

return [
    'modules' => [
        'recursos' => [
            'class' => 'common\modules\recursos\Recursos',
        ],
        'colecciones' => [
            'class' => 'common\modules\recursos\Colecciones',
        ],        
        'caracteristicas' => [
            'class' => 'backend\modules\caracteristicas\Caracteristicas',
        ],
        'paginas' => [
            'class' => 'common\modules\paginas\Paginas',
        ],
        'preguntas-frecuentes' => [
            'class' => 'common\modules\preguntas_frecuentes\PreguntasFrecuentes',
        ],
        'datos-de-interes' => [
            'class' => 'common\modules\datos_de_interes\DatosDeInteres',
        ],
        'filemanager' => [
            'class' => 'common\modules\filemanager\Module',
            // Upload routes
            'routes' => [
                // Base absolute path to web directory
                'baseUrl' => '',
                // Base web directory url
                'basePath' => '@backend/web',
                // Path for uploaded files in web directory
                'uploadPath' => "uploads",
            ],
            'thumbFilenameTemplate' => '{original}-{width}-{height}.{extension}',
            'rename' => true,
            // Thumbnails info
            'thumbs' => [
                'small' => [
                    'name' => 'small',
                    'size' => [78, 78],
                ],
                'thumbnail' => [
                    'name' => 'thumbnail',
                    'size' => [150, 150],
                ],
                'medium' => [
                    'name' => 'medium',
                    'size' => [300, 300],
                ],
                'large' => [
                    'name' => 'large',
                    'size' => [640, 640],
                ]
            ],
        ],
        'comment' => [
            'class' => 'common\modules\yii2comments\Module',
            // use comment controller event example
            'controllerMap' => [
                'default' => [
                    'class' => 'common\modules\yii2comments\controllers\DefaultController',
                    'on afterCreate' => function ($event) {
                        // get saved comment model
                        $event->getCommentModel();
                        // your custom code
                    }
                ]
            ],
        // .
        ],
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            //'dsn' => 'sqlsrv:Server=brrec-sql.database.windows.net;Database=BDBancoDeRecursos',
            'dsn' => 'dblib:host=brrec-sql.database.windows.net:1433;dbname=BDBancoDeRecursos',
            'username' => 'adminfodsql@brrec-sql',
            'password' => 'Fod1./ofd41987sql',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '192.168.51.133',
                'username' => 'webmaster@fod.ac.cr',
                'password' => '',
                'port' => '25',
            ],
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'CustomFunctionsHelper' => [
            'class' => 'common\components\CustomFunctionsHelper',
        ],
        'Formatting' => [
            'class' => 'common\components\Formatting',
        ],
    ],
];

<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    // set target language to be spanish
    'language' => 'es-Es',
    // set source language to be spanish
    'sourceLanguage' => 'es-Es',
    'timezone' => 'America/Costa_Rica',
    'modules' => [
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'i18n' => [
            'translations' => [
                'yii2mod.comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => 'common\modules\yii2comments\messages',
                ],
            // ...
            ],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'debug/*',
            'site/*',
            'recursos/recursos/resultados-de-busqueda',
            'recursos/recursos/tags',
            'recursos/colecciones/crear-coleccion',
            'recursos/colecciones/mis-colecciones',            
            'recursos/recursos/ver-recurso',
            'recursos/recursos/mas-recientes',
            'recursos/recursos/autor-favorito',
            'recursos/recursos/delete-file',
            'recursos/recursos/recurso-coleccion',
            'recursos/recursos/crear-coleccion',
            'recursos/recursos/crear-perfil',            
            'recursos/recursos/count-eventos',
            'recursos/recursos/view-eventos',
            'recursos/recursos/ver-busquedas',
            'recursos/recursos/collection-list',
            'gii/*',
//            'admin/assignment/*',
//            'preguntas-frecuentes/preguntas-frecuentes/*',
//            'caracteristicas/valores-de-caracteristicas/*',
//            'caracteristicas/caracteristicas/*', 
//            'admin/*',
        // The actions listed here will be allowed to everyone including guests.
        // So, 'admin/*' should not appear here in the production, of course.
        // But in the earlier stages of your development, you may probably want to
        // add a lot of actions here until you finally completed setting up rbac,
        // otherwise you may not even take a first step.
        ]
    ],
];

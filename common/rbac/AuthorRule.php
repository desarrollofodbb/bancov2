<?php

namespace common\rbac;

use yii;
use yii\rbac\Rule;
use common\models\User;

/**
 * Checks if authorID matches user passed via params
 */
class AuthorRule extends Rule {

    public $name = 'isAuthor';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params) {
        if (isset($params['recurso'])) {
            $recurso = \common\modules\recursos\models\Recursos::find()->where('Slug = :recursoslug', [':recursoslug' => $params['recurso']])->one();

            if (!is_null($recurso) && ($recurso->Autor == $user)) {
                return true;
            }
        } else if (isset($params['rIdRecursos']) && isset($params['rIdUsuarios'])) {//regla para favoritos
            $favorito = \common\modules\recursos\models\Favoritos::find()
                    ->andWhere('rIdRecursos = :rIdRecursos', [':rIdRecursos' => intval($params['rIdRecursos'])])
                    ->andWhere('rIdUsuarios = :rIdUsuarios', [':rIdUsuarios' => intval($params['rIdUsuarios'])])
                    ->one();

            if (!is_null($favorito) && ($favorito->rIdUsuarios == $user)) {
                return true;
            }
        }
        return false;
    }

}

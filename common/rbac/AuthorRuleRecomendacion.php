<?php

namespace common\rbac;

use yii;
use yii\rbac\Rule;
use common\modules\recursos\models\Recomendaciones;

/**
 * Checks if authorID matches user passed via params
 */
class AuthorRuleRecomendacion extends Rule {

    public $name = 'isRecomendacionAuthor';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params) {
        if (isset($params['id'])) {
            $recomendacion = Recomendaciones::find()->where('idRecomendaciones = :idRecomendaciones', [':idRecomendaciones' => intval($params['id'])])->one();

            if (!is_null($recomendacion) && (($recomendacion->rIdUsuarios == $user) || ($recomendacion->recurso->autor->id == $user))) {
                return true;
            }
        }

        return false;
    }

}

<?php

namespace common\rbac;

use yii;
use yii\rbac\Rule;
use \common\modules\yii2comments\models\CommentModel;

/**
 * Checks if authorID matches user passed via params
 */
class AuthorRuleComment extends Rule {

    public $name = 'isCommentAuthor';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params) {
        if (isset($params['id'])) {
            $comment = CommentModel::find()->where('id = :id', [':id' => intval($params['id'])])->one();

            if (!is_null($comment) && ($comment->createdBy == $user)) {
                return true;
            }
        }

        return false;
    }

}

<?php
// example of a PHP server code that is called in `uploadUrl` above
// file-upload.php script
header('Content-Type: application/json'); // set json response headers
$outData = upload(); // a function to upload the bootstrap-fileinput files
echo json_encode($outData); // return json data
// echo $outData;
exit(); // terminate
 
// main upload function used above
// upload the bootstrap-fileinput files
// returns associative array
function upload() {
  $id=$_POST['id'];
  $idtime = $_POST['idtime'];    
  $link = $_POST['youtube'];    
  if (strpos($link, 'youtube') == false) {
    return json_encode( [
                'url' => '/recursos/recursos/delete-file',
                'type' => '',
                'rutaurl' => 'Not valid link',
                'caption' => '', // caption
                'key' =>  '',       // keys for deleting/reorganizing preview
                'size' => '',    // file size
              ]);
  }
  $vistapreliminar = $_POST['preview'];    
  $preview = $config = $errors = [];
  $targetDir = getcwd(). '/uploads/' . $id . '/' . $idtime;
  if (!file_exists($targetDir)) {
      mkdir($targetDir, 0777);
  }

  // $video_id = explode("?v=", $link);
  // $video_id = $video_id[1];
  $video_id = explode("?v=", $link);
  $video_id2 = explode("&", $video_id[1]);

  $video_id3 = $video_id2[0];
  $imageUrl="http://img.youtube.com/vi/".$video_id3."/maxresdefault.jpg";

  $toArray = explode('/',$imageUrl);
  $fileNameWithExt = 'youtube.'.substr(strrchr(end($toArray),'.'),1);

  if (!file_exists(getcwd(). '/uploads/' . $id . '/' . $idtime)) {
    mkdir(getcwd(). '/uploads/' . $id . '/' . $idtime, 0777);
  }
  $fileDownLoadPath = getcwd(). '/uploads/' . $id . '/' . $idtime . '/';
  $fileAlternative = '/../../uploads/' . $id . '/' . $idtime . '/';
  file_put_contents($fileDownLoadPath.$fileNameWithExt, file_get_contents($imageUrl));

  $type = mime_content_type($fileDownLoadPath.$fileNameWithExt);
  $size = filesize($fileDownLoadPath.$fileNameWithExt);  

  return json_encode( [
                'url' => '/recursos/recursos/delete-file',
                'type' => $type,
                'rutaurl' => $imageUrl,
                'caption' => $fileAlternative.$fileNameWithExt, // caption
                'key' =>  $fileAlternative.$fileNameWithExt,       // keys for deleting/reorganizing preview
                'size' => $size,    // file size
              ]);
            

  return [
    'sucess' => 'Archivos cargados exitosamente'  // return access control error
  ];

}
 

return;
//previous code to previous functionality 
$id=$_POST['userid'];
$idtime = $_POST['idtime'];
foreach($_FILES['file']['name'] as $key=>$val){
    $file_name = $_FILES['file']['name'][$key];
 
    // get file extension
    $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
 
    // get filename without extension
    $filenamewithoutextension = pathinfo($file_name, PATHINFO_FILENAME);
    $filenamewithoutextension = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $filenamewithoutextension);

    if (!file_exists(getcwd(). '/uploads/' . $id . '/' . $idtime)) {
        mkdir(getcwd(). '/uploads/' . $id . '/' . $idtime, 0777);
    }
 
    //$filename_to_store = $filenamewithoutextension. '_' .uniqid(). '.' .$ext;
    $filename_to_store = $filenamewithoutextension . "." . $ext;
    move_uploaded_file($_FILES['file']['tmp_name'][$key], getcwd(). '/uploads/' . $id . '/' . $idtime . '/' .$filename_to_store);
}
die;
<?php
// example of a PHP server code that is called in `uploadUrl` above
// file-upload.php script
header('Content-Type: application/json'); // set json response headers

$outData = upload(); // a function to upload the bootstrap-fileinput files
echo json_encode($outData); // return json data
exit(); // terminate
 
// main upload function used above
// upload the bootstrap-fileinput files
// returns associative array
function upload() {
  $id=$_POST['id'];
  $idtime = $_POST['idtime'];    
  $field = $_POST['field'];    
  $vistapreliminar = $_POST['preview'];    
  $preview = $config = $errors = [];
  $targetDir = getcwd(). '/uploads/' . $id . '/' . $idtime;

  if (!file_exists($targetDir)) {
      mkdir($targetDir, 0777, true);
  }    


  foreach($_FILES[$field]['name'] as $key=>$val){
    $file_name = $_FILES[$field]['name'][$key];
 
    // get file extension
    $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
    $type = mime_content_type($_FILES[$field]['tmp_name'][$key]);
    $size = filesize($_FILES[$field]['tmp_name'][$key]);
    $Tipo = $type;
    if(strpos($type,"text")>0){
      $Tipo = "text";  
    }               
    if(strpos($type,"mage")>0){
      $Tipo = "image";  
    }
    if(strpos($type,"pdf")>0){
      $Tipo = "pdf";  
    }    
    if(strpos($type,"xls")>0||strpos($type,"doc")>0){
      $Tipo = "office";  
    } 
    if(strpos($type,"html")>0){
      $Tipo = "html";  
    }              
    if(strpos($type,"xml")>0){
      $Tipo = "xml";  
    } 
    // get filename without extension
    $filenamewithoutextension = pathinfo($file_name, PATHINFO_FILENAME);
    $filenamewithoutextension = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $filenamewithoutextension);

    if (!file_exists(getcwd(). '/uploads/' . $id . '/' . $idtime)) {
        mkdir(getcwd(). '/uploads/' . $id . '/' . $idtime, 0777);
    }
 
    //$filename_to_store = $filenamewithoutextension. '_' .uniqid(). '.' .$ext;
    if($vistapreliminar==1){
      $filenamewithoutextension =  "preview_" . $filenamewithoutextension;
    }
    $filename_to_store = $filenamewithoutextension . "." . $ext;
    move_uploaded_file($_FILES[$field]['tmp_name'][$key], getcwd(). '/uploads/' . $id . '/' . $idtime . '/' .$filename_to_store);
  }

  return [      'initialPreviewConfig' => [
                    [
                        'url' => '/recursos/recursos/delete-file',
                        'type' => $Tipo,
                        'caption' => '/uploads/' . $id . '/' . $idtime . '/' .$filename_to_store, // caption
                        'key' =>  '/uploads/' . $id . '/' . $idtime . '/' .$filename_to_store,       // keys for deleting/reorganizing preview
                        'size' => $size,    // file size
                    ]
                ],
         ];

  return [
    'sucess' => 'Archivos cargados exitosamente'  // return access control error
  ];

}
 

return;
//previous code to previous functionality 
$id=$_POST['userid'];
$idtime = $_POST['idtime'];
foreach($_FILES['file']['name'] as $key=>$val){
    $file_name = $_FILES['file']['name'][$key];
 
    // get file extension
    $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
 
    // get filename without extension
    $filenamewithoutextension = pathinfo($file_name, PATHINFO_FILENAME);
    $filenamewithoutextension = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $filenamewithoutextension);

    if (!file_exists(getcwd(). '/uploads/' . $id . '/' . $idtime)) {
        mkdir(getcwd(). '/uploads/' . $id . '/' . $idtime, 0777);
    }
 
    //$filename_to_store = $filenamewithoutextension. '_' .uniqid(). '.' .$ext;
    $filename_to_store = $filenamewithoutextension . "." . $ext;
    move_uploaded_file($_FILES['file']['tmp_name'][$key], getcwd(). '/uploads/' . $id . '/' . $idtime . '/' .$filename_to_store);
}
die;
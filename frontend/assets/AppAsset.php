<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      //  'css/site.css',
      //  'css/bootstrap.min.css',
        'css/fod.min.css',
      //  'css/custom.css',
        'css/demo.css',        
    ];
    public $js = [
//        'js/jquery.min.js',
       // 'js/bootstrap.min.js',
        'js/fod.min.js',
      //  'js/recursos.js',
        'js/responsive_waterfall.js',   
        'js/registro.js', 
        'js/app.js',              
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113771826-1"></script> -->
<?php

use common\modules\recursos\widgets\BusquedaPersonaje;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

$url = Yii::$app->params["urlBack"] . $configuraciones["personajeImagen"]->getThumbUrl("medium");
$options = [
    "alt" => Yii::t("app", "Fundación Omar Dengo"),
    "class" => "default-character character-right",
];

echo BusquedaPersonaje::widget();
?>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-113771826-1');
    var URLAPI = "<?php echo Yii::$app->params["urlWS"]; ?>";
</script>
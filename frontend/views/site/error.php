<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?php echo  "Atención" ?></h1>

    <div class="alert alert-danger">
        <?php echo  nl2br(Html::encode($message)) ?>
    </div>

    <p>
        En caso de persistir, comunicarse por medio del correo banco.recursos@fod.ac.cr.
    </p>
    <p>
        Fundación Omar Dengo.
    </p>

</div>

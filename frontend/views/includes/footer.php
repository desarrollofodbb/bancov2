<?php
use yii\helpers\Html;
?>
<style type="text/css">
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
}    
</style>
<!-- FOOTER -->
<footer class="w3-container w3-border-top" style="z-index: 1">
    <div class="w3-row w3-hide-small">
        <div class="w3-third w3-left-align w3-padding-16" style="width: 25%;">
            <br>
            <p style="color:black;font-size:15px"><?php echo Yii::t("app", "San José, Costa Rica | 2020"); ?></p> 
            <br>

              <p style="color:black;font-size: 12px">Derechos Reservados - <a target="_blank" href="<?php echo Yii::$app->params["urlTerminos"]; ?>"><?php echo Yii::t("app", "Términos y condiciones"); ?></a></p>               
              

        </div>

        <div class="w3-third" style="width: 50%;">
            <p style="color:white">.</p>
        </div>
        <div class="w3-third w3-left-align w3-padding-16" style="width: 50%; text-align: center !important; display: inline-block;">
        <p style="color:white">.</p>
        <div class="MenuCentralFooter">
                <a href="/quienes-somos" style="padding-right: 13px;">¿Quiénes somos?</a>
                <a href="/sobre-el-banco-de-recursos" style="padding-right: 13px;">Sobre Banco Recursos</a>
                <a href="/contactenos" style="padding-right: 13px;">Contacto</a>
                <a href="/preguntas-frecuentes" style="padding-right: 13px;">Preguntas Frecuentes</a>

            </div>
        </div>


        <div class="w3-third" style="width: 25%;">
            <p style="color:white">.</p>
        </div>

        <div class="w3-third w3-right-align" style="width: 25%;">
        <?php
        if ($configuraciones_generales["generalFooterLogo"]) {
            echo Html::a(Html::img(Yii::$app->params["urlBack"] . $configuraciones_generales["generalFooterLogo"]->getThumbUrl(''), ["alt" => "Fundación Omar Dengo", 'class' => "pull-right","style" => "width:100%;max-width:200px"]), Yii::$app->params["urlFOD"], ["target" => "_blank"]);
        }
        ?> 
        </div>        
    </div>
    <div class="w3-row w3-hide-medium w3-hide-large">
        <div class="w3-third">

        <?php

        if ($configuraciones_generales["generalFooterLogo"]) {                  
            echo Html::a(Html::img(Yii::$app->params["urlBack"] . $configuraciones_generales["generalFooterLogo"]->getThumbUrl(''), ["alt" => "Fundación Omar Dengo","style" => "width:100%;max-width:200px;margin: 10px auto 20px;"]), Yii::$app->params["urlFOD"], ["target" => "_blank"]);
        }
        ?>       
    
        </div>
        <div class="w3-third">
            
          <a href="/quienes-somos" style="text-decoration: none" class="w3-bar-item w3-button"><p style="font-size:15px;color:#707070">¿Quiénes somos?</p></a><br>
    <?php if (Yii::$app->user->can(Yii::$app->params['verificador_default_role'])): ?>
          <a class="w3-bar-item w3-button w3-ro" style="text-decoration: none" href="<?php echo \yii\helpers\Url::to(["/recursos/recursos/aprobar-recursos"]) ?>"><p style="font-size:15px;color:#707070"><?php echo Yii::t("app", "Aprobar recursos"); ?></p></a><br>
    <?php endif; ?> 
          <a href="/sobre-el-banco-de-recursos" style="text-decoration: none" class="w3-bar-item w3-button"><p style="font-size:15px;color:#707070">Sobre Banco Recursos</p></a><br>
          <a href="/contactenos" style="text-decoration: none" class="w3-bar-item w3-button"><p style="font-size:15px;color:#707070">Contacto</p></a><br>
          <a href="/preguntas-frecuentes" style="text-decoration: none" class="w3-bar-item w3-button"><p style="font-size:15px;color:#707070">Preguntas Frecuentes</p></a><br>
        <?=
            Html::a(
            Yii::t("app", 'Cerrar sesión'), ['/site/logout'], ['data-method' => 'post', 'class' => 'w3-bar-item w3-button','style' => 'text-decoration: none;font-size:15px;color:#707070']
            )
        ?>
        </div>

        <div class="w3-third w3-center">
            <br>
            <p style="color:black"><?php echo Yii::t("app", "San José, Costa Rica | 2020"); ?></p> 
            <br>
            <small style="color:black"> 
              <p style="color:black">Derechos Reservados - <a target="_blank" href="<?php echo Yii::$app->params["urlTerminos"]; ?>"><?php echo Yii::t("app", "Términos y condiciones"); ?></a></p>               
              
            </small>
 
        </div>        
    </div>    
</footer>
<!-- /END FOOTER -->
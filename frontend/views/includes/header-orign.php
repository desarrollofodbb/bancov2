<?php

use yii\helpers\Html;
use dmstr\widgets\Menu;
use yii\helpers\Url;
use common\modules\recursos\widgets\BusquedaGeneral;
use common\components\AutoLoginFODWidget;
?>  
<!-- HEADER -->
<header>
    <div class="header-bar" style="background-image: url('<?php
    if ($configuraciones_generales["generalHeaderImageFeatured"]) {
        echo Url::to(Yii::$app->params["urlBack"] . $configuraciones_generales["generalHeaderImageFeatured"]->getThumbUrl('original'));
    }
    ?> ')">
        <div class="container">
            <a class="navbar-brand" href="<?php echo Yii::$app->homeUrl; ?>">
                <?php
                if ($configuraciones_generales["generalHeaderLogo"]) {
                    echo Html::img(Yii::$app->params["urlBack"] . $configuraciones_generales["generalHeaderLogo"]->getThumbUrl('medium'), ["alt" => Yii::$app->name, 'class' => "pull-left"]);
                }
                ?> 
            </a>
            <div id="login-header">
                <!-- LOGIN / USER STATUS -->
                <?php
                if (Yii::$app->user->isGuest) {
                    echo $this->render("../includes/header_logout", ["configuraciones_generales" => $this->params["configuraciones_generales"]]);
                } else {
                    echo $this->render("../includes/header_login", ["configuraciones_generales" => $this->params["configuraciones_generales"]]);
                }
                ?>
                <!-- /END LOGIN / USER STATUS -->
            </div>
        </div>

        <div class="nav-wrap">
            <div class="container">
                <a href="#" id="myAccount">
                    <span class="icon icon-fod-icon-triangle"></span> 
                    <?php echo Yii::t("app", "Mi cuenta"); ?>
                </a>
                <?php
                echo BusquedaGeneral::widget();
                ?>
                <!--NAV-->
                <div id = "fod-nav" >
                    <button type = "button" class = "navbar-toggle collapsed" data-toggle = "collapse" data-target = "#fod-nav-responsive" aria-expanded = "false" aria-controls = "fod-nav-responsive">
                        <span class = "icon icon-fod-icon-hamburger"></span>
                        <strong>Menú</strong>
                    </button>
                    <div id = "fod-nav-responsive" class = "collapse navbar-collapse">
                        <?php
                        $current_route = $this->context->route;
                        $query_params = Yii::$app->request->queryParams;
                        $current_page = isset($query_params["slug"]) ? $query_params["slug"] : "";
//                        var_dump($current_route);
//                        var_dump(Yii::$app->request->queryParams);
                        echo
                        Menu::widget(
                                [
                                    'options' => ['class' => 'nav navbar-nav'],
                                    'items' => [
                                        [
                                            'label' => '',
                                            'icon' => 'icon icon-fod-icon-home',
                                            'url' => Yii::$app->homeUrl,
                                            'active' => $current_route == "site/index" ? true : false
                                        ],
                                        [
                                            'label' => Yii::t("app", "¿Quiénes somos?"),
                                            'icon' => '',
                                            'url' => '/quienes-somos',
                                            'active' => $current_page == "quienes-somos" ? true : false
                                        ],
                                        [
                                            'label' => Yii::t("app", "Sobre el banco de recursos"),
                                            'icon' => '',
                                            'url' => '/sobre-el-banco-de-recursos',
                                            'active' => $current_page == "sobre-el-banco-de-recursos" ? true : false
                                        ],
                                        [
                                            'label' => Yii::t("app", "Preguntas Frecuentes"),
                                            'icon' => '',
                                            'url' => '/preguntas-frecuentes',
                                            'active' => $current_page == "preguntas-frecuentes" ? true : false
                                        ],
//                                        [
//                                            'label' => Yii::t("app", "Oferta virtual"),
//                                            'icon' => '',
//                                            'url' => '/preguntas-frecuentes',
//                                            'active' => $current_page == "preguntas-frecuentes" ? true : false
//                                        ],
                                        [
                                            'label' => Yii::t("app", "Contáctenos"),
                                            'icon' => '',
                                            'url' => '/contactenos',
                                            'active' => $current_route == "paginas/paginas/contactenos" ? true : false
                                        ],
                                    ],
                                ]
                        )
                        ?>
                    </div>
                </div>
                <!-- /END NAV-->
            </div>
        </div>
    </div>
</header>
<!-- /END HEADER -->
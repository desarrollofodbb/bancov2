<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$loginform = new common\models\LoginForm();
?> 
<div class="">
    <div class="user-box-status"> 
        <div class="error-login-wrap">
            <?php echo Alert::widget(["options" => ["class" => "error-login"]]) ?>
        </div>
        <?php $form = ActiveForm::begin(["action" => Url::to(['/site/login']), 'id' => 'login-form']); ?>

        <div class="form-inline" class="col-lg-6 col-md-6 col-sm-6">

            <?php echo $form->field($loginform, 'username')->textInput([ "class" => "header-login", "placeholder" => "Usuario"])->label(false); ?>

            <?php echo $form->field($loginform, 'password')->passwordInput(["class" => "header-login", "placeholder" => "Contraseña"])->label(false); ?>

<!--             <?php echo $form->field($loginform, 'rememberMe')->checkbox()->label("Recordarme"); ?> -->
            <?php echo Html::submitButton('Ingresar', ['class' => 'button', 'name' => 'login-button']) ?>

        </div>
        <div class="user-actions"> 
            <div class="links-left">
<!--                 <?php echo Html::a(Yii::t("app", "Registrarme"), Yii::$app->params["perfilUsuarioCrearCuenta"], ["target" => "_blank"]) ?> -->
                <?php echo Html::a(Yii::t("app", "Olvidé mi contraseña"), Yii::$app->params["perfilUsuarioEditarCuenta"], ["target" => "_blank"]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div> 
</div> 

<style type="text/css">
.header-login {
    position: sticky;
    border: none;
    display: inline-block;

    border-radius: 25px;
    top: 25px;

}    
.header-login form {
    display: table;
}    
.header-login .form-control {

    resize: horizontal;

    height: 18px;
    left: 5px;
    font-size: 11px;
    color: #EAECEB;
    background-color:  #EAECEB;
    border: none;
    border-radius: 0;
}    
.header-login .input-group-addon {
    width: 26px;
    vertical-align: middle;
    white-space: nowrap;
}
.header-login .btn {
    width: 26px;
    height: 18px;
    padding: 4px;
    border: none;
    font-size: 11px;
    line-height: 1;
}
.header-login .help-block {
    display: none;
}
.header-login .form-group {
    margin-bottom: 0;
}

/* Style the form - display items horizontally */
.form-inline {
  display: flex;
  flex-flow: row wrap;
  align-items: center;
}

/* Add some margins for each label */
.form-inline label {
  margin: 5px 10px 5px 0;
}

/* Style the input fields */
.form-inline input {
  vertical-align: middle;
  height: 18px;  
  margin: 5px 10px 5px 0;
  padding: 10px;
  background-color: #fff;
  border: 1px solid #ddd;
}

/* Style the submit button */
.form-inline button {

  background-color: #45B390;
  border: 1px solid #ddd;
  border-radius: 4px;
  vertical-align: middle;  
    font-size: 11px;

  color: white;
  height: 18px;

  font-family: "Sans Pro Regular";
}

.form-inline button:hover {
  background-color: #EAECEB;
  color:black;
}

/* Add responsiveness - display the form controls vertically instead of horizontally on screens that are less than 800px wide */
@media (max-width: 800px) {
  .form-inline input {
    margin: 10px 0; 
  }
  .form-inline {
        display: none;
  }  

  .form-inline {
    flex-direction: column;
    align-items: stretch;
  }
}


</style>
<?php

use yii\helpers\Html;
use common\models\User;
?>
<div class="box-status-wrap">
    <div class="user-box-status">
        <?php echo Html::beginForm(['/site/logout'], 'post'); ?>
        <div class="user-info">
            <?php
            $user = common\models\User::getUserById(Yii::$app->user->identity->id);
            // echo $user->getAvatarImg();
            ?>
             <div class="user-text <?php echo Yii::$app->user->can("autor") ? 'insignia-nivel-' . $user->setUserInsignia() : ""; ?>">
                <p><?php
                    if (Yii::$app->user->can("autor")) {
                        echo Yii::$app->user->identity->nombre;
                    } else {
                        echo Yii::$app->user->identity->username;
                    }
                    ?></p>
                <p><?php echo Yii::$app->user->identity->email; ?></p>
            </div> 
        </div>
        <div class="user-actions">  
            <?php
            // echo Html::submitButton(
            //         'Salir', ['class' => 'btn btn-default btn-sm']
            // );
            ?>
            <div class="links-left">
                <a href="#" id="userOption_btn">
                    <span class="icon icon-fod-icon-triangle"></span>
                    <?php echo Yii::t("app", "Opciones de usuario"); ?>
                </a>
                <ul class="user-options">
                    <li> 
                        <?php echo Html::a(Yii::t("app", "Ver Perfil"), Yii::$app->params["perfilUsuarioVerCuenta"], ["target" => "_blank"]) ?>
                    </li>
                    <?php if (Yii::$app->user->can(Yii::$app->params['verificador_default_role'])): ?>
                        <li>
                            <a href="<?php echo \yii\helpers\Url::to(["/recursos/recursos/aprobar-recursos"]) ?>"><?php echo Yii::t("app", "Aprobar recursos"); ?></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php echo Html::endForm(); ?>
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>  
<style type="text/css">
#menu {
  position: fixed;
  right: 0;
  top: 80%;
  width: 4em;
  margin-top: -2.5em;
}    
        .btn-circle { 
            width: 35px; 
            height: 35px; 
            padding: 6px 0px; 
            border-radius: 18px; 
            font-size: 8px; 
            text-align: center; 
        } 
        .btn-circle.btn-md { 
            width: 50px; 
            height: 50px; 
            padding: 7px 10px; 
            border-radius: 25px; 
            font-size: 10px; 
            text-align: center; 
        } 
        .btn-circle.btn-xl { 
            width: 70px; 
            height: 70px; 
            padding: 10px 16px; 
            border-radius: 35px; 
            font-size: 12px; 
            text-align: center; 
        } 
</style>
<!-- SIDE MENU --> 
<?php
  if($_SERVER["REQUEST_URI"]!="/recursos/recursos/crear-recurso"){
?>
<div id="side-nav" class="w3-hide-small w3-hide-medium" >
    <div class="pull-right">
<ul id=menu>
<?php
  if (!Yii::$app->user->isGuest) {    
?>
<li><a href="/recursos/recursos/crear-recurso" class="btn btn-success btn-circle  w3-text-white fa fa-upload fa-2x" title="Subir recurso" style="background-color:#666666; font-size: 20px; margin-bottom: 10px"></a>      
<?php } 
?>

<li><a href="/preguntas-frecuentes" class="btn btn-success btn-circle  w3-text-white fa fa-question fa-2x" title="Preguntas frecuentes" style="background-color:#666666; font-size: 20px; "></a>

</ul>
    </div>
</div>
<?php } ?>
<!-- /END SIDE MENU -->

<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>  
<!-- SIDE MENU --> 
<div id="side-nav" class="container">
    <div class="pull-right">
        <ul class="side-line">
            <li class="side-button">
                <a href="#"><span class="icon icon-fod-icon-glass"></span></a>
                <div id="sideBuscar" class="side-lateral">
                    <p>
                        <span class="icon icon-fod-icon-triangle"></span>
                        <?php echo Yii::t("app", "Buscar"); ?>
                    </p>
                    <ul class="side-box">
                        <li><?php
                            $text = Yii::t("app", "Búsqueda Avanzada");
                            echo
                            Html::a($text, Url::to(["/recursos/recursos/resultados-de-busqueda"]));
                            ?></li>
                        <li><?php
                            $text = Yii::t("app", "Más recientes");
                            echo
                            Html::a($text, Url::to(["/recursos/recursos/mas-recientes"]));
                            ?></li>
                        <!--                        <li>
                        <?php
                        $text = Yii::t("app", "Mejor calificados");
                        echo
                        Html::a($text, Url::to(["/recursos/recursos/resultados-de-busqueda", "filtro" => "mejor-calificados"]));
                        ?>
                                                </li>
                                                <li>
                        <?php
                        $text = Yii::t("app", "Más descargados");
                        echo
                        Html::a($text, Url::to(["/recursos/recursos/resultados-de-busqueda", "filtro" => "mas-descargados"]));
                        ?>
                                                </li>-->
                    </ul>
                </div>
            </li>
            <?php if (Yii::$app->user->can(Yii::$app->params["autor_default_role"]))://show the menu only if is logedin ?>
                <li class="side-button">
                    <a href="#"><span class="icon icon-fod-icon-link"></span></a>
                    <div id="sideCompartir" class="side-lateral">
                        <p>
                            <span class="icon icon-fod-icon-triangle"></span>
                            <?php echo Yii::t("app", "Compartir"); ?>
                        </p>
                        <ul class="side-box">
                            <!--                        <li>
                            <?php
                            $text = '<span class="icon icon-fod-icon-twitter"></span>';
                            echo
                            Html::a($text, Url::to(["esto es administrable"]), ["target" => "_blank"]);
                            $text = '<span class="icon icon-fod-icon-facebook"></span>';
                            echo
                            Html::a($text, Url::to(["esto es administrable"], ["target" => "_blank"]));
                            echo Yii::t("app", "Redes sociales");
                            ?>
                                                    </li>-->

                            <li><?php
                                $text = '<span class="icon icon-fod-icon-upload"></span> ' . Yii::t("app", "Publicar recurso");
                                echo
                                Html::a($text, Url::to(["/recursos/recursos/crear-recurso"]));
                                ?></li>

                        </ul>
                    </div>
                </li>
            <?php endif; ?>
            <?php if (Yii::$app->user->can(Yii::$app->params["autor_default_role"]))://show the menu only if is logedin ?>
                <li class="side-button">
                    <a href="#"><span class="icon icon-fod-icon-pencil"></span></a>
                    <div id="sideCompartir" class="side-lateral">
                        <p>
                            <span class="icon icon-fod-icon-triangle"></span>
                            <?php
                            echo Yii::t("app", "Editar");
                            ?>
                        </p>
                        <ul class = "side-box">
                            <li><?php
                                echo
                                Html::a(Yii::t("app", "Mi mochila"), Url::to(["/recursos/recursos/mi-mochila"]));
                                ?></li>
                            <li><?php
                                echo
                                Html::a(Yii::t("app", "Mis recursos"), Url::to(["/recursos/recursos/mis-recursos"]));
                                ?></li>
                        </ul>
                    </div>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<!-- /END SIDE MENU -->
<?php

/* @var $this yii\web\View */
/* @var $model common\modules\paginas\models\Paginas */
$this->title = $model->Titulo;
?>
<?php

switch ($page) {
//        case "contactenos":
//            echo $this->render("content/contactenos", [
//                "model" => $model,
//                "configuraciones" => $configuraciones
//            ]);
//            break;
    case "preguntas-frecuentes":
        echo $this->render("content/preguntas-frecuentes", [
            "dataProvider" => $dataProvider,
            "model" => $model,
            "ContactForm" => $ContactForm,
            "configuraciones" => $configuraciones
        ]);
        break;
    case "quienes-somos":
        echo $this->render("content/quienes-somos", [
            "model" => $model,
            "configuraciones" => $configuraciones
        ]);
        break;
    case "sobre-el-banco-de-recursos":
        echo $this->render("content/sobre-el-banco-de-recursos", [
            "dataProvider" => $dataProvider,
            "model" => $model,
            "configuraciones" => $configuraciones
        ]);
        break;

    default:
        echo $this->render("single", [
            "model" => $model,
            "configuraciones" => $configuraciones
        ]);
        break;
}
?>

<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="main-box large-box">
    <h1><?php echo Html::encode($model->Titulo); ?><span class="icon icon-fod-icon-question"></span></h1>
        <?php echo str_replace("/uploads/", Yii::$app->params["urlBack"] . "/uploads/", HtmlPurifier::process($model->Descripcion)); ?>
</div>
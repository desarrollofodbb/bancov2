<?php
use yii\helpers\Html;
?> 
<style type="text/css">
    .container {
        height: 95px;
        position: relative;

    }
    .horizontal-center{
        padding: 32px 0;
        text-align: center;
    }
    * {
        box-sizing: border-box;
    }  
</style>
<div class="w3-container" style="min-height: 80vh;">  
    <h1 style="color:#00CC99;font-weight: bold;padding-top:85px;"><?php echo Html::encode($pagina->Titulo); ?>   </h1>
    <div style="background-color: white">
        <div class="w3-half w3-padding-small">
            <div class="w3-panel w3-round-xlarge container" style="background-color: #EBEDEC;margin-right: 15px; margin-bottom: 25px">              
                <div class="horizontal-center">   
                    <a href="mailto:<?php echo Html::encode($configuraciones["contacto_correo_info"]); ?>">
                        <text style="font-size: 15px;color:#666666;font-weight: normal"><i class="fa fa-envelope fa-lg" style="margin-right: 10px"></i> <?php echo '    ' . Html::encode($configuraciones["contacto_correo_info"]); ?></text>
                    </a>
                </div>
            </div>          
        </div>

        <div class="w3-half w3-padding-small">
            <div class="w3-panel w3-round-xlarge container" style="background-color: #EBEDEC;margin-right: 15px; margin-bottom: 25px">  
                <div class="horizontal-center">
                    <text style="font-size: 15px;color:#666666;font-weight: normal"><i class="fa fa-phone fa-lg" style="margin-right: 10px"></i> <?php echo '    ' . Html::encode($configuraciones["contacto_telefono"]); ?></text>
                </div>
            </div>
        </div>

        <div class="w3-rest w3-padding-small">
            <div class="w3-panel w3-round-xlarge container" style="background-color: #EBEDEC; margin-bottom: 25px">              
                <div class="w3-padding horizontal-center" style="margin-top: 15px">
                    <i class="fa fa-map-marker fa-2x" style="margin-right: 10px;color:#666666"></i>  
                    <text style="font-size: 15px;color:#666666;font-weight: normal" >  <?php echo '    ' . Html::encode($configuraciones["contacto_direccion"]); ?>           
                    </text>
                </div>
            </div>
        </div>      
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="panel-heading">
    <h4 class="panel-title">
        <?php
        $text = Html::encode($model->Titulo) . '<span class="icon icon-fod-icon-triangle"></span>';
        $url = "#question" . $model->Slug;
        $options = [
            "data-toggle" => "collapse",
            "data-parent" => "#preguntas-frecuentes"
        ];
        echo Html::a($text, $url, $options)
        ?>
    </h4>
</div>
<div id="question<?php echo $model->Slug; ?>" class="panel-collapse collapse <?php echo $index == 0 ? "in" : "" ?>">
    <div class="panel-body">
        <?php echo HTMLPurifier::process($model->Descripcion); ?>
    </div>
</div> 
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
?>
<div class="balloon-group">
    <div class="globe-left globe-style">
        <h3><?php echo Html::encode($configuraciones["personaje_titulo"]); ?></h3>
        <p><?php echo Html::encode($configuraciones["personaje_descripcion"]); ?></p>
        <?php
        $text = '<span class="icon icon-fod-icon-plus"></span>';
        $options = [
            "class" => "md-link azul-link"
        ];
        $url = Url::to(Yii::$app->params["urlFront"] . Html::encode($configuraciones["personaje_link"]));
        echo Html::a($text, $url, $options);
        ?>
    </div>
    <?php
    $url = Yii::$app->params["urlBack"] . $configuraciones["personajeImagen"]->getThumbUrl("medium");
    $options = [
        "alt" => Yii::t("app", "Fundación Omar Dengo"),
        "class" => "default-character character-left",
    ];
    echo Html::img($url, $options);
    ?> 
</div>
<?php
use yii\helpers\Html;
?>
<div class="w3-container">
    <h1 style="color:#00CC99;font-weight: bold;padding-top: 85px;"><?php echo Html::encode($this->title); ?></h1>
    <div class="w3-half w3-padding">   
        <div class="w3-panel w3-round-xlarge" style="background-color: #EBEDEC">
            <h2 style="color:#666666;font-weight: bold;padding-top:15px;padding-bottom: 15px"><?php echo Html::encode('¿Cómo buscar un recurso?'); ?></h2>   
            <h3 style="font-weight: bold;padding-bottom: 15px"><text style="color:#00CC99;"><?php echo Html::encode('Opción 1.'); ?></text> <text style="color:#666666;"><?php echo Html::encode('Buscador ubicado en pantalla de inicio'); ?></text></h3>  
            <div class="w3-padding">
                <ul class="a">     
                <h4 class="TextoDescripcion"><li><?php echo Html::encode('Escriba el nombre del recurso que desea buscar o seleccione una etiqueta de los recursos que busca.'); ?></li></h4>
                <h4 class="TextoDescripcion"><li><?php echo Html::encode('Seleccione buscar.'); ?></li></h4>
                </ul>
            </div>

            <h3 style="font-weight: bold;padding-bottom: 15px"><text style="color:#00CC99;"><?php echo Html::encode('Opción 2.'); ?></text> <text style="color:#666666;"><?php echo Html::encode('Búsqueda avanzada'); ?></text></h3>  
            <div class="w3-padding">
                <ul class="a">     
                <h4 class="TextoDescripcion"><li><?php echo Html::encode('En el menú derecho ingrese a la pestaña > buscar > Búsqueda Avanzada o ingrese desde el botón Búsqueda Avanzada ubicado en la parte inferior del buscador de la pantalla de inicio.'); ?></li></h4>
                    <h4 class="TextoDescripcion"><li><?php echo Html::encode('Seleccione las características del recurso que busca.'); ?></li></h4>  
                    <h4 class="TextoDescripcion"><li><?php echo Html::encode('Seleccione buscar.'); ?></li></h4>
                </ul>
            </div>  

            <h3 style="font-weight: bold;padding-bottom: 15px"><text style="color:#00CC99;"><?php echo Html::encode('Opción 3.'); ?></text> <text style="color:#666666;"><?php echo Html::encode('Más recientes'); ?></text></h3>  
            <div class="w3-padding">
                <ul class="a">     
                <h4 class="TextoDescripcion"><li><?php echo Html::encode('En el menú derecho ingrese a la pestaña > buscar > Más recientes, se listarán los recursos agregados más recientemente.'); ?></li></h4>
                </ul>
            </div>  
        </div>    
    </div>
    <div class="w3-half w3-padding">
        <div class="w3-panel w3-round-xlarge" style="background-color: #EBEDEC">
            <h2 style="color:#666666;font-weight: bold;padding-top:15px;padding-bottom: 15px"><?php echo Html::encode('¿Qué es un recurso?'); ?></h2>       
            <?php
                $text = "Videos, fotografías, audios, archivos, infográficos, links o cualquier documento que se comparta en el Banco de Recursos, disponibles a todos los usuarios.";
            ?>  
            <h4 class="TextoDescripcion"><?php echo Html::encode($text); ?></h4>
        </div>       
    </div>
</div>
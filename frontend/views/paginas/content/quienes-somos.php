<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="w3-container">  
    <h1 style="color:#00CC99;font-weight: bold;padding-top:85px;"><?php echo Html::encode($model->Titulo); ?></h1>
    <div class="w3-half w3-padding-small">
        <?php echo '<img style="width:100%" src="' . Yii::$app->params["urlBack"] . "/uploads/foto_FOD_Mesa_de_trabajo.png" . '">' ?>
    </div>
    <div class="w3-half w3-padding" style="color:#666666;">
        <p style="font-size:15px; text-align: left; color:#666666; padding-bottom: 25px; padding-right: 20px">	
            <?php echo str_replace("/uploads/", Yii::$app->params["urlBack"] . "/uploads/", HtmlPurifier::process('La Fundación Omar Dengo (FOD), es una organización sin fines de lucro, cuyo objetivo principal, es el desarrollo de las capacidades de las personas, por medio de propuestas educativas innovadoras, apoyadas en el aprovechamiento de nuevas tecnologías. <br><br>

            Desde su creación en 1987, la FOD de Costa Rica gesta y ejecuta proyectos nacionales y regionales en el campo del desarrollo humano, la innovación educativa y las nuevas tecnologías. Estas iniciativas, han contribuido en forma decisiva a entender el uso de las tecnologías en la educación, como instrumentos para ampliar las potencialidades y funcionalidades de las personas. <br><br>

            Desde este contexto nace el Banco de Recursos para la Educación Activa, un esfuerzo por crear espacios virtuales en los cuales la comunidad educativa nacional, tenga acceso a distintos recursos que contribuyan a un crecimiento integral como parte de su desarrollo profesional docente.
            <br><br>
            Las generaciones actuales demandan de los docentes y entidades relacionadas con la educación, estar a la vanguardia de los cambios que se desarrollan en su contexto social inmediato. Por ende, para instituciones como el PRONIE-MEP-FOD, es fundamental la generación de recursos que contribuyan a la mejora paulatina de los procesos de enseñanza y aprendizaje.')); ?>	
        </p>
    </div>    		
</div>
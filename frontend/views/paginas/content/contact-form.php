<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use common\widgets\Alert;
?>
<?php if (Yii::$app->user->can("autor")): ?>
    <?php
    $nombre = Yii::$app->user->identity->nombre;
    $correo = Yii::$app->user->identity->email;
    ?>
    <?php $form = ActiveForm::begin(['id' => 'contact-form']);
    ?>
    <?php echo $form->field($ContactForm, 'name')->hiddenInput(["value" => $nombre])->label(false); ?>
    <?php echo $form->field($ContactForm, 'email')->hiddenInput(["value" => $correo])->label(false); ?>

    <div class="round-form-group">
        <?php echo Alert::widget() ?>
        <div class="form-group lined-title">
            <?php if (!Yii::$app->user->isGuest): ?>
                <div class="round-ico-wrap">
                    <div class="round-ico small-ico">
                        <?php
                        $user = common\models\User::getUserById(Yii::$app->user->identity->id);
                        echo $user->getAvatarImg();
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php echo $form->field($ContactForm, 'subject')->textInput(["placeholder" => Yii::t("app", "Asunto *"), "class" => "form-control round-form-control"])->label(false); ?>
        </div>
        <div class="form-group">
            <?php echo $form->field($ContactForm, 'body')->textarea(['rows' => 6, "placeholder" => Yii::t("app", "Dejenos su consulta *"), "class" => "form-control round-form-control"])->label(false); ?>
        </div>
        <div class="form-group">
            <?php
            echo $form->field($ContactForm, 'verifyCode')->widget(Captcha::className(), [
                'captchaAction' => ['/site/captcha'],
                'template' => '<div class="row"><div class="col-xs-12 col-sm-3">{image}</div><div class="col-xs-12 col-sm-9">{input}</div></div>',
                'options' => ['placeholder' => 'Enter verification code', "class" => "form-control round-form-control"],
            ])->label(false)
            ?>
        </div>
        <?php echo Html::submitButton(Yii::t("app", "Enviar"), ['class' => 'btn btn-default btn-md', 'name' => 'contact-button']) ?>
    </div>
    <?php ActiveForm::end(); ?> 
<?php endif; ?>
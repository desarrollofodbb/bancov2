<?php

use yii\helpers\Html;
?>
<div class="w3-container">
    <h1 style="color:#00CC99;font-weight: bold;padding-top:85px;"><?php echo Html::encode('Sobre el Banco de Recursos'); ?></h1>    
    <div class="w3-half">
    <?php echo '<img style="width:100%" src="' . Yii::$app->params["urlBack"] . "/uploads/Imagen_sobre_banco_recursos_Mesa de trabajo.png" . '">' ?>
    </div>    
    <?php
    $text = 'El Banco de Recursos es una plataforma útil y eficaz que impulsa la búsqueda de recursos pedagógicos para todas las personas interesadas en el uso y la incorporación de las TIC en educación. Además, los usuarios encontrarán investigaciones, artículos, lineamientos y documentos relacionados con el manejo administrativo del Programa Nacional de Informática Educativa PRONIE MEP-FOD.

                El Banco provee recursos digitales que han sido sometidos a validaciones de expertos, de manera que cumplen con estándares determinados, asegurando un aporte de calidad para docentes, estudiantes, padres y madres de familia y usuarios en general.';
    ?>
    <div class="w3-half w3-padding" style="color:#666666;">
        <p style="font-size:15px; text-align: left; color:#666666; padding-bottom:15px"><?php echo Html::encode($text); ?></p>
        <div class="w3-panel w3-round-xlarge" style="background-color: #EBEDEC">
            <h1 style="color:#00CC99;font-weight: bold;padding-top:15px;padding-bottom: 10px"><?php echo Html::encode('Misión'); ?></h1>       
            <?php
            $text = "Contribuir a la divulgación de recursos pedagógicos que incorporan el uso de las tecnologías para el aprendizaje activo y el acceso a documentación administrativa pertinente al PRONIE MEP-FOD para toda la comunidad educativa nacional y otros grupos de interés.";
            ?>  
            <p style="font-size:15px; text-align: left; color:#666666; padding-bottom:15px"><?php echo Html::encode($text); ?></p>
        </div>
    </div>
</div>

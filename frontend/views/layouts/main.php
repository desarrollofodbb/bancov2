<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\components\AutoLoginFODComponent;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-MBJ5W87YPV"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-MBJ5W87YPV');
        </script>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <?php
        echo $this->render("../includes/header", ["configuraciones_generales" => $this->params["configuraciones_generales"]]);
        AutoLoginFODComponent::validateFodSession();
        ?>
        <!-- BODY -->
        <div class="body-wrap"> 
            <?php
            echo $this->render("../includes/side_menu", ["configuraciones_generales" => $this->params["configuraciones_generales"]]);
            ?>

                <?php echo $content ?>

            <?php
            echo $this->render("../includes/footer", ["configuraciones_generales" => $this->params["configuraciones_generales"]]);
            ?>
            <?php 
            $this->registerJsFile('@web/js/fingerprint.js');
            $this->registerJsFile('@web/js/FODSesion.js'
        );?>
        </div>
        <!-- BODY -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

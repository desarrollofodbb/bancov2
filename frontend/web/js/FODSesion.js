/* 
 * Archivo de control para administracion de galletas y sesion del FOD
 */

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function hash() {
  return '_' + Math.random().toString(36).substr(2, 9);
};

//esta galleta no es la de iniciar sesion
//Esta lo que hace es aplicar los algoritmos de FOD para formar la sesion final
var cookieFP = getCookie('FODSessionKey');
var fp1 = new Fingerprint();    
var cookieValue = fp1.get() + "-" + hash();

var date = new Date();        
date.setTime(+ date + (1 * 86400000));
document.cookie = "FODSessionKey=" + cookieValue + ';expires='+ date.toGMTString() + ";path=/"; 
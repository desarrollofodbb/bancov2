var urlSistema=null;
var nacional = "1";
var sexo = "1";

var reg = 0;
var per = 0;
var usu = 0;
var reside= 0;

$(document).ready(function () {
  //  restaurar("1");
  var path = window.location.pathname;
  var page = path.split("/").pop();
  if(page=="crear-perfil"){
    cargarPaises();
    cargarProvincias();
    cargarProfesiones();
    cargarCE();
    cargarMaterias();
    ocultar();
  }


});

$("#cedula").keyup(function (event) {
    if (document.getElementById("personanac").checked) {
        verificarCedula();
    }
});

$("#email").blur(function(){
  email = $("#email").val();
  existeCorreo(email,"si");
});

function mostrarDenunciar(){
  var x = document.getElementById("modalDenuncia");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function encode_utf8( s )
{
  return unescape( encodeURIComponent( s ) );
}

function decode_utf8( s )
{
  return decodeURIComponent( escape( s ) );
}

function nacionalidad(val) {
    restaurar(val);
}

function restaurar(nac) {

    nacional = nac;
    if (nac === "1") {
        $("#nacional").prop("checked", true);
        $("#rowNombre").hide();
        $("#rowCorreo").hide();
        $("#wellIdentificacion").hide();
        $("#wellOtros").hide();
        $("#rowBotones").hide();
        $("#btn-cedula").show();

        $("#sexoE").hide();
        $("#sexoN").show();

        //****//

        $("#nombre").prop("readonly", true);
        $("#apellido1").prop("readonly", true);
        $("#apellido2").prop("readonly", true);
        $("#fecha").prop("readonly", true);
        $("#sexoM").prop("readonly", true);
        $("#sexoF").prop("readonly", true);
        $("#fecha").prop("readonly", true);

    } else {
        $("#extranjero").prop("checked", true);
        cargarPaises();
        $("#rowNombre").show();
        $("#rowCorreo").show();
        $("#wellIdentificacion").show();
        $("#wellOtros").show();
        $("#rowBotones").show();
        $("#btn-cedula").hide();

        $("#sexoE").show();
        $("#sexoN").hide();

        //****//

        $("#nombre").prop("readonly", false);
        $("#apellido1").prop("readonly", false);
        $("#apellido2").prop("readonly", false);
        $("#fecha").prop("readonly", false);
    }
    $("#sexoM").prop("checked", true);

    cargarProvincias();
    cargarCantones(0);
    cargarDistritos(0);

    limpiar();
}

function limpiar() {
    $("#cedula").val("");
    $("#nombre").val("");
    $("#apellido1").val("");
    $("#apellido2").val("");
    $("#fecha").val("");
    $("#email").val("");
    $("#telefono").val("");
    $("#direccion").val("");

    $("#clave").val("");
    $("#confirmaClave").val("");
    $("#ce_provincia").val("");
    $("#ce_canton").val("");
    $("#ce_distrito").val("");
    $("#direccion").html("");
    $('#recursos-ocupacion').val('').trigger("change");
    $('#recursos-centroeducativo').val('').trigger("change");
}

function mostrar() {
    $("#rowNombre").show();
    $("#rowCorreo").show();
    $("#wellIdentificacion").show();
    $("#wellOtros").show();
    $("#rowBotones").show();
    $("#mensajeErrorCedula").html("");
}

function ocultar() {

  $("#div-nombre").hide();
  $("#div-apellido1").hide();        
  $("#div-apellido2").hide();
  $("#div-fecha").hide();
  $("#div-genero").hide();        
  $("#div-email").hide();   
  $("#div-telefono").hide();   
  $("#div-ocupacion").hide();   
  $("#div-pais").hide();
  $("#div-provincia").hide();
  $("#div-canton").hide();
  $("#div-distrito").hide();
  $("#div-direccion").hide();
  $("#div-ce_encabezado").hide();
  $("#div-centroeducativo").hide();
  $("#div-ce_direccion").hide();
  $("#div-materias").hide();
  $("#div-menor").hide();
  $("#div-clave").hide();
  $("#div-confirmaClave").hide(); 
  $("#but-registrarse").hide();
}

function cargarLista(obj, dat) {
    // $(obj).html("");   

    if(obj=="pais"){
      $("#recursos-pais").empty();

      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].value, dat[i].key, false, false);     
        $('#recursos-pais').append(newOption).trigger('change');   
      }
      if(document.getElementById("personanac").checked){
        $('#recursos-pais').val("CRC").trigger('change');     
      }      
    }
    if(obj=="provincia"){
      $("#recursos-provincia").empty();

      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].value, dat[i].key, false, false);  
        $('#recursos-provincia').append(newOption).trigger('change');   
      }   
    }
    if(obj=="canton"){
      $("#recursos-canton").empty();

      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].value, dat[i].key, false, false);  
        $('#recursos-canton').append(newOption).trigger('change');   
      }   
    }     
    if(obj=="distrito"){
      $("#recursos-distrito").empty();

      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].value, dat[i].key, false, false);  
        $('#recursos-distrito').append(newOption).trigger('change');   
      }   
    }   
    if(obj=="ocupacion"){
      $("#recursos-ocupacion").empty();
      bandera = 1;
      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].Nombre, dat[i].idProfesion, false, false);  
        $('#recursos-ocupacion').append(newOption).trigger('change');   
      }   
      $('#recursos-ocupacion').val('').trigger("change");
      bandera = 0;
    }    
    if(obj=="ce"){
      $("#recursos-centroeducativo").empty();
      bandera = 1;
      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].Nombre, dat[i].ID, false, false);  
        $('#recursos-centroeducativo').append(newOption).trigger('change');   
      }   
      $('#recursos-centroeducativo').val('').trigger("change");
      bandera = 0;
    }     
    if(obj=="materia"){
      $("#recursos-materia_f").empty();

      for (i = 0; i < dat.length; i++) {
        var newOption = new Option(dat[i].Nombre, dat[i].idValoresdeCaracteristicas, false, false);  
        $('#recursos-materia_f').append(newOption).trigger('change');   
      }   
      $('#recursos-materia_f').val('').trigger("change");
    }           
}

function cargarUbicacion(valor){

    if(valor==='1'){
        $("#divNacional").show(); 
         $("#btnRegistro").removeAttr('disabled');
         reside=1;
    }
    else{
       $("#divNacional").hide();
       $("#btnRegistro").removeAttr('disabled');
       $("#provincia").val("8");
       $("#canton").val("82");
       $("#distrito").val("486");
       $("#direccion").val("No reside en CR");
       reside=2;
    }
}

function cargarPaises() {

    $.ajax({
        url: URLAPI + "/pais/getJSon/",
        dataType: "json",
        success: function (dat) {
         
            cargarLista("pais", dat);
        }
    });
}

/**
 * 
 * @returns {undefined}
 */
function cargarProvincias() {

  url = URLAPI + "/provincia/getJSon/";  
  $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {
      cargarLista("provincia", dat);
      cargarCantones(dat[0].key);
    }
  });
}

/**
 * 
 * @returns {undefined}
 */
function cargarProfesiones() {

  url = URLAPI + "utilidades/getProfesiones";  
  $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {
      cargarLista("ocupacion", dat);
    }
  });
}


/**
 * 
 * @returns {undefined}
 */
function existeCorreo(email,mensaje) {

  url = URLAPI + "correo/existe/" + email;  
  $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {

      if(dat.estado === "1"){     
        if(mensaje==="si"){
          document.getElementById("msgmail").innerHTML = "Correo ya está registrado, favor utilizar otro";
          $("#errorEmailEx").show();            
        }
        else{
          return true;
        }
      }
      else{
        $("#errorEmailEx").hide(); 
        return false;
      }
    }
  });
}

/**
 * 
 * @returns {undefined}
 */
function cargarCE() {

  url = URLAPI + "utilidades/getCE";  
  $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {
      cargarLista("ce", dat);
    }
  });
}

/**
 * 
 * @returns {undefined}
 */
function cargarMaterias() {

  url = URLAPI + "utilidades/getMateriasBR";  
  $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {
      cargarLista("materia", dat);
    }
  });
}

function showMT(){
  desc = document.getElementById("select2-recursos-ocupacion-container").innerHTML;
  docente = desc.substring(0, 7);
  estudiante = desc.substring(0, 24);
  if(docente=="Docente" || estudiante=="Estudiante Universitario"){ 
    $('#recursos-materia_f').val('').trigger("change"); 
    $("#div-materias").show();
  }
  else{
    $("#div-materias").hide();
  }
}

function showCE(){

  desc = document.getElementById("select2-recursos-ocupacion-container").innerHTML;
  
  docente = desc.substring(0, 7);
  estudiante = desc.substring(0, 24);
  if(docente=="Docente" || estudiante=="Estudiante Universitario"){
    $('#recursos-centroeducativo').val('').trigger("change");
    $('#recursos-materia_f').val('').trigger("change");
    $('#ce_provincia').val('');
    $('#ce_canton').val('');
    $('#ce_distrito').val('');
    $("#div-ce_encabezado").show();
    $("#div-centroeducativo").show();    
    $("#div-ce_direccion").show();
    $("#ce_publico").prop("checked", true);
    $("#ce_privado").prop("checked", false);
    $("#ce_otro").prop("checked", false);
  }
  else{
    $("#div-ce_encabezado").hide();
    $("#div-centroeducativo").hide();
    $("#div-ce_direccion").hide();
  }
}
/**
 * 
 * @returns {undefined}
 */
function cargarCEUbicacion(id) {
  if(id==""){
    id=0;
    }
    url = URLAPI + "utilidades/getCEUbicacion/" + id;
    $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {
      if(dat[0] !== undefined){
        $("#ce_provincia").val(dat[0].Provincia);
        $("#ce_canton").val(dat[0].Canton);
        $("#ce_distrito").val(dat[0].Distrito);
      }
    }
    });
}

/**
 * 
 * @param {type} prv
 * @returns {undefined}
 */
function cargarCantones(prv) {

  url = URLAPI + "/canton/getJSon/" + prv;

  $.ajax({
    url: url,
    dataType: "json",
    success: function (dat) {
      if(dat.length > 0) {
        // $("#canton").html("");      
        
        // for (i = 0; i < dat.length; i++) {
        //   opt = "<option value='" + dat[i]["key"] + "'>" + dat[i]["value"] + "</option>";
        //   $("#canton").append(opt);
        // }
        cargarLista("canton", dat);
        // cargarDistritos(dat[0].key);
      }
    }
  });
}
/**
 * 
 * @param {type} cnt
 * @returns {undefined}
 */
function cargarDistritos(cnt) {

    $.ajax({
        url: URLAPI + "/distrito/getJSon/" + cnt,
        dataType: "json",
        success: function (dat) {
            cargarLista("distrito", dat);
        }
    });
}

/**
 * 
 * @returns {undefined}
 */
function verificarCedula() {

  cedula = $("#cedula").val();

  msj = '<img src="public/assets/img/loading2.gif" style="width: 30px;"> <b>Verificando información en Registro Civil</b>';
  $("#mensajeErrorCedula").html(msj);
  if ((/^\d{9}$/.test(cedula))) { 
    reg = 0;
    per = 0;
    usu = 0;
    $.ajax({
      url: URLAPI + "/registrocivil/getByCedula/" + cedula,
      dataType: "json",
      success: function (dat) {
        if (dat.existe > 0) {
          reg = dat.datos;
          $.ajax({
            url: URLAPI + "/persona/getByCedula/" + cedula,
            dataType: "json",
            success: function (dat) {       
              if (dat.existe > 0) {            
                per = dat.datos;
                $.ajax({
                  url: URLAPI + "/usuario/getByPersona/" + per.idPersona,
                  dataType: "json",
                  success: function (dat) {
                    if (dat.existe > 0) {
                      usu = dat.datos;
                      msj = '<div class="alert alert-warning"><i class="fa fa-warning"></i> La cédula que se esta indicando ya esta relacionada a un usuario en nuestra base de datos. Para recuperar los datos de inicio de sesión de clic en el siguiente enlace <a href="http://aplicaciones.fod.ac.cr/perfilFOD/recuperar">Recuperar cuenta</a></div>';
                      $("#mensajeErrorCedula").html(msj);
                      ocultar();
                    } else {
                      mostrar();
                      cargarDatos(reg.nombre, reg.apellido1, reg.apellido2, reg.fechaNacimiento, per.CorreoElectronico, reg.sexo, per.poblado, per.Direccion, per.idDistrito);
                      $("#mensajeErrorCedula").html("");
                      // $("#div-ce_encabezado").hide();
                      // $("#div-centroeducativo").hide();
                      // $("#div-ce_direccion").hide();
                      $("#div-materias").hide();
                    }
                  }
                });
              } else {               
                mostrar();
                cargarDatos(reg.nombre, reg.apellido1, reg.apellido2, reg.fechaNacimiento, '', reg.sexo, '', '', 0);
                $("#mensajeErrorCedula").html("");
                // $("#div-ce_encabezado").hide();
                // $("#div-centroeducativo").hide();
                // $("#div-ce_direccion").hide();
                $("#div-materias").hide();
              }
            }
          });
        } else {           
          document.getElementById("menor").disabled = false;
          document.getElementById("nombre").disabled = false;
          document.getElementById("apellido1").disabled = false;
          document.getElementById("apellido2").disabled = false;
          document.getElementById("fecha").disabled = false;
          document.getElementById("recursos-genero").disabled = false;

          msj = '<div class="alert alert-warning"><i class="fa fa-warning"></i> La cédula que se esta indicando no se encuentra en la base de datos del Registro Civil. Por favor verfique la cédula.</div>';
          $("#mensajeErrorCedula").html(msj);
          ocultar();
        }
      }
    });
  } else {
    msj = '<div class="alert alert-info"><i class="fa fa-warning"></i> El formato de la cédula debe ser de 9 dígitos continuos (con los ceros) y sin guiones</div>';
    $("#mensajeErrorCedula").html(msj);
    ocultar();
  }
}
/**
 * 
 * @param {type} sex
 * @returns {undefined}
 */
function cargarSexo(sex) {
    sexo = sex;
}

function diff_years(dt2, dt1) 
 {

  var diff =(dt2 - dt1) / 1000;
   diff /= (60 * 60 * 24);
  return Math.abs(Math.round(diff/365.25));
   
 }

/**
 * 
 * @param {type} nombre
 * @param {type} apellido1
 * @param {type} apellido2
 * @param {type} fecha
 * @param {type} correo
 * @param {type} sexo
 * @param {type} poblado
 * @param {type} direccion
 * @param {type} distrito
 * @returns {undefined}
 */
function cargarDatos(nombre, apellido1, apellido2, fecha, correo, sexo, poblado, direccion, distrito) {
  var today = Date.now();
 
  var d1 = new Date(fecha);
  var yearsDiff = diff_years(today,d1);
  $("#div-nombre").show();
  $("#div-apellido1").show();        
  $("#div-apellido2").show();
  $("#div-fecha").show();
  $("#div-genero").show();        
  $("#div-email").show();   
  $("#div-telefono").show();   
  $("#div-ocupacion").show();   
  $("#div-pais").show();
  $("#div-provincia").show();
  $("#div-canton").show();
  $("#div-distrito").show();
  $("#div-direccion").show();
  $("#div-ce_encabezado").hide();
  $("#div-centroeducativo").hide();
  $("#div-ce_direccion").hide();
  $("#div-materias").show();    
  $("#div-menor").show();
  $("#div-clave").show();
  $("#div-confirmaClave").show();
  $("#but-registrarse").show();     
  document.getElementById("recursos-pais").disabled = true; 
 

  if(yearsDiff < 18){
    document.getElementById("menor").checked = true;
    document.getElementById("menor").disabled = true;
    $("#div-ce_encabezado").hide();    
    $("#div-ce_direccion").hide();    
    $("#div-centroeducativo").show();   
  }
  else{
    document.getElementById("menor").checked = false;  
    document.getElementById("menor").disabled = true;  
  }
  $('#menor').trigger('change'); 

  $("#recursos-genero").val(sexo);
  document.getElementById("recursos-genero").disabled = true;
  $('#recursos-genero').trigger('change');
   // if (sexo === "1") {
   //   $("#recursos-genero").val("Masculino");
   // } else {
   //   $("#recursos-genero").val("Femenino");
   // }

  $("#nombre").val(decode_utf8(nombre));
  document.getElementById("nombre").disabled = true;
  $("#apellido1").val(decode_utf8(apellido1));
  document.getElementById("apellido1").disabled = true;
  $("#apellido2").val(decode_utf8(apellido2));
  document.getElementById("apellido2").disabled = true;
  $("#fecha").val(fecha);
  document.getElementById("fecha").disabled = true;
  $("#email").val(correo);

  $("#poblado").val(poblado);
  $("#direccion").html(direccion);

  if (distrito > 0) {
    $.ajax({
      url: URLAPI + "/distrito/getCanton/" + distrito,
      dataType: "json",
      success: function (dat) {
        $("#provincia").val(dat.idProvincia);
        cargarCantones(dat.idProvincia);
        $("#canton").val(dat.idCanton);
        cargarDistritos(dat.idCanton);
        $("#distrito").val(dat.idDistrito);
      }
    });
  }
}

function isValidClave(str) {

  var code, i, len;
  var letras = false;
  var especiales = false;
  var strpermitido = "*%+!$&()?";
  var totvalidchars = 0;

  for (i = 0, len = str.length; i < len; i++) {
    code = str.charCodeAt(i);
    caracter = str.charAt(i);
    if ((code > 47 && code < 58) || // numeric (0-9)
        (code > 64 && code < 91) || // upper alpha (A-Z)
        (code > 96 && code < 123)) { // lower alpha (a-z)
      totvalidchars++;
      letras = true;
    }
    var n = strpermitido.includes(caracter);
    if (n == true) {   
      especiales = true;
      totvalidchars++;
    }    
  }
  if(totvalidchars = str.length && letras == true && especiales == true){
    return true;
  }
  else{
    return false;
  }
};

/**
 * 
 * @returns {undefined}
 */
function validarFormulario() {
    pasoOK = true;
    if (($("#cedula").val() == "") || ($("#cedula").val().length <= 5)) {
      $("#errorIDEx").show();
      pasoOK = false;
    } else {
      $("#errorIDEx").hide();
    }

    email = $("#email").val();
    if (!(/^[a-z|0-9|A-Z]*([_][a-z|0-9|A-Z]+)*([.][a-z|0-9|A-Z]+)*([.][a-z|0-9|A-Z]+)*(([_][a-z|0-9|A-Z]+)*)?@[a-z][a-z|0-9|A-Z]*\.([a-z][a-z|0-9|A-Z]*(\.[a-z][a-z|0-9|A-Z]*)?)$/.test(email))) {
        // document.getElementById("errorEmailGrl").className = "has-error";
      document.getElementById("msgmail").innerHTML = "Revise el formato del correo";
      $("#errorEmailEx").show();
      pasoOK = false;
    } else {
      if(existeCorreo(email,"no")){
        document.getElementById("msgmail").innerHTML = "Correo ya está registrado, favor utilizar otro";
        $("#errorEmailEx").show();            
        pasoOK = false;
      }
      else{
        $("#errorEmailEx").hide();          
      }
        // document.getElementById("errorEmailGrl").className = "";
    }

    clave = $("#clave").val();
    confirmaClave = $("#confirmaClave").val();

    if ((clave == "") || (clave.length < 8)) {
      document.getElementById("msgclave").innerHTML = "Clave debe tener al menos 8 caracteres";
      $("#errorClave").show();
      pasoOK = false;
    } else {
      $("#errorClave").hide();
    }

    if (clave != confirmaClave) {
      $("#errorClaveConfirm").show();
        pasoOK = false;
    } else {
      $("#errorClaveConfirm").hide();
    } 
    if(pasoOK){
      if(isValidClave(clave)){
        pasoOK = true;
        $("#errorClave").hide();      
      }
      else{
        document.getElementById("msgclave").innerHTML = "La contraseña debe tener mayúsculas, minúsculas, números y al menos uno de estos caracteres *%+!$&()?";
        $("#errorClave").show();
        pasoOK = false;      
      }      
    }

    if (pasoOK) {
      registrar(clave);
    }
}
/**
 * 
 * @param {type} clave
 * @returns {undefined}
 */
function registrar(clave) {
  $nacionalidad = 1;
  if (document.getElementById("personanac").checked == false) {
    $nacionalidad = 0;
  }

  $publico = 1;
  if (document.getElementById("ce_privado").checked == true) {
    $publico = 2;
  }
  if (document.getElementById("ce_otro").checked == true) {
    $publico = 3;
  }

  if (reg) {
    if (per) {
      actualizarPersona(per.idPersona, reg.cedula, reg.nombre, reg.apellido1, reg.apellido2, $("#email").val(), reg.sexo, reg.fechaNacimiento, $("#recursos-distrito").val(), "ocupacion-" + $("#recursos-ocupacion").val() + "-" + $nacionalidad + "-" + $publico + "-" + $("#recursos-centroeducativo").val(), $("#direccion").val(), clave);
    } else {
      actualizarPersona(0, reg.cedula, reg.nombre, reg.apellido1, reg.apellido2, $("#email").val(), reg.sexo, reg.fechaNacimiento, $("#recursos-distrito").val(), "ocupacion-" + $("#recursos-ocupacion").val() + "-" + $nacionalidad + "-" + $publico + "-" + $("#recursos-centroeducativo").val(), $("#direccion").val(), clave);
    }
  } else {
    if (document.getElementById("personanac").checked == false) {
      actualizarPersona(0, $("#recursos-pais").val() + '-' + $("#cedula").val(), $("#nombre").val(), $("#apellido1").val(), $("#apellido2").val(), $("#email").val(), sexo, $("#fecha").val(), $("#recursos-distrito").val(), "ocupacion-" + $("#recursos-ocupacion").val() + "-" + $nacionalidad + "-" + $publico + "-" + $("#recursos-centroeducativo").val() , $("#direccion").val(), clave);
    }
    else{
      alert("No se puede guardar registro.  Verifique!");
    }
  }
}
/**
 * 
 * @param {type} persona
 * @param {type} clave
 * @returns {undefined}
 */
function registrarUsuario(persona, clave) {
  if(urlSistema===null) {
    urlWS = URLAPI + "usuarioporconfirmar/registrarbr/" + persona + "/" + clave;
    
    $.ajax({
      url: urlWS,
      dataType: "text",
      success: function (dat) {
console.log(dat);
        document.getElementById('spinner').style.display='none';
        document.getElementById('confirmar').style.display='block';
        // $("#confirmar").on('hidden.bs.modal', function (e) {
        //   window.open(URL+"/correo", "_self");
        // });
      }
    }).done(function () {

      // document.getElementById('spinner').style.display='none';
    });
  }
  else{
    $.ajax({
      url: URLAPI + "/usuarioporconfirmar/registrarFOD/" + persona + "/" + clave + "/"+ urlSistema,
      dataType: "text",
      success: function (dat) {
        document.getElementById('spinner').style.display='none';
        document.getElementById('confirmar').style.display='block';

        // $("#confirmar").on('hidden.bs.modal', function (e) {
        //   window.open(URL+"/correo", "_self");
        // });
      }
    }).done(function () {
      document.getElementById('spinner').style.display='none';
    });
  }
}
/**
 * 
 * @param {type} id
 * @param {type} cedula
 * @param {type} nombre
 * @param {type} apellido1
 * @param {type} apellido2
 * @param {type} correo
 * @param {type} sexo
 * @param {type} nacimiento
 * @param {type} distrito
 * @param {type} poblado
 * @param {type} direccion
 * @param {type} clave
 * @returns {undefined}
 */
function actualizarPersona(id, cedula, nombre, apellido1, apellido2, correo, sexo, nacimiento, distrito, poblado, direccion, clave) {
  $("#btnRegistro").attr('disabled',true);
  nombre = nombre.trim();
  // nombre = decode_utf8(nombre);
  apellido1 = apellido1.trim();
  // apellido1 = decode_utf8(apellido1);
  apellido2 = apellido2.trim();
  // apellido2 = decode_utf8(apellido2);
  correo=correo.trim();
  

  if (poblado === ''){
    poblado = 'ND';
  }

  if (direccion === '')
      direccion = 'ND';
  if (apellido2 === '')
      apellido2 = '-';
  nacimiento = nacimiento.replace(new RegExp('/', 'g'),'-');
  urlWS = URLAPI + "persona/registrar/" + id + "/" + cedula + "/" + nombre + "/" + apellido1 + "/" + apellido2 + "/" + correo + "/" + sexo + "/" + nacimiento + "/" + distrito + "/" + poblado + "/" + direccion;  
  
  document.getElementById('spinner').style.display='block';
  $.ajax({
    url: urlWS,
    dataType: "json",
    success: function (dat) {
      registrarUsuario(dat.id, clave);
    }
  });
//actualizarTelefonoPrincipal($idPersona, $telefonoPrincipal)  
}

<?php

use kartik\helpers\Enum;

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Banco de Recursos',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
//            'name' => Enum::getBrowser()["version"],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                "/contactenos" => "paginas/paginas/contactenos",
                "/<slug>" => "paginas/paginas/slug",
            ],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'debug/*',
            'site/*',
            'recursos/recursos/resultados-de-busqueda',
            'recursos/recursos/tags',
            'recursos/recursos/ver-recurso',
            'recursos/recursos/mas-recientes',
            'recursos/colecciones/crear-coleccion',
            'recursos/colecciones/elimina-coleccion',
            'paginas/paginas/*',
            'recursos/recursos/autor-favorito',
            'recursos/recursos/delete-file',
            'recursos/recursos/recurso-coleccion',
            'recursos/recursos/crear-coleccion',
            'recursos/recursos/elimina-coleccion',
            'recursos/recursos/count-eventos',
            'recursos/recursos/view-eventos',
            'recursos/recursos/last-eventos', 
            'recursos/recursos/view-busquedas',
            'recursos/recursos/collection-list',
            'gii/*',            
        // The actions listed here will be allowed to everyone including guests.
        // So, 'admin/*' should not appear here in the production, of course.
        // But in the earlier stages of your development, you may probably want to
        // add a lot of actions here until you finally completed setting up rbac,
        // otherwise you may not even take a first step.
        ]
    ],
    'params' => $params,
];

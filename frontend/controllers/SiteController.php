<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\helpers\Json;
use common\models\Configuraciones;
use kartik\helpers\Enum;
use common\models\User;
use common\modules\reportes\models\Historial;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

        /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings());

        $this->view->params['configuraciones_generales'] = $configuraciones;
                
        return $this->render('index', ["configuraciones" => $configuraciones]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $configuraciones = array_merge(Configuraciones::generalSettings(), Configuraciones::personajeSettings());

        $this->view->params['configuraciones_generales'] = $configuraciones;

        $model = new LoginForm();

        $post_data = Yii::$app->request->post();

        if ($model->load($post_data)) {                 //validate post request
            if ($this->validateLogInFod($post_data)) {  //validate fod login              
                if (!$model->validate()) {
                    Yii::$app->session->setFlash("error", "Los credenciales son incorrectos, o el usuario está inactivo en la plataforma, si cree que sus credenciales son correctos, por favor pongase en contacto con un administrador del sistema");
                    $nuevo_login = new Historial();
                    $nuevo_login->save();
                    return $this->goHome();
                }
                if (!$model->login()) {
                    $nuevo_login = new Historial();
                    $nuevo_login->save();
                    return $this->goHome();
                }
                
                $nuevo_login = new Historial();
                $nuevo_login->save();
                                
                return $this->redirect('index');
            } else {
                $nuevo_login = new Historial();
                $nuevo_login->save();
                Yii::$app->session->setFlash("error", "Los credenciales son incorrectos, por favor verifique nuevamente");
                return $this->goHome();
            }
        }
        
        return $this->redirect('index');
    }
    
    /**
     * Crear la galleta de sesion que sirve para ingresar de forma automática a a otros sistemas del FOD
     * Forma el valor con dos hash del cliente y la ip de origen.
     */
    protected function setCookieSession(){        
        $cookie =  explode('-', $_COOKIE['FODSessionKey']); //Separa las variables que se forman en el cliente con la libreria de FOD        
        $valor =  
            $cookie[0].'-'.                             //FOD Code js
            Yii::$app->getRequest()->getUserIP().'-'.   //IP actual del cliente
            $cookie[1];                                 //Codigo Hash FOD Code js  
        
        setcookie(
            'idFODSessionKey',                          //Nombre de la galleta
            $valor,
            time()+3600,
                '/'
            );                               //La Galleta expira en una hora
        return $valor;
    }
    
   

    





    protected function validateLogInFod($post_data) {
        $IsLogin = false;
        $estado = false;
        $personaIsActive = false;
        $user = null;
        $user_agent = Enum::getBrowser();

        $cookie=  $this->setCookieSession(); 
        
        //1- Get del servicio 

        $urlws = Yii::$app->params["urlWS"] 
                . "/usuario/login/" . $post_data["LoginForm"]["username"]
                . '/' . $post_data["LoginForm"]["password"]
                . '/' . Yii::$app->getRequest()->getUserIP()
                . '/1/' . $user_agent['code']
                . '/' . Yii::$app->params['idSistemaFOD']
                . '/' . $cookie;
   
        $ch = curl_init($urlws);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);


        $resultadologin = json_decode($result);
     
        if(!isset($resultadologin->persona->Nombre)){
            $IsLogin = false;
            return $IsLogin;
        }       
        $nombreCompleto = $resultadologin->persona->Nombre . " " . $resultadologin->persona->PrimerApellido . " " . $resultadologin->persona->SegundoApellido;
        $idPersona = $resultadologin->persona->idPersona;
        $estado = $resultadologin->estado;
        $personaIsActive = $resultadologin->persona->estado;
        $username = $resultadologin->usuario->username;
        $email = $resultadologin->persona->CorreoElectronico;
        $poblado = $resultadologin->persona->poblado;
        $ocupacion = 0;
        $nacionalidad = "";
        $publico = 0;
        $centroeducativo = 0;
        $esmenor = 0;
        $arr = explode("-",$poblado);  
        

        if(count($arr) == 6 && $arr[0]=="ocupacion"){
          $ocupacion = $arr[1];
          $nacionalidad = $arr[2];
          $publico = $arr[3];
          $centroeducativo = $arr[4];
          $esmenor = $arr[5];
        }        

        // $userLoginRespose = Yii::$app->params["api"] .
        //         'usuario/login/' . $post_data["LoginForm"]["username"]
        //         . '/' . $post_data["LoginForm"]["password"]
        //         . '/' . Yii::$app->getRequest()->getUserIP()
        //         . '/1/' . $user_agent['code']
        //         . '/' . Yii::$app->params['idSistemaFOD']
        //         . '/' . $cookie;
       
        // $jsoncall = @file_get_contents($userLoginRespose);
        // $jsonResponse = $jsoncall ? Json::decode($jsoncall) : false;    
        
        //2- Si el servicio es true validar si el usuario esta activo
        // if ($jsonResponse) {
        //     if (isset($jsonResponse["estado"]) && ($jsonResponse["estado"])) {
        //         $estado = true;
        //     }
        //     if (isset($jsonResponse["persona"]["estado"]) && ($jsonResponse["persona"]["estado"])) {
        //         $personaIsActive = true;
        //     }
        // }

        if ($estado) {
            $user = User::findOne(["username" => $username]);
            $IsLogin = true;
            if(!$user){ 
                // el usuario si existe en fod pero no en el sistema de banca (actual)
                // se debe proceder a un registro de usuario con un rol por defecto
                $user = new User();
                // $user->username = $jsonResponse["usuario"]["username"];
                // $user->email = $jsonResponse["persona"]["CorreoElectronico"];
                $user->nombre = $nombreCompleto;
                $user->username = $username;
                $user->email = $email;  
                $user->idProfesion = $ocupacion;
                $user->nacionalidad = $nacionalidad;
                $user->tipoCentro = $publico;
                $user->esmenor = $esmenor;
                $user->idCentroEducativo = $centroeducativo;
                $user->setPassword($post_data["LoginForm"]["password"]); //assign the correct password to the user
                $user->generateAuthKey();
                $user->generatePasswordResetToken();
                if( !$user->save() ){                              
                    $user = null;
                    $IsLogin = false;
                    Yii::$app->session->setFlash("error", 'Los datos suministrados por FOD para este usuario están incompletos, favor comicarse con la administración de la herramienta');
                }else{
                    $auth = Yii::$app->authManager;
                    $authorRole = $auth->getRole('autor');
                    $auth->assign($authorRole, $user->getId());
                }
            }
        }
        
//        2.1 Si la persona esta inactiva actualizar en la base de datos local como inactiva
        if ($estado && $user && !$personaIsActive) {
//            $user->status = User::STATUS_DELETED;
//            $user->save();
        }

        if ($estado && $user) {//when user is created or not always the data is updated
            $user->foto = $resultadologin->persona->imagenUsuario;
            $user->nombre = $nombreCompleto;
            $user->setPassword($post_data["LoginForm"]["password"]); //assign the correct password to the user
            $user->generateAuthKey();
            $user->generatePasswordResetToken();
            $user->save();
        }

        return $IsLogin;
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        if (Yii::$app->user->logout()) {
            $userCerrarSesionRespose = Yii::$app->params["api"] . 'usuario/cerrarSesion/';
            if(isset($_COOKIE['idFODSessionKey'])){        
            $userCerrarSesionRespose = Yii::$app->params["api"] .
                    'usuario/cerrarSesion/' . $_COOKIE['idFODSessionKey'];
            }
            $request = @file_get_contents($userCerrarSesionRespose);
        }

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}

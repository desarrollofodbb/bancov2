<?php

namespace backend\modules\caracteristicas\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\caracteristicas\models\Caracteristicas;

/**
 * CaracteristicasSearch represents the model behind the search form of `backend\modules\caracteristicas\models\Caracteristicas`.
 */
class CaracteristicasSearch extends Caracteristicas {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [['idCaracteristicas'], 'integer'],
//            [['Nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Caracteristicas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idCaracteristicas' => $this->idCaracteristicas,
        ]);

        $query->andFilterWhere(['like', 'Nombre', $this->Nombre]);

        $query->orderBy(['Posicion' => SORT_ASC]);

        return $dataProvider;
    }

}

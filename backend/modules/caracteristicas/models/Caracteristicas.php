<?php

namespace backend\modules\caracteristicas\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\position\PositionBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "TCaracteristicas".
 *
 * @property integer $idCaracteristicas
 * @property string $Nombre
 * @property string $Slug
 *
 * @property ValoresDeCaracteristicas[] $slidersItems
 */
class Caracteristicas extends \yii\db\ActiveRecord {

    const SELECCION_SIMPLE = 0;
    const SELECCION_MULTIPLE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TCaracteristicas';
    }

    public function behaviors() {
        return [
            //            SluggableBehavior automatically fills the specified attribute with a value that can be used a slug in a URL.
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'Nombre',
                'slugAttribute' => 'Slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
//            TimestampBehavior automatically fills the specified attributes with the current timestamp.
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['CreadoEn', 'ActualizadoEn'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ActualizadoEn'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('GETDATE()'), //for sql server database
//                'value' => new Expression('NOW()'), // for mysql database
            ],
//            BlameableBehavior automatically fills the specified attributes with the current user ID.
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CreadoPor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
            //            This extension provides support for custom records order setup via column-based Posicion index.
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
                'positionAttribute' => 'Posicion',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Nombre', 'TipoSeleccion', 'EnMenu', 'Posicion'], 'required'],
            [['Nombre'], 'string'],
            [['Nombre'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idCaracteristicas' => Yii::t('app', 'Id Caracteristicas'),
            'Nombre' => Yii::t('app', 'Nombre'),
            'TipoSeleccion' => Yii::t('app', 'Selección multiple'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValoresDeCaracteristicas() {
        return $this->hasMany(ValoresDeCaracteristicas::className(), ['Caracteristicas_id' => 'idCaracteristicas']);
    }

}

<?php

namespace backend\modules\caracteristicas\models;

use Yii;
use backend\modules\caracteristicas\models\Caracteristicas;
use common\modules\recursos\models\Recursos;
use \yii\db\ActiveRecord;
use yii\db\Expression;
use \yii2tech\ar\position\PositionBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "TValoresDeCaracteristicas".
 *
 * @property integer $idValoresDeCaracteristicas
 * @property string $Nombre
 * @property string $Slug
 * @property string $CreadoEn
 * @property string $ActualizadoEn
 * @property string $CreadoPor
 * @property string $ActualizadoPor
 * @property string $Caracteristicas_id
 *
 * @property Caracteristicas $caracteristicas
 */
class ValoresDeCaracteristicas extends \yii\db\ActiveRecord {
    /**
     * @var mixed image the attribute for rendering the file input
     * widget for upload on the form
     */

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'TValoresDeCaracteristicas';
    }

    public function behaviors() {
        return [
            //            SluggableBehavior automatically fills the specified attribute with a value that can be used a slug in a URL.
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'Nombre',
                'slugAttribute' => 'Slug',
//                'immutable' => true,
                'ensureUnique' => true,
            ],
//            TimestampBehavior automatically fills the specified attributes with the current timestamp.
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['CreadoEn', 'ActualizadoEn'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ActualizadoEn'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('GETDATE()'), //for sql server database
//                'value' => new Expression('NOW()'), // for mysql database
            ],
//            BlameableBehavior automatically fills the specified attributes with the current user ID.
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CreadoPor',
                'updatedByAttribute' => 'ActualizadoPor',
            ],
            //            This extension provides support for custom records order setup via column-based position index.
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
                'positionAttribute' => 'Posicion',
                'groupAttributes' => [
                    'Caracteristicas_id' // multiple lists varying by 'categoryId'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['Nombre', 'Caracteristicas_id', 'Posicion'], 'required'],
            [['Nombre'], 'string'],
            [['Caracteristicas_id'], 'integer'],
            [['Caracteristicas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Caracteristicas::className(), 'targetAttribute' => ['Caracteristicas_id' => 'idCaracteristicas']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idValoresDeCaracteristicas' => Yii::t('app', 'Id'),
            'Nombre' => Yii::t('app', 'Nombre'),
            'Caracteristicas_id' => Yii::t('app', 'Caracteristica'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristicas() {
        return $this->hasOne(Caracteristicas::className(), ['idCaracteristicas' => 'Caracteristicas_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos() {
        return $this->hasMany(Recursos::className(), ['idRecursos' => 'rIdRecursos'])
                        ->viaTable('RRecursosValoresDeCaracteristicas', ['rIdValores' => 'idValoresDeCaracteristicas'])
                        ->andOnCondition(["TRecursos.Estado" => \yii2mod\moderation\enums\Status::APPROVED])
                        ->andOnCondition(["TRecursos.isDeleted" => false]);
    }

    /**
     * @return array $menu_items
     */
    public function MenuItems($context_route) {
        $valores_caracteristicas = Caracteristicas::find()->select(['Nombre', 'idCaracteristicas', 'EnMenu'])->indexBy('idCaracteristicas')->orderBy(['Posicion' => SORT_ASC])->all();

        $menu_items = array();

        $request_params = Yii::$app->request->queryParams;
        $valores_url = "caracteristicas/valores-de-caracteristicas";
        $caracteristicaSearch_request_id = isset($request_params['ValoresDeCaracteristicasSearch']) ? $request_params['ValoresDeCaracteristicasSearch']["Caracteristicas_id"] : "";
        $caracteristica_request_id = isset($request_params['caracteristica']) ? $request_params['caracteristica'] : "";

        foreach ($valores_caracteristicas as $caracteristica) {

            if ($caracteristica->EnMenu) {
                $caracteristica_item_id = $caracteristica->idCaracteristicas;

                $menu_items[] = array(
                    'label' => $caracteristica->Nombre,
                    "options" => ["class" => "valor-caracteristica"],
                    'icon' => '',
                    'url' => Url::toRoute(["/$valores_url/index", 'ValoresDeCaracteristicasSearch[Caracteristicas_id]' => $caracteristica_item_id]),
                    'active' => ($context_route . '/' . $caracteristicaSearch_request_id == '' . $valores_url . "/index/$caracteristica_item_id") || ($context_route . '/' . $caracteristica_request_id == '' . $valores_url . "/create/$caracteristica_item_id") || ($context_route . '/' . $caracteristica_request_id == '' . $valores_url . "/update/$caracteristica_item_id"),
                );
            }
        }
        return $menu_items;
    }

    public static function getValoresPorCaracteristicas($idCaracteristicas="") {

      if($idCaracteristicas==""){
        $caracteristicas = Caracteristicas::find()->select(['Nombre', 'idCaracteristicas', 'EnMenu', 'Slug', 'TipoSeleccion'])->indexBy('idCaracteristicas')->orderBy(['Posicion' => SORT_ASC])->all();
      }
      else{
        $caracteristicas = Caracteristicas::find()->select(['Nombre', 'idCaracteristicas', 'EnMenu', 'Slug', 'TipoSeleccion'])->where(["idCaracteristicas" => $idCaracteristicas])->indexBy('idCaracteristicas')->orderBy(['Posicion' => SORT_ASC])->all();        
      }

        $valoresPorCaracteristicas = array();

        foreach ($caracteristicas as $caracteristica) {
            $valores = ValoresDeCaracteristicas::find()
                    ->select(['Nombre', 'idValoresDeCaracteristicas'])
                    ->where(["Caracteristicas_id" => $caracteristica->idCaracteristicas])
                    ->indexBy('idValoresDeCaracteristicas')
                    ->all();
            if ($valores) {
                $valoresPorCaracteristicas[$caracteristica->TipoSeleccion . "-" . str_replace("-", "_", $caracteristica->Slug) . "-" . $caracteristica->Nombre . "-" . $caracteristica->idCaracteristicas] = $valores;
            }
        }

        return $valoresPorCaracteristicas;
    }

}

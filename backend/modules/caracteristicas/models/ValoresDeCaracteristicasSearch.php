<?php

namespace backend\modules\caracteristicas\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;

/**
 * ValoresDeCaracteristicasSearch represents the model behind the search form of `backend\modules\caracteristicas\models\ValoresDeCaracteristicas`.
 */
class ValoresDeCaracteristicasSearch extends ValoresDeCaracteristicas {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['idValoresDeCaracteristicas', 'Caracteristicas_id'], 'integer'],
            [['Nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ValoresDeCaracteristicas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idValoresDeCaracteristicas' => $this->idValoresDeCaracteristicas,
            'Caracteristicas_id' => $this->Caracteristicas_id,
        ]);

        $query->andFilterWhere(['COLLATE Latin1_General_CI_AI like', 'Nombre', "%" . $this->Nombre . "%"]);

        $query->orderBy(['Posicion' => SORT_ASC]);

        return $dataProvider;
    }

}

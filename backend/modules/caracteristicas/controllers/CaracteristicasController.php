<?php

namespace backend\modules\caracteristicas\controllers;

use Yii;
use backend\modules\caracteristicas\models\Caracteristicas;
use backend\modules\caracteristicas\models\CaracteristicasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CaracteristicasController implements the CRUD actions for Caracteristicas model.
 */
class CaracteristicasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

//    public function beforeAction($action){
//
//        if (!parent::beforeAction($action)) {
//            return false;
//        }
//
//        $operacion = str_replace("/", "-", Yii::$app->controller->route);
//
//        $permitirSiempre =  Yii::$app->params['siemprePermitidos'];
//        if (in_array($operacion, $permitirSiempre) || (AccessHelpers::getAcceso('admin-admin')) ) {
//            return true;
//        }
//
//        if (!AccessHelpers::getAcceso($operacion)) {
//            echo $this->render('/error/general');
//            return false;
//        }
//
//        return true;
//    }
    /**
     * Lists all Caracteristicas models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CaracteristicasSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);


        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Caracteristicas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Caracteristicas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Caracteristicas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', yii::t('app', 'El item se ha creado con éxito'));
            return $this->redirect(['update', 'id' => $model->idCaracteristicas]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Caracteristicas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', yii::t('app', 'El item se ha actualizado con éxito'));
            return $this->redirect(['update', 'id' => $model->idCaracteristicas]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Caracteristicas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Caracteristicas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Caracteristicas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Caracteristicas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php

namespace backend\modules\caracteristicas\controllers;

use Yii;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;
use backend\modules\caracteristicas\models\ValoresDeCaracteristicasSearch;
use backend\modules\caracteristicas\models\Caracteristicas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ValoresDeCaracteristicasController implements the CRUD actions for ValoresDeCaracteristicas model.
 */
class ValoresDeCaracteristicasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ValoresDeCaracteristicas models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ValoresDeCaracteristicasSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);
        $caracteristica = isset($queryParams['ValoresDeCaracteristicasSearch']) ? $queryParams['ValoresDeCaracteristicasSearch']["Caracteristicas_id"] : "";

        $model_caracteristica = $caracteristica ? Caracteristicas::findOne($caracteristica) : false;

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'caracteristica' => $caracteristica,
                    'model_caracteristica' => $model_caracteristica,
        ]);
    }

    /**
     * Displays a single ValoresDeCaracteristicas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ValoresDeCaracteristicas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new ValoresDeCaracteristicas();

        $queryParams = Yii::$app->request->queryParams;
        $caracteristica = isset($queryParams['caracteristica']) ? $queryParams['caracteristica'] : "";

        $caracteristicas = Caracteristicas::find()->select(['Nombre', 'idCaracteristicas'])->indexBy('idCaracteristicas')->column();



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', yii::t('app', 'El item se creo con éxito'));

            return $this->redirect(
                            ['update',
                                'id' => $model->idValoresDeCaracteristicas,
                                'caracteristica' => $caracteristica,
                                'caracteristicas' => $caracteristicas,
                            ]
            );
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'caracteristica' => $caracteristica,
                        'caracteristicas' => $caracteristicas
            ]);
        }
    }

    /**
     * Updates an existing ValoresDeCaracteristicas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $queryParams = Yii::$app->request->queryParams;
        $caracteristica = isset($queryParams['caracteristica']) ? $queryParams['caracteristica'] : "";

        $caracteristicas = Caracteristicas::find()->select(['Nombre', 'idCaracteristicas'])->indexBy('idCaracteristicas')->column();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', yii::t('app', 'El item fue editado con éxito'));

            return $this->redirect(['update',
                        'id' => $model->idValoresDeCaracteristicas,
                        'caracteristica' => $caracteristica,
                        'caracteristicas' => $caracteristicas,
            ]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'caracteristica' => $caracteristica,
                        'caracteristicas' => $caracteristicas,
            ]);
        }
    }

    /**
     * Deletes an existing ValoresDeCaracteristicas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        $queryParams = Yii::$app->request->queryParams;
        $caracteristica = isset($queryParams['ValoresDeCaracteristicasSearch']) ? $queryParams['ValoresDeCaracteristicasSearch']["Caracteristicas_id"] : "";


        return $this->redirect(['index', 'ValoresDeCaracteristicasSearch[Caracteristicas_id]' => $caracteristica]);
    }

    /**
     * Finds the ValoresDeCaracteristicas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ValoresDeCaracteristicas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ValoresDeCaracteristicas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe.');
        }
    }

}

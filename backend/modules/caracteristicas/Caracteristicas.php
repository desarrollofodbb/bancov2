<?php

namespace backend\modules\caracteristicas;

/**
 * sliders module definition class
 */
class Caracteristicas extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\caracteristicas\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

}

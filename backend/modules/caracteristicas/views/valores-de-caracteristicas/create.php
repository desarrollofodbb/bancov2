<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\ValoresDeCaracteristicas */

$this->title = Yii::t('app', 'Agregar nuevo item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caracteristicas Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caracteristicas-items-crear">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php
    echo
    $this->render('_form', [
        'model' => $model,
        'caracteristica' => $caracteristica,
        'caracteristicas' => $caracteristicas,
    ])
    ?>

</div>

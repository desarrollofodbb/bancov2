<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\ValoresDeCaracteristicas */

$this->title = $model->Titulo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caracteristicas Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sliders-items-view">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <p>
        <?php echo Html::a(Yii::t('app', 'Actualizar'), ['actualizar', 'id' => $model->idValoresDeCaracteristicas], ['class' => 'btn btn-primary']) ?>
        <?php
        echo
        Html::a(Yii::t('app', 'Eliminar'), ['eliminar', 'id' => $model->idValoresDeCaracteristicas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to eliminar this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    echo
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idValoresDeCaracteristicas',
            'Titulo',
            'Descripcion',
            'UrlImagen:url',
            'CallToActionTexto',
            'CallToActionUrl:url',
            'idCaracteristicas',
        ],
    ])
    ?>

</div>

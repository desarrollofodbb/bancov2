<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\ValoresDeCaracteristicas */

$this->title = Yii::t('app', 'Actualizar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caracteristicas Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idValoresDeCaracteristicas, 'url' => ['view', 'id' => $model->idValoresDeCaracteristicas]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="caracteristicas-items-actualizar">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <p> 
        <?php echo Html::a(Yii::t('app', 'Agregar nuevo'), ['create', 'caracteristica' => $caracteristica], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo
    $this->render('_form', [
        'model' => $model,
        'caracteristica' => $caracteristica,
        'caracteristicas' => $caracteristicas,
    ])
    ?>

</div>

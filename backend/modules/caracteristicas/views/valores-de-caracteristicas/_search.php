<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\ValoresDeCaracteristicasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-items-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>

    <?php echo $form->field($model, 'idValoresDeCaracteristicas') ?>

    <?php echo $form->field($model, 'Titulo') ?>

    <?php echo $form->field($model, 'Descripcion') ?>

    <?php echo $form->field($model, 'UrlImagen') ?>

    <?php echo $form->field($model, 'CallToActionTexto') ?>

    <?php // echo $form->field($model, 'CallToActionUrl') ?>

    <?php echo $form->field($model, 'idCaracteristicas') ?>

    <div class="form-group">
        <?php echo  Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo  Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

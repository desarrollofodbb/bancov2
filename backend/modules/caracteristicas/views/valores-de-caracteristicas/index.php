<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\caracteristicas\models\ValoresDeCaracteristicasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;

$this->title = $model_caracteristica ? $model_caracteristica->Nombre : "Valores características";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caracteristicas-items-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

    <p>
        <?php
        global $caracteristica_object;
        $caracteristica_object = $caracteristica;
        ?>
        <?php echo Html::a(Yii::t('app', 'Agregar nuevo'), ['create', 'caracteristica' => $caracteristica], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'Nombre',
            ['class' => ActionColumn::className(),
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        global $caracteristica_object;
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['/caracteristicas/valores-de-caracteristicas/update', 'caracteristica' => $caracteristica_object, 'id' => $model->idValoresDeCaracteristicas]));
                    },
                            'delete' => function ($url, $model, $key) {
                        global $caracteristica_object;
                        $data_confirm = Yii::t('app', 'Está seguro que desea eliminar este item?');
                        $text = '<span class="glyphicon glyphicon-trash"></span>';
                        $toroute = Url::toRoute(['/caracteristicas/valores-de-caracteristicas/delete', 'ValoresDeCaracteristicasSearch[Caracteristicas_id]' => $caracteristica_object, 'id' => $key]);
                        $aria_label = Yii::t('app', 'Eliminar');
                        return Html::a($text, $toroute, ['data-method' => "post", "data-confirm" => $data_confirm, "data-pjax" => "0", "aria-label" => $aria_label, "title" => $aria_label]);
                    },
                        ]
                    ]
                ],
            ]);
            ?>

</div>

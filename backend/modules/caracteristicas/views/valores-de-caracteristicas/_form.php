<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\ValoresDeCaracteristicas */
/* @var $form yii\widgets\ActiveForm */

// get the module to which the currently requested controller belongs
$module = \Yii::$app->controller->module;
?>



<div class="caracteristicas-items-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?php echo $form->field($model, 'Nombre')->textInput() ?>


    <?php
    if (isset($caracteristica) && !empty($caracteristica)) {
        echo $form->field($model, 'Caracteristicas_id')->hiddenInput(['value' => $caracteristica])->label(false);
    } else {
        echo $form->field($model, 'Caracteristicas_id')->dropdownList($caracteristicas, ['prompt' => $module->params["m_select_the_caracteristica"]]);
    }
    ?>

    <?php
    echo $form->field($model, 'Posicion')->hiddenInput(['type' => 'number', 'value' => $model->Posicion ? $model->Posicion : 1, "class" => "hidden"])->label(false);
    ?>

    <div class="form-inline">
        <div class="form-group">
            <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <div class="form-group">
            <?php echo Html::a(Yii::t('app', 'Cancelar'), ['/caracteristicas/valores-de-caracteristicas', "ValoresDeCaracteristicasSearch[Caracteristicas_id]" => $caracteristica], ['class' => 'btn btn-default']) ?>
        </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>

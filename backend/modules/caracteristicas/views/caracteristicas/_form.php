<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\Caracteristicas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'Nombre')->textInput() ?>

    <?php echo $form->field($model, 'TipoSeleccion')->checkbox(array('label' => 'Selección multiple?'))->hint("Si selecciona esta opción podran seleccionarse varios valores de esta característica en el recurso"); ?>
    <div class="hidden">
        <?php
        echo $form->field($model, 'EnMenu')
                ->checkbox(['label' => 'Agregar al menú?'])
                ->hint("Si selecciona esta opción se agregará un sub menú al menú principal características para poder administrar los valores de la característica");
        ?>
    </div>
    <?php
    echo $form->field($model, 'Posicion')->textInput(['type' => 'number', 'value' => $model->Posicion ? $model->Posicion : 1])->hint("Seleccione el orden en que se muestra esta caracteristica, tanto en el listado principal como en el menú en caso de ser agregada al menú de características (1 se pone al primero en la lista)");
    ?>

    <div class="col col-md-12">
        <div class="form-inline">
            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <div class="form-group">
                <?php echo Html::a(Yii::t('app', 'Cancelar'), ['/caracteristicas/caracteristicas'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

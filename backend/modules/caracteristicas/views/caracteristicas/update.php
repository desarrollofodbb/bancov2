<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\Caracteristicas */

$this->title = Yii::t('app', 'Actualizar característica');
$idSlider = $model->idCaracteristicas;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caracteristicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idCaracteristicas, 'url' => ['view', 'id' => $model->idCaracteristicas]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>

<div class="sliders-actualizar">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php
    echo
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

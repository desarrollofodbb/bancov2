<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\CaracteristicasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>

    <?php echo  $form->field($model, 'idCaracteristicas') ?>

    <?php echo  $form->field($model, 'Nombre') ?>

        <?php echo  $form->field($model, 'Descripcion') ?>

    <div class="form-group">
<?php echo  Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    <?php echo  Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\caracteristicas\models\Caracteristicas */

$this->title = Yii::t('app', 'Crear nueva característica');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caracteristicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sliders-crear">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php
    echo $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

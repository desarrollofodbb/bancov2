<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\caracteristicas\models\CaracteristicasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Caracteristicas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sliders-index">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
    <?php echo Html::a(Yii::t('app', 'Crear nueva característica'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?php
    echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'Nombre',
            [
                'attribute' => 'TipoSeleccion',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->TipoSeleccion ? '<i class="fa fa-check" aria-hidden="true"></i>' : '';
                },
                'visible' => true
            ],
//            ['class' => ActionColumn::className(), 'template' => '{update} {delete}']
            ['class' => ActionColumn::className(), 'template' => '{update}']
        ],
    ]);
    ?>
</div>

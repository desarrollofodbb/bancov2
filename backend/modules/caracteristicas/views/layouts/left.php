<?php

use yii\helpers\Html;
?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">

            <?php echo  Html::a('<i class="fa fa-th-list" aria-hidden="true"></i> ' . Yii::t('app', 'All sliders'), ['/sliders/sliders'], ['class' => 'navbar-brand']) ?>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <?php echo 
            dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'nav navbar-nav'],
                        'items' => [
                            [
                                'label' => 'Slider settings',
                                'icon' => 'fa fa-pencil',
                                'url' => ['actualizar', 'id' => $idSlider],
                                'options' => ['class' => $this->context->route == 'sliders/sliders/actualizar' ? 'active' : '']
                            ],
                            [
                                'label' => 'Slide editor',
                                'icon' => 'fa fa-eye',
                                'url' => ['view', 'id' => $idSlider],
                                'options' => ['class' => $this->context->route == 'sliders/sliders/view' ? 'active' : '']
                            ],
                        ],
                    ]
            )
            ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
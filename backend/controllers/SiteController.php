<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\Json;
use backend\models\SignupForm;
use common\models\Historial;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['administrador_de_plataforma'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->goHome();
//        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->loginBackend()) {
//                $nuevo_login = new Historial();
//                $nuevo_login->save();
                return $this->goBack();
            } else {
//                $nuevo_login = new Historial();
//                $nuevo_login->save();

                return $this->render('login', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionCreate() {
        $model = new SignupForm();

        $model->scenario = SignupForm::SCENARIO_CREATE;

//        1- validar si existe en el sistema centralizado el usuario
//        URL-WEB-SERVICE/usuario/existe/{usr}
//        2-- validar si existe en el sistema centralizado el correo
//        URL-WEB-SERVICE/correo/existe/{correo}

        $post_data = Yii::$app->request->post();
        if ($model->load($post_data)) {

            //1- Validate if the username exist in the fod user centralized database
            $userExist = Yii::$app->params["api"] .
                    'usuario/existe/' . $post_data["SignupForm"]["username"];


            if ($userExist) {
                $jsoncall = @file_get_contents($userExist);
                if ($jsoncall) {
                    $userExistjsonResponse = Json::decode();
                    if ($userExistjsonResponse["estado"]) {
                        Yii::$app->session->setFlash("error", Yii::t("app", "El usuario ya existe en la base de datos centralizada de la FOD"));
                        return $this->render('create', [
                                    'model' => $model,
                        ]);
                    }
                }
            }


            //1- Validate if the username exist in the fod user centralized database
            $correoExist = Yii::$app->params["api"] .
                    'correo/existe/' . $post_data["SignupForm"]["email"];

            if ($correoExist) {
                $jsonCall = @file_get_contents($correoExist);
                $correoExistjsonResponse = Json::decode($jsonCall);
                if ($jsonCall) {
                    if ($correoExistjsonResponse["estado"]) {
                        Yii::$app->session->setFlash("error", Yii::t("app", "El correo ya existe en la base de datos centralizada de la FOD"));
                        return $this->render('create', [
                                    'model' => $model,
                        ]);
                    }
                }
            }

            if ($model->validate()) {

                $model->setPassword($model->password);
                $model->generateAuthKey();
                $model->generatePasswordResetToken();

                if ($model->save()) {
                    // the following three lines were added:
//                    $auth = Yii::$app->authManager;
//                    $authorRole = $auth->getRole('administrador_general');
//                    $auth->assign($authorRole, $model->getId());

                    return $this->redirect(['/site/update', 'id' => $model->getId()]);
                }
            }
        }


        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Paginas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = SignupForm::SCENARIO_UPDATE;

        $post_data = Yii::$app->request->post();

        if ($model->load($post_data) && $model->userupdate($model)) {
            Yii::$app->session->setFlash("success", "El usuario se actualizó correctamente");
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['/admin/assignment']);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Finds the Paginas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paginas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SignupForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe');
        }
    }

}

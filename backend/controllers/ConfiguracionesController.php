<?php

namespace backend\controllers;

use Yii;
use common\models\Configuraciones;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * ConfiguracionesController implements the CRUD actions for Configuraciones model.
 */
class ConfiguracionesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Configuraciones models.
     * @return mixed
     */
    public function actionIndex() {
        $model_configuraciones = Configuraciones::findOne(1);
        $post_fields = Yii::$app->request->post();
        if ($post_fields) {

            //save data from form
            $model_configuraciones->saveConfiguraciones($model_configuraciones, $post_fields["Configuraciones"]);

            return $this->render('index', [
                        "model_configuraciones" => $model_configuraciones,
                        "nav_tab" => $post_fields["_nav_tab"],
            ]);
        }

        return $this->render('index', [
                    "model_configuraciones" => $model_configuraciones,
                    "nav_tab" => "general_settings"
        ]);
    }

}

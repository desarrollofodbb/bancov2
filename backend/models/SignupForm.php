<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends User {

//    public $username;
//    public $email;
    public $password;
    public $newPassword;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['id', 'username', 'email', 'status', 'password'];
        $scenarios[self::SCENARIO_UPDATE] = ['id', 'username', 'email', 'status', 'newPassword'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message' => "El usuario es requerido"],
            ['username', 'unique', 'message' => 'Ya existe un usuario con el mismo username.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required', 'message' => "El correo es requerido"],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['status', 'required', 'message' => "El estado es requerido"],
            ['email', 'unique', 'message' => 'Ya existe un usuario con el mismo correo.'],
            ['password', 'required', 'message' => "La contraseña es requerida"],
            ['password', 'string', 'min' => 6, 'message' => "La contraseña debe tener al menos 6 caracteres"],
            ['newPassword', 'string', 'min' => 6, 'message' => "La contraseña debe tener al menos 6 caracteres"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => Yii::t('app', 'Usuario'),
            'email' => Yii::t('app', 'Correo'),
            'password' => Yii::t('app', 'Contraseña'),
            'newPassword' => Yii::t('app', 'Nueva contraseña'),
            'status' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function userupdate($user) {

        if ($user->newPassword) {
            $user->setPassword($user->newPassword);
            $this->updatePassword($user->newPassword);
        }

        if ($user->save()) {
            return $user;
        }

        return null;
    }

    public function updatePassword($new_password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($new_password);
    }

}

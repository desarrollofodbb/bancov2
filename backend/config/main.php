<?php

use yii\helpers\Html;
use yii\helpers\Url;

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name' => 'Banco de Recursos',
    'homeUrl' => '/reportes/reportes/overview',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'backend\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'searchClass' => 'common\models\UserSearch',
                    'idField' => 'id',
                    'usernameField' => 'username',
                    'extraColumns' => [
                        [
                            'attribute' => 'email',
                            'label' => 'Correo',
                            'format' => 'email',
                            'value' => function($model, $key, $index, $column) {
                                return $model->email;
                            },
                        ],
//                        [
//                            'attribute' => 'created_at',
//                            'label' => 'Fecha creación',
//                            'format' => 'date',
//                            'value' => function($model, $key, $index, $column) {
//                                return $model->created_at;
//                            },
//                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Estado',
                            'value' => function($model) {
                                return $model->status == 0 ? 'Inactivo' : 'Activo';
                            },
                            'filter' => [
                                0 => 'Inactivo',
                                10 => 'Activo'
                            ]
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '',
                            'buttons' => [
//                                'update' => function ($url, $model, $key) {
//                                    $update_content = '<span class="glyphicon glyphicon-pencil"></span>';
//                                    return Html::a($update_content, Url::toRoute(['/site/update', 'id' => $key]));
//                                },
//                                        'delete' => function ($url, $model, $key) {
//                                    $data_confirm = Yii::t('app', 'Está seguro que desea eliminar este usuario?');
//                                    $text = '<span class="glyphicon glyphicon-trash"></span>';
//                                    $toroute = Url::toRoute(['/site/delete', 'id' => $key]);
//                                    $aria_label = Yii::t('app', 'Eliminar');
//                                    return Html::a($text, $toroute, ['data-method' => "post", "data-confirm" => $data_confirm, "data-pjax" => "0", "aria-label" => $aria_label, "title" => $aria_label]);
//                                },
                            ]
                        ]
                    ],
                ],
            ],
        ],
        'reportes' => [
            'class' => 'common\modules\reportes\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ]
    ],
    'components' => [
      'i18n' => [
        'translations' => [
            'kvgrid' => [
                'class' => 'yii\i18n\PhpMessageSource', 
                'sourceLanguage' => 'es-ES',
                'basePath' => '@vendor/kartik-v/yii2-grid/messages/es/kvgrid.php'
            ],
      ],    
      ],    
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    /*
      'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
      ],
      ],
     */
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
//            'site/signup',
        ]
    ],
    'params' => $params,
];

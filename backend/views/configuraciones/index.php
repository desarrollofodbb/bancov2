<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\filemanager\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Default */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->title = Yii::t("app", "Configuraciones");
?>

<h1><?php echo Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin(); ?>
<input type="hidden" name="_nav_tab" value="<?php echo isset($nav_tab) ? $nav_tab : "general_settings"; ?>">
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?php echo (!isset($nav_tab) || (isset($nav_tab) && ($nav_tab == "general_settings"))) ? "active" : "" ?>">
        <a href="#general_settings" aria-controls="general_settings" role="tab" data-toggle="tab">
            <?php echo Yii::t("app", "General"); ?>
        </a>
    </li>
    <li role="presentation" class="<?php echo (isset($nav_tab) && ($nav_tab == "personaje_settings")) ? "active" : "" ?>">
        <a href="#personaje_settings" aria-controls="personaje_settings" role="tab" data-toggle="tab">
            <?php echo Yii::t("app", "Personaje"); ?>
        </a>
    </li>
    <li role="presentation" class="<?php echo (isset($nav_tab) && ($nav_tab == "contacto_settings")) ? "active" : "" ?>">
        <a href="#contacto_settings" aria-controls="contacto_settings" role="tab" data-toggle="tab">
            <?php echo Yii::t("app", "Correo/Contacto"); ?>
        </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane <?php echo (!isset($nav_tab) || (isset($nav_tab) && ($nav_tab == "general_settings"))) ? "active" : "" ?>" id="general_settings">
        <br>

        <div class="row">
            <div class="col col-md-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo Yii::t("app", "Logo banco de recursos"); ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div><!--// /.box-tools-->
                    </div> <!--/.box-header-->
                    <div class="box-body">
                        <div class="text-center img-general_header_logo">
                            <?php
                            if ($model_configuraciones->general_header_logo) {
                                if ($model_configuraciones->generalHeaderLogo) {
                                    echo Html::img(Yii::$app->params['urlBack'] . $model_configuraciones->generalHeaderLogo->getThumbUrl('thumbnail'));
                                } else {
                                    echo Html::img("");
                                }
                            }
                            ?>
                        </div>
                        <?php
                        echo $form->field($model_configuraciones, 'general_header_logo')->widget(FileInput::className(), [
                            'buttonTag' => 'button',
                            'buttonName' => Yii::t("app", "Asignar imagen"),
                            'buttonOptions' => ['class' => 'btn btn-default btn-block'],
                            'options' => ['class' => 'form-control'],
                            // Widget template
                            'template' => '<div class=""><div class="hidden">{input}</div><div class="input-group"><span class="input-group-btn">{button}</span></div></div>',
                            // Optional, if set, only this image can be selected by user
                            'thumb' => 'small',
                            // Optional, if set, in container will be inserted selected image
                            'imageContainer' => '.img-general_header_logo',
                            // Default to FileInput::DATA_URL. This data will be inserted in input field
                            'pasteData' => FileInput::DATA_ID,
                            // JavaScript function, which will be called before insert file data to input.
                            // Argument data contains file data.
                            // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                            'callbackBeforeInsert' => 'function(e, data) {
        console.log( data );
    }',])->label(false);
                        ?>
                    </div> <!-- /.box-body-->
                </div> <!--/.box-->

            </div>

            <div class="col col-md-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo Yii::t("app", "Imagen destacada del header home"); ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div><!--// /.box-tools-->
                    </div> <!--/.box-header-->
                    <div class="box-body">

                        <div class="text-center img-general_header_image_featured">
                            <?php
                            if ($model_configuraciones->general_header_image_featured) {
                                if ($model_configuraciones->generalHeaderImageFeatured) {
                                    echo Html::img(Yii::$app->params['urlBack'] . $model_configuraciones->generalHeaderImageFeatured->getThumbUrl('thumbnail'));
                                } else {
                                    echo Html::img("");
                                }
                            }
                            ?>
                        </div>
                        <?php
                        echo $form->field($model_configuraciones, 'general_header_image_featured')->widget(FileInput::className(), [
                            'buttonTag' => 'button',
                            'buttonName' => Yii::t("app", "Asignar imagen"),
                            'buttonOptions' => ['class' => 'btn btn-default btn-block'],
                            'options' => ['class' => 'form-control'],
                            // Widget template
                            'template' => '<div class=""><div class="hidden">{input}</div><div class="input-group"><span class="input-group-btn">{button}</span></div></div>',
                            // Optional, if set, only this image can be selected by user
                            'thumb' => 'small',
                            // Optional, if set, in container will be inserted selected image
                            'imageContainer' => '.img-general_header_image_featured',
                            // Default to FileInput::DATA_URL. This data will be inserted in input field
                            'pasteData' => FileInput::DATA_ID,
                            // JavaScript function, which will be called before insert file data to input.
                            // Argument data contains file data.
                            // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                            'callbackBeforeInsert' => 'function(e, data) {
        console.log( data );
    }',])->label(false);
                        ?>
                    </div> <!-- /.box-body-->
                </div> <!--/.box-->

            </div>

            <div class="col col-md-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo Yii::t("app", "Logo Fundación Omar Dengo"); ?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div><!--// /.box-tools-->
                    </div> <!--/.box-header-->
                    <div class="box-body">

                        <div class="text-center img-general_footer_logo">
                            <?php
                            if ($model_configuraciones->general_footer_logo) {
                                if ($model_configuraciones->generalFooterLogo) {
                                    echo Html::img(Yii::$app->params['urlBack'] . $model_configuraciones->generalFooterLogo->getThumbUrl('thumbnail'));
                                } else {
                                    echo Html::img("");
                                }
                            }
                            ?>
                        </div>
                        <?php
                        echo $form->field($model_configuraciones, 'general_footer_logo')->widget(FileInput::className(), [
                            'buttonTag' => 'button',
                            'buttonName' => Yii::t("app", "Asignar imagen"),
                            'buttonOptions' => ['class' => 'btn btn-default btn-block'],
                            'options' => ['class' => 'form-control'],
                            // Widget template
                            'template' => '<div class=""><div class="hidden">{input}</div><div class="input-group"><span class="input-group-btn">{button}</span></div></div>',
                            // Optional, if set, only this image can be selected by user
                            'thumb' => 'small',
                            // Optional, if set, in container will be inserted selected image
                            'imageContainer' => '.img-general_footer_logo',
                            // Default to FileInput::DATA_URL. This data will be inserted in input field
                            'pasteData' => FileInput::DATA_ID,
                            // JavaScript function, which will be called before insert file data to input.
                            // Argument data contains file data.
                            // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                            'callbackBeforeInsert' => 'function(e, data) {
        console.log( data );
    }',])->label(false);
                        ?>
                    </div> <!-- /.box-body-->
                </div> <!--/.box-->

            </div>
        </div>

        <?php echo $form->field($model_configuraciones, 'general_footer_copyright')->textInput(); ?>

    </div>
    <div role="tabpanel" class="tab-pane <?php echo (isset($nav_tab) && ($nav_tab == "personaje_settings")) ? "active" : "" ?>" id="personaje_settings">
        <br>

        <div class="row">
            <div class="col col-md-3">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php echo Yii::t("app", "Imagen del personaje"); ?>
                        </h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div><!--// /.box-tools-->
                    </div> <!--/.box-header-->
                    <div class="box-body">

                        <div class="text-center img-personaje_imagen">
                            <?php
                            if ($model_configuraciones->personaje_imagen) {
                                if ($model_configuraciones->personajeImagen) {
                                    echo Html::img(Yii::$app->params['urlBack'] . $model_configuraciones->personajeImagen->getThumbUrl('thumbnail'));
                                } else {
                                    echo Html::img("");
                                }
                            }
                            ?>
                        </div>
                        <?php
                        echo $form->field($model_configuraciones, 'personaje_imagen')->widget(FileInput::className(), [
                            'buttonTag' => 'button',
                            'buttonName' => Yii::t("app", "Asignar imagen"),
                            'buttonOptions' => ['class' => 'btn btn-default btn-block'],
                            'options' => ['class' => 'form-control'],
                            // Widget template
                            'template' => '<div class=""><div class="hidden">{input}</div><div class="input-group"><span class="input-group-btn">{button}</span></div></div>',
                            // Optional, if set, only this image can be selected by user
                            'thumb' => 'small',
                            // Optional, if set, in container will be inserted selected image
                            'imageContainer' => '.img-personaje_imagen',
                            // Default to FileInput::DATA_URL. This data will be inserted in input field
                            'pasteData' => FileInput::DATA_ID,
                            // JavaScript function, which will be called before insert file data to input.
                            // Argument data contains file data.
                            // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
                            'callbackBeforeInsert' => 'function(e, data) {
        console.log( data );
    }',])->label(false);
                        ?>
                    </div> <!-- /.box-body-->
                </div> <!--/.box-->

            </div>
        </div>

        <?php echo $form->field($model_configuraciones, 'personaje_titulo')->textInput(); ?>
        <?php echo $form->field($model_configuraciones, 'personaje_descripcion')->textInput(); ?>
        <?php echo $form->field($model_configuraciones, 'personaje_link')->textInput(); ?>


    </div>
    <div role="tabpanel" class="tab-pane <?php echo (isset($nav_tab) && ($nav_tab == "contacto_settings")) ? "active" : "" ?>" id="contacto_settings">
        <br>
        <?php echo $form->field($model_configuraciones, 'contacto_mensaje_gracias')->textInput(); ?>
        <?php echo $form->field($model_configuraciones, 'contacto_correo_info')->textInput(); ?>
        <?php echo $form->field($model_configuraciones, 'contacto_telefono')->textInput(); ?>
        <?php echo $form->field($model_configuraciones, 'contacto_direccion')->textInput(); ?>

    </div>
</div> 
<div class="form-group">
    <?php echo Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
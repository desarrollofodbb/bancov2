<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Sitio</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->params["urlFront"], ['class' => 'logo', 'target' => '_blank']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php
                            $user = common\models\User::getUserById(Yii::$app->user->identity->id);
                            echo $user->getAvatarImg();
                            ?>
                            <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                        <?php endif; ?>
                    </a> 
                </li>  
                <li class="dropdown notifications-menu">
                    <?=
                    Html::a(
                            Yii::t("app", 'Cerrar sesión'), ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-primary btn-flat']
                    )
                    ?>
                </li>
            </ul>
        </div>
    </nav>
</header>

 <?php

use backend\modules\caracteristicas\models\ValoresDeCaracteristicas;

$caracteristicasValores = new ValoresDeCaracteristicas();
?>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>

<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<style type="text/css">


* {
  box-sizing: border-box;
}


body {
  
  font-family: "Source Sans Pro", sans-serif;
  line-height: 1.6;
  min-height: 100vh;
/*  display: grid;*/
/*  place-items: center;*/
}

.chart-container {
  position: relative;
  margin: auto;
  /* height: 80vh; */
/*  width: 72vw;*/
}

input[type="date"]::-webkit-calendar-picker-indicator {
  cursor: pointer;
  border-radius: 4px;
  margin-right: 2px;
  width: 30px;
  height: 30px;  
/*  opacity: 0.6;*/
  background-color: #00CC99;
  background-image: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="-9 -9 42 42"><path fill="%23ffffff" d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z"/></svg>');    
  color: white;
  border-radius: 50%;
/*  filter: invert(0.8);*/
}
</style>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <?php
        $context_route = $this->context->route;

        $caracteristicasValores_menu = $caracteristicasValores->MenuItems($context_route);
        $caracteristicas_admin = array(
            'label' => Yii::t('app', 'Administrar Características'),
            'icon' => '',
            'options' => ['class' => 'w3-small'],            
            'url' => '/caracteristicas/caracteristicas',
            'active' => ($context_route == 'caracteristicas/caracteristicas/index') ||
            ($context_route == 'caracteristicas/caracteristicas/update'),
        );
        array_unshift($caracteristicasValores_menu, $caracteristicas_admin);
//        $caracteristicasValores_menu[] = $caracteristicas_admin;
        ?>

        <?php
        echo
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        [
                            'label' => Yii::t("app", 'Páginas'),
                            'icon' => 'fa fa-map-o',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => ['/paginas/paginas'],
                            'active' => ($context_route == "paginas/paginas/index") ||
                            ($context_route == "paginas/paginas/update")
                        ],
                        [
                            'label' => Yii::t("app", 'Datos de interés'),
                            'icon' => 'fa fa-info-circle',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => ['/datos-de-interes/datos-de-interes'],
                            'items' => [
                                [
                                    'label' => Yii::t("app", 'Administrar datos de interés'),
                                    'icon' => '',
                                    'options' => ['class' => 'w3-small'],                                    
                                    'url' => ['/datos-de-interes/datos-de-interes'],
                                    'active' => ($context_route == "datos-de-interes/datos-de-interes/index") ||
                                    ($context_route == "datos-de-interes/datos-de-interes/update")
                                ],
                                [
                                    'label' => Yii::t("app", 'Agregar nuevo'),
                                    'icon' => '',
                                    'options' => ['class' => 'w3-small'],                                    
                                    'url' => ['/datos-de-interes/datos-de-interes/create'],
                                    'active' => ($context_route == "datos-de-interes/datos-de-interes/create")
                                ],
                            ],
                        ],
                        [
                            'label' => Yii::t("app", 'Preguntas frecuentes'),
                            'icon' => 'fa fa-question-circle',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => ['/preguntas-frecuentes/preguntas-frecuentes'],
                            'items' => [
                                [
                                    'label' => Yii::t("app", 'Administrar preguntas'),
                                    'icon' => '',
                                    'options' => ['class' => 'w3-small'],                                    
                                    'url' => ['/preguntas-frecuentes/preguntas-frecuentes'],
                                    'active' => ($context_route == "preguntas-frecuentes/preguntas-frecuentes/index") ||
                                    ($context_route == "preguntas-frecuentes/preguntas-frecuentes/update")
                                ],
                                [
                                    'label' => Yii::t("app", 'Agregar pregunta'),
                                    'icon' => '',
                                    'options' => ['class' => 'w3-small'],                                    
                                    'url' => ['/preguntas-frecuentes/preguntas-frecuentes/create'],
                                    'active' => ($context_route == "preguntas-frecuentes/preguntas-frecuentes/create")
                                ],
                            ],
                        ],
                        [
                            'label' => Yii::t('app', 'Características Recursos'),
                            'font-size' => "15px",
                            'icon' => 'fa fa-file-image-o',
                            'options' => ['class' => 'w3-small'],
                            'url' => '/caracteristicas/caracteristicas',
                            'items' => $caracteristicasValores_menu
                        ],
                        [
                            'label' => Yii::t('app', 'Administración Usuarios'),
                            'icon' => 'fa fa-users',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => '/admin/assignment',
                            'active' => ($context_route == "admin/assignment/index") ||
                            ($context_route == "site/update")
                        ],
                        [
                            'label' => Yii::t('app', 'Denuncias'),
                            'icon' => 'fa fa-gavel',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => '/recursos/denuncias',
                            'active' => ($context_route == "recursos/denuncias/index") ||
                            ($context_route == "recursos/denuncias/ver-denuncia")
                        ],
                        [
                            'label' => Yii::t('app', 'Reportes'),
                            'icon' => 'fa fa-bar-chart',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => '/reportes/reportes/overview',
                            'active' => ($context_route == "reportes/reportes/overview") ||
                            ($context_route == "reportes/reportes/log-in-usuarios") ||
                            ($context_route == "reportes/reportes/usuarios-que-publican-recursos") ||
                            ($context_route == "reportes/reportes/recursos-por-usuario") ||
                            ($context_route == "reportes/reportes/recursos-publicados-por-mes") ||
                            ($context_route == "reportes/reportes/recursos-mas-vistos") ||
                            ($context_route == "reportes/reportes/recursos-mas-descargados") ||
                            ($context_route == "reportes/reportes/recursos-mas-compartidos") ||
                            ($context_route == "reportes/reportes/recursos-mas-votados") ||
                            ($context_route == "reportes/reportes/busquedas-por-mes") ||
                            ($context_route == "reportes/reportes/filtros-de-busquedas") ||
                            ($context_route == "reportes/reportes/cantidad-de-palabras-por-mes") ||
                            ($context_route == "reportes/reportes/usuarios-mas-recursos-vistos")
                        ],
                        [
                            'label' => Yii::t('app', 'Gráficos'),
                            'icon' => 'fa fa-line-chart',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => '/reportes/reportes/los-reportes',
                            'active' => ($context_route == "/reportes/reportes/los-reportes")
                        ],                        
                        [
                            'label' => Yii::t('app', 'Configuraciones'),
                            'icon' => 'fa fa-cogs',
                            'options' => ['class' => 'w3-small'],                            
                            'url' => '/configuraciones',
                            'active' => ($context_route == "configuraciones/index")
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Iniciar sesión';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#">
            <b>Administración </b> Sistema Banco de recursos
        </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?php Yii::t("app", "Identifiquese para iniciar sesión") ?></p>
        <?php echo Alert::widget() ?>
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?php
        echo $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => Yii::t("app", "usuario")]);
        ?>

        <?php
        echo $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => Yii::t("app", "Contraseña")])
        ?>

        <div class="row">
            <div class="col-xs-6 col-lg-7">
                <?php echo $form->field($model, 'rememberMe')->checkbox(["label" => "Recordarme"]) ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-6 col-lg-5">
                <?php echo Html::submitButton('Iniciar sesión', ['class' => 'btn btn-primary  btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>
        <!--        <a href="#">
        <?php echo Yii::t("app", "Olvidé mi contraseña"); ?>
                </a>
                <br>
                <a href="/admin/user/signup" class="text-center">
        <?php echo Yii::t("app", "Registre una nueva cuenta"); ?>
                </a>-->

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->

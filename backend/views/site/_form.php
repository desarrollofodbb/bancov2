<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SignupForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form"> 
    <?php $form = ActiveForm::begin(); ?>

    <?php // echo $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?php // echo $form->field($model, 'email')->textInput() ?>

    <?php
//    if ($model->isNewRecord) {
//        echo $form->field($model, 'password')->passwordInput();
//    } else {
//        echo $form->field($model, 'newPassword')->passwordInput();
//    }
    ?>

    <?php
    echo
    $form->field($model, 'status')->dropdownList(array("10" => "Activo", "0" => "Inactivo"), ['prompt' => Yii::t("app", "Seleccione el estado")]
    );
    ?>

    <div class="row">
        <div class="col col-md-12">
            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? "crear" : "actualizar", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

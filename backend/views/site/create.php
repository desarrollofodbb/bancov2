<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SignupForm */


$this->title = "crear usuario";
?>
<div class="user-create">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php
    echo
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

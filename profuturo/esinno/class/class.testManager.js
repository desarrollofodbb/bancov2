/**
 * Modulo de gestión de Tests
 * @author Arturo Macías Fernández
 */
var TestManager = function (xmlPath) {
	//Propiedades privadas
	var scoreFactor = 100;
	var masteryFactor = 80;
	var score = 0;
	
	//Propiedades públicas
	this.tests = new Array();	
	
	_global.externalTrace("TestManager.constructor");
	
	
	/**
	 * Añade un test
	 * @param obj (Object)
	 */
	this.addTest = function (obj)
	{
		var newObj = new Object;
		newObj.id = obj.id;
		newObj.score = obj.score;
		newObj.mastery = obj.mastery;
		newObj.currentScore = 0;
		newObj.status = false;
		this.tests.push(newObj);
		this.updateFactors();
		_global.externalTrace("TestManager.addTest",this.tests);
	}	
	
	/**
	 * Actualiza un test con su score
	 * @param id (int)
	 * @param score (int)
	 */
	this.updateTest = function (id, sco) {
		for (var i = 0; i < this.tests.length; i++) {
			if (this.tests[i].id == id) {
				this.tests[i].currentScore = sco;
				if (sco >= this.tests[i].mastery) {
					this.tests[i].status = true;
				}
			}
		}
	}
	
	/**
	 * Devuelve si un test determinado se ha pasado o no
	 * @param id (int)
	 * @return boolean
	 */
	this.isTestPassed = function (id) {
		var a = courseMgr.getModule(id);
		var minPage = a.paginaMinima;
		var maxPage = a.paginaMaxima;
		var res = false;
		for(var i = 0; i < this.tests.length; i++) {
			if(this.tests[i].id >= minPage && this.tests[i].id <= maxPage) {
				res = this.tests[i].status;
			}
		}	
		return res;
	}
	
	this.hasTest = function (id) {
		var a = courseMgr.getModule(id);
		var minPage = a.paginaMinima;
		var maxPage = a.paginaMaxima;
		_global.externalTrace("TestManager.isTestPassed2 min",a.paginaMinima);
		_global.externalTrace("TestManager.isTestPassed2 max",a.paginaMaxima);
		var res = false;
		for(var i = 0; i < this.tests.length; i++) {
			//_global.externalTrace("TestManager.isTestPassed2 test",this.tests[i]);
			if(this.tests[i].id >= minPage && this.tests[i].id <= maxPage) {
				res = true;
			}
		}
		return res;
	}
	
	/**
	 * Actualiza los factores de los test
	 */
	this.updateFactors = function() {
		var j = this.tests.length;
		scoreFactor = 0;
		masteryFactor = 0;
		for (var i = 0; i < j; i++) {
			scoreFactor += this.tests[i].score;
			masteryFactor += this.tests[i].mastery;
		}
		_global.externalTrace("TestManager.updateFactors scoreFactor",scoreFactor);
		_global.externalTrace("TestManager.updateFactors masteryFactor",masteryFactor);
	}
	
	/**
	 * Almacena un test con su score
	 * @param score (int)
	 * @param id (int)
	 */
	this.storeResult =  function(score, id) {
		_global.externalTrace("TestManager.storeResult", score);
		_global.externalTrace("TestManager.storeResult", id);
		var j = this.tests.length;
		_global.externalTrace("TestManager.currentTests", this.tests);
		
		var checkAll = true;
		for (var i = 0; i < j; i++) {
			//if (i == id) {
				_global.externalTrace("TestManager.storeResult id found", id);
				if(parseInt(this.tests[i].currentScore) <= parseInt(score)) {
					_global.externalTrace("TestManager.storeResult new score", score);
					this.tests[i].currentScore = score;
					if (parseInt(score) >= parseInt(this.tests[i].mastery)) {
						this.tests[i].status = true;
					}
				}
				courseMgr.setModuleApproved(i+1);
			//}
		}
		_global.externalTrace("TestManager.currentTests", this.tests);
		this.sendResults();
	}
	
	/**
	 * Resetea todos los tests
	 */
	this.reset =  function() {
		for (var i = 0; i < this.tests.length; i++) {
			this.tests[i].currentScore = 0;
			this.tests[i].status = false;
		}
	}
	
	/**
	 * Almacena un pretest con su score
	 * @param score (int)
	 * @param id (int)
	 */
	this.storeResultPretest = function (score, id) {
	_global.externalTrace("TestManager.storeResultPretest", id);
		this.tests[id].currentScore = score;
		if (score >= this.tests[id].mastery) {
			this.tests[id].status = true;
		} else {
			this.tests[id].status = false;
		}
		//TODO set passed module
		_global.externalTrace("TestManager.storeResultPretest", this.tests);
		//if (this.tests[id].status == true) {
			courseMgr.setModuleApproved(id+1);
		//}
		this.sendResults();
	}
	
	/**
	 * Envia los resultados a la plataforma
	 */
	this.sendResults = function () {
		var approvedUnits = true;
		var completedUnits = true;
		var course = courseMgr.getChapters()
		var currScore = 0;
		var j = this.tests.length;
		for (var i = 0; i < j; i++) {
			currScore += this.tests[i].currentScore;
			if (!this.tests[i].status) {
				approvedUnits = false;
			}
			_global.externalTrace("TestManager.sendResults cumpleted" + (i+1), course[i+1].completado);
			if (!course[i+1].completado) {
				completedUnits = false;
			}
		}
		_global.externalTrace("TestManager.sendResults currentScore", currScore);
		_global.externalTrace("TestManager.sendResults completedUnits", completedUnits);
		
		if (completedUnits) {
			var status;
			if (approvedUnits) {
				status = "passed";
			} else {
				status = "failed";
			}
			_global.externalTrace("TestManager.sendResults status", status);
			
			var normalizedScore = Math.floor( (currScore * 100) / scoreFactor);
			_global.externalTrace("TestManager.sendResults normalizedScore", normalizedScore);
			if (_global.operationMode == "test") {
				//SCOFunctions
				guardaTest(normalizedScore,status);
				//main.js
				sendScorm();
			} else {
				_global.externalTrace("JavaScript bloqueado");	
			}
		} 
		
		sendScorm();
		guardaMenu();
		//TODO: implementar envío
	}
	
}
	
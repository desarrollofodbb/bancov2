/*
 * Objeto de configuración del índice de la aplicación. Debe recibir como parametro 
 *  el listado de módulos y páginas (desde CourseManager) y el tipo de curso desde ConfigManager
 * @author Arturo Macías Fernández
 */

var IndexManager = function (modules, elements, courseType) {
	
	this.msg = "";
	
	this.init = function(){
		if(clickEvent == undefined){
			clickEvent = "click";
		}
		
		_global.externalTrace("IndexManager.init");
		this.fillIndex();
		
		//Añadimos el elemento al menú de la aplicación
		$("#menu_app").append('<a href="javascript:void(0)" class="text_app" id="menu_index">'+ langMgr.get("indice_boton") +'</a>');
		
		//Añadimos el título de la caja
		$("#menu_index_wrap > H1").html(langMgr.get("indice_titulo"));
		
		//Cargamos el contenido del index en el contenedor
		$("#menu_index_wrap > div.wrap_text_index").html(this.msg).jScrollPane({showArrows: false});
		
		//Bind de eventos
		// $("ul.menu li.menu_item a").click(function(){
			// TODO: gotoPage
		// });
		

		$("a#menu_index").bind(clickEvent,function(e){
			e.preventDefault();
			pauseVideo();	
			pauseAudio();	
			//Actualizamos el índice
			for (var i = 0; i < modules.length; i++) {
				if (modules[i].tipo != "intro") {
					for (var j = modules[i].paginaMinima; j <= modules[i].paginaMaxima; j++) {
						if (elements[j].completado || courseType == "abierto") {
							$("li#menu_item_" + elements[j].id).html('<a href="javascript:void(0)" rel="'+j+'">' + elements[j].titulo +'</a>');
						}
					}
				}
			}
			
			$("#menu_index_container").fadeIn();
			if ($("#menu_index").hasClass("opened")){
				$("#menu_index_wrap").animate({
					'top' : '597px'
				},300);
				$("#menu_index").removeClass("opened").addClass("closed");
				$("#menu_index_container").fadeOut();

			}else{

				$("#menu_app_control").animate({
				'top' : '130px'
				}, 300, function(){
					$("#menu_wrap_control").hide();
					}); 
				$("#flecha").hide();
				$("#menu_control").removeClass("opened").addClass("closed");
				$("#menu_index_wrap").animate({
					'top' : '130px'
				},300);
				$("#menu_index").removeClass("closed").addClass("opened");
			}
			
			var api = $("#menu_index_wrap > div.wrap_text_index").data('jsp');
			api.reinitialise();
			//$(".menu .menu_item a").click(function(e){
			$(".menu .menu_item a").bind(clickEvent,function(e){
				e.preventDefault();
				//$("#menu_index_container").fadeOut();
				$("#animActivo").hide();
				$("#animActivo").stop(true);
				$("#menu_index_wrap").animate({
					'top' : '597px'
				},300);
				$("#menu_index").removeClass("opened").addClass("closed");
				$("#menu_index_container").fadeOut();
				courseMgr.setCurrentPage($(this).attr("rel"));
				gotoPage("next");
			});
			
		})
		
		//$("#menu_index_wrap a.close").click(function(e){
		$("#menu_index_wrap a.close,#fnd_shadow").bind(clickEvent,function(e){
			e.preventDefault();			
				$("#menu_index_wrap").animate({
					'top' : '597px'
				},300);
				$("#menu_index").removeClass("opened").addClass("closed");
				$("#menu_index_container").fadeOut();
			resumeVideo();				
			resumeAudio();				
		});
	}
	
	this.fillIndex = function () {
		var msg = '<ul class="menu">';
		var grupActu = "";
				
		for (var i = 0; i < modules.length; i++) {
			if (modules[i].tipo != "intro") {
				msg += '<li class="module">' + modules[i].titulo + '<ul>';
				for (var j = modules[i].paginaMinima; j <= modules[i].paginaMaxima; j++) {
					if (elements[j].completado || courseType == "abierto") {
						msg += '<li id="menu_item_'+ elements[j].id +'" class="menu_item"><a href="javascript:void(0)" rel="'+j+'">' + elements[j].titulo +'</a></li>';
					} else {
						msg += '<li id="menu_item_'+ elements[j].id +'" class="menu_item"><span class="noVisitado">'+ elements[j].titulo +"</span></li>";
					}
				}
				msg += "</ul></li>";
			}
		}
		
		msg += "</ul>";
		this.msg = msg;
	}
	
}
	
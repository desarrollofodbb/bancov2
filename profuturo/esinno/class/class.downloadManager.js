/*
 * Objeto de configuración de las descargas. Hace uso del fichero material.xml de la carpeta contenido
 * @author Arturo Macías Fernández
 */

var DownloadManager = function (debug) {
	
	// var xml;
	
	
	// //Carga del XML
	// $.ajax({
	// url: 'contenido/download/material.xml',

	// 	async: false,
	// 	success: function(_xml) {
	// 		xml = _xml;
	// 	},
	// 	error:function(jqXHR, textStatus, errorThrown){
	// 		_global.externalTrace("ERROR: No se ha podido leer el fichero XML de descargas", textStatus);
	// 	}
	// });
	
	this.init = function(){
		if(clickEvent == undefined){
			clickEvent = "click";
		}
		
		var msg = "";
		_global.externalTrace("DownloadManager.init");
		
		$('materiales').each(function(index){
			msg += '<div class="download">';
			//msg += '	<a href="' + this.textContent +'" class="' + this.attributes.getNamedItem('tipo').nodeValue +'" target="_blank"></a><p>' + this.attributes.getNamedItem('titulo').nodeValue + '</p>';
			msg += '	<a href="' + this.firstChild.nodeValue +'" class="' + this.attributes.getNamedItem('tipo').nodeValue +'" target="_blank">' + this.attributes.getNamedItem('titulo').nodeValue + '</a>';
			msg += '</div>';
			//alert(this.textContent);
		});

	 
		
		//Añadimos el elemento al menú de la aplicación
		//$("#menu_app").append('<a href="javascript:void(0)" class="text" id="menu_downloads">'+ langMgr.get("descargas_boton") +'</a>');
		
	//	$("#menu_downloads_wrap > div").html(msg).jScrollPane({showArrows: true});
		$("#menu_downloads_wrap > div").html(msg).jScrollPane({});
	 
		
		//Bind de eventos
		//$("a#menu_downloads").click(function(e){
		$("a#menu_downloads").bind(clickEvent,function(e){
			e.preventDefault();
			$("#menu_downloads_container").fadeIn();
			var api = $("#menu_downloads_wrap > div").data('jsp');
			api.reinitialise();
		})
		
		//$("#menu_downloads_wrap a.close").click(function(e){
		//$("#menu_downloads_wrap a.close").bind(clickEvent,function(e){
		$("#menu_downloads_container a.close,#fnd_shadow").bind(clickEvent,function(e){
			e.preventDefault();
			$("#menu_downloads_container").fadeOut();
		});
	
	};
}
	
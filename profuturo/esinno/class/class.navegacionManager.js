/*
 * Objeto de configuración del control de navegación de la aplicación. Debe recibir como parametro 
 *  el id del contenedor donde se insertará el control
 * @author Arturo Macías Fernández
 */

var NavegacionManager = function (idDom) {

	if(clickEvent == undefined){
		clickEvent = "click";
	}
		
	_global.externalTrace("NavegacionManager.init");
		
	// Añadimos el elemento al menú de la aplicación
	$("#" + idDom).append(   '<a href="javascript:void(0)" id="btn_prev" class="btn_navegacion_prev_activo"></a>' +
										'<div class="counter">1 / 11</div> ' +
										 
										'<a href="javascript:void(0)" id="btn_next" class="btn_navegacion_next_inactivo"></a>' +
										
										'<div class="fix"></div>');
	
	// Bind de eventos
	//$("#btn_prev").click(function(e){
	$("#btn_prev").bind(clickEvent,function(e){
		e.preventDefault();
		if ($(this).hasClass("btn_navegacion_prev_activo")){
			try {
				if (myPlayer != undefined) {
					if((ie8)||(oldIe)){
					} else {
						myPlayer.setSrc('');
						myPlayer.remove();
						myPlayer = null;
					}
				}




				//_global.externalTrace("NavegacionManager myPlayer eliminado");

				soundMgr.stopAudios();
				//_global.externalTrace("NavegacionManager > Parar audios en reproducción");

			} catch (e) { 
				//_global.externalTrace("NavegacionManager fallo eliminar myPlayer");
			}

			doPrevPage();
			$("#animActivo").hide();
			$("#animActivo").stop(true);
		}
	});
	//$("#btn_next").click(function(e){


	

		$("#animActivo").css({height: '111px', width: '115px', top:'247px', left:'966px', position:'absolute', display:'none' });

		function animActivo(){
			$("#animActivo").css({display:'block'});
		    $("#animActivo").animate({height: '131px', width: '140px', top:'237px', left:'961px'}, "slow");
		    $("#animActivo").animate({height: '111px', width: '115px', top:'247px', left:'966px'}, "slow", animActivo);
		}	

		

	$("#btn_next").bind(clickEvent,function(e){
		e.preventDefault();
		if ($(this).hasClass("btn_navegacion_next_activo")){
			$("#animActivo").css({display:'block'});
			
		
			try {
				if (myPlayer != undefined) {
					if((ie8)||(oldIe)){
					} else {
						myPlayer.setSrc('');
						myPlayer.remove();
						myPlayer = null;
					}
				}


				
				//_global.externalTrace("NavegacionManager myPlayer eliminado");
				
				soundMgr.stopAudios();
				//_global.externalTrace("NavegacionManager > Parar audios en reproducción");
			} catch (e) { 
				//_global.externalTrace("NavegacionManager fallo eliminar myPlayer");
			}
			doNextPage();

			$("#animActivo").hide();
			$("#animActivo").stop(true);
			

		}
	});
	
	this.activateNext = function(){
		$("#btn_next").removeClass("btn_navegacion_next_inactivo").addClass("btn_navegacion_next_activo");
		animActivo();
	}
	
	this.desactivateNext = function(){
		$("#btn_next").removeClass("btn_navegacion_next_activo").addClass("btn_navegacion_next_inactivo");
	}
	
	this.desactivatePrev = function(){
		$("#btn_prev").removeClass("btn_navegacion_prev_activo").addClass("btn_navegacion_prev_inactivo");
		$("#animActivo").stop(true);
	}
	
	this.checkStatus = function() {
		if (configMgr.courseType == "abierto") {
			$("#btn_next").removeClass().addClass("btn_navegacion_next_activo");
		} else {
			if (courseMgr.getPageStatus(courseMgr.getCurrentPage())) {
				$("#btn_next").removeClass().addClass("btn_navegacion_next_activo");
				_global.externalTrace("navigation.checkStatus", "next enabled");
			} else {
				$("#btn_next").removeClass().addClass("btn_navegacion_next_inactivo");
				_global.externalTrace("navigation.checkStatus", "next disabled");
			}
		}
		this.updateLabel();
	}
	
	this.updateLabel = function () {
		_global.externalTrace("navigation.updateLabel", true);
		if(courseMgr.getCurrentPage() != "m") {
			//TODO: Add support for no menu option
			var maxPage; 
			var currPage;
			var currentModule = courseMgr.getModule(courseMgr.getCurrentModule());
			var actualModule = courseMgr.getCurrentModule();
		
			if(_global.usemenu) {
				currPage = courseMgr.getCurrentPage() - currentModule.paginaMinima + 1;
				maxPage = courseMgr.paginaMaxima - currentModule.paginaMinima + 1;
			} else {
				currPage = courseMgr.getCurrentPage();
				maxPage = courseMgr.getTotalPages() - 1;
			}
			if(currPage==maxPage){$("#btn_next").removeClass().addClass("btn_navegacion_next_inactivo");}
		}
		
		$("#" + idDom + " > div.counter").html(currPage + " / " + maxPage);
					
		// var npaginas_mod = (currentModule.paginaMaxima - currentModule.paginaMinima)+1;
		// var npaginaactual_mod = _root.courseMgr.getCurrentPage() - currentModule.paginaMinima + 1;
		// var porcentaje = (npaginaactual_mod*100)/npaginas_mod;
	}

	
	function doPrevPage() {
		_global.externalTrace("navigation.doPrevPage", true);
		prevPage();
	}

	function doNextPage() {
		_global.externalTrace("navigation.doNextPage", true);
		nextPage();
	}

	function doMenu() {
		_global.externalTrace("navigation.domenu", true);
		gotoMenu();
	}

	this.show = function(){
		$("#" + idDom).show();
	}

	this.hide = function(){
		$("#" + idDom).hide();
	}
	
}
	
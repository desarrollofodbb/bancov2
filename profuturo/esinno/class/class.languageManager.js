/*
 * Objeto de gestión del idioma. Sirve para devolver los literales del engine
 * @author Arturo Macías Fernández
 */

var LanguageManager = function (path) {
	
	var xml;
	
	$.ajax({
		url: path,
		// dataType : "xml",
		async: false,
		success: function(_xml) {
			xml = _xml;
		},
		error:function(jqXHR, textStatus, errorThrown){
			_global.externalTrace("ERROR: No se ha podido leer el fichero XML de idioma", textStatus);
		}
	});
	
	this.get = function(id){
		return $(xml).find("#" + id).text();
	}
	
	$("#close_screen H1").html(this.get("salir_titulo"));
	$("#btn_continue_course").html(this.get("salir_positivo"));
	$("#btn_close_course").html(this.get("salir_negativo"));
	
	$("#continue_screen H1").html(this.get("continuar_titulo"));
	$("#btn_continue_course2").html(this.get("continuar_positivo"));
	$("#btn_start_course").html(this.get("continuar_negativo"));
	// Get no internet message and store in a global variable
	window._noInternet = this.get("no_internet");
}
	
/**
 * Modulo de Configuración general de la aplicación
 * @author Arturo Macías Fernández
 */

// var ConfigManager = function (xmlPath) {
var ConfigManager = function (xmlPath) {
	
	//Propiedades privadas
	var _debug = true;
	var _useJavaScript = false;
	var _preloadUse = 0;
	var _accesibilidad = true;
	// this.audiosInterfaz = "contenidos/_audios/interfaz.swf";
	var _courseType = "abierto";
	var _colorNombreModulo = "#9FD4E3";
	var _colorNombrePagina = "#FFFFFF";
	var _mostrarModulo = true;
	var _operationMode = "page";
	var _textos = "langpack.xml";
	var _xmlPath = xmlPath;
	// var _UIloader = '';
	// var _UIcontrolpanel = '';
	// var _UItitle = '';
	// var _UInavigation = '';
	// var _UImenu = '';
	// var _UIclose = '';
	// var _UIcontinue = '';
	var _xml;
	var _useMenu = true;
	var _menu_index = true;
	var _menu_download = false;
	var _menu_help = false;
	var _menu_credits = false;
	var _menu_audio = false;
	var _menu_glosario = false;
	
	//Propiedades públicas
	this.debug;
	this.useJavaScript;
	this.preloadUse;
	this.accesibilidad;
	this.courseType;
	this.colorNombreModulo;
	this.colorNombrePagina;
	this.mostrarModulo;
	this.operationMode;
	this.textos;
	// this.UIloader;
	// this.UIcontrolpanel;
	// this.UItitle;
	// this.UInavigation;
	// this.UImenu;
	// this.UIclose;
	// this.UIcontinue;
	this.xml;
	this.useMenu;
	this.menu_index;
	this.menu_download;
	this.menu_help;
	this.menu_credits;
	this.menu_audio;
	this.menu_glosario;
	
	//Asignaciones a variables públicas
	this.setPublics = function(){
		this.debug = _debug;
		this.useJavaScript = _useJavaScript;
		this.preloadUse = _preloadUse;
		this.accesibilidad = _accesibilidad;
		this.courseType = _courseType;
		this.colorNombreModulo = _colorNombreModulo;
		this.colorNombrePagina = _colorNombrePagina;
		this.mostrarModulo = _mostrarModulo;
		this.operationMode = _operationMode;
		this.textos = _textos;
		// this.UIloader = _UIloader;
		// this.UIcontrolpanel = _UIcontrolpanel;
		// this.UItitle = _UItitle;
		// this.UInavigation = _UInavigation;
		// this.UImenu = _UImenu;
		// this.UIclose = _UIclose;
		// this.UIcontinue = _UIcontinue;
		this.xml = _xml;
		this.useMenu = _useMenu;
		this.menu_index = _menu_index;
		this.menu_download = _menu_download;
		this.menu_help = _menu_help;
		this.menu_credits = _menu_credits;
		this.menu_glosario = _menu_glosario;
		this.menu_audio = _menu_audio;
	}	
		
	_global.externalTrace("ConfigManager.constructor");
	//Carga del XML
	$.ajax({
		url: _xmlPath,
		async: false,
		success: function(__xml) {
			_xml = __xml;
		},
		error:function(jqXHR, textStatus, errorThrown){
			_global.externalTrace("ERROR: No se ha podido leer el fichero XML de configuración", textStatus);
		}
	});
	
	$('configuraciones > *', _xml).each(function (index) {
		//debugger
		_text = this.textContent === undefined ? this.text : this.textContent;
		switch(this.nodeName.toLowerCase()) {
			case "debug":
				_debug = _text=='true'?true:false;
				break;
			case "textos":
				_textos = _text;
				break;
			case "javascript":
				_useJavaScript = _text=='true'?true:false;
				break;
			case "precarga":
				_preloadUse = new Number(_text);
				break;
			case "accesibilidad":
				_accesibilidad = _text=='true'?true:false;
				break;
			case "audiosInterfaz":
				_audiosInterfaz = _text;
				break;
			case "tipocurso":
				_courseType = _text;
				break;
			case "modo":
				_operationMode = _text;
				break;
			case "mostrarmodulo":
				_mostrarModulo = _text=='true'?true:false;
				break;
			case "colormodulo":
				_colorNombreModulo = _text;
				break;
			case "colorpagina":
				_colorNombrePagina = _text;
				break;
			case "usemenu":
				_useMenu = _text=='true'?true:false;
				break;
			case "index":
				_menu_index = _text=='true'?true:false;
				break;
			case "downloads":
				_menu_download = _text=='true'?true:false;
				break; 
			case "help":
				_menu_help = _text=='true'?true:false;
				break; 
			case "credits":
				_menu_credits = _text=='true'?true:false;
				break; 
			case "glosario":
				_menu_glosario = _text=='true'?true:false;
				break; 
			case "audio":
				_menu_audio = _text=='true'?true:false;
				break; 				
			// case "uimodules":
					// $('configuraciones > uimodules > *', _xml).each(function(index){
						// switch(this.attributes.getNamedItem('type').nodeValue) {
							// case "loader":
								// _UIloader = this.attributes.getNamedItem('path').nodeValue;
								// break;
							// case "close":
								// _UIclose = this.attributes.getNamedItem('path').nodeValue;
								// break;
							// case "menu":
								// _UImenu = this.attributes.getNamedItem('path').nodeValue;
								// break;
							// case "navigation":
								// _UInavigation = this.attributes.getNamedItem('path').nodeValue;
								// break;
							// case "title":
								// _UItitle = this.attributes.getNamedItem('path').nodeValue;
								// break;
							// case "controlpanel":
								// _UIcontrolpanel = this.attributes.getNamedItem('path').nodeValue;
								// break;
							// case "continue":
								// _UIcontinue = this.attributes.getNamedItem('path').nodeValue;
								// break;
						// }
					// });
					// break;
			default:
				_global.externalTrace("ConfigManager.init Config not recognized",this.nodeName);
				break;
		}
	});
	
	
	//Asignaciones a variables públicas
	this.setPublics();
}
	
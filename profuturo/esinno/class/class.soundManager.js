/**
 * Modulo de gestión de Sonidos
 * @author Juan Luis Ventura
 */
var SoundManager = function () {
	// Configuración SoundJS
	var audioPath = "contenido/audio/";
	
	_global.externalTrace("SoundManager.constructor");

	var inicializado = false;
	
	//Propiedades privadas
	var isPaused = false;
	var soundVolume = 1; // 0 -> 1 
	
	var instanciaSonidoPrimario = null;
	var instanciaSonidoSecundario = null;

	var isPlayingPrimario = false;
	var isPlayingSecundario = false;
	
	var callbackPrimario = null;
	var callbackSecundario = null;
	
	// div para el mensaje de pulsar si es un dispositivo móvil
	var mobileDiv;
	
	function inicializar () {
		if (!inicializado) {
			audioInit();
			inicializado = true;
		} 
	}

	function launchMobilePopup () {
		inicia_nav_audio++;
		removeMobilePopup ();
		// div para el mensaje de pulsar si es un dispositivo móvil
		mobileDiv = $('<div />').appendTo('#content');
		mobileDiv.attr('id', 'TouchToPlay');

        $("#TouchToPlay").css ("display", "block");
		$("#TouchToPlay").html( "<p>Please touch to start audio</p>" )
		$("#TouchToPlay")[0].addEventListener("click", handleClickPrimario, false);
		config_audio();
	}
	
	function config_audio(){
		$("#TouchToPlay").css({
				'position' : 'absolute',
				'height' : '26px',
				'width' : '33px',
				'top' : '510px',
				'left' : '940px',
				'z-index' : '100',
				'cursor' : 'pointer'
		});
		
		$("#TouchToPlay").find('p').css({
			'width' : '200px',
			'color' : '#fff',
			'margin-left' : '-150px',
			'margin-top' : '8px',
			'font-size' : '12px',
			'display' : 'none'
		});
		
		queue = new createjs.LoadQueue(true);	
		queue.addEventListener("fileload", handleSoundFileLoad);
		queue.addEventListener("complete", handleSoundComplete);
		queue.loadManifest([{id:"TouchToPlay", src:"img/sprite_audio.png"}]);
	}
	
	
	function handleSoundFileLoad(event) {
		var item = event.item; // Referencia al item que genera el evento
		if (item.id == "TouchToPlay"){
				$("#TouchToPlay").css("background","url("+ item.src +") no-repeat -50px 0px");
		}
	}
	
	function handleSoundComplete(event) {
		if(color_audio=="white"){
			$("#TouchToPlay").css("background-position","-50px 0px");
			$("#TouchToPlay").find('p').css({
					'color' : '#fff'
				});
		} else if(color_audio=="blue"){
				$("#TouchToPlay").css("background-position","0px 0px");
				$("#TouchToPlay").find('p').css({
					'color' : '#004050'
				});
		}
		if(portada){
			$("#TouchToPlay").find('p').css({
				'display' : 'block'
			});
		} else {
			if(inicia_nav_audio>1){
				$("#TouchToPlay").find('p').css({
					'display' : 'none'
				});

			} else {
				$("#TouchToPlay").find('p').css({
					'display' : 'block'
				});
			}
		}
	}
	
	function removeMobilePopup () {
		_global.externalTrace("SoundManager.removeMobilePopup ");	
		
		try {
			$("#TouchToPlay")[0].removeEventListener("click", handleClickPrimario);
		} catch (err) {}
		
		try {
			$("#TouchToPlay").css ("display", "none");
		} catch (err) {}
		
		try {
			$("#TouchToPlay").remove();
		} catch (err) {}			
	}
	
	/**
	 * Reproduce un sonido primario
	 * @param path (string)
	 * @param callback (function)
	 */
	SoundManager.prototype.playSoundPrimario = function  (path, _callbackPrimario) {
		inicializar();
		endPlaySoundPrimario();
		
		_global.externalTrace("SoundManager.playSoundPrimario");
		callbackPrimario = _callbackPrimario;
		
		createjs.Sound.addEventListener("fileload", handleLoadPrimario);		
		createjs.Sound.registerSound({id:"soundPrimario", src:path});
	}
	 
	function handleLoadPrimario(event) {
		_global.externalTrace("SoundManager.handleLoadPrimario > sound primario cargado [id: " + event.id + "] [src: " + event.src + "]");
		createjs.Sound.removeEventListener("fileload", handleLoadPrimario);
		if (isMobileBrowser()) {
			// se muestra el mensaje de hacer click para oír el audio
			launchMobilePopup();
		} else {
			// se reproduce el audio
			actionPlaySoundPrimario();
		}
		
	}
	
	function handleClickPrimario(event) {
		removeMobilePopup();
		
		actionPlaySoundPrimario();
    }
	
	function actionPlaySoundPrimario () {
		isPlayingPrimario = true;
		
		instanciaSonidoPrimario = createjs.Sound.play("soundPrimario");
		instanciaSonidoPrimario.setVolume(soundVolume);
		if (isPaused) {
			// previene que se reproduzca el sonido al cargar si se ha lanzado antes un evento de pausa
			instanciaSonidoPrimario.pause();
		}
		
		instanciaSonidoPrimario.addEventListener("complete", endPlaySoundPrimario);

		_global.externalTrace("SoundManager.actionPlaySoundPrimario > play sound primario");	

		removeMobilePopup ();
	}
	
	function endPlaySoundPrimario () {
		removeMobilePopup ();
		createjs.Sound.removeEventListener("fileload", handleLoadPrimario);
		
		_global.externalTrace("SoundManager.endPlaySoundPrimario");
		
		// instanciaSonidoPrimario.stop();
		if ((instanciaSonidoPrimario != null) && (instanciaSonidoPrimario.playState != createjs.Sound.PLAY_FINISHED)) {
			instanciaSonidoPrimario.stop();
			instanciaSonidoPrimario.removeEventListener("complete");
		}		
		
		try {
			createjs.Sound.removeSound("soundPrimario");
		} catch (err) {}
		
		isPlayingPrimario = false;
		
		if (callbackPrimario && typeof(callbackPrimario) === "function") {
			callbackPrimario();
		}
		
		// instanciaSonidoPrimario.removeEventListener("complete");
		// createjs.Sound.removeSound("soundPrimario");
	}
	

	/**
	 * Reproduce un sonido secundario
	 * @param path (string)
	 * @param callback (function)
	 */
	SoundManager.prototype.playSoundSecundario = function  (path, _callbackSecundario) {
		inicializar();
		
		endPlaySoundSecundario();
		
		_global.externalTrace("SoundManager.playSoundSecundario");
		callbackSecundario = _callbackSecundario;
		
		createjs.Sound.addEventListener("fileload", handleLoadSecundario);		
		createjs.Sound.registerSound({id:"soundSecundario", src:path});
	}
	 
	function handleLoadSecundario(event) {
		_global.externalTrace("SoundManager.handleLoadSecundario > sound Secundario cargado [id: " + event.id + "] [src: " + event.src + "]");
		createjs.Sound.removeEventListener("fileload", handleLoadSecundario);
		if (isMobileBrowser()) {
			// se muestra el mensaje de hacer click para oír el audio
			launchMobilePopup();
		} else {
			// se reproduce el audio
			actionPlaySoundSecundario();
		}
	}
	
	function handleClickSecundario(event) {
		removeMobilePopup();
		
		actionPlaySoundSecundario();
    }
	
	function actionPlaySoundSecundario () {
		isPlayingSecundario = true;
		
		instanciaSonidoSecundario = createjs.Sound.play("soundSecundario");
		instanciaSonidoSecundario.setVolume(soundVolume);
		if (isPaused) {
			// previene que se reproduzca el sonido al cargar si se ha lanzado antes un evento de pausa
			instanciaSonidoSecundario.pause();
		}
		
		instanciaSonidoSecundario.addEventListener("complete", endPlaySoundSecundario);

		_global.externalTrace("SoundManager.actionPlaySoundSecundario > play sound Secundario");	

		removeMobilePopup ();
	}
	
	function endPlaySoundSecundario () {
		removeMobilePopup ();
		createjs.Sound.removeEventListener("fileload", handleLoadSecundario);
		
		_global.externalTrace("SoundManager.endPlaySoundSecundario");
		
		// instanciaSonidoSecundario.stop();
		if ((instanciaSonidoSecundario != null) && (instanciaSonidoSecundario.playState != createjs.Sound.PLAY_FINISHED)) {
			instanciaSonidoSecundario.stop();
			instanciaSonidoSecundario.removeEventListener("complete");
		}		

		try {
			createjs.Sound.removeSound("soundSecundario");
		} catch (err) {}
		
		isPlayingSecundario = false;
		
		if (callbackSecundario && typeof(callbackSecundario) === "function") {
			callbackSecundario();
		}
		
		// instanciaSonidoSecundario.removeEventListener("complete");
		// createjs.Sound.removeSound("soundSecundario");
	}
	
/****/	
	
	
	SoundManager.prototype.isMuted = function () {
		if (soundVolume == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	SoundManager.prototype.mute = function () {
		_global.externalTrace("SoundManager.mute Audio");

		soundVolume = 0;
		if (instanciaSonidoPrimario != null) {
			instanciaSonidoPrimario.setVolume(soundVolume);
		}
		
		if (instanciaSonidoSecundario != null) {
			instanciaSonidoSecundario.setVolume(soundVolume);
		}
	}
	
	SoundManager.prototype.unmute = function() {
		_global.externalTrace("SoundManager.unmute Audio");
		
		soundVolume = 1;
		if (instanciaSonidoPrimario != null) {
			instanciaSonidoPrimario.setVolume(soundVolume);
		}
		
		if (instanciaSonidoSecundario != null) {
			instanciaSonidoSecundario.setVolume(soundVolume);
		}
	}
	
	
	SoundManager.prototype.pause = function () {
		_global.externalTrace("SoundManager.pause [isPlaying:" + isPlaying() + "]");
		
		if ((instanciaSonidoPrimario != null) && (instanciaSonidoPrimario.playState != createjs.Sound.PLAY_FINISHED)) {
			if (!instanciaSonidoPrimario.paused) 
				instanciaSonidoPrimario.pause();
		}
		
		if ((instanciaSonidoSecundario != null) && (instanciaSonidoSecundario.playState != createjs.Sound.PLAY_FINISHED)) {
			if (!instanciaSonidoSecundario.paused) 
				instanciaSonidoSecundario.pause();
		}
		
		isPaused = true;
	}
	
	SoundManager.prototype.unpause = function () {
		_global.externalTrace("SoundManager.unpause [isPaused:" + isPaused + "]");
		
		if (isPaused) {
			if ((instanciaSonidoPrimario != null) && (instanciaSonidoPrimario.playState != createjs.Sound.PLAY_FINISHED)) {
				_global.externalTrace("SoundManager.unpause instanciaSonidoPrimario");
				instanciaSonidoPrimario.resume();
			} 
			
			if ((instanciaSonidoSecundario != null) && (instanciaSonidoSecundario.playState != createjs.Sound.PLAY_FINISHED)) {
				_global.externalTrace("SoundManager.unpause instanciaSonidoSecundario");
				instanciaSonidoSecundario.resume();
			} 
			
			isPaused = false;
		}
	}
	
	SoundManager.prototype.stopAudios = function () {
		_global.externalTrace("SoundManager.stopAudios");
		
		if ((instanciaSonidoPrimario != null) && (instanciaSonidoPrimario.playState != createjs.Sound.PLAY_FINISHED)) {
			instanciaSonidoPrimario.stop();

			if (callbackPrimario && typeof(callbackPrimario) === "function") {
				callbackPrimario();
			}
			
			instanciaSonidoPrimario.removeEventListener("complete");
			createjs.Sound.removeSound("soundPrimario");			
		} 
		
		if ((instanciaSonidoSecundario != null) && (instanciaSonidoSecundario.playState != createjs.Sound.PLAY_FINISHED)) {
			instanciaSonidoSecundario.stop();
			
			if (callbackSecundario && typeof(callbackSecundario) === "function") {
				callbackSecundario();
			}
			
			instanciaSonidoSecundario.removeEventListener("complete");
			createjs.Sound.removeSound("soundSecundario");			
		} 
		
		this.unpause();
		
		removeMobilePopup();
	}
	
	function isPlaying() {
		return (isPlayingPrimario || isPlayingSecundario);
	}
	
	/**
	 * Carga y registra el uso de SoundJS
	 */
	function audioInit() {
		// div para el reproductor Flash de audio 
		// <div id="SoundJSFlashContainer" class="SoundJSFlashContainer" style="width:1px; height:1px; position:fixed; left:-1px;"></div>
		var flashDiv = $('<div />').appendTo('body');
		flashDiv.attr('id', 'SoundJSFlashContainer');
		flashDiv.addClass('SoundJSFlashContainer');

		// navegadores que no soportan WebAudio o HTMLAudio
		createjs.FlashPlugin.BASE_PATH = "js/soundjs/src/soundjs/";
		createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashPlugin]);
		// Adds FlashPlugin as a fallback if WebAudio and HTMLAudio do not work.	

		// if initializeDefaultPlugins returns false, we cannot play sound in this browser
		if (!createjs.Sound.initializeDefaultPlugins()) { return; }
	}

}	

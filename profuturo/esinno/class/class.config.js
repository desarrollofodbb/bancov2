/*
 * Objeto de configuración general. Contiene datos globlales
 * @author Arturo Macías Fernández
 */

var Config = function (debug) {
	
	//Propiedades
    this.debug = new Boolean(debug);
    this.engineVersion = "0.001";
	this.operationMode = '';
	this.useMenu = true;
	
	this.externalTrace = function(msg, obj) {
		if (window.console != undefined){
			if (this.debug) {
				if (obj != undefined ) {
					console.log(msg, obj);
				} else {
					console.log(msg);
				}
			} 
		}
	}
}
	
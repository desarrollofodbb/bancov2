/**
 * Maneja las paginas disponbles, y coloca metodos para manejar la información que requiere la navegación
 * @author Arturo Macías Fernández
 */

var CourseManager = function () {
	//TODO: Add suport to no menu
	
	//Propiedades privadas
	var docArray = new Array();
	var capitulosArray = new Array();
	var currentPage = 0;
	var currentMaxPage = 0;
	var currentModule = 0;
	var viewedPages;
	_global.externalTrace("CourseManager.constructor");
	
	//***** definimos array para enviar a menú ****************
	var page_menu = new Array();
	page_menu = [];
	var complete_status = new Array();
	complete_status = [];
	// *************************************************************
	
	this.setCourse = function(xml, act){
		_global.externalTrace("CourseManager.setCourse");
		var chap;
		var page = 0;
		var i = 0;
				
		//busca el nodo contenidos
		$('contenidos > modulo', xml).each(function(index){
			//define los capitulos
			chap = i;
			capitulosArray[chap] = new Object();
			capitulosArray[chap].paginaMinima = page;
			capitulosArray[chap].paginaMaxima = page;
			//***** añadimos array para enviar a menú ****************
			if(page > 0){
				page_menu.push(page);
			}
			
			// *************************************************************
			capitulosArray[chap].titulo = this.attributes.getNamedItem('titulo').nodeValue;
			capitulosArray[chap].tipo = this.attributes.getNamedItem('tipo').nodeValue;
			if (this.attributes.getNamedItem('tipo').nodeValue == 'contenido')
				capitulosArray[chap].porcentaje = new Number(this.attributes.getNamedItem('porcentaje').nodeValue);
			else
				capitulosArray[chap].porcentaje = 0;
			capitulosArray[chap].completado = false;
			capitulosArray[chap].started = false;
			capitulosArray[chap].id = chap;
			// _global.externalTrace(this.childNodes);
			$('contenidos > modulo[id='+ this.attributes.getNamedItem('id').nodeValue+'] > item', xml).each(function(index){
				docArray[page] = new Object();
				docArray[page].carpeta = this.attributes.getNamedItem('carpeta').nodeValue;
				docArray[page].tipo = this.attributes.getNamedItem('tipo').nodeValue;;
				docArray[page].peli = this.attributes.getNamedItem('peli').nodeValue;;
				docArray[page].botonera = this.attributes.getNamedItem('botonera').nodeValue;;
				docArray[page].titulo = this.attributes.getNamedItem('titulo').nodeValue;;
				docArray[page].completado = false;
				docArray[page].id = page;
				if (this.attributes.getNamedItem('instruccion') == null) 
					docArray[page].instruccion = '';
				else
					docArray[page].instruccion = this.attributes.getNamedItem('instruccion').nodeValue;
				
				if (_global.operationMode == "test" && docArray[page].tipo == "test") {
					docArray[page].score = new Number(this.attributes.getNamedItem('score').nodeValue);
					docArray[page].mastery = new Number(this.attributes.getNamedItem('mastery').nodeValue);
					testMgr.addTest(docArray[page]);
				}
				
				//agrega mp3 del xml, opcional
				if (this.attributes.getNamedItem('mp3') == null)
					docArray[page].mp3 = '' ;
				else
					docArray[page].mp3 = this.attributes.getNamedItem('mp3').nodeValue ;
				
				//actualiza el ID de la ultima página del capitulo
				capitulosArray[chap].paginaMaxima = page;
				page++;
			});
			i++;
		});
		_global.externalTrace("CourseManager.setCourse.capitulosArray", capitulosArray);
		_global.externalTrace("CourseManager.setCourse.docArray", docArray);
		
		this.setPreviousLocation(act);
	}
	
	this.setPreviousLocation = function (act) {
		_global.externalTrace("CourseManager.setPreviousLocation", act);
		var main = act.split("|");
		var k = main[0].split(":");
		
		//valiadcion de alguna vez cursado algo
		if (k.length > 3) {
			_global.externalTrace("CourseManager.setPreviousLocation order array", k);
			viewedPages = new Number(k.shift());
			var totalPages = new Number(k.shift());
			var a = k.shift();
			if(a != "m") {
				currentPage = new Number(a);
			} else {
				currentPage = a;
			}
			
			currentModule = this.pageToModule(currentPage);
						
			//marca modulos vistos
			for (var i = 0; i < k.length; i +=  2) {
				var t = Math.floor(i / 2);
				if((k[i]) == 1) {
					capitulosArray[t].completado = true;
				} else {
					capitulosArray[t].completado = false;
				}
			}
			for (var i = 1; i < k.length; i +=  2) {
				var t = Math.floor(i / 2);
				for (var j = capitulosArray[t].paginaMinima; j <= Number(k[i]); j++) {
					docArray[j].completado = true;
				}
			}
			
		}
		
		if (main.length > 1) {		
			for (var i = 1; i < main.length; i++) {
				var l = main[i].split(":");
				testMgr.updateTest(parseInt(l[0]),parseInt(l[1]));
			}
		}
		//marca modulos comenzados
		for (var i = 0; i < k.length; i +=  2) {
				var t = Math.floor(i / 2);
				if(parseInt(k[i]) == 0 && capitulosArray[t].paginaMinima <= parseInt(k[i+1])) {
					capitulosArray[t].started = true;
				} else {
					capitulosArray[t].started = false;
				}
			}
		
		_global.externalTrace("CourseManager.setPreviousLocation check capitulos", capitulosArray);
		_global.externalTrace("CourseManager.setPreviousLocation check capitulos", docArray);
	}
	
	this.pageToModule = function (id) {
		_global.externalTrace("CourseManager.pageToModule", id);
		var moduleId;
		
		for (var i = 0; i < capitulosArray.length; i++) {
			if (id >= capitulosArray[i].paginaMinima && id <= capitulosArray[i].paginaMaxima) {
				moduleId = i;
			}
		}
		return moduleId;
	}
	
	/**
	 * Se llama desde un pretest para dar por completado un modulo
	 * @param id (int) 
	 */
	this.setModuleApproved = function(id) {
		_global.externalTrace("CourseManager.setModuleApproved", id);
		capitulosArray[id].completado = true;
	}
	
	/**
	 * Devuelve el numero de páginas vista
	 * @return viewedPages int
	 */
	this.hasViewedPages = function() {
		return viewedPages;
	}
	
	/**
	 * Devuelve una página de un módulo
	 * @param id (int) 
	 * @return object 
	 */
	this.getElement = function(id) {
		_global.externalTrace("CourseManager.getElement", id);
		return docArray[id];
	}
	
	/**
	 * Devuelve un módulo
	 * @param id (int) 
	 * @return object 
	 */
	this.getModule = function(id) {
		_global.externalTrace("CourseManager.getModule", id);
		return capitulosArray[id];
	}
	
	/**
	 * Devuelve todas las páginas de un módulo
	 * @param id (int) 
	 * @return array 
	 */
	this.getElements = function() {
		_global.externalTrace("CourseManager.getModules");
		return docArray;
	}
	
	/**
	 * Devuelve todos los módulos
	 * @param id (int) 
	 * @return array 
	 */
	this.getModules = function() {
		_global.externalTrace("CourseManager.getModules");
		///////this.getStatus_menu();
		return capitulosArray;
	}
	
		
	// ******* devuelve array page_menu ***************************
	this.getPage_menu = function(){
		_global.externalTrace("getPage_menu");
		return page_menu;
	}
	// *************************************************************
	
	
	
	// ************** estado lineal engine *************************
	this.getStatus_menu = function(){
		_global.externalTrace("getStatus_menu");
		complete_status = [];
		if(configMgr.courseType=="lineal"){
			$.each(capitulosArray,function(ind){
				if(capitulosArray[ind].tipo!="intro"){
					complete_status.push(capitulosArray[ind].completado);
				}
			});
		}
		
		return complete_status;
	}
	// *************************************************************
	
	/**
	 * Devuelve todos los módulos
	 * @param id (int) 
	 * @param preload (int) 
	 * @return array 
	 */
	this.getMultipleElements = function(id, preload) {
		_global.externalTrace("CourseManager.getMultipleElements", id);
		_global.externalTrace("CourseManager.getMultipleElements.preload", preload);
		var elements = new Array();
		if (id > 0) {
			elements[0] = docArray[id - 1];
		} else {
			elements[0] = null;
		}
		elements[1] = docArray[id];
		
		if (preload > 0) {
			for (var i = 1; i <= preload; i++) {
				if(id+i < docArray.length) {
					elements[1 + i] = docArray[id + i];
				} else {
					elements[1 + i] = null;
				}
			}
		}
		return elements;
	}
	
	/**
	 * Devuelve el número de páginas de un módulo
	 * @param page (int) 
	 * @return int 
	 */
	this.getChapterElementsCount = function (page){
		_global.externalTrace("CourseManager.getChapterElementsCount",page);
		
		var chapterId = this.pageToModule(page);
		var amount = capitulosArray[page].paginaTotal;
		
		return amount;
	}
	
	/**
	 * Devuelve el listado con todos los capítulos
	 * @return array 
	 */
	this.getChapters = function () {
		_global.externalTrace("CourseManager.getChapters");
		return capitulosArray;
	}
	
	/**
	 * Devuelve la página actual (índice)
	 * @return int 
	 */
	this.getCurrentPage = function () {
		_global.externalTrace("CourseManager.getCurrentPage",currentPage);
		return currentPage;
	}
	
	/**
	 * Devuelve el módulo actual (índice)
	 * @return int 
	 */
	this.getCurrentModule = function() {
		_global.externalTrace("CourseManager.getCurrentModule",currentModule);
		return currentModule;
	}
	
	/**
	 * Devuelve la página máxima 
	 * @return int 
	 */
	this.getMaxPage = function() {
		_global.externalTrace("CourseManager.getCurrentPage");
		return currentMaxPage;
	}
	
	/**
	 * Devuelve la página máxima
	 * @return object 
	 */
	this.getMaxChapter = function () {
			_global.externalTrace("CourseManager.getMaxChapter");
			return this.pageToModule(currentMaxPage);
	}
	
	/**
	 * Devuelve el total de páginas
	 * @return int 
	 */
	this.getTotalPages = function() {
		_global.externalTrace("CourseManager.getTotalPages");
		return docArray.length;
	}
	
	/**
	 * Establece la siguiente página
	 */
	this.setNextPage =  function() {
		_global.externalTrace("CourseManager.setNextPage",currentPage);
		_global.externalTrace("CourseManager.setNextPage maxPage",capitulosArray[currentModule].paginaMaxima);
		_global.externalTrace("CourseManager.setNextPage currentModule",currentModule);
		docArray[currentPage].completado = true;
		var tempPage = new Number(currentPage) + 1;
		_global.externalTrace("CourseManager.setNextPage temppage", tempPage);
				
		currentPage = tempPage;
				
		if (tempPage > capitulosArray[currentModule].paginaMaxima) {
			if (_global.usemenu) {
				capitulosArray[currentModule].completado = true;
				/////////this.getStatus_menu();
				currentPage = "m";
			} else {
				capitulosArray[currentModule].completado = true;
				currentModule++;
			}
		}
		
		if (currentPage > currentMaxPage && currentPage != "m") {
			currentMaxPage = currentPage+1;
		}
		
		_global.externalTrace("CourseManager.setNextPage:Results",currentPage);
	}
	
	/**
	 * Establece la página anterior
	 */
	this.setPrevPage = function() {
		_global.externalTrace("CourseManager.setPrevPage",currentPage);
		var tempPage = new Number(currentPage) - 1;
		currentPage = tempPage;
		if (tempPage < capitulosArray[currentModule].paginaMinima && _global.usemenu) {
		////////this.getStatus_menu();
			currentPage = "m";
		}
		
		if (tempPage < capitulosArray[currentModule].paginaMinima) {
			currentModule--;
		} 
		
		
		_global.externalTrace("CourseManager.setPrevPage:Results",currentPage);
	}
	
	/**
	 * Establece la página actual
	 * @param id (int) 
	 */
	this.setCurrentPage = function (id) {
		_global.externalTrace("CourseManager.setCurrentPage", id);
		//currentPage = id;
		currentPage = id;
		//docArray[currentPage].completado = true;

		currentModule = this.pageToModule(currentPage);
		if (id == capitulosArray[currentModule].paginaMinima) {
			capitulosArray[currentModule].started = true;
		}
			_global.externalTrace("CourseManager.setCurrentPage started", capitulosArray[currentModule].started);
	}
		
	/**
	 * Establece la página actual como la página de menu principal
	 */
	this.setMenu = function () {
		//this.getStatus_menu();
		currentPage = "m";
	}
	
	/**
	 * Marca todo el curso como completado (módulos y páginas)
	 */
	this.setCompleted = function() {
		_global.externalTrace("CourseManager.setCompleted");
		for (var i = 0; i < docArray.length; i++) {
			docArray[i].completado = true;
		}
		for (i = 0; i < capitulosArray.length; i++) {
			capitulosArray[i].completado = true;
		}
		currentMaxPage = currentPage+1;
	}
	
	/**
	 * Resetea el curso, deja todo como no completado y coloca los cursores al inicio del curso
	 */
	this.reset = function() {
		_global.externalTrace("CourseManager.reset");
		currentPage = 0;
		currentModule = 0;
		for (var i = 0; i < docArray.length; i++) {
			docArray[i].completado = false;
		}
		for (i = 0; i < capitulosArray.length; i++) {
			capitulosArray[i].completado = false;
		}
	}
	
	/**
	 * Devuelve el status (completado o no) de la página pasada como parámetro
	 * @param id (int)
	 * @return boolean
	 */
	this.getPageStatus =  function (id) {
		return docArray[id].completado;
	}
	
	/**
	 * Devuelve el status (completado o no) del módulo pasado como parámetro
	 * @param id (int)
	 * @return boolean
	 */
	this.getModuleStatus = function (id) {
		return capitulosArray[id].completado;
	}
	
	/**
	 * 
	 * @return string
	 */
	this.getLocationString =  function () {
		
		_global.externalTrace("CourseManager.getLocationString");
		var msg = "";
		var t;
		var k;
		//get pantallas vistas total
		msg += this.countViewedScreens() + ":";
		msg += docArray.length + ":";
		msg += currentPage + ":";
		for (var i = 0; i < capitulosArray.length; i++) {
			//calcula ultima pantlla vista del modulos
			k = 0;
			for (var j = capitulosArray[i].paginaMinima; j <= capitulosArray[i].paginaMaxima; j++) {
				if (docArray[j].completado) { 
					k = j;
				}
			}
			//ve si el modulo esta completo
			if (capitulosArray[i].completado) {
				t = 1;
			} else {
				t = 0;
			}
			msg += t + ":" + k;
			if (i < capitulosArray.length - 1) {
				msg += ":";
			}
		}
		return msg;
	}
	
	/**
	 * Devuelve el número de páginas completadas 
	 * @return string
	 */
	this.countViewedScreens = function () {
		var k = 0;
		for (var i = 0; i < docArray.length; i++) {
			if (docArray[i].completado) {
				k++;
			}
		}
		return k;
	}
	
	
}
	
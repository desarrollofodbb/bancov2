/**
 * Modulo de gestión de Accesibilidad
 * @author Juan Luis Ventura
 */
 
//function AccesibilityManager(_mc:MovieClip, sound:Boolean, inter:String) {
// function AccesibilityManager(_mc, sound, inter) {
 
 
var AccesibilityManager = function () {

	// Propiedades privadas
	var container; // :MovieClip;
	var interfaz = ""; // :String;
	var interfazMC; // :MovieClip;
	var hasSound = false; //:Boolean;
	var interfazElements = new Array(); // :Array;
	var locucion; // :Sound;
	


		_global.externalTrace("AccesibilityManager", inter);
		container = _mc;
		hasSound = sound;
		if(hasSound) {
			interfaz = inter;
			interfazMC = container.createEmptyMovieClip("intSound", container.getNextHighestDepth());
			interfazMC.loadMovie(interfaz);
		}
	
		interfazElements = new Array();
		interfazElements.push("indice");
		interfazElements.push("ayuda");
		interfazElements.push("descargas");
		interfazElements.push("creditos");
		interfazElements.push("glosario");
		interfazElements.push("avanzar");
		interfazElements.push("retroceder");
		interfazElements.push("salir");
		interfazElements.push("si");
		interfazElements.push("no");
		interfazElements.push("contiuar");
		interfazElements.push("iniciar");
		interfazElements.push("cerrar");
		interfazElements.push("bajar");
		interfazElements.push("subir");
		interfazElements.push("cerrarayuda");
		interfazElements.push("volver");
		//elementos de menu
		interfazElements[20] = "menu0";
		interfazElements[21] = "menu1";
		interfazElements[22] = "menu2";
		interfazElements[23] = "menu3";
		interfazElements[24] = "menu4";
		interfazElements[25] = "menu5";
		interfazElements[26] = "menu6";
		interfazElements[27] = "menu7";
		interfazElements[28] = "menu8";
		interfazElements[29] = "menu9";

}
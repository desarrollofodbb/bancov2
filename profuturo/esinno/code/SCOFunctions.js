// VARIABLES **************************************************************
//*************************************************************************
var reloj_inicial;
// Major version of Flash required
var requiredMajorVersion = 7;
// Minor version of Flash required
var requiredMinorVersion = 0;
// Minor version of Flash required
var requiredRevision = 0;
// ACTIVAR DEBUG **********************************************************
//*************************************************************************

var debug_lectura=0; // activamos, desactivamos debug para leer de la plataforma.
var debug_escritura=0; //activamos, desactivamos debug para ver qué mandamos escribir en la plataforma.
var debug_escritura_confirmacion=0; //activamos, desactivamos debug para ver si se está ecribiendo bien en la plataforma.
var debug_evento_load=0; //activamos, desactivamos debug sobre evento load de la página.
var debug_evento_unload=0; //activamos, desactivamos debug sobre evento unload de la página.
var debug=false; // activamos, desactivamos debug de operación general.

if(debug) {
	debug_lectura=1; // activamos, desactivamos debug para leer de la plataforma.
	debug_escritura=1; //activamos, desactivamos debug para ver qué mandamos escribir en la plataforma.
	debug_escritura_confirmacion=1; //activamos, desactivamos debug para ver si se está ecribiendo bien en la plataforma.
	debug_evento_load=1; //activamos, desactivamos debug sobre evento load de la página.
	debug_evento_unload=1
}

//-----------INICIALIZAMOS
//var modo_operacion = "test";
var modo_operacion;
var numero_paginas=0;
var hacerrado =0;
var automa ="";
//creating shortcut for less verbose code
var scorm = pipwerks.SCORM;
var courseStatus = "";
var actu = "";

// control del aviso de fallo del curso. Sólo debe salir una vez
var paso_test_passed;
var paso_test_failed;

//######################################################## FUNCIONES COMUNES 
//##########################################################################

/*
	Ejecutado en el onload, inicializa plataforma y variables de entorno
*/
function inicia() {
	
	//SCORM TYPE
	scorm.version = "1.2";
	scorm.handleCompletionStatus = false;
	
	paso_test_passed = false;
	paso_test_failed = false;
	
	var mensaje="";	
	reloj_inicial= new Date();
	
	//inicializa plataforma
	var result = plataforma_On();

	if (result) {
		mensaje="Plataforma inicializada";
		//if ((leer("status") == "not attempted" || leer("status") == "unknown") && leer("sucess") == "unknown") {
		if ((leer("status") == "not attempted") || (leer("status") == "unknown") && leer("sucess") == "unknown") {
			if(debug) {
				alert("iniciando curso");
			}
			//Inicializa la WBT si no se ha intentado nunca
	  		escribir("status","incomplete");
			//CHECK
			escribir("raw","0");
			escribir("location","0:0:0");
	  		escribir("data","0:0:0");
	  	}
	} else {
		mensaje="Error al inicializar la plataforma";
	}
	escribir("min",0);
	escribir("max",100);
	if(debug) {
		alert("ejecutando commit");	
	}
	ejecutar();
	
	//Lee el location para pasar al engine flash
	automa = leer("data");
	
	if (automa.legth < 1){
		automa="0:0:0";
	}
	//DEBUG DE EVENTO LOAD
	if(debug_evento_load==1){
		alert("debug evento load " +  mensaje);
	}
	//FIN DEBUG EVENTO LOAD
	
	actu = automa;
}

/*
define el modo de operacion del score
La opciones son 
page (avance calculado por página) (modo por defecto)
module (avance calculado por módulo)
test (avance calculado por test)
*/
function modoDeOperacion(tipo) {
	//alert("mode operación " + tipo);
	if(tipo == "page" || tipo == "none" || tipo == "test") {
		modo_operacion = tipo;
		if(debug) {
			alert("modo de operación activado, tipo " + modo_operacion);	
		}
	} else {
		alert("modo de operación " + tipo + "no está soportado");
	}
}

/*
Calcula tiempo del intento
*/
function calculaTiempo(){
	var reloj_final = new Date();
	var ms = reloj_final.getTime() - reloj_inicial.getTime();
	
	if(scorm.version == "2004") {
		var n = ms/10;
		n = Math.max(n,0); // there is no such thing as a negative duration 
		var str = "P"; 
		var nCs = n; 
		// Next set of operations uses whole seconds 
		var nY = Math.floor(nCs / 3155760000); 
		nCs -= nY * 3155760000; 
		var nM = Math.floor(nCs / 262980000); 
		nCs -= nM * 262980000; 
		var nD = Math.floor(nCs / 8640000); 
		nCs -= nD * 8640000; 
		var nH = Math.floor(nCs / 360000); 
		nCs -= nH * 360000; 
		var nMin = Math.floor(nCs /6000); 
		nCs -= nMin * 6000 
		// Now we can construct string 
		if (nY > 0) str += nY + "Y"; 
		if (nM > 0) str += nM + "M"; 
		if (nD > 0) str += nD + "D"; 
		if ((nH > 0) || (nMin > 0) || (nCs > 0)) { 
        str += "T"; 
        if (nH > 0) str += nH + "H"; 
        if (nMin > 0) str += nMin + "M"; 
            if (nCs > 0) str += (nCs / 100) + "S"; 
		} 
		if (str == "P") str = "PT0H0M0S"; 
        // technically PT0S should do but SCORM test suite assumes longer form. 
		return str; 
	} else {
		var milliseconds = parseInt((ms%1000)/100)
			, seconds = parseInt((ms/1000)%60)
			, minutes = parseInt((ms/(1000*60))%60)
			, hours = parseInt((ms/(1000*60*60))%24);
		
		hours = (hours < 10) ? "0" + hours : hours;
		minutes = (minutes < 10) ? "0" + minutes : minutes;
		seconds = (seconds < 10) ? "0" + seconds : seconds;
		return  hours + ":" + minutes + ":" + seconds;
	}
}

/*
Cierra la plataforma (desde el flash o cerrando la ventana)
*/

function descarga() {
	if(debug_evento_unload==1){
		alert("Entra en descargar página " + hacerrado);
	}
	if(hacerrado==0){
		escribir("location",automa);
		if(modo_operacion != "test") {
			escribir("raw",automa);
		}
		escribir("time",calculaTiempo());
		ejecutar();
		plataforma_Off();
		//DEBUG DE EVENTO UNLOAD
		if(debug_evento_unload==1){
			alert("Entra en descargar página definitiva");
		}
		//FIN DEBUG EVENTO UNLOAD
		hacerrado=1;
		if (modo_operacion != "none") {
			//PATH SANTANDER, no cierra la ventana en su modo de operación
			window.top.close();
		}
		
	}
}

function ejecutar(){
	if(debug) {
		alert("commit");
	}
	scorm.save();
}

function guardaDatos(valor){
	_global.externalTrace("guardaDatos", valor);
	if(debug) {
		alert("guardaDatos " + valor);
	}
	escribir("data",valor);
	escribir("location",valor);
	escribir("raw",valor);
	ejecutar();
}

function guardaMenu(){
	var g = leer('data');
	var arr = g.split(":");
	var s = "";
	_global.externalTrace("guardaMenu", arr);
	for (var i = 0; i< arr.length; i++) {
		if(i == 2) {
			s += ":m";
		} else if (i == 0) {
			s += arr[i];
		} else {
			s+= ":" + arr[i];
		}
	}
	_global.externalTrace("guardaMenu", s);
	guardaDatos(s);
}

function guardaTest(scr,status){
	_global.externalTrace("guardaDatos", scr + " " + status);
	if(debug) {
		alert("guardaTest " + scr + " " + status);
	}
	if(status == "failed" && parseInt(scr) >= 75) {
		scr = 75;
	}
	escribir('raw',parseInt(scr));
	escribir('status',status);


	ejecutar();
}

//################################################### FUNCTION SCORM EVO 1.2 ;D
//##########################################################################


function leer(cual){
	var resultado="";
	switch (cual){
		//STATUS
		case "status":
		if(scorm.version == "2004") {
			resultado= scorm.get("cmi.completion_status");
		} else {
			resultado= scorm.get("cmi.core.lesson_status");
		}
		break;
		//STATUS
		case "sucess":
		if(scorm.version == "2004") {
			resultado= scorm.get("cmi.success_status");
		} else {
			resultado= scorm.get("cmi.core.lesson_status");
		}
		break;
		//LOCATION
		case "location":
		if(scorm.version == "2004") {
			resultado= scorm.get("cmi.location");
		} else {
			resultado=scorm.get("cmi.core.lesson_location");
		}
		break;
		//RAW
		case "raw":
		if(scorm.version == "2004") {
			resultado=scorm.get("cmi.score.raw");
		} else {
			resultado=scorm.get("cmi.core.score.raw");
		}
		if(resultado.length == 0) {
			resultado = 0;
		}
		if(scorm.get("cmi.core.lesson_status")=="passed"){
			resultado = 0;
		}
		break;
		//MIN
		case "min":
		if(scorm.version == "2004") {
			resultado= scorm.get("cmi.score.min");
		} else {
			resultado=scorm.get("cmi.core.score.min");
		}
		break;
		//MAX
		case "max":
		if(scorm.version == "2004") {
			resultado= scorm.get("cmi.score.max");
		} else {
			resultado=scorm.get("cmi.core.score.max");
		}
		break;
		//TIME
		case "time":
		resultado="leer time no se puede";
		//resultado=LMSGetValue("cmi.core.session_time");
		break;
		//SUSPEND DATA
		case "data":
		if(scorm.version == "2004") {
			resultado= scorm.get("cmi.suspend_data");
		} else {
			resultado=scorm.get("cmi.suspend_data");
		}
		break;
		//STUDENT_NAME
		case "student":
		if(scorm.version == "2004") {
			resultado= "";
		} else {
			resultado=scorm.get("cmi.core.student_name");
		}
		break;
	}
	//DEBUG LECTURA
	if(debug_lectura==1){
		alert("variable de lectura:"+cual+", valor:"+resultado);
	}
	//FIN DEBUG LECTURA
	return resultado;
}

function escribir(cual,valor){
	var resultado="";
	//alert(cual + " " + valor);
	switch (cual){
		//STATUS
		case "status":
			if(valor == "incomplete") {
				if(scorm.version == "2004") {
					scorm.set("cmi.completion_status", valor);
				} else {
					scorm.set("cmi.core.lesson_status", valor);
				}
				courseStatus = valor;
			}
			if(valor == "completed") {
				if(scorm.version == "2004") {
					scorm.set("cmi.completion_status", valor);
				} else {
					scorm.set("cmi.core.lesson_status", valor);
				}
				courseStatus = valor;
			}
			if(valor == "passed") {
				if(scorm.version == "2004") {
					scorm.set("cmi.completion_status", "completed");
					scorm.set("cmi.success_status", valor);
				} else {
					scorm.set("cmi.core.lesson_status", valor);
				}
				if(!paso_test_passed){
					paso_test_passed = true;
				}
				courseStatus = "completed";
			}
			if(valor == "failed") {
				if(scorm.version == "2004") {
					scorm.set("cmi.completion_status", "completed");
					scorm.set("cmi.success_status", valor);
				} else {
					scorm.set("cmi.core.lesson_status", valor);
				}
				courseStatus = "completed";
				if(!paso_test_failed){
					paso_test_failed = true;
				}
			}
			
			break;
		//LOCATION
		case "location":
			var f = valor;
			var temp=new Array();
			temp=f.split(":");
			//LMSSetValue("cmi.core.lesson_location", temp[2]);
			if(scorm.version == "2004") {
				scorm.set("cmi.location", temp[2]);
			} else {
				scorm.set("cmi.core.lesson_location", temp[2]);
			}
			break;
		//SUSPEND DATA
		case "data":
			if(scorm.version == "2004") {
				scorm.set("cmi.suspend_data", valor);
			} else {
				scorm.set("cmi.suspend_data", valor);
			}

			automa=valor;
			break;
		//RAW
		case "raw":
			//CASO PAGE
			if(modo_operacion == "page") {
				tt=calculaRaw(valor);
				if(leer("raw")<=tt){
					if(tt>=99){
						tt=100;
						escribir("status","completed");
						/*escribir("status","passed");
						LMSFinish();*/
					}
					if(scorm.version == "2004") {
						scorm.set("cmi.score.raw", tt);
					} else {
						scorm.set("cmi.core.score.raw", tt);
					}
				}
				
			} else if (modo_operacion == "test") {
				tt=calculaRaw(valor);
				if(parseInt(leer("raw"))<=tt){
					if(tt>=100){
						tt=100;
					}
					if(scorm.version == "2004") {
						scorm.set("cmi.score.scaled", tt/100);
						//scorm.set("cmi.score.raw", tt);
					} else {
						scorm.set("cmi.core.score.raw", tt);
					}
				}
			} else if ( modo_operacion == "none" ) {
				//DO not send value to score
				tt=calculaRaw(valor);
				if(tt>=99){
					escribir("status","completed");
				}
			}
			break;
		//MIN
		case "min":
			if(scorm.version == "2004") {
				scorm.set("cmi.score.min", valor);
			} else {
				scorm.set("cmi.core.score.min", valor);
			}
			break;
		//MAX
		case "max":
			if(scorm.version == "2004") {
				scorm.set("cmi.score.max", valor);
			} else {
				scorm.set("cmi.core.score.max", valor);
			}
			break;
		//TIME
		case "time":
			if(scorm.version == "2004") {
				scorm.set("cmi.session_time", valor);
			} else {
				scorm.set("cmi.core.session_time", valor);
			}
			break;
		}
		//DEBUG ESCRITURA
		if(debug_escritura==1){
			alert("variable de escritura:"+cual+", valor:"+valor);
	    }
		if(debug_escritura_confirmacion==1){
			if(debug_lectura==0){
				alert("ERROR ESCRITURA CONFIRMACIÓN: debe activar el debug de lectura.");
			} else {
				leer(cual);	
			}
		}
		//FIN DEBUG ESCRITURA
};

function calculaRaw(v){
	var calculo=0;
	if(modo_operacion == "page" || modo_operacion == "none") {
		var f=v;
		var temp=new Array();
		temp=f.split(":");
		var suma;
		suma=(parseInt(temp[0])*100)/parseInt(temp[1]-1); //subtract 1 to the total in mode "page" to exclude the introduction page
		calculo=suma;
	} else if (modo_operacion == "test") {
		calculo = parseInt(v);
	}
	
	return calculo;
}

function plataforma_On(){
	var ini=scorm.init();
	return ini;
}

function plataforma_Off(){
	scorm.quit();
}


setInterval("ejecutar()",300000);
﻿var xml;

$(document).ready(function(){
	//alert(currentPage.carpeta);
	parseXML_Ex();
});


var totalActivos;

//#################### CARGA XML ########################
var xhttpExercise;
var $xmlEx;
var nEx;
var totalElements;
var box_complete;

var respuestas;
var repite = false;


function parseXML_Ex(){
	var urlTest = 'contenido/'+ currentPage.carpeta + '/data_drag.xml';
	
	if(window.location.protocol == 'file:') {
		var response;
			if (window.DOMParser)
			{
				parser=new DOMParser();
				//response=parser.parseFromString(myObj[nombreTest],"text/xml");
				response=parser.parseFromString("data_drag.xml","text/xml");

			}
			else // Internet Explorer
			{
				response=new ActiveXObject("Microsoft.XMLDOM");
				response.async=false;
				response.loadXML(myObj[nombreTest]); 
			}
			
			$xmlEx = $(response);
	
	} else {
	
		xhttpExercise=new XMLHttpRequest();
		xhttpExercise.open("GET",urlTest,false);
		xhttpExercise.setRequestHeader('Content-Type', 'text/xml');
		xhttpExercise.send("");
		$xmlEx = $(xhttpExercise.responseXML);
	}
	
	// elementos  a colocar
	totalElements = nEx = $xmlEx.find('element').length;
	config_drag();
}

var posx_contenedor;
var posy_contenedor;
var w_contenedor;
var h_contenedor;


var element;
var elementCorrect;
var elementDropEnable;
var content_box;
var posx_element;
var posy_element;
var posoriginx_element;
var posoriginy_element;

var objDrag;
var n_objDrag;

var element_drag;

var entra_box;
var success = 0;

var nItems_box_v;
var nItems_box_h;

function config_drag(){
	respuestas = new Array(0,0,0,0);
	$('#container_page').append('<div id="contenedores"></div>');
	$('#container_page').append('<div id="elementos"></div>');
	$('#container_page').append('<div id="feedback_back"></div>');
	$('#container_page').append('<div id="feedback"></div>');
	$('#container_page').append('<div id="feedback_individual"></div>');
	
	if(repite){
		$('#contenedores').css('opacity','1.0');
		$('#elementos').css('opacity','1.0');
		
	}
	
	/* ************* CONTENEDOR ****************** */
	posx_contenedor = new Array();
	posy_contenedor = new Array();
	w_contenedor = new Array();
	h_contenedor = new Array();
	
	box_complete = 0;
	nItems_box_v = {};
	nItems_box_h = {};
	objDrag = {};
	n_objDrag = 0;
	
	
	//$xmlEx.find('individual_feedbacks').each(function(){
		$('#feedback_individual').append('<div id="back"></div>');
		$('#feedback_individual').append('<div id="tipo_feed" class="feedright"><div id="close_feed"></div><p id="titular" class="titular"></p><p id="texto"></p></div>');
	//});
	
	$xmlEx.find('contenido_feedback').each(function(){
		var children = $(this).children();
		var len = children.length;
		if(len < 2 && children.text().length > 0) {
            $('#feedback').append('<h1>'+$(this).find('titulo_feedback').text()+'</h1>')
						.append('<p>'+$(this).find('texto_feedback').text()+'</p>');
		} else {
			var item;
			for(var i = 0; i < len; i++) {
				item = $(children[i]);
				$('<div id="' + item.prop("tagName") + '"></div>').appendTo('#feedback')
						.append('<h1>' + item.find('titulo_feedback').text() + '</h1>')
						.append('<p>' + item.find('texto_feedback').text() + '</p>')
						.append('<div class="close_feed"></div>');
			}
		}
	});
	
	$xmlEx.find('content').each(function(s){
		//alert(currentPage.carpeta);
		posx_contenedor[s]=$(this).attr('posx');
		posy_contenedor[s]=$(this).attr('posy');
		w_contenedor[s]=$(this).attr('wwidth');
		h_contenedor[s]=$(this).attr('hheight');
		if($(this).find('data_content').attr('type_data')=="text"){
			$('#contenedores').append('<div id="outer_'+$(this).attr('id')+'" class="outer_box"></div>');
			$('#contenedores').append('<div id="'+$(this).attr('id')+'" class="box"><div class="header"><p>'+$(this).find('data_content').text()+'</p></div></div>');
		} else if($(this).find('data_content').attr('type_data')=="image"){
			$('#contenedores').append('<div id="outer_'+$(this).attr('id')+'" class="outer_box"></div>');
			$('#contenedores').append('<div id="'+$(this).attr('id')+'" class="box"><div class="header"><img src="contenido/'+currentPage.carpeta+'/'+$(this).find('data_content').text()+'"/></div></div>');
		}
		objDrag[$(this).attr('id')] = "";	
	});
	n_objDrag = $xmlEx.find('content').length;
	entra_box = false;

	
	$('#contenedores').find('.outer_box').each(function(dd){
		$(this).css({
			'width' : Math.round(Number(w_contenedor[dd])+10)+'px',
			'height' : Math.round(Number(h_contenedor[dd])+10)+'px',
			'top' : Math.round(Number(posy_contenedor[dd])-5)+'px',
			'left' : Math.round(Number(posx_contenedor[dd])-5)+'px'
		});
	});
	
	$('#contenedores').find('.box').each(function(d){
		$(this).css({
			'width': w_contenedor[d]+'px',
			'height': h_contenedor[d]+'px',
			'top': posy_contenedor[d]+'px',
			'left': posx_contenedor[d]+'px'
		});
		
		nItems_box_v[$(this).attr('id')] = 0;
		nItems_box_h[$(this).attr('id')] = 0;
		
		$(this).droppable({
			drop: function(event, ui) {	
				var id_caja = $(this).attr('id');
				var cajas_ar = new Array();
				cajas_ar=content_box[element_drag].split(",");
				$("a#test_confirm").animate({
					'top':'465px'
				}, 300);
				$("a#test_confirm").removeClass("inactive").addClass("active");
					
				$.each(cajas_ar,function(indice,valor){
					if(id_caja==valor) {
				//	alert(id_caja+" / "+valor);
						$("#" + id_caja).removeClass("box_wrong");
						$("#" + id_caja).addClass("box_right");
						entra_box = true;
						success++;
					}
				});
				if(!entra_box) {
					$(this).removeClass("box_right");
					$(this).addClass("box_wrong");
					if(!elementCorrect[element_drag]) entra_box = true;
				}
				
				posiciona(id_caja);
			},
			deactivate: function(){
				//alert("2. pasa");
				if(!entra_box){
					$('#elementos').find('#'+element_drag).stop().animate({
						'top': posoriginy_element[element_drag]+'px',
						'left': posoriginx_element[element_drag]+'px'
					},300,function(){
						$(this).find('p').css('border','1px solid grey');
						f_activa_box($(this).find('p').text());
						success--;
					});
				}
			}
		});
	});
	
	function f_activa_box(data){
		$('#contenedores').find('.box').each(function(){
			if ($(this).find('p').text()==data){
				$(this).find('p').text("");
				$(this).droppable('enable');
				$(this).removeClass("box_wrong");
				$(this).removeClass("box_right");
			}
			//$('#contenedores').find('#'+id_caja).droppable('disable');
		});
	}

	/* ************* ELEMENTOS ****************** */

	element = {};
	elementCorrect = {};
	elementDropEnable = {};
	content_box = {};
	posx_element = {};
	posy_element = {};
	posoriginx_element = {};
	posoriginy_element = {};
	
	

	
	
	$xmlEx.find('element').each(function(n){
		element[$(this).attr('id')] = $(this).find('data_element').attr('position')+","+$(this).find('data_element').attr('direction')+","+$(this).find('content_element').text();
		elementCorrect[$(this).attr('id')] = $(this).find('data_element').attr('correct') != 'false';
		elementDropEnable[$(this).attr('id')] = $(this).find('data_element').attr('drop_enable') != 'false';
		posx_element[$(this).attr('id')] = $(this).find('posContent_x').text();
		posy_element[$(this).attr('id')] = $(this).find('posContent_y').text();
		posoriginx_element[$(this).attr('id')] = $(this).find('posIni_x').text();
		posoriginy_element[$(this).attr('id')] = $(this).find('posIni_y').text();
		if($(this).find('data_element').attr('position')=="dinamic"){
			if($(this).find('data_element').attr('direction')=="v"){
				$('#contenedores').find('.box').each(function(){
					nItems_box_h[$(this).attr('id')] = $(this).position().left+1;
				});
				
			} else if($(this).find('data_element').attr('direction')=="h"){
				$('#contenedores').find('.box').each(function(){
					nItems_box_v[$(this).attr('id')] = $(this).position().top+1;
				});
			} 
		}
		// cajas a las q puede ir cada elemento
		$(this).find('box').each(function(nn){
			if (nn==0){
				content_box[$(this).parent().parent().attr('id')]=$(this).text()+",";
			} else if (nn>0){
				content_box[$(this).parent().parent().attr('id')]+=$(this).text()+",";
			}
		});
		content_box[$(this).attr('id')]	= content_box[$(this).attr('id')].slice(0,content_box[$(this).attr('id')].length-1);
		
		if($(this).find('data_element').attr('type_data')=="text"){
			//$('#elementos').append('<div id="'+$(this).attr('id')+'" class="element"><div class="yellow_box"></div><p>'+$(this).find('content_element').text()+'</p></div>');
			$('#elementos').append('<div id="'+$(this).attr('id')+'" class="element"><p>'+$(this).find('content_element').text()+'</p></div>');
		} else if($(this).find('data_element').attr('type_data')=="image"){
			//$('#elementos').append('<div id="'+$(this).attr('id')+'" class="element"><div class="yellow_box"></div><img src="contenido/'+currentPage.carpeta+'/'+$(this).find('content_element').text()+'"/></div>');
			$('#elementos').append('<div id="'+$(this).attr('id')+'" class="element"><img src="contenido/'+currentPage.carpeta+'/'+$(this).find('content_element').text()+'"/></div>');
		}
	});

	
	$('#elementos').find('.element').each(function(h){
		$(this).css({
			'top': posoriginy_element[$(this).attr('id')]+'px',
			'left': posoriginx_element[$(this).attr('id')]+'px'
		});


		$(this).draggable({ 
			start: function( event, ui ){
				element_drag = $(this).attr('id');
				f_isDragging($(this).attr('id'));
				$(this).css({
					'z-index' : '5'
				});
			},
			stop: function( event, ui ) {
				$(this).css({
					'z-index' : '1'
				});
			}
				
			// alert(ui.item)); //	alert(ui.position.top);	//	alert(ui.helper.attr('id')); 
			// alert(event.target);	//alert($(this).attr('id')); //alert($(this).attr('name'));
		});
	});
	
}

function posiciona(id_caja){
	var is_get_unique_element = false;
/*	var color_right = #a4a90d;
	var color_regular = #00a5b7;
	var color_wrong = #993300;*/
	if (entra_box){
		var config_element_ar = new Array();
		var posiciona_x;
		var posiciona_y;
		var tiempo;
		var tipo_clase;
		config_element_ar = element[element_drag].split(',');
		
		if (config_element_ar[0] == 'fixed'){
			
			posiciona_x = posx_element[element_drag];
			posiciona_y = posy_element[element_drag];
			tiempo = 300;
			
		} else if (config_element_ar[0] == 'dinamic'){
		
			if (config_element_ar[1]=='v'){
				posiciona_x = Number(nItems_box_h[id_caja]);
				posiciona_y = Math.round(Number(posy_element[element_drag])+Number(nItems_box_v[id_caja]));
				nItems_box_v[id_caja] = Math.round(nItems_box_v[id_caja]+$('#'+element_drag).find('p').outerHeight());
			} else if(config_element_ar[1]=='h'){
				posiciona_x = Math.round(posx_element[element_drag]+nItems_box_v[id_caja]);
				posiciona_y = Number(nItems_box_v[id_caja]);
				nItems_box_h[id_caja] = Math.round(nItems_box_h[id_caja]+$('#'+element_drag).find('p').outerWidth());
			} 
			tiempo = 300;
			
		} else if (config_element_ar[0] == 'origin'){
		
			var posicionArrayRespuestas = id_caja.substring(id_caja.length, id_caja.length-1) - 1 ;
			respuestas[posicionArrayRespuestas] = config_element_ar[2];
			
			var todosElegidos = true;
			
			for (i = 0; i < respuestas.length; i++) {
				if(respuestas[i] == 0){
					todosElegidos = false;
				}
			}

			$xmlEx.find('content').each(function(){
				if($(this).attr('id')==id_caja){
					$(this).find('individual_feedback').each(function(){
						if ($(this).attr('feed_element') == element_drag){
						
							tipo_clase = $(this).attr('type_data');
							
							var clase = $('#feedback_individual').find('#tipo_feed').attr('class');
							var titulo = $(this).find('title').text();
							var texto = $(this).find('texto').text();
							
							$('#feedback_individual').find('#tipo_feed').removeClass(clase);
							$('#feedback_individual').find('#tipo_feed').addClass(tipo_clase);
							$('#feedback_individual').find('#tipo_feed').each(function(){
								$(this).find('#titular').text(titulo);
								$(this).find('#texto').text(texto);
							});
								$('#feedback_individual').fadeIn();
							$('#feedback_individual').find('#close_feed').click(function(e){
								$('#feedback_individual').fadeOut();
							});
						}
					});
					
					/* 
					var color_feed = {};
					color_feed["feedright"] = "#A4A90D";
					color_feed["feedregular"] = "#00A5B7";
					color_feed["feedwrong"] = "#993300";
					$('#contenedores').find('#outer_'+id_caja).css({
						'border' : '2px dashed '+color_feed[tipo_clase]
					});
					$('#contenedores').find('#outer_'+id_caja).find('.selloElegido').remove();
					*/
					
					//$('#contenedores').find('#'+id_caja).droppable('disable');
					//$('#contenedores').find('#outer_'+id_caja).append('<img class="selloElegido" src="contenido/M4_P07/' + config_element_ar[2] + '" />');
					
				}
			});
			
			
			if (todosElegidos){
				$('#elementos').find('.element').remove();
				$('#b_ya').show();
				$("#b_ya").animate({
					'opacity':'1',
					'top':'470px'
				}, 300);
				$('#b_ma').show();
				$("#b_ma").delay(200).animate({
					'opacity':'1',
					'top':'470px'
				}, 300);
			}
			
			posiciona_x = posoriginx_element[element_drag];
			posiciona_y = posoriginy_element[element_drag];
			tiempo = 0;
			
		} else if (config_element_ar[0]=='get_unique_element'){
			is_get_unique_element = true;
			tiempo = 300;
		}
		
		if (is_get_unique_element){
			var nn1;
			var nn2;
			nn2 = $('#'+element_drag).text();
			//elementDropEnable
			if(elementDropEnable[element_drag]){
				var n_ancho = Number($('#contenedores').find('#'+id_caja).outerWidth())-Number($('#elementos').find('#'+element_drag).outerWidth());
				var n_pos_elm = Number($('#contenedores').find('#'+id_caja).position().left)+(n_ancho/2);// restamos 5 x el padding
				$('#elementos').find('#'+element_drag).stop().animate({
					'top' : Number($('#contenedores').find('#'+id_caja).position().top)+1+"px",
					'left' : n_pos_elm+"px"
				}, tiempo, function(){
					$(this).find('p').css('border','none');
					$('#'+id_caja).find('.header').each(function(){
						$(this).find('p').text(nn2);
					});
					////////$('#contenedores').find('#'+id_caja).droppable('disable');
					entra_box = false;	
				});
				objDrag[id_caja] = element_drag;
				f_isDragged();
				
			} else {
				$('#elementos').find('#'+element_drag).stop().animate({
					'opacity' : '0'
				}, tiempo, function(){
					$(this).hide();
					$('#contenedores').find('#'+id_caja).removeClass('box');
					//$('#contenedores').find('#'+id_caja).addClass('box_disable');
					$('#contenedores').find('#'+id_caja).addClass('box_unique');
					$('#contenedores').find('#'+id_caja).droppable('disable');
					nn1 = $('#'+id_caja).find('.header').text();
					$('#'+id_caja).find('.header').each(function(){
						//$(this).find('p').text(nn1+" - "+nn2);
						$(this).find('p').text(nn2);
					});
					entra_box = false;
				});
			}
		} else {
			$('#elementos').find('#'+element_drag).stop().animate({
				'top': posiciona_y+'px',
				'left': posiciona_x+'px'
			}, tiempo, function(){
				if (config_element_ar[0] != 'origin'){
					$(this).removeClass('element');
					$(this).addClass('element_position');
					$(this).draggable('disable');
					//$(this).find('.yellow_box').remove();
					entra_box = false;
				}
			});
		}
		
		/*if(nEx>0){
			nEx--;
			if(nEx==0){
				if(!elementCorrect[element_drag]) {
					$(".close_feed").on("click", function() { $("#feedback").hide(); });
					if(success == totalElements) {
						$('#feedback_right').show();
						$('#feedback_wrong').hide();
					} else {
						$('#feedback_right').hide();
						$('#feedback_wrong').show();
					}
				}
				setTimeout(function() {
					$(".box_right, .box_wrong").addClass("box_solution");
					$('#feedback').show()
						.animate({
							'opacity' : '1'
						},300);
				}, tiempo);
				
			}
		}
		box_complete++;
		descontar(box_complete);*/

	} else {
		$('#elementos').find('#'+element_drag).stop().animate({
			'top': posoriginy_element[element_drag]+'px',
			'left': posoriginx_element[element_drag]+'px'
		},300);
	}
}

	function validarTest() {
		var ret = false;
		$("a#test_confirm").hide();
		$("a#test_confirm").animate({
			'top':'15px'
		}, 300);
		$("a#test_confirm").removeClass("active").addClass("inactive");
		$(".close_feed").on("click", function() { $("#feedback").fadeOut(); $("#feedback_back").fadeOut();});
		$("#feedback_back").on("click", function() { $("#feedback").fadeOut(); $("#feedback_back").fadeOut();});
		$(".box_right, .box_wrong").addClass("box_solution");
		$('#elementos').find('.element').each(function(s){
			if(Number($(this).position().top)==Number(posoriginy_element["element"+(s+1)])){
				$(this).css('opacity', '0.8');
				$(this).draggable('disable');
				$(this).css('cursor', 'default');
				$(".element p").css('cursor', 'default');
			} else {
				$(this).hide();
			}
		});
		
		if(success == totalElements) {
			$('#feedback_right').fadeIn();
			$('#feedback_wrong').fadeOut();
			ret = true;
		} else {
			$('#feedback_right').fadeOut();
			$('#feedback_wrong').fadeIn();
			$('#container_page').append('<div id="volver"></div>');
			$('#volver').bind('click',function(){
				repite = true;
				$(this).remove();
				$('#contenedores').remove();
				$('#elementos').remove();
				$('#feedback').remove();
				$('#feedback_individual').remove();
				success = 0;
				totalElements = 0;
				$("a#test_confirm").show();
				$("a#test_confirm").animate({
					'top':'465px'
				}, 300);
				$("a#test_confirm").removeClass("inactive").addClass("active");
				parseXML_Ex();
				precarga();
			});
		}
		$(".box_right, .box_wrong").addClass("box_solution");
		$("#feedback_back").fadeIn();
		$('#feedback').fadeIn();
		$('#feedback').animate({
						'opacity' : '1'
					},300);
		return ret;
	}
		
	//	

	function f_isDragging(data){
		$.each(objDrag,function(indice,valor){
			if(data==valor){
				objDrag[indice] = "";
			}
		});
	}
	
	function f_isDragged(){
		$.each(objDrag,function(indice,valor){
			//alert(indice+" / "+valor+" / "+valor.length);
			if(valor.length==0){
				$('#'+indice).droppable('enable');
			}
			// activamos box q tengan valor.length == 0
		});
	}

/************************************************************************************************************************************************/
/************************************************************************************************************************************************/

/* AVANCE */

		
	//descontar($(this).attr("rel"));
	
	function descontar(cual) {
		_global.externalTrace("plantilla descontar ",cual);
		totalActivos[cual] = 1;
		compruebaBotones();
	}
	
	function compruebaBotones() {
		alguno = false;
		for (i = 1; i < totalActivos.length + 1; i++) {
			if (totalActivos[i] == 0) {
				alguno = true;
				break;
			}
		}
		if (!alguno) {
			HabilitaBotones();
		}
		_global.externalTrace("plantilla compruebaBotones ",alguno);
	}

(function($) {
	var START_EV, MOVE_EV, END_EV, body = $("body");
	if('ontouchstart' in window && !(/hp-tablet/gi).test(navigator.appVersion)) {
		START_EV = 'touchstart';
		MOVE_EV = 'touchmove';
		END_EV = 'touchend';
	} else {
		START_EV = 'mousedown';
		MOVE_EV = 'mousemove';
		END_EV = 'mouseup';
	}
	
	function getPosX(ev) {
		return typeof ev.clientX != "undefined" ? ev.clientX : ev.pageX;
	}
	
	function getPosY(ev) {
		return typeof ev.clientY != "undefined" ? ev.clientY : ev.pageY;
	}
	
	/**
	* id:			[Optional]	(string)	Prefix for the styles "[id]_container", "[id]_item", "[id]_prev" and "[id]_next"
	*										[default: id of the container item]
	* visible:		[Optional]	(number)	Image visible by 
	default
	*										[default: 1]
	* direction:	[Optional]	(string)	"vertical" to move vertically, horizontally otherwise
	*										[default: horizontally]
	* distance:		[Optional]	(number)	Minimun distance in pixels of the touch to change of image
	*										[default: 20]
	* duration:		[Optional]	(number)	Duration of the transition in milliseconds
	*										[default: 500]
	* easing:		[Optional]	(string)	Easing function for the transition
	*										[default: "swing"]
	* callback:		[Optional]	(function)	Function to be called any time that an item is focused. The argument of the
	*										function is the jQuery object of the item itself.
	**/
	jQuery.fn.gallery = function(obj) {
		if(!obj) obj = {};
		var id = obj.id || this.attr("id"),
			items = this.find("." + id + "_item"),
			length = items.length,
			visible = obj.visible && obj.visible > 0 && obj.visible <= length ? obj.visible - 1 : 0,
			axis = obj.direction == "vertical" ? "top" : "left",
			distance = obj.distance && obj.distance >= 0 ? obj.distance : 20,
			duration = obj.duration && obj.duration > 0 ? obj.duration : 500,
			easing = obj.easing || "swing",
			callback = typeof obj.callback === "function" ? obj.callback : null,
			prev = this.find("." + id + "_prev"),
			next = this.find("." + id + "_next"),
			size = $(items[0])[axis == "top" ? "outerHeight" : "outerWidth"](),
			maxPos = size * (1 - length),
			container = this.find("#" + id + "_container").css(axis == "top" ? "height" : "width", (size * length) + "px").css(axis, (-visible * size) + "px"),
			animate = axis == "top" ? animateY : animateX,
			getPos = axis == "top" ? getPosY : getPosX;
			
		if(visible == 0) prev.hide();
		if(visible == length - 1) next.hide();
		if(callback) complete();
		
		container.on(START_EV, function(ev) {
			container.stop(true);
			var initTouch = 0,
				initPos = parseInt(container.css(axis)) || 0,
				dir = 0,
				lastTouch = 0,
				touch = ev.originalEvent;
			if(touch) {
				touch = touch.touches && touch.touches[0] || touch;
				touch = getPos(touch);
				if(typeof touch != "undefined") initTouch = lastTouch = touch;
			}
			
			body.on(MOVE_EV, function(ev){
				ev.preventDefault();
				var touch = ev.originalEvent;
				if(touch) {
					touch = touch.touches && touch.touches[0] || touch;
					touch = getPos(touch);
					if(typeof touch == "undefined") touch = lastTouch;
				}
				if(touch != lastTouch) {
					var endPos = initPos + touch - initTouch;
					if(endPos <= 0 && endPos >= maxPos) {
						container.css(axis, endPos + "px");
						if(dir * (lastTouch - touch) < 0) {
							initPos = parseInt(container.css(axis)) || 0;
							initTouch = touch;
						}
						dir = (lastTouch - touch) / Math.abs(lastTouch - touch);
						lastTouch = touch;
					}
				}
			});
			
			body.on(END_EV, function(ev) {
				body.off(END_EV);
				body.off(MOVE_EV);
				var endTouch = 0,
					init = parseInt(container.css(axis)) || 0,
					touch = ev.originalEvent,
					end;
				if(touch) {
					touch = touch.changedTouches && touch.changedTouches[0] || touch;
					touch = getPos(touch);
					if(typeof touch != "undefined") endTouch = touch;
				}
				var dif = endTouch - initTouch;
				if(Math.abs(dif) < distance) { // Coming back to the original position
					visible = Math.round(-init / size); // The image that takes the most part
					end = -visible * size;
					animate(end, Math.abs(duration * (end - init) / size));
				} else {
					var first = Math.floor(-init / size); // The image on the top/left of the two images in sight
					if(dif > 0) {
						if(first >= 0) {
							visible = first;
							end = -visible * size;
							animate(end, Math.abs(duration * (end - init) / size));
						}
					} else if(first < length - 1) {
						visible = first + 1;
						end = -visible * size;
						animate(end, Math.abs(duration * (end - init) / size));
					}
				}
				if(visible == 0) prev.hide();
				else prev.show();
				if(visible == length - 1) next.hide();
				else next.show();
			});
		});
		
		prev.on("click", function() {
			visible--;
			animate(-visible * size, duration);
			next.show();
			if(visible == 0) prev.hide();
		});
		
		next.on("click", function() {
			visible++;
			animate(-visible * size, duration);
			prev.show();
			if(visible == length - 1) next.hide();
		});
		
		function animateX(pos, time) {
			container.stop(true).animate({left : pos + "px"}, time, easing, callback ? complete : null);
		}
		
		function animateY(pos, time) {
			container.stop(true).animate({top : pos + "px"}, time, easing, callback ? complete : null);
		}
		
		function complete() {
			callback($(items[visible]));
		}
		
		return this;
	};
})(jQuery);
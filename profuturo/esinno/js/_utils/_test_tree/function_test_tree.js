﻿var xml;
var ruta_xml;

$(document).ready(function(){
	if(clickEvent == undefined){
		clickEvent = "click";
	}
	//if(valor_elegido == 1){
	if(currentCourse == 1){
		ruta_xml = "dataTest_sales.xml";
	//} else if(valor_elegido == 2){
	} else if(currentCourse == 2){
		ruta_xml = "dataTest_other.xml";
	} else {
		ruta_xml = "dataTest_sales.xml";
	}
	parseXML_Ex();
});


var totalActivos;

//#################### CARGA XML ########################
var xhttpExercise;
var $xmlEx;
var nEx;
var box_complete;

function parseXML_Ex(){	
	//var urlTest = 'contenido/'+ currentPage.carpeta + '/dataTest.xml';
	var urlTest = 'contenido/'+ currentPage.carpeta + '/'+ruta_xml;
	
	if(window.location.protocol == 'file:') {
		var response;
			if (window.DOMParser)
			{
				parser=new DOMParser();
				//response=parser.parseFromString("dataTest.xml","text/xml");
				response=parser.parseFromString(ruta_xml,"text/xml");

			}
			else // Internet Explorer
			{
				response=new ActiveXObject("Microsoft.XMLDOM");
				response.async=false;
				response.loadXML(myObj[nombreTest]); 
			}
			
			$xmlEx = $(response);
	
	} else {
	
		xhttpExercise=new XMLHttpRequest();
		xhttpExercise.open("GET",urlTest,false);
		xhttpExercise.setRequestHeader('Content-Type', 'text/xml');
		xhttpExercise.send("");
		$xmlEx = $(xhttpExercise.responseXML);
	}
	
	// elementos  a colocar
	nEx = $xmlEx.find('question').length;
	config_intro();
	
}


var dialogo;
var $comun;
var $question;
var id_;
var feed_titulo;
var feed_texto;
var $options;
var id_next;

var introduction_titulo;
var introduction_texto;
var introduction_instrucciones;

/************ AUDIO ****************/

function config_audio_intro(){
	soundMgr.stopAudios();
	$xmlEx.find('content').each(function(){
		if($(this).find('audio').text().length>0){
			lanzarAudio_tree($(this).find('audio').text());
		} 
	});
}

function config_audio(data){
	soundMgr.stopAudios();
	$xmlEx.find('question').each(function(){
		if($(this).attr('id')==data){
			if($(this).find('audio').text().length>0){
				lanzarAudio_tree($(this).find('audio').text());
			} 
		}else{
			config_audio2(data);
		}
	});
}

function config_audio2(data){
	soundMgr.stopAudios();
	$xmlEx.find('final_question').each(function(){
		if($(this).attr('id')==data){
			if($(this).find('audio').text().length>0){
				lanzarAudio_tree($(this).find('audio').text());
			} 
		}
	});
}

function lanzarAudio_tree(data) {
    soundMgr.stopAudios();
	playAudioPrimario (data, handleAudioComplete2);
}
	
function handleAudioComplete2 () {
	_global.externalTrace("fin reproducción audio");
	// navigation.checkStatus se pondría aquí para validar que se ha escuchado el audio completo
	//navigation.checkStatus();
}	

	/***************************************/

function config_intro(){
	$('#container_page').append('<div id="bt_start"></div>');
	//$('#bt_start').click(function(){
	$('#bt_start').bind(clickEvent,function(){
		$(this).hide();
		config_audio_intro();
		config_treeTest();
	});
}

function config_treeTest(){
	dialogo = "";
	id_ = 0;
	nn = 0;
	introduction_titulo = "";
	introduction_texto = "";
	introduction_instrucciones = "";
	$comun = {};
	$question = {};
	
	feed_titulo = new Array();
	feed_texto = new Array();
	id_next = {},
	
	$xmlEx.find('introduction').each(function(){
		introduction_titulo = $(this).find('title').text();
		introduction_texto = $(this).find('description').text();
		introduction_instrucciones = $(this).find('instructions').text();
	});
	
	$xmlEx.find('ctext').each(function(data_comun){
		$comun["comun_"+Number(data_comun+1)] = $(this).text();
	});
	$xmlEx.find('question').each(function(s){
		$question[s]= $(this);
	});
	
	/////////////////config_audio();

	/*****************************************************************************/
	$('#info_box').find('.info_box_azul').html(introduction_titulo);
	$('#info_box').find('.info_box_contenido').html(introduction_texto);
	$('#info_box').find('.info_box_instruction').html(introduction_instrucciones);
	
	/*****************************************************************************/
	$('#content_test').append('<div id="play"></div>');
	$('#content_test').append('<div id="show_result"></div>');
	$('#content_test').append('<div id="dialog"><div id="content_dialog"><p></p></div></div>');

	$('#content_test').append('<div id="options"><ul></ul></div>');
	
	$('#dialog').css({
		'opacity' : '0'
	});
	$('#options').css({
		'opacity' : '0'
	});
	
	$('#dialog').hide();
	$('#options').hide();
	
	//$('#play').click(function(){
	$('#play').bind(clickEvent,unction(){
		config_audio(1);
		$('#dialog').show();
		$('#options').show();
		$('#dialog').animate({
			'opacity' : '1'
		},500);
		$('#options').delay(200).animate({
			'opacity' : '1'
		},500);
		$(this).hide();
	});
	
	fdialog();
	
}

var nxt;
var $qt;

function fdialog(){

	if(id_next[Number(id_)-1]== undefined){
		nxt = id_;
	} else {
		nxt = id_next[Number(id_)-1];
		config_audio(nxt);
	}
	//alert("nxt: "+nxt);
	//dialogo += $comun["comun_1"];
	if(nxt==0){
		$xmlEx.find('question').each(function(){
			if($(this).attr('id')== 1){
				dialogo += $(this).find('text').text();
			}
		});
	} else if(nxt>0){
		$xmlEx.find('question').each(function(){
			if($(this).attr('id')== nxt){	
				dialogo += $(this).find('text').text();
			} else {
				if(nxt>$(this).attr('id')){
					fdialog_final();
				}
			}
		});
	}

	
	dialogo += "<br><br>";
	$('#content_dialog').find('p').empty();
	$('#content_dialog').find('p').html(dialogo);
	
	/////////////////////////////////////////////////////////
	$('#content_dialog').jScrollPane({showArrows: false});
	$('#content_dialog').find('.jspTrack').width('5px');
	$('#content_dialog').find('.jspTrack').css({
		'width' : '5px',
		'margin-left' : '6px'
	});
	//alert($('#content_dialog').find('.jspPane').height());
	var alto = 0;
	if($('#content_dialog').find('.jspPane').height()>190){
		alto = -$('#content_dialog').find('.jspPane').height()+190;
	}
	//jspDrag
	$('#content_dialog').find('.jspPane').css({
		'top' : alto+'px'
	});
	if($('#content_dialog').find('.jspDrag').height()!=null){
		var altoDrag = 190-$('#content_dialog').find('.jspDrag').height();
		$('#content_dialog').find('.jspDrag').css({
			'top' : altoDrag+'px'
		});
	}
	/////////////////////////////////////////////////////////

	fquestion();
	
}

var nn = 0;

function fdialog_final(){	
	
	$xmlEx.find('final_question').each(function(){
		if($(this).attr('id')== nxt){
			//alert("pasa");
			nn++;
			if(nn == 1){
				dialogo += $(this).find('text').text();
				//alert(nn);
			}
		}
		
	});
}



function fquestion(){	
	var continua_cuestionario = false;
	$xmlEx.find('question').each(function(){
		if(nxt==0){
			$qt = $question[0]
		} else if($(this).attr('id')==nxt){
			$qt = $(this);
		} 	
	});
////////	lanzarAudio_tree(nxt);
	// define donde termina el test
	$xmlEx.find('final_question').each(function(){
		if($(this).attr('id')==nxt){
			var info_feed_tx;
			continua_cuestionario = true;
			$('#show_result').show();
			//$('#show_result').click(function(){
			$('#show_result').bind(clickEvent,function(){
				$('#info_box').find('.info_box_contenido').hide();
				$('#info_box').find('.info_box_instruction').hide();
				$xmlEx.find('final_question').each(function(){
					if(id_next[Number(id_)-1]==$(this).attr('id')){
						info_feed_tx = $(this).find('text_score').text();
						$('#dialog').remove();
						$('#show_result').hide();
						$('#container_page').append('<div id="box_feed"><div id="content_feed"></div></div>');
						$(this).find('feed').each(function(){
							feed_titulo.push($(this).find('title_feed').text());
							feed_texto.push($(this).find('text_feed').text());
						});
						$(this).find('final_feed').each(function(){
							feed_titulo.push($(this).find('title_feed').text());
							feed_texto.push($(this).find('text_feed').text());
						});
						descontar(1);	
						if($(this).parent().parent().attr('sendSCORM')=="true"){
							var nscore = Number($(this).attr('score'));
							// envío de SCORM::: $(this).attr('score') es la puntuación que se obtiene definida en el xml (30 / 80 / 100)
							testMgr.storeResult(nscore, courseMgr.getCurrentPage());							
						}
					}
				});
				
				
				for(var i=0; i<feed_titulo.length; i++){
					if(feed_titulo[i]==feed_titulo[i-1]){
						delete(feed_titulo[i]);
					}
					
				}
				
				$.each(feed_titulo,function(indice, valor){
					$('#content_feed').append('<div id="feed_titulo_'+indice+'" class="info_tit_feed">'+valor+'</div><div id="feed_texto_'+indice+'" class="info_box_contenido"></div>');
					if((valor==undefined)||(valor.length==0)){
						$('#feed_titulo_'+indice).remove();
					}
					
				});
				$.each(feed_texto,function(indice, valor){
					$('#content_feed').find('#feed_texto_'+indice).html(valor);
				});
				
						
				$('#content_feed').jScrollPane({showArrows: false});
				/*$('#content_feed').find('.jspTrack').width('5px');
				$('#content_feed').find('.jspVerticalBar').css({
					'padding-left' : '20px'
				});*/
				$('#content_feed').find('.jspTrack').css({
					'width' : '5px',
					'margin-left' : '16px'
				});
				
				$('#info_box').append('<div class="info_feed">'+info_feed_tx+'</div>');
			});
		}
	});
	
	$options = {}
	if(!continua_cuestionario){
		$qt.find('option').each(function(s){
			$options[s]=$(this);
			if($comun["comun_2"]==$options[s].find('option_text').text().slice(0,$comun["comun_2"].length)){
				$('#options').find('ul').append('<li><p>'+$options[s].find('option_text').text().slice($comun["comun_2"].length,$options[s].find('option_text').text().length)+'</p></li>');
			} else if($comun["comun_1"]==$options[s].find('option_text').text().slice(0,$comun["comun_1"].length)){
				$('#options').find('ul').append('<li><p>'+$options[s].find('option_text').text().slice($comun["comun_1"].length,$options[s].find('option_text').text().length)+'</p></li>');
			} else {
				$('#options').find('ul').append('<li><p>'+$options[s].find('option_text').text()+'</p></li>');
			}
		});
	}
	
	var posY_li_ar = new Array();


	$('#options').find('li').each(function(d){
		//$(this).click(function(){
		$(this).bind(clickEvent,function(){
			feed_titulo[id_] = $options[d].find('title_feed').text();
			feed_texto[id_] = $options[d].find('text_feed').text();
			id_next[id_] = $options[d].attr('id_question');
		//	alert(id_next[id_]);
			//dialogo += $comun["comun_2"];
			dialogo += $options[d].find('option_text').text();
			dialogo += "<br><br>";
			$('#dialog').find('p').html(dialogo);
			$('#options').find('ul').children().remove();
			id_++;
			fdialog();
		});
	});
}



/************************************************************************************************************************************************/
/************************************************************************************************************************************************/

/* AVANCE */

		
	//descontar($(this).attr("rel"));
	
	function descontar(cual) {
		_global.externalTrace("plantilla descontar ",cual);
		totalActivos[cual] = 1;
		compruebaBotones();
	}
	
	function compruebaBotones() {
		alguno = false;
		for (i = 1; i < totalActivos.length + 1; i++) {
			if (totalActivos[i] == 0) {
				alguno = true;
				break;
			}
		}
		if (!alguno) {
			HabilitaBotones();
		}
		_global.externalTrace("plantilla compruebaBotones ",alguno);
	}

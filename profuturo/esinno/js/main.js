// 2019-05-09
/* Offline links update */
var _online = false;
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js';
document.getElementsByTagName('head')[0].appendChild(script);
script.onload = function(script) {
  console.warn("Online");
  window._online = true
}
script.onerror = function() {
  console.warn("No internet detected!");
  window._online = false
  removeInternetLinks("container_web")
};

/*!
 * Remove internet link from no internet access computers
 */
function removeInternetLinks(obj) {
  if (_online == false) {
    console.warn("Remove link:", obj);
    var $target = $("#" + obj)
    var openWin = window.open;
    window.open = function (url, windowName, windowFeatures) {
      if ((/http/.test(url)) == true || (/\/\//.test(url)) == true || (/www/.test(url)) == true ) {
        showCartelAlerta()
      }else{
        openWin(url, windowName, windowFeatures)
      }
    }
    $target.find('a').each(function() {
      var term = $(this).attr('href');
      if ((/http/.test(term)) == true || (/\/\//.test(term)) == true || (/www/.test(term)) == true ) {
        $(this).attr('href', "#")
        $(this).removeAttr('target')
        $(this).click(function(e) {
          e.preventDefault()
          showCartelAlerta()
        })
      }
    })
  }
}
/*!
 * Show Message when link is offline
 */
function showCartelAlerta() {
    $('#cartelAlerta').stop().fadeOut(150)
  // get message (_noInternet) from langpack.xml
  $("#container_web").append('<div id="cartelAlerta" class="cartelAlerta" style="display:none;"><p>'+_noInternet+'</p></div>')
  $('#cartelAlerta').stop().fadeIn(300).delay(1000).fadeOut(250, function() {
    $(this).remove();
  })
}

/**
 * updateScale
 * Adaptar el tamaño del div '#container_web'
 * al ancho o alto de la ventala
 * @author Gaspar Argüello
 */

function updateScale(event, mobile) {
  // console.error(event);
  var $win = $(window);
  var $target = $('#container_web');
  var baseSize = {
    w: $target.width(),
    h: $target.height()
  }
  var ww = $win.width();
  var wh = $win.height();
  var scaleRatio = 1;

  // compare ratios
  if ((ww / wh) < (baseSize.w / baseSize.h)) { // tall ratio
    scaleRatio = ww / baseSize.w;
    // console.error("Tall:", scaleRatio);
  } else { // wide ratio
    // console.error("Wide:", scaleRatio);
    scaleRatio = wh / baseSize.h;
  }
  if (scaleRatio > 1.1 && mobile != true) {
    scaleRatio = 1.1
  }

  var nRatio = scaleRatio.toFixed(2)
  var newScale = "scale(" + nRatio + ", " + nRatio + ")";

  $target.css({
    transform: newScale
  });

}

$(document).ready(function() {
	if (isMobileBrowser()) {
    updateScale(event, true)
	}else {
    window.addEventListener("resize", updateScale)
    updateScale()
  }

});

$(document).keypress(function (e) {
    // console.log(e, keyCode, e.which)
    if (e.keyCode == 113) {
      $('.subpagePopUp').each(function(){
        if( $(this).css('display') == 'block' ){
          $(this).stop().fadeOut(0)
        }
      })
    }
})

/* Offline links update end */

//Modulos
var configMgr;
var courseMgr;
var testMgr;
var langMgr;
var soundMgr;

var navigation;
var header;
var footer;
var title;


//Variables globales
var currentPage;

var currentCourse;

// variables audio tablet
var color_audio;
var portada;
// variable para saber si es necesario meter la leyenda de audio en el icono de tablets
var inicia_nav_audio;

//variables de uso general
var time = new Date();

//Objeto de configuracion general
_global = new Config(true);
_global.externalTrace("Comenzando operación Engine TLS versión", _global.engineVersion);
// modo_operacion viene de SCOFunctions
_global.operationMode = modo_operacion;


var navegador=navigator.userAgent;
var user_navegador;
var ie;

var myPlayer;
var isVideo;

// eventos menu
var npagina_ar =new Array();


$(document).ready(function() {
	if(clickEvent == undefined){
		clickEvent = "click";
	}
	initObjects();
	initEvents();
	startEngine();
});


function initObjects(){
	_global.externalTrace("_root.initObjects");
	langMgr = new LanguageManager("langpack.xml");
	configMgr = new ConfigManager("estructura.xml");
	navigation = new NavegacionManager("botonera_navegacion");
	courseMgr = new CourseManager();
	testMgr = new TestManager();
	soundMgr = new SoundManager();
	currentPage = new Object();

	header = $("#header");
	footer = $("#footer");
	title = $("#header h2");
	//modoDeOperacion(_global.operationMode);
	_global.operationMode = configMgr.operationMode;
	modoDeOperacion(configMgr.operationMode);


	//actu viene de SCOfunctions
	//actu = "1:3:0";
	//actu = "1:49:12:0:0:0:0:0:11:0:0:0:0:0:0|10:0|20:0|29:0|39:0|48:0/1";
	var data_actu = actu.split("/");
	currentCourse = data_actu[1];
	inicia_nav_audio = 0;

	_global.externalTrace ("Actu:", actu);
	_global.externalTrace ("currentCourse:", data_actu[1]);

	courseMgr.setCourse(configMgr.xml, data_actu[0]);
	_global.externalTrace (configMgr);
	_global.externalTrace (courseMgr);


	if (configMgr.usemenu == true) {
		$("#menu_app").append('<a href="javascript:void(0)" class="" id="menu_main"></a>');
		//$("#menu_main").click(function(e){
		$("#menu_main").bind(clickEvent,function(e){
			e.preventDefault();
			pauseVideo();
			stopAudio();
			gotoMenu();
		});
	}

	if (configMgr.menu_index == true) {
		indexMgr = new IndexManager(courseMgr.getModules(), courseMgr.getElements(), configMgr.courseType);
		indexMgr.init();
	}
	if (configMgr.menu_download == true) {

		$("#menu_app").append('<a href="javascript:void(0)" class="text" id="menu_download">'+ langMgr.get("descargas_boton") +'</a>');
		$("#menu_downloads_container").load('contenido/download/download.html', function() {

      // Remove internet links if offline
      removeInternetLinks("menu_downloads_container")

			$("#menu_download").bind(clickEvent,function(e){
				e.preventDefault();
				pauseVideo();
				pauseAudio();
        $("#menu_downloads_container").fadeIn();
				download_init();

			});

		});

	}


	if (configMgr.menu_help == true) {
		$("#menu_app").append('<a href="javascript:void(0)" class="text" id="menu_help">'+ langMgr.get("ayuda_boton") +'</a>');
		$("#menu_help_container").load('contenido/help/help.html', function() {
			//bind de eventos
			//$("#menu_help").click(function(e){
			$("#menu_help").bind(clickEvent,function(e){
				e.preventDefault();
				pauseVideo();
				pauseAudio();
				$("#menu_help_container").fadeIn();
				help_init();
			});
		});
	}

	if (configMgr.menu_credits == true) {
		$("#menu_app").append('<a href="javascript:void(0)" class="text" id="menu_credits">'+ langMgr.get("creditos_boton") +'</a>');
		$("#menu_credits_container").load('contenido/credits/credits.html', function() {
			//bind de eventos
			//$("#menu_credits").click(function(e){
			$("#menu_credits").bind(clickEvent,function(e){
				e.preventDefault();
				pauseVideo();
				pauseAudio();
				$("#menu_credits_container").fadeIn();
				credits_init();
			});
		});
	}

	if (configMgr.menu_glosario == true) {
		$("#menu_app").append('<a href="javascript:void(0)" class="text" id="menu_glosario">'+ langMgr.get("glosario_boton") +'</a>');
		$("#menu_glosario_container").load('contenido/glosario/index.html', function() {
			//bind de eventos
			//$("#menu_credits").click(function(e){
			$("#menu_glosario").bind(clickEvent,function(e){
				e.preventDefault();
				pauseVideo();
				pauseAudio();
				$("#menu_glosario_container").fadeIn();
				glosario_init();

			});
		});
	}

	if (configMgr.menu_audio == true) {
		//$("#menu_app").append('<a href="javascript:void(0)" id="menu_audio" style="height: 26px; width: 33px;">'+ '<div id="menu_audio_img" style="background: url(img/sprite_audio.png) no-repeat"></div>' +'</a>');
		$("#menu_app").append('<div id="menu_audio_img" style="display:block; float:left; margin-top: 10px; height: 26px; width: 33px; background: url(img/sprite.png) no-repeat -7px -121px"><a href="javascript:void(0)" id="menu_audio" style="height: 26px; width: 33px;"></a></div>');
		//bind de eventos
		//$("#menu_audio").click(function(e){
		$("#menu_audio").bind(clickEvent,function(e){
			e.preventDefault();
			if (soundMgr.isMuted()) {
				soundMgr.unmute();
				$("#menu_audio_img").css("background-position", "-7px -121px");
			} else {
				soundMgr.mute();
				$("#menu_audio_img").css("background-position", "-44px -121px")
			}
		});
	}

	// eventos menu
	npagina_ar = courseMgr.getPage_menu();

}

function startEngine() {
	// menu.init();
	// navigation.init();
	// title.init();
	// close.init();
	// controlpanel.init();

	// nos aseguramos que ha cargado navigation para evitar que el botónd e atras esté desactivado
	if($('#btn_prev').hasClass("btn_navegacion_prev_activo")){
	} else {
		// no ha cargado bien navigation
		$('#btn_prev').addClass('btn_navegacion_prev_activo');
		//$("#btn_prev").click(function(e){
		$("#btn_prev").bind(clickEvent,function(e){
		e.preventDefault();
		if ($(this).hasClass("btn_navegacion_prev_activo")){
			try {
				if (myPlayer != undefined) {
					if((ie8)||(oldIe)){
					} else {
						myPlayer.setSrc('');
						myPlayer.remove();
						myPlayer = null;
					}
				}
				//_global.externalTrace("NavegacionManager myPlayer eliminado");

				soundMgr.stopAudios();
				//_global.externalTrace("NavegacionManager > Parar audios en reproducción");

			} catch (e) {
				//_global.externalTrace("NavegacionManager fallo eliminar myPlayer");
			}

			doPrevPage();
		}
	});
	}



	if (courseMgr.hasViewedPages() >= 0) {
		// UIdisplay("continue");
		// cont.play();
		$("#continue_screen").show();
		_global.externalTrace("CONTINUE");
	} else {
		gotoPage();
	}
}

function initEvents(){
	//$("#close_course").click(function(e){
	$("#close_course").bind(clickEvent,function(e){
		e.preventDefault();
		_global.externalTrace("CERRAR");
		$("#close_screen").fadeIn();
	});

	$("#btn_continue_course").bind(clickEvent,function(e){
		e.preventDefault();
		$("#close_screen").fadeOut();
	});

	$("#btn_close_course").bind(clickEvent,function(e){
		e.preventDefault();
		closeCourse();
	});

	$("#btn_continue_course2").bind(clickEvent,function(e){
		e.preventDefault();
		gotoPage('next');
		$("#continue_screen").fadeOut();
	});

$("#btn_start_course").bind(clickEvent,function(e){
		e.preventDefault();
		courseMgr.setCurrentPage(0);
		gotoPage('next');
		$("#continue_screen").fadeOut();
	});



}

//#################### NAVEGATION  ##############################

function gotoPage(type) {
// INICIA LOADING
	$('#loading').show();
	navigation.updateLabel();
	var currPage =courseMgr.getCurrentPage();
	_global.externalTrace("_root.gotoPage", currPage);
	if (currPage == "m") {
		_global.externalTrace("MENU");
		UIdisplay("menu");
	} else {
		currentPage = courseMgr.getElement(currPage);
		var currentModule = courseMgr.getModule(courseMgr.getCurrentModule());
		_global.externalTrace("_root.gotoPage", currentPage.tipo);
		if (currentPage.tipo == "intro") {
			_global.externalTrace("INTRO");
			UIdisplay("intro", currentPage);
		} else {
			_global.externalTrace("PAGE");
			UIdisplay("page", currentPage, type);
		}
	}
	sendScorm();
	/*if(res) {
		testMgr.sendResults();
	}*/
}

function gotoMenu() {
	_global.externalTrace("_root.menu");
	courseMgr.setMenu();
	gotoPage("menu");
}


function nextPage() {
	_global.externalTrace("_root.nextPage");
	stopAudio();
	courseMgr.setNextPage();
	gotoPage('next');
}

function prevPage() {
	_global.externalTrace("_root.prevPage");
	stopAudio();
	courseMgr.setPrevPage();
	gotoPage('prev');
}

//Función que llaman las páginas cuando se puede activar el paso a la siguiente
function HabilitaBotones() {
	_global.externalTrace("_root.HabilitaBotones", currentPage.tipo);
	if (currentPage.tipo == "intro") {
		nextPage();
	} else {
		var currPage = courseMgr.getCurrentPage();
		var maxPage = courseMgr.getTotalPages() - 1;
		if (!_global.useMenu && currPage >= maxPage) {

		} else {
			navigation.activateNext();
		}
	}
}

//#################### VIDEO CONTROL FUNCTIONS  ##############################

// Función para pausar el reproductor de vídeo si está activo
function pauseVideo() {
	try {
		if ((myPlayer != undefined) && (myPlayer != null)){
			myPlayer.pause();
			_global.externalTrace("NavegacionManager myPlayer pausado");
		}
	} catch (e) {
		//_global.externalTrace("NavegacionManager fallo myPlayer pausado");
	}
 }


 // Función para reanudar el reproductor de vídeo si está activo
function resumeVideo() {
	try {
		if ((myPlayer != undefined) && (myPlayer != null)){
			myPlayer.play();
			_global.externalTrace("NavegacionManager myPlayer reiniciado");
		}
	} catch (e) {
		// _global.externalTrace("NavegacionManager fallo myPlayer reiniciado");
	}
 }

//#################### DISPLAY  ##############################

function UIdisplay(state, currentPage, type) {
	_global.externalTrace("_root.UIdisplay", currentPage);

	_global.externalTrace("_root.UIdisplay module", currentModule);
	switch(state) {
		case "continue":
			navigation.hide();
			// loader.hide();
			// cont.show();
			// navigation.hide();
			// title.hide();
			// controlpanel.hide();
			// close.hide();
			// menu.hide();
			break;
		case "menu":
			header.show();
			//header.hide();
			//footer.show();
			footer.hide();
			title.hide();
			navigation.hide();
			$("#content_wrap").html('<div id="loading"></div>').load('contenido/menu/index.html?t' + time.getTime(), function() {
				// bind de eventos
				init();
			});
			// loader.hide();
			// cont.hide();
			// navigation.hide();
			// title.hide();
			// controlpanel.show();
			// close.show();
			// menu.show();
			break;
		case "intro":
			header.hide();
			footer.hide();
			navigation.hide();
			$("#content_wrap").load('contenido/'+ currentPage.carpeta + '/' + currentPage.peli, function() {
				// bind de eventos
				init();
			});
			// loader.hide();
			// cont.hide();
			// navigation.hide();
			// title.hide();
			// controlpanel.hide();
			// close.show();
			// menu.hide();
			break;
		case "page":
			header.show();
			footer.show();
			navigation.show();
			var currentModule = courseMgr.getModule(courseMgr.getCurrentModule());
			title.html(currentModule.titulo);
			title.show();

			if (type == 'next') {
				navigation.desactivateNext();
				$("#content_wrap").animate({
					left:'-980px'
				}, 300, function() {
					// OCULTA LOADING
					$('#loading').hide();
					$("#content_wrap").load('contenido/'+ currentPage.carpeta + '/' + currentPage.peli, function() {
						// bind de eventos

            // Remove internet links if offline
            removeInternetLinks("content_wrap")
						$("#content_wrap").css("left","0");
						init();
					});
				});

			}else if (type == 'prev') {
				navigation.desactivateNext();
				$("#content_wrap").animate({
					left:'1960px'
				}, 300, function() {
					// OCULTA LOADING
					$('#loading').hide();
					$("#content_wrap").load('contenido/'+ currentPage.carpeta + '/' + currentPage.peli, function() {
						// bind de eventos
            // Remove internet links if offline
            removeInternetLinks("content_wrap")
						$("#content_wrap").css("left","0");
						init();
					});
				});

			}

			// loader.hide();
			// cont.hide();
			// navigation.show();
			// title.show();
			// controlpanel.show();
			// close.show();
			// menu.hide();
			break;
	}
}



//#################### SCORM DATA ##############################
function sendScorm() {
	var scormString = courseMgr.getLocationString();

	//PATCH para guardar resultados de examenes
	var tests = testMgr.tests;
	for (var i = 0; i < tests.length; i++) {
		scormString += "|";
		scormString += tests[i].id + ":";
		scormString += tests[i].currentScore;
	}

	scormString += "/"+currentCourse;
	_global.externalTrace("sendScorm",scormString);
	//////alert(scormString);
	guardaDatos(scormString);
}

function closeCourse() {
	//TODO IMPLEMENT COUSE EXIT
	_global.externalTrace("_root.closeCourse", true);

	descarga();
	location.reload(true);
}


//#################### AUDIO PLAYBACK ##############################

function playAudioPrimario (srcAudio, _callback) {
	soundMgr.playSoundPrimario(srcAudio, _callback);
}

function playAudioSecundario (srcAudio, _callback) {
	soundMgr.playSoundSecundario(srcAudio, _callback);
}

function pauseAudio() {
	soundMgr.pause();
}

function resumeAudio() {
	soundMgr.unpause();
}

function stopAudio() {
	soundMgr.stopAudios();
}

//#################### DETECTAR DISPOSITIVO MÓVIL ##############################

function isMobileBrowser (a) {
	// |android|ipad|playbook|silk
	var a = navigator.userAgent;
	(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino |android|ipad|playbook|silk /i .test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4));

	return jQuery.browser.mobile; // true si se ha accedido con un dispositivo móvil
}


//#################### EVENTOS MENÚ ##############################
var active_item = new Array();
function eventMenu(){
	var modeCourse = configMgr.courseType;
	active_item = [];
	$('.main_menu_item').each(function(s){
		if(modeCourse == "lineal"){
			active_item.push("false");
		} else {
			active_item.push("true");
		}

		$(this).bind(clickEvent,function(e){
			e.preventDefault();
			bind_eventMenu(npagina_ar[s])
		});
	});



	var dataStatus = new Array();
	var nStatus = new Array();
	if(modeCourse == "lineal"){
		dataStatus = courseMgr.getStatus_menu();
		var ncompleted;

		$('.main_menu_item').each(function(){
			$(this).unbind(clickEvent);
			$(this).css('cursor','default');
		});

		active_item[0] = "true";
		$('.main_menu_item:first').removeClass('main_menu_item').addClass('main_menu_item_active');
		$('.main_menu_item_active:first').css('cursor','pointer');
		$('.main_menu_item_active:first').bind(clickEvent,function(e){
			e.preventDefault();
			bind_eventMenu(npagina_ar[0]);
		});


		$.each(dataStatus,function(ind){
			nStatus.push(0);
			if(dataStatus[ind]==true){
				nStatus[ind] = 1;
				nStatus[Number(ind+1)] = 1;

			}
		});

		$.each(nStatus,function(nind){
			if(nStatus[nind]){
				ncompleted = Number(nind+1);
				active_item[nind] = "true";
				$('#main_menu_item'+ncompleted).removeClass('main_menu_item').addClass('main_menu_item_active');
				$('#main_menu_item'+ncompleted).css('cursor','pointer');
				$('#main_menu_item'+ncompleted).bind(clickEvent,function(e){
					e.preventDefault();
					bind_eventMenu(npagina_ar[Number(nind)]);
				});
			}
		});

	}else{
	$('.main_menu_item').removeClass('main_menu_item').addClass('main_menu_item_active');
	}
}

function bind_eventMenu(dataEventMenu){
	courseMgr.setCurrentPage(dataEventMenu);
	gotoPage('next');
}

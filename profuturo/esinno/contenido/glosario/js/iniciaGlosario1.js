﻿var xmlGlosario1;
var letras_desactivadas1_ar = new Array();
var letras_activadas1_ar = new Array();

$(document).ready(function(){
	$.ajax({
		url: 'contenido/glosario/glosario.xml',
		dataType: 'xml',
		async:false,
		success: configLoaded,
		error: function(data){
			//console.log('Error loading XML data');
		}
	});

});

function configLoaded(xml){
	xmlGlosario1 = xml;
	fglosario();
}

var letras1;

function fglosario(){
	letras1 = $(xmlGlosario1).find('letra');
	// INSERTAMOS MENÚ	

	$('#glosario').append('<div id="menu_glos"></div>');
	$('#menu_glos').append('<ul class="listado_menu_glos"></ul>');
	$('#menu_glos').find('.listado_menu_glos').each(function(){
		$(this).append('<li id="a" class="menu_glos">A</li>');
		$(this).append('<li id="b" class="menu_glos">B</li>');
		$(this).append('<li id="c" class="menu_glos">C</li>');
		$(this).append('<li id="d" class="menu_glos">D</li>');
		$(this).append('<li id="e" class="menu_glos">E</li>');
		$(this).append('<li id="f" class="menu_glos">F</li>');
		$(this).append('<li id="g" class="menu_glos">G</li>');
		$(this).append('<li id="h" class="menu_glos">H</li>');
		$(this).append('<li id="i" class="menu_glos">I</li>');
		$(this).append('<li id="j" class="menu_glos">J</li>');
		$(this).append('<li id="k" class="menu_glos">K</li>');
		$(this).append('<li id="l" class="menu_glos">L</li>');
		$(this).append('<li id="m" class="menu_glos">M</li>');
		$(this).append('<li id="n" class="menu_glos">N</li>');
		$(this).append('<li id="gn" class="menu_glos">&Ntilde;</li>');
		$(this).append('<li id="o" class="menu_glos">O</li>');
		$(this).append('<li id="p" class="menu_glos">P</li>');
		$(this).append('<li id="q" class="menu_glos">Q</li>');
		$(this).append('<li id="r" class="menu_glos">R</li>');
		$(this).append('<li id="s" class="menu_glos">S</li>');
		$(this).append('<li id="t" class="menu_glos">T</li>');
		$(this).append('<li id="u" class="menu_glos">U</li>');
		$(this).append('<li id="v" class="menu_glos">V</li>');
		$(this).append('<li id="w" class="menu_glos">W</li>');
		$(this).append('<li id="x" class="menu_glos">X</li>');
		$(this).append('<li id="y" class="menu_glos">Y</li>');
		$(this).append('<li id="z" class="menu_glos">Z</li>');
	});
	
	$('#menu_glos').find('.menu_glos').each(function(){
		$(this).mouseover(function(){
			$(this).addClass('menu_glos_activo');
		});	
		$(this).mouseout(function(){
			$(this).removeClass('menu_glos_activo');
		});
		//$(this).click(function(){
		$(this).bind(clickEvent,function(){
			freset_selected();
			$(this).addClass('letra_seleccionada');
			infoGlos($(this).text());
		});
	});
	
	configGlos();
	//parseXMLGlosario();
}


function freset_selected(){
	$('#menu_glos').find('.menu_glos').each(function(){
		$(this).removeClass('letra_seleccionada');
	});
}

var $xmlLetras = new Array();
function configGlos(){
	$.each(letras1,function(s){
		if($(this).find('palabra').length>0){
			letras_activadas1_ar.push($(this).attr('nombre'));
		} else {
			letras_desactivadas1_ar.push($(this).attr('nombre'));
		}
	});
	$.each(letras_desactivadas1_ar,function(indice,valor) {
		$('#menu_glos').find('.menu_glos').each(function(){
			if(valor == ($(this).attr('id').toUpperCase())){		
				//$(this).unbind('click');
				$(this).unbind(clickEvent);
				$(this).unbind('mouseover');
				$(this).unbind('mouseout');
				$(this).addClass('letra_desactivada');
			}
		});
	});
	configContentGlos();
	
}

function configContentGlos(){
	$('#glosario').append('<div id="contenido_glos"></div>');
	$('#contenido_glos').append('<div id="fondo_glos"></div>');
	$('#contenido_glos').append('<div id="content_words"></div>');
	$('#content_words').append('<div id="contennido_words"></div>');
	$('#contenido_glos').css({
		'top' : '70px'
	});

	activaPrimera();
}

function activaPrimera(){
	if(letras_activadas1_ar.length>0){
		$('#menu_glos').find('.menu_glos').each(function(){
			if(letras_activadas1_ar[0] == ($(this).attr('id').toUpperCase())){	
				$(this).addClass('letra_seleccionada');
				infoGlos($(this).text());
			}
		});
	}
}

function infoGlos(data){

	$(xmlGlosario1).find('letra').each(function(){
		if($(this).attr('nombre')==data){
			$('#contennido_words').empty();
			
			$(this).find('palabra').each(function(s){
				$('#contennido_words').append('<div class="termino"><p>'+$(this).find('termino').text()+'</p></div>');
				$('#contennido_words').append('<div class="descripcion"><p>'+$(this).find('descripcion').text()+'</p></div>');
			});
			$('#content_words').jScrollPane({showArrows: false});
			if(!ie){
				//$('#content_words').jScrollPane({showArrows: true});
			} 
			//
			
		}
	});
}
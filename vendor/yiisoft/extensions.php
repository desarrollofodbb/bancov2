<?php

$vendorDir = dirname(__DIR__);

return array (
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'bryglen/yii2-sendgrid' => 
  array (
    'name' => 'bryglen/yii2-sendgrid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@bryglen/sendgrid' => $vendorDir . '/bryglen/yii2-sendgrid',
    ),
  ),
  'paulzi/yii2-sortable' => 
  array (
    'name' => 'paulzi/yii2-sortable',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@paulzi/sortable' => $vendorDir . '/paulzi/yii2-sortable',
    ),
  ),
  'et-soft/yii2-widget-select-year' => 
  array (
    'name' => 'et-soft/yii2-widget-select-year',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@etsoft/widgets' => $vendorDir . '/et-soft/yii2-widget-select-year',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '3.9999999.9999999.9999999-dev',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.7.0.0-beta1',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  'paulzi/yii2-adjacency-list' => 
  array (
    'name' => 'paulzi/yii2-adjacency-list',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@paulzi/adjacencyList' => $vendorDir . '/paulzi/yii2-adjacency-list',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
  'kartik-v/yii2-widget-growl' => 
  array (
    'name' => 'kartik-v/yii2-widget-growl',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/growl' => $vendorDir . '/kartik-v/yii2-widget-growl/src',
    ),
  ),
  'yii2mod/yii2-editable' => 
  array (
    'name' => 'yii2mod/yii2-editable',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/editable' => $vendorDir . '/yii2mod/yii2-editable',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'kartik-v/yii2-widget-alert' => 
  array (
    'name' => 'kartik-v/yii2-widget-alert',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/alert' => $vendorDir . '/kartik-v/yii2-widget-alert/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
  'pendalf89/yii2-filemanager' => 
  array (
    'name' => 'pendalf89/yii2-filemanager',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@pendalf89/filemanager' => $vendorDir . '/pendalf89/yii2-filemanager',
    ),
  ),
  'yii2tech/ar-position' => 
  array (
    'name' => 'yii2tech/ar-position',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2tech/ar/position' => $vendorDir . '/yii2tech/ar-position/src',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'cornernote/yii2-linkall' => 
  array (
    'name' => 'cornernote/yii2-linkall',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@cornernote/linkall' => $vendorDir . '/cornernote/yii2-linkall/src',
    ),
  ),
  'yii2mod/yii2-enum' => 
  array (
    'name' => 'yii2mod/yii2-enum',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/enum' => $vendorDir . '/yii2mod/yii2-enum',
    ),
  ),
  'yii2mod/yii2-moderation' => 
  array (
    'name' => 'yii2mod/yii2-moderation',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/moderation' => $vendorDir . '/yii2mod/yii2-moderation',
    ),
  ),
  'yii2mod/yii2-behaviors' => 
  array (
    'name' => 'yii2mod/yii2-behaviors',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/behaviors' => $vendorDir . '/yii2mod/yii2-behaviors',
    ),
  ),
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'yii2mod/yii2-comments' => 
  array (
    'name' => 'yii2mod/yii2-comments',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/comments' => $vendorDir . '/yii2mod/yii2-comments',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating/src',
    ),
  ),
  'yii2tech/ar-softdelete' => 
  array (
    'name' => 'yii2tech/ar-softdelete',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2tech/ar/softdelete' => $vendorDir . '/yii2tech/ar-softdelete/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.17.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'onmotion/yii2-widget-apexcharts' => 
  array (
    'name' => 'onmotion/yii2-widget-apexcharts',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@onmotion/apexcharts' => $vendorDir . '/onmotion/yii2-widget-apexcharts',
    ),
  ),
);
